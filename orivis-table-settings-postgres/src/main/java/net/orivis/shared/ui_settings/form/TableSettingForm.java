package net.orivis.shared.ui_settings.form;

import lombok.Data;
import net.orivis.shared.annotations.FormTransient;
import net.orivis.shared.annotations.Validation;
import net.orivis.shared.controller.orivis.OrivisRequest;
import net.orivis.shared.pojo_form.Checkable;
import net.orivis.shared.pojo_form.PojoFormElement;
import net.orivis.shared.pojo_view.DEFAULT_TYPES;
import net.orivis.shared.pojo_view.UIAction;
import net.orivis.shared.pojo_view.list.PojoListView;
import net.orivis.shared.pojo_view.list.PojoListViewField;
import net.orivis.shared.pojo_view.list.Remote;
import net.orivis.shared.postgres.form.OrivisPojo;
import net.orivis.shared.validation.ValueNotEmptyValidator;

import java.util.List;

import static net.orivis.shared.pojo_view.DEFAULT_TYPES.HIDDEN;

@Data
@PojoListView(actions = {"settings"})
public class TableSettingForm implements OrivisPojo {

    @PojoFormElement(type = HIDDEN)
    private Long id;

    @Validation(ValueNotEmptyValidator.class)
    @PojoFormElement(required = true)
    @PojoListViewField(searchable = true)
    private String name;


    @PojoFormElement(required = true)
    @PojoListViewField(searchable = true)
    private String topic;

    @PojoFormElement(required = true)
    @PojoListViewField (available = false)
    private List<String> fieldsOrder;

    @PojoFormElement(required = true)
    @PojoListViewField (available = false)
    private List<String> visibleFields;

    @PojoListViewField (available = false)
    private OrivisRequest magicQuery;

    @PojoFormElement
    @PojoListViewField (available = false)

    private String url;

    @PojoFormElement(type = DEFAULT_TYPES.CHECKBOX, checked = @Checkable, remote = @Remote(searchType = DEFAULT_TYPES.CHECKBOX))
    @PojoListViewField(searchable = true)
    private Boolean addToMenu;


    @PojoListViewField(actions = {@UIAction(component = "delete")}, visible = false)
    @PojoFormElement(available = false)
    private Integer action;

    @Override
    public String asValue() {
        return name;
    }
}
