package net.orivis.shared.ui_settings.service;

import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.form.GeneratedPojoForm;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import net.orivis.shared.controller.items.magic.UserSettingsFormInterface;
import net.orivis.shared.ui_settings.model.UserFormSettingsModel;
import net.orivis.shared.utils.PojoFormField;
import net.orivis.shared.utils.bean_copier.BeanCopier;

import java.util.Comparator;
import java.util.Optional;

@Service @Primary
public class UserSettingServiceEx extends UserSettingsFormInterface {
    @Override
    public void configForm(OrivisContext context, GeneratedPojoForm form, Class<?> formClass) {
        var bean = context.getBean(UserFormSettingsService.class);
        try {
            Optional<UserFormSettingsModel> exists = bean.getRepository().filteredFirst(bean.createUserFormSpecification(formClass.getSimpleName()));
            if (exists.isPresent()) {
                var changedForm = context.getBean(BeanCopier.class).clone(form);
                for (var item : changedForm.getFieldList()) {
                    for (var leftItem : exists.get().getPositions()) {
                        if (item.getField().equalsIgnoreCase(leftItem.getFieldName())) {
                            item.setOrder(leftItem.getPosition());
                            //if group is not yet modified we set it to the same as group
                            item.setCustomName(leftItem.getTabName() == null ?  item.getGroup() : leftItem.getTabName());
                        }
                    }
                }
                changedForm.getFieldList().sort(Comparator.comparing(PojoFormField::getOrder));
            }
        } catch (Exception e) {
            bean.getLogger().info("Cannot find saved form, ", e);
        }
    }
}
