package net.orivis.shared.ui_settings.form;

import lombok.Data;
import net.orivis.shared.annotations.Validation;
import net.orivis.shared.postgres.form.OrivisPojo;
import net.orivis.shared.validation.ValueNotEmptyValidator;

import java.util.TimeZone;

@Data
public class UserSettingForm implements OrivisPojo {

    private Long id;

    @Validation(ValueNotEmptyValidator.class)
    private String language;

    @Validation(ValueNotEmptyValidator.class)
    private TimeZone timeZone;
}
