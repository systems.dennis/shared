package net.orivis.shared.ui_settings.repository;


import net.orivis.shared.postgres.repository.OrivisRepository;
import net.orivis.shared.ui_settings.model.UserSettingModel;

import java.util.Optional;

public interface UserSettingRepo extends OrivisRepository<UserSettingModel> {
    Optional<UserSettingModel> findFirstByUserDataId(Long user);
}
