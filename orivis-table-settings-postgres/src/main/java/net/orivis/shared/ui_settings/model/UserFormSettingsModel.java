package net.orivis.shared.ui_settings.model;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import lombok.Data;
import net.orivis.shared.annotations.ObjectByIdPresentation;
import net.orivis.shared.postgres.model.LongAssignableEntity;

import java.util.List;

@Data
@Entity
public class UserFormSettingsModel extends LongAssignableEntity {

    private String entityType;
    @OneToMany
    @ObjectByIdPresentation
    private List<FieldFormPosition> positions;

    @Override
    public String asValue() {
        return entityType;
    }
}
