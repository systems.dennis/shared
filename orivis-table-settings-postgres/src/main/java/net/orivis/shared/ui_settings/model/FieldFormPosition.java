package net.orivis.shared.ui_settings.model;

import jakarta.persistence.Entity;
import lombok.Data;
import net.orivis.shared.postgres.model.OrivisEntity;

@Data
@Entity
public class FieldFormPosition extends OrivisEntity {
    private String fieldName;
    private Integer position;

    private String tabName;

    @Override
    public String asValue() {
        return fieldName + " " + position;
    }
}
