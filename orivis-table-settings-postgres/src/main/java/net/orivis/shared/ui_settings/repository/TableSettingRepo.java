package net.orivis.shared.ui_settings.repository;


import net.orivis.shared.postgres.repository.OrivisRepository;
import org.springframework.stereotype.Repository;
import net.orivis.shared.ui_settings.model.TableSettingModel;

import java.io.Serializable;

@Repository
public interface TableSettingRepo<ID_TYPE extends Serializable> extends OrivisRepository<TableSettingModel> {
}
