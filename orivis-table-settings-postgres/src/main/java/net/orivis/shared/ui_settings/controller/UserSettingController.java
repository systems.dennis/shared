package net.orivis.shared.ui_settings.controller;

import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import net.orivis.shared.postgres.controller.AddItemController;
import net.orivis.shared.postgres.controller.DeleteItemController;
import net.orivis.shared.postgres.controller.EditItemController;
import net.orivis.shared.postgres.controller.ListItemController;
import net.orivis.shared.ui_settings.form.UserSettingForm;
import net.orivis.shared.ui_settings.model.UserSettingModel;
import net.orivis.shared.ui_settings.service.UserSettingService;

@RestController
@RequestMapping("/settings")
@OrivisController(value =  UserSettingService.class)
public class UserSettingController extends OrivisContextable implements
        AddItemController<UserSettingModel, UserSettingForm>,
        ListItemController<UserSettingModel, UserSettingForm>,
        DeleteItemController<UserSettingModel>,
        EditItemController<UserSettingModel, UserSettingForm> {
    public UserSettingController(OrivisContext context) {
        super(context);
    }
}
