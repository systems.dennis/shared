package net.orivis.shared.ui_settings.repository;


import net.orivis.shared.postgres.repository.OrivisRepository;
import net.orivis.shared.ui_settings.model.FieldFormPosition;


public interface FieldFormPositionRepository  extends OrivisRepository<FieldFormPosition> {
}
