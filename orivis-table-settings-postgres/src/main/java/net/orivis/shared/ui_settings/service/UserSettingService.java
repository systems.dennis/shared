package net.orivis.shared.ui_settings.service;

import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.config.OrivisContext;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import net.orivis.shared.exceptions.ItemForAddContainsIdException;
import net.orivis.shared.postgres.service.PaginationService;
import net.orivis.shared.ui_settings.form.UserSettingForm;
import net.orivis.shared.ui_settings.model.UserSettingModel;
import net.orivis.shared.ui_settings.repository.UserSettingRepo;

import java.util.TimeZone;

@Service
@Primary
@OrivisService(model = UserSettingModel.class, form = UserSettingForm.class, repo = UserSettingRepo.class)
public class UserSettingService extends PaginationService<UserSettingModel>  {
    public UserSettingService(OrivisContext holder) {
        super(holder);
    }

    @Override
    public UserSettingModel preAdd(UserSettingModel object) throws ItemForAddContainsIdException {

        if (object.getId() == null) {
            UserSettingRepo repo = getRepository();
            var item =   repo.findFirstByUserDataId(getCurrentUser());
            item.ifPresent(userSettingModel -> object.setId(userSettingModel.getId()));
        }
        return super.preAdd(object);
    }

    public UserSettingModel findUserSetting(Long userId) {

        return getRepository().findFirstByUserDataId(userId).orElseGet(this::createNewSetting);
    }

    @Override
    public UserSettingRepo getRepository() {
        return getBean(UserSettingRepo.class);
    }

    private UserSettingModel createNewSetting() {
        UserSettingModel model = new UserSettingModel();
        model.setTimeZone(TimeZone.getDefault());
        model.setLanguage("en");
        return  save(model);
    }

}
