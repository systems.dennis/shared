package net.orivis.shared.ui_settings.repository;


import net.orivis.shared.postgres.repository.OrivisRepository;
import net.orivis.shared.ui_settings.model.UserFormSettingsModel;

public interface UserFormSettingsRepo extends OrivisRepository<UserFormSettingsModel> {
}
