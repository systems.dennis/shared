package net.orivis.shared.ui_settings.service;

import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.repository.OrivisFilter;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import net.orivis.shared.exceptions.ItemForAddContainsIdException;
import net.orivis.shared.postgres.service.PaginationService;
import net.orivis.shared.ui_settings.form.TableSettingForm;
import net.orivis.shared.ui_settings.model.TableSettingModel;
import net.orivis.shared.ui_settings.repository.TableSettingRepo;

import java.util.List;
import java.util.Optional;

@Service
@OrivisService(model = TableSettingModel.class, form = TableSettingForm.class, repo = TableSettingRepo.class)
public class TableSettingService extends PaginationService<TableSettingModel> {

    public TableSettingService(OrivisContext holder) {
        super(holder);
    }

    @Override
    public TableSettingModel preAdd(TableSettingModel object) throws ItemForAddContainsIdException {


        Optional<TableSettingModel> obj = getRepository().filteredFirst(getFilterImpl().eq("topic",
                        object.getTopic()).and(getFilterImpl().eq("name", object.getName()))
                        .and(getFilterImpl().ofUser(TableSettingModel.class, object.getUserDataId())));



        obj.ifPresent(tableSettingModel -> object.setId(tableSettingModel.getId()));

        return object;
    }

    public List<TableSettingModel> findByUserAndTopic(String topic) {

       return getRepository().filteredData(getFilterImpl().eq("topic", topic).and(getFilterImpl().ofUser(TableSettingModel.class, getCurrentUser())), Pageable.unpaged()).getContent();

    }

    @Override
    public OrivisFilter<TableSettingModel> getAdditionalSpecification() {
        var spec = super.getAdditionalSpecification();
        return spec.and(getFilterImpl().ofUser(TableSettingModel.class, getCurrentUser()));
    }

    @Override
    public TableSettingRepo getRepository() {
        return super.getRepository();
    }
}
