package net.orivis.shared.ui_settings.model;

import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import lombok.Data;
import net.orivis.shared.controller.orivis.OrivisRequest;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;
import net.orivis.shared.annotations.DeleteStrategy;
import net.orivis.shared.converters.MagicQueryConverter;
import net.orivis.shared.postgres.model.LongAssignableEntity;

import java.util.List;

import static net.orivis.shared.annotations.DeleteStrategy.DELETE_STRATEGY_PROPERTY;

@Data
@Entity
@DeleteStrategy (value = DELETE_STRATEGY_PROPERTY, selfOnly = true)
public class TableSettingModel extends LongAssignableEntity {

    private String name;

    @Column(columnDefinition = "varchar(1500)")
    private String topic;

    @ElementCollection
    private List<String> fieldsOrder;
    @ElementCollection
    List<String> visibleFields;

    @Column(columnDefinition = "text")
    @JdbcTypeCode(SqlTypes.JSON)
    @Convert(converter = MagicQueryConverter.class)
    private OrivisRequest magicQuery;

    private Boolean addToMenu;
    @Column(columnDefinition = "varchar(1500)")
    private String url;

    @Override
    public String asValue() {
        return name;
    }
}
