package net.orivis.shared.scopes.repository;

import net.orivis.shared.postgres.repository.OrivisRepository;
import net.orivis.shared.scopes.model.ScopeRuleModel;

public interface ScopeRuleRepo extends OrivisRepository<ScopeRuleModel> {
}
