package net.orivis.shared.scopes.model;

import jakarta.persistence.Entity;
import lombok.Data;
import net.orivis.shared.postgres.model.LongAssignableEntity;

@Entity
@Data
public class ScopeRuleModel extends LongAssignableEntity {

    private String name;

    private Boolean verificationRequired;

    private Boolean registrationAllowed;

    private Boolean invitationRequired;

    private Integer verificationType;

    private Boolean allowPhoneOperations;

    private Integer triesBeforeBlock;

    private Integer tokenDuration;

    private Integer verificationTokenDuration;


    private String passwordRules = "8:SYMBOL,NUMBER,UPPER";

    private String scopeTokenSymbols= "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$%&+_&:-(*)'[{}]/\\\\}";

    @Override
    public String asValue() {
        return name;
    }
}
