package net.orivis.shared.scopes.exception;

import net.orivis.shared.exceptions.StandardException;

import java.io.Serializable;

public class ScopeRuleException extends StandardException {

    public ScopeRuleException(Serializable target, String message) {
        super(target, message);
    }

    public ScopeRuleException(String message) {
        super(null, message);
    }
}
