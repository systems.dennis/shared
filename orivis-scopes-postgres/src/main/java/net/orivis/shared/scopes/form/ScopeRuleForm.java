package net.orivis.shared.scopes.form;

import lombok.Data;
import net.orivis.shared.annotations.Validation;
import net.orivis.shared.pojo_form.Checkable;
import net.orivis.shared.pojo_form.PojoFormElement;
import net.orivis.shared.pojo_view.DEFAULT_TYPES;
import net.orivis.shared.pojo_view.UIAction;
import net.orivis.shared.pojo_view.list.PojoListView;
import net.orivis.shared.pojo_view.list.PojoListViewField;
import net.orivis.shared.pojo_view.list.Remote;
import net.orivis.shared.postgres.form.OrivisPojo;
import net.orivis.shared.validation.ValueNotEmptyValidator;

import static net.orivis.shared.pojo_view.DEFAULT_TYPES.HIDDEN;

@Data
@PojoListView(actions = {UIAction.ACTION_DOWNLOAD, UIAction.ACTION_NEW, UIAction.ACTION_SETTINGS})
public class ScopeRuleForm implements OrivisPojo {
    @PojoFormElement(type = HIDDEN)
    private Long id;

    @PojoFormElement
    @PojoListViewField(searchable = true)
    @Validation(ValueNotEmptyValidator.class)
    private String name;

    @PojoFormElement(type = DEFAULT_TYPES.CHECKBOX, checked = @Checkable, remote = @Remote(searchType = DEFAULT_TYPES.CHECKBOX))
    @PojoListViewField(searchable = true)
    private Boolean verificationRequired;

    @PojoFormElement(type = DEFAULT_TYPES.CHECKBOX, checked = @Checkable, remote = @Remote(searchType = DEFAULT_TYPES.CHECKBOX))
    @PojoListViewField(searchable = true)
    private Boolean registrationAllowed;

    @PojoFormElement(type = DEFAULT_TYPES.CHECKBOX, checked = @Checkable, remote = @Remote(searchType = DEFAULT_TYPES.CHECKBOX))
    @PojoListViewField(searchable = true)
    private Boolean invitationRequired;

    @PojoFormElement(type = "number")
    @PojoListViewField(searchable = true)
    private Integer verificationType;

    @PojoFormElement(type = "number")
    @PojoListViewField(searchable = true)
    private Integer tokenDuration;

    @PojoFormElement(type = "number")
    @PojoListViewField(searchable = true)
    private Integer verificationTokenDuration;


    @PojoFormElement(type = DEFAULT_TYPES.CHECKBOX, checked = @Checkable, remote = @Remote(searchType = DEFAULT_TYPES.CHECKBOX))
    @PojoListViewField(searchable = true)
    private Boolean allowPhoneOperations;

    @PojoFormElement(type = "number")
    @PojoListViewField(searchable = true)
    private Integer triesBeforeBlock;

    @PojoListViewField(actions = {@UIAction(component = "edit"), @UIAction(component = "delete", allowOnMultipleRows = true)}, visible = false)
    @PojoFormElement(available = false)
    private Integer action;


    @PojoFormElement(type = "text")
    @PojoListViewField(searchable = true)
    private String passwordRules = "15:SYMBOL,NUMBER,UPPER";


    @PojoFormElement(type = "text")
    @PojoListViewField(searchable = true)
    private String scopeTokenSymbols= "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$%&+_&:-(*)'[{}]/\\\\}";

    @Override
    public String asValue() {
        return name;
    }
}
