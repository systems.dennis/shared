package net.orivis.shared.scopes.service;

import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.config.OrivisContext;
import org.springframework.stereotype.Service;
import net.orivis.shared.annotations.DeleteStrategy;
import net.orivis.shared.postgres.service.PaginationService;
import net.orivis.shared.scopes.form.ScopeRuleForm;
import net.orivis.shared.scopes.model.ScopeRuleModel;
import net.orivis.shared.scopes.repository.ScopeRuleRepo;

import static net.orivis.shared.annotations.DeleteStrategy.DELETE_STRATEGY_PROPERTY;

@Service
@OrivisService(model = ScopeRuleModel.class, form = ScopeRuleForm.class, repo = ScopeRuleRepo.class)
@DeleteStrategy(value = DELETE_STRATEGY_PROPERTY)
public class ScopeRuleService extends PaginationService<ScopeRuleModel> {

    public ScopeRuleService(OrivisContext holder) {
        super(holder);
    }

}
