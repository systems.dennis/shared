package net.orivis.shared.scopes.repository;

import net.orivis.shared.postgres.repository.OrivisRepository;
import org.springframework.stereotype.Repository;
import net.orivis.shared.scopes.model.ScopeModel;


@Repository
public interface ScopeRepo extends OrivisRepository<ScopeModel> {
}
