package net.orivis.shared.scopes.service;

import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.repository.OrivisFilter;
import org.springframework.stereotype.Service;
import net.orivis.shared.annotations.DeleteStrategy;
import net.orivis.shared.exceptions.ItemForAddContainsIdException;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.postgres.service.PaginationService;
import net.orivis.shared.scopes.form.ScopeForm;
import net.orivis.shared.scopes.model.ScopeModel;
import net.orivis.shared.scopes.repository.ScopeRepo;

import java.util.Date;
import java.util.Objects;

import static net.orivis.shared.annotations.DeleteStrategy.DELETE_STRATEGY_PROPERTY;

@Service
@OrivisService(model = ScopeModel.class, form = ScopeForm.class, repo = ScopeRepo.class)
@DeleteStrategy(value = DELETE_STRATEGY_PROPERTY)
public class ScopeService extends PaginationService<ScopeModel> {

    public ScopeService(OrivisContext holder) {
        super(holder);
    }

    @Override
    public ScopeModel preAdd(ScopeModel object) throws ItemForAddContainsIdException {
        object.setDate(new Date());

        return super.preAdd(object);
    }

    public ScopeModel findByName(String name, Long userData, boolean fullAccess) {
        if (name == null) {
            throw new ItemNotFoundException("global.scope.cannot_be_null_in_request");
        }

        var filter = getFilterImpl().eq("name", name).and(getNotDeletedQuery());

        if (!fullAccess && Objects.nonNull(addSelfSpecWithUserScopes(userData))) {
            filter.and(addSelfSpecWithUserScopes(userData));
        }

        return getRepository().filteredOne(filter).orElseThrow(() -> new ItemNotFoundException(name));
    }

    public OrivisFilter<ScopeModel> addSelfSpecWithUserScopes(Long userData) {
        return null;
    }
}
