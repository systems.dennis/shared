package net.orivis.shared.scopes.controller;

import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.controller.SearchEntityApi;
import net.orivis.shared.controller.SearcherInfo;
import net.orivis.shared.exceptions.ItemDoesNotContainsIdValueException;
import net.orivis.shared.exceptions.ItemForAddContainsIdException;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.exceptions.UnmodifiedItemSaveAttemptException;
import net.orivis.shared.postgres.controller.AddItemController;
import net.orivis.shared.postgres.controller.DeleteItemController;
import net.orivis.shared.postgres.controller.EditItemController;
import net.orivis.shared.postgres.controller.SelfItemsListController;
import net.orivis.shared.scopes.form.ScopeForm;
import net.orivis.shared.scopes.model.ScopeModel;
import net.orivis.shared.scopes.service.ScopeService;

@RestController
@RequestMapping("/api/v2/shared/scope")
@OrivisController(value = ScopeService.class)
@CrossOrigin
@Secured (roles = "ROLE_ADMIN")
public class ScopeController extends OrivisContextable
        implements AddItemController<ScopeModel, ScopeForm>,
        EditItemController<ScopeModel, ScopeForm>,
        SelfItemsListController<ScopeModel, ScopeForm>,
        DeleteItemController<ScopeModel> {

    static  {
        SearchEntityApi.registerSearch("scope", new SearcherInfo("name", ScopeService.class));
    }

    public ScopeController(OrivisContext context) {
        super(context);
    }

    @Override
    @WithRole("ROLE_ADMIN")
    public ResponseEntity<ScopeForm> edit(ScopeForm form) throws ItemForAddContainsIdException, ItemDoesNotContainsIdValueException, UnmodifiedItemSaveAttemptException, ItemNotFoundException {
        return EditItemController.super.edit(form);
    }


    @Override
    public ScopeService getService() {
        return AddItemController.super.getService();
    }
}
