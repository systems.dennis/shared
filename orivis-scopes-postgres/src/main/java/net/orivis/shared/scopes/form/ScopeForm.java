package net.orivis.shared.scopes.form;

import lombok.Data;
import net.orivis.shared.annotations.ObjectByIdPresentation;
import net.orivis.shared.annotations.Validation;
import net.orivis.shared.pojo_form.PojoFormElement;
import net.orivis.shared.pojo_view.DEFAULT_TYPES;
import net.orivis.shared.pojo_view.UIAction;
import net.orivis.shared.pojo_view.list.PojoListView;
import net.orivis.shared.pojo_view.list.PojoListViewField;
import net.orivis.shared.pojo_view.list.Remote;
import net.orivis.shared.postgres.form.OrivisPojo;
import net.orivis.shared.scopes.service.ScopeRuleService;
import net.orivis.shared.utils.OrivisDate;
import net.orivis.shared.utils.bean_copier.DateToUTCConverter;
import net.orivis.shared.utils.bean_copier.OrivisIdToObjectTransformer;
import net.orivis.shared.utils.bean_copier.OrivisTranformer;
import net.orivis.shared.validation.FieldIsUniqueValidator;
import net.orivis.shared.validation.ValueNotEmptyValidator;

import static net.orivis.shared.pojo_view.DEFAULT_TYPES.*;

@Data
@PojoListView(actions = {UIAction.ACTION_DOWNLOAD, UIAction.ACTION_NEW, UIAction.ACTION_SETTINGS})
public class ScopeForm implements OrivisPojo {
    @PojoFormElement(type = HIDDEN)
    private Long id;

    @PojoFormElement
    @PojoListViewField(searchable = true)
    @Validation({ValueNotEmptyValidator.class, FieldIsUniqueValidator.class})
    private String name;

    @PojoFormElement (required = true, type =  TEXT_AREA)
    @PojoListViewField(searchable = true)
    private String description;

    @PojoFormElement(type = DATE, available = false)
    @PojoListViewField(searchable = true, type = DATE, format =  DATE_FORMAT_FULL,  remote = @Remote(searchType = DATE_TIME))
    @OrivisTranformer(transformWith = DateToUTCConverter.class)
    private OrivisDate date;

    @PojoFormElement (type = HIDDEN)
    @PojoListViewField (type = TEXT, visible = false)
    private String secretKey;

    @PojoFormElement (type = HIDDEN)
    @PojoListViewField (type = TEXT, visible = false)
    private String refreshSecretKey;

    @PojoFormElement
    @PojoListViewField(searchable = true)
    private String url;

    @PojoFormElement
    @PojoListViewField(searchable = true)
    private String verificationUrl;

    @ObjectByIdPresentation
    @OrivisTranformer(transformWith = OrivisIdToObjectTransformer.class, additionalClass = ScopeRuleService.class)
    @PojoFormElement( type = DEFAULT_TYPES.OBJECT_SEARCH,
            remote = @Remote(fetcher = "shared/scope_rule", searchType = DEFAULT_TYPES.OBJECT_SEARCH, searchField = "name", searchName = "scope_rule"))
    @PojoListViewField(type = DEFAULT_TYPES.OBJECT_SEARCH,
            remote = @Remote(fetcher = "shared/scope_rule", searchType = DEFAULT_TYPES.OBJECT_SEARCH, searchField = "name", searchName = "scope_rule"))
    private Long scopeRule;

    @PojoListViewField(actions = {@UIAction(component = "edit"), @UIAction(component = "delete", allowOnMultipleRows = true)}, visible = false)
    @PojoFormElement(available = false)
    private Integer action;

    @Override
    public String asValue() {
        return name;
    }
}
