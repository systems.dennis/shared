package net.orivis.shared.scopes.controller;

import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.controller.SearchEntityApi;
import net.orivis.shared.controller.SearcherInfo;
import net.orivis.shared.postgres.controller.AddItemController;
import net.orivis.shared.postgres.controller.DeleteItemController;
import net.orivis.shared.postgres.controller.EditItemController;
import net.orivis.shared.postgres.controller.SelfItemsListController;
import net.orivis.shared.scopes.form.ScopeRuleForm;
import net.orivis.shared.scopes.model.ScopeRuleModel;
import net.orivis.shared.scopes.service.ScopeRuleService;

import java.io.Serializable;

@RestController
@RequestMapping("/api/v2/shared/scope_rule")
@OrivisController(value = ScopeRuleService.class)
@CrossOrigin
@Secured(roles = "ROLE_ADMIN")
public class ScopeRuleController<ID_TYPE extends Serializable> extends OrivisContextable
        implements AddItemController<ScopeRuleModel, ScopeRuleForm>,
        EditItemController<ScopeRuleModel, ScopeRuleForm>,
        SelfItemsListController<ScopeRuleModel, ScopeRuleForm>,
        DeleteItemController<ScopeRuleModel> {

    static  {
        SearchEntityApi.registerSearch("scope_rule", new SearcherInfo("name", ScopeRuleService.class));
    }

    public ScopeRuleController(OrivisContext context) {
        super(context);
    }
}
