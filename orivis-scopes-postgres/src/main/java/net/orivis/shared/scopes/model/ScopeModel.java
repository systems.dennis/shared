package net.orivis.shared.scopes.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.Data;
import net.orivis.shared.annotations.FormTransient;
import net.orivis.shared.annotations.ObjectByIdPresentation;
import net.orivis.shared.annotations.entity.Created;
import net.orivis.shared.postgres.model.LongAssignableEntity;
import net.orivis.shared.scopes.service.ScopeRuleService;
import net.orivis.shared.utils.bean_copier.DateToUTCConverter;
import net.orivis.shared.utils.bean_copier.OrivisIdToObjectTransformer;
import net.orivis.shared.utils.bean_copier.OrivisTranformer;

import java.util.Date;

@Entity
@Data
public class ScopeModel  extends LongAssignableEntity {

    private String name;

    @Column(columnDefinition = "text")
    private String description;

    @FormTransient
    private String secretKey;

    @FormTransient
    private String refreshSecretKey;

    private Boolean blocked = Boolean.FALSE;

    @FormTransient
    @OrivisTranformer(transformWith  = DateToUTCConverter.class)
    @Created
    private Date date;

    @Column(columnDefinition = "varchar(1000)")
    private String url;

    private String verificationUrl;

    @ManyToOne
    @ObjectByIdPresentation
    @OrivisTranformer(transformWith = OrivisIdToObjectTransformer.class, additionalClass = ScopeRuleService.class)
    private ScopeRuleModel scopeRule;

    @Override
    public String asValue() {
        return name;
    }
}
