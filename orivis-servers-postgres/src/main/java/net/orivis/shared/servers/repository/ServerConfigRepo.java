package net.orivis.shared.servers.repository;

import net.orivis.shared.postgres.repository.OrivisRepository;
import org.springframework.stereotype.Repository;
import net.orivis.shared.servers.model.ServerConfig;

import java.io.Serializable;

@Repository
public interface ServerConfigRepo<TYPE_ID extends Serializable> extends OrivisRepository<ServerConfig> {

}
