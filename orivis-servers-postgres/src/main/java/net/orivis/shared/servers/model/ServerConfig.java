package net.orivis.shared.servers.model;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Transient;
import lombok.Data;
import net.orivis.shared.annotations.ObjectByIdPresentation;
import net.orivis.shared.postgres.model.OrivisEntity;
import net.orivis.shared.servers.service.ServerConfigTypeService;
import net.orivis.shared.utils.bean_copier.OrivisIdToObjectTransformer;
import net.orivis.shared.utils.bean_copier.OrivisTranformer;

@Entity
@Data
public class ServerConfig  extends OrivisEntity {
    private String host;
    private String name;
    private String userName;
    private String password;
    private Integer port;
    private Long type;
    private Boolean active;
    private String serverParam;
    private String timeZone;

    public static final String SERVER_CONFIG_FIELD = "serverConfigType";

    @OneToOne
    @ObjectByIdPresentation
    @OrivisTranformer(transformWith = OrivisIdToObjectTransformer.class, additionalClass = ServerConfigTypeService.class)
    private ServerConfigType serverConfigType;


    @Transient
    public String getRoot(){
        return host + ":" + port + "/";
    }

    @Override
    public String asValue() {
        return getRoot();
    }
}
