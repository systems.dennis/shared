package net.orivis.shared.servers.providers;

import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.pojo_form.DataProvider;
import net.orivis.shared.pojo_form.ItemValue;
import net.orivis.shared.pojo_view.DefaultDataConverter;

import java.util.ArrayList;
import java.util.Objects;

public class ServerTypeProvider implements DataProvider<Long>, DefaultDataConverter<Long, String> {

    public static final Long LDAP  = 1L;
    public static final Long FILE_STORAGE  = 0L;
    public static final Long AUTH  = 3L;
    public static final Long VIRTUAL_AUTH  = 6L;
    public static final Long PAYMENT  = 4L;
    public static final Long ELASTIC_SEARCH  = 7L;
    public static final Long OTHER  = 5L;
    public static final Long TIME_ZONE  = 2L;
    public static final Long REMOTE_CRON  = 8L;
    public static final Long VERSION_CONTROL  = 9L;

    @Override
    public ArrayList<ItemValue<Long>> getItems( OrivisContext context) {

        var data = new ArrayList<ItemValue<Long>>();
        data.add(new ItemValue<>("FILE_STORAGE", FILE_STORAGE));
        data.add(new ItemValue<>("LDAP", LDAP));
        data.add(new ItemValue<>("AUTH", AUTH));
        data.add(new ItemValue<>("VIRTUAL_AUTH", VIRTUAL_AUTH));
        data.add(new ItemValue<>("PAYMENT", PAYMENT));
        data.add(new ItemValue<>("TIME_ZONE", TIME_ZONE));
        data.add(new ItemValue<>("ELASTIC_SEARCH", ELASTIC_SEARCH));
        data.add(new ItemValue<>("REMOTE_CRON", REMOTE_CRON));
        data.add(new ItemValue<>("OTHER", OTHER));
        data.add(new ItemValue<>("VERSION_CONTROL", VERSION_CONTROL));

        return data;
    }

    @Override
    public String convert(Long object, String data, OrivisContext context) {
        var items = getItems(  context);

        for (ItemValue value : items){
            if (Objects.equals(value.getValue(), object)){
                return String.valueOf(value.getLabel());
            }
        }
        return String.valueOf(object);
    }
}
