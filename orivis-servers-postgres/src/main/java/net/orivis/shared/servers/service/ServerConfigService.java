package net.orivis.shared.servers.service;

import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.postgres.model.OrivisEntity;
import org.springframework.stereotype.Service;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.postgres.service.PaginationService;

import net.orivis.shared.servers.exception.ServerException;
import net.orivis.shared.servers.form.ServerConfigForm;
import net.orivis.shared.servers.model.ServerConfig;
import net.orivis.shared.servers.providers.ServerTypeProvider;
import net.orivis.shared.servers.repository.ServerConfigRepo;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
@OrivisService(repo = ServerConfigRepo.class, form = ServerConfigForm.class, model = ServerConfig.class)
public class ServerConfigService extends PaginationService<ServerConfig> {
    private Map<Object, ServerConfig> serverConfigs = new HashMap<>();

    public ServerConfigService(OrivisContext holder) {
        super(holder);
    }

    public void checkActiveTypeExists(ServerConfigForm element) {

        if (element.getActive() == null || !element.getActive()) {
            return;
        }

        if (Objects.equals(element.getType(), ServerTypeProvider.OTHER)) {
            return;
        }

        if (element.getId() == null) {
            if (count(getFilterImpl().eq("type", element.getType())
                    .and(getFilterImpl().eq("active", true)))> 0){
                throw new ServerException("server_already_exists");
            }
        } else {
            if (count(getFilterImpl().eq("type", element.getType()).and(getFilterImpl().eq("active", true))
                    .and(getFilterImpl().notEq(OrivisEntity.ID_FIELD, element.getId())))> 0) {
                throw new ServerException("server_already_exists");
            }
        }

    }

    @Override
    public ServerConfig afterAdd(ServerConfig object) {
        addToCache(object);

        return super.afterAdd(object);
    }

    @Override
    public void afterEdit(ServerConfig object, ServerConfig original) {
        addToCache(object);

        super.afterEdit(object, original);
    }

    @Override
    public ServerConfig afterDelete(ServerConfig object) {
        deleteFromCache(object);

        return super.afterDelete(object);
    }

    public ServerConfig findByServerConfigTypeName(String serverConfigTypeName) {
        ServerConfig fromCache = getFromCache(serverConfigTypeName);
        if (fromCache != null) {
            return fromCache;
        }

        var filter = getFilterImpl().eq("name", serverConfigTypeName  ).setJoinOn(ServerConfig.SERVER_CONFIG_FIELD)
                .and(getFilterImpl().eq("active", true));

        ServerConfig serverConfig = getRepository().filteredFirst(filter).orElse(null);

        if (serverConfig != null) {
            addToCache(serverConfig);
        }

        return serverConfig;
    }

    public ServerConfig findByType(Long type, boolean silent) {

        ServerConfig fromCache = getFromCache(type);
        if (fromCache != null) {
            return fromCache;
        }

        var filter = getFilterImpl().eq("type", type).and(getFilterImpl().eq("active", true));


        ServerConfig res =getRepository().filteredFirst(filter).orElse(null);

        if (silent && res == null) {
            return res;
        }

        if (res == null) throw ItemNotFoundException.fromId(type);

        addToCache(res);


        return res;
    }

    private void addToCache(ServerConfig serverConfig) {
        Object key = getKey(serverConfig);

        if (serverConfig.getActive()) {
            serverConfigs.put(key, serverConfig);
        } else {
            serverConfigs.remove(key);
        }
    }

    private ServerConfig getFromCache(Object key) {
        return serverConfigs.get(key);
    }

    private void deleteFromCache(ServerConfig serverConfig) {
        Object key = getKey(serverConfig);
        serverConfigs.remove(key);
    }

    private Object getKey(ServerConfig serverConfig) {
        if (Objects.nonNull(serverConfig.getServerConfigType())) {
            return serverConfig.getServerConfigType().getName();
        } else {
            return serverConfig.getType();
        }
    }
}
