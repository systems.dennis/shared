package net.orivis.shared.servers.validation;

import net.orivis.shared.annotations.ValidationContent;

import net.orivis.shared.pojo_form.ValidationResult;
import net.orivis.shared.servers.form.ServerConfigForm;
import net.orivis.shared.servers.service.ServerConfigService;
import net.orivis.shared.validation.ValueValidator;

public class ServerTypeValidator implements ValueValidator<ServerConfigForm, Long> {
    @Override
    public ValidationResult validate(ServerConfigForm element,Long value, ValidationContent content) {

        try {
            content.getContext().getBean(ServerConfigService.class).checkActiveTypeExists(element);
        } catch (Exception e){
            return ValidationResult.fail("server.type.already_active");
        }

        return ValidationResult.PASSED;
    }
}
