package net.orivis.shared.servers.repository;

import net.orivis.shared.postgres.repository.OrivisRepository;
import org.springframework.stereotype.Repository;
import net.orivis.shared.servers.model.ServerConfigType;

@Repository
public interface ServerConfigTypeRepo extends OrivisRepository<ServerConfigType> {

}
