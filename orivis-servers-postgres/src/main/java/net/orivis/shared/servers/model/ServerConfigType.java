package net.orivis.shared.servers.model;

import jakarta.persistence.Entity;
import lombok.Data;
import net.orivis.shared.postgres.model.OrivisEntity;

@Data
@Entity
public class ServerConfigType extends OrivisEntity {
    private String name;

    @Override
    public String asValue() {
        return name;
    }
}
