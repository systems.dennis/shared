package net.orivis.shared.servers.exception;

import net.orivis.shared.exceptions.StandardException;

public class ServerException extends StandardException {
    public ServerException(String text){
        super(text, "Error");
    }
}
