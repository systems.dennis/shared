package net.orivis.shared.servers.controller;

import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.web.bind.annotation.*;
import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.controller.SearchEntityApi;
import net.orivis.shared.controller.SearcherInfo;
import net.orivis.shared.postgres.controller.AddItemController;
import net.orivis.shared.postgres.controller.DeleteItemController;
import net.orivis.shared.postgres.controller.EditItemController;
import net.orivis.shared.postgres.controller.ListItemController;
import net.orivis.shared.servers.form.ServerConfigForm;
import net.orivis.shared.servers.model.ServerConfig;
import net.orivis.shared.servers.service.ServerConfigService;

import java.io.Serializable;

/**
 * Provides Role adding, editing, deleting and assigning
 */
@RestController
@RequestMapping("/api/v2/shared/servers")
@Secured (roles = "ROLE_ADMIN")
@CrossOrigin
@OrivisController(ServerConfigService.class)
public class ServerController<TYPE_ID extends Serializable> extends OrivisContextable implements
        DeleteItemController<ServerConfig>,
        AddItemController<ServerConfig, ServerConfigForm>,
        EditItemController<ServerConfig, ServerConfigForm>,
        ListItemController<ServerConfig, ServerConfigForm> {

    static  {
        SearchEntityApi.registerSearch("server", new SearcherInfo("name", ServerConfigService.class));
    }

    public ServerController(OrivisContext context) {
        super(context );
    }

    @Override
    public ServerConfigService getService() {
        return getBean(ServerConfigService.class);
    }

    @WithRole("ROLE_SYNC")
    @GetMapping("/type/{type}")
    public ServerConfigForm findByType(@PathVariable("type") Long type) {


        return toForm(getService().findByType(type, false));
    }
}
