package net.orivis.shared.servers.beans;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.servers.converter.FormUrlEncodedConverter;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import net.orivis.shared.annotations.NeverNullResponse;
import net.orivis.shared.servers.model.ServerConfig;
import net.orivis.shared.servers.service.ServerConfigService;
import net.orivis.shared.utils.Mapper;
import net.orivis.shared.utils.Supplier;

import java.util.HashMap;
import java.util.Map;

@Slf4j
public class ServerRequest<T> {
    @Getter
    private RestTemplate restTemplate;
    @Getter
    private OrivisContext context;

    @Getter
    private Map<Integer, Supplier<?>> onError = new HashMap<>();
    @Getter
    private T result;

    @Getter
    private String url;

    public  static String AUTH_TYPE_HEADER = "AUTH_TYPE";
    public  static String AUTH_SCOPE_HEADER = "AUTH_SCOPE";
    public static final String AUTH_TYPE_DEFAULT = "DEFAULT";
    public  static String AUTH_TYPE_VIRTUAL = "VIRTUAL";

    public ServerRequest(RestTemplate restTemplate, OrivisContext context) {
        this.restTemplate = restTemplate;

        this.context = context;
    }


    @Getter
    private final HttpHeaders headers = new HttpHeaders();

    private ServerConfig server;

    @NeverNullResponse
    public ServerRequest<T> virtualAuth() {
        headers.set(AUTH_TYPE_HEADER, AUTH_TYPE_VIRTUAL);
        return this;
    }

    @NeverNullResponse
    public ServerRequest<T> uri(String uri) {

        this.url = uri;
        return this;
    }

    @NeverNullResponse
    public ServerRequest<T> server(Long type) {
        this.server = context.getBean(ServerConfigService.class).findByType(type, true);
        return this;
    }

    @NeverNullResponse
    public ServerRequest<T> server(ServerConfig server) {
        if (server.getId() == null){
            this.server = server;
        } else {
            this.server = context.getBean(ServerConfigService.class).findByIdOrThrow(server.getId());
        }
        return this;
    }

    @NeverNullResponse
    public ServerRequest<T> token(String token) {
        headers.setBearerAuth(token);
        return this;
    }

    @NeverNullResponse
    public ServerRequest<T> scope() {
        headers.set(AUTH_SCOPE_HEADER, getCurrentScope());
        return this;
    }

    @NeverNullResponse
    public ServerRequest<T> scope(String scope) {
        headers.set(AUTH_SCOPE_HEADER, scope);
        return this;
    }

    @NeverNullResponse
    public ServerRequest<T> header(Map<String, String> headerMap) {
        for (Map.Entry<String, String> entry : headerMap.entrySet()) {
            headers.set(entry.getKey(), entry.getValue());
        }
        return this;
    }

    @NeverNullResponse
    public ServerRequest<T> contentType(MediaType contentType) {
        headers.setContentType(contentType);
        return this;
    }

    @NeverNullResponse
    public ServerRequest<T> onErrorStatusError(int status, Supplier<?> e) {
        onError.put(status, e);
        return this;
    }

    @NeverNullResponse
    public ServerRequest<T> onAnyErrorStatusError(Supplier<?> e) {
        onError.put(-1, e);
        return this;
    }


    /**
     * @deprecated as normally default type doesn't require this header
     */
    @Deprecated
    @NeverNullResponse
    public ServerRequest<T> defaultAuthType() {
        headers.set(AUTH_TYPE_HEADER, AUTH_TYPE_DEFAULT);
        return this;
    }


    public <E> T executePost(E object, Class<T> tClass) {
        try {
            if (!this.headers.containsKey(HttpHeaders.CONTENT_TYPE)) {
                this.headers.setContentType(MediaType.APPLICATION_JSON);
            }

            var el = new HttpEntity<>(object, this.headers);
            String contentType = this.headers.getFirst(HttpHeaders.CONTENT_TYPE);

            ResponseEntity<T> resp;
            if (contentType != null && contentType.equalsIgnoreCase(MediaType.APPLICATION_FORM_URLENCODED_VALUE)) {
                MultiValueMap<String, String> formParams = FormUrlEncodedConverter.convertToFormUrlEncoded(object);

                resp = restTemplate.postForEntity((server == null ?  "" :  server.getRoot()) + url, formParams, tClass);
            } else {
                resp = restTemplate.postForEntity((server == null ?  "" :  server.getRoot()) + url, el, tClass);
            }

            validateExceptions(resp);

            this.result = resp.getBody();
            return this.result;
        } catch (Exception e) {
            printRequest(e);
            throw e;
        }
    }

    public <E> T executePut(E object, Class<T> tClass) {
        if (!this.headers.containsKey(HttpHeaders.CONTENT_TYPE)) {
            this.headers.setContentType(MediaType.APPLICATION_JSON);
        }
        HttpEntity<E> entity = new HttpEntity<>(object, this.headers);
        ResponseEntity<T> response = restTemplate.exchange((server == null ?  "" :  server.getRoot()) + url, HttpMethod.PUT, entity, tClass);
        validateExceptions(response);

        this.result = response.getBody();
        return this.result;
    }

    public <E> T executePatch(E object, Class<T> tClass) {
        if (!this.headers.containsKey(HttpHeaders.CONTENT_TYPE)) {
            this.headers.setContentType(MediaType.APPLICATION_JSON);
        }
        HttpEntity<E> entity = new HttpEntity<>(object, this.headers);
        ResponseEntity<T> response = restTemplate.exchange((server == null ?  "" :  server.getRoot()) + url, HttpMethod.PATCH, entity, tClass);
        validateExceptions(response);

        this.result = response.getBody();
        return this.result;
    }

    private void validateExceptions(ResponseEntity<T> resp) {
        printResponse(resp);
        var anyError = onError.get(-1);
        if (200 != resp.getStatusCode().value()) {
            this.onError.keySet().forEach((code) -> {
                if (code != -1) {
                    if (resp.getStatusCode().value() == code) {
                        onError.get(code).onNull(resp.getStatusCode().value());
                    }
                }
            });

            if (anyError != null) {
                anyError.onNull(resp.getStatusCode().value());
            }
        }


    }

    private void printResponse(ResponseEntity<T> resp) {
        log.debug("Url: " + this.url);
        log.debug("code: " + resp.getStatusCode());
        if (resp.getBody() != null) {
            try {
                log.debug("message: " + Mapper.mapper.writeValueAsString(resp.getBody()));
            } catch (Exception e) {
                log.debug("message: " + resp.getBody());
            }
        } else {
            log.debug("message: null");
        }
    }

    private void printRequest(Exception e) {
        log.error("Url: " + this.url);
        log.error("exception", e);
    }

    public <E> T executeGet(Class<T> tClass) {
        try {
            var el = new HttpEntity<>(this.headers);
            var resp = restTemplate.exchange((server.getRoot() == null ? "" : server.getRoot()) + url, HttpMethod.GET, el, tClass);
            validateExceptions(resp);

            this.result = resp.getBody();
            return this.result;
        } catch (Exception e) {
            printRequest(e);
            throw e;
        }
    }
    public <E> T executeGet(ParameterizedTypeReference<T> respType ) {
        try {
            var el = new HttpEntity<>(this.headers);
            var resp = restTemplate.exchange((server.getRoot() == null ? "" : server.getRoot()) + url, HttpMethod.GET, el, respType);
            validateExceptions(resp);

            this.result = resp.getBody();
            return this.result;
        } catch (Exception e) {
            printRequest(e);
            throw e;
        }
    }

    public void executeDelete(Class<T> tClass) {
        try {
            var requestEntity = new HttpEntity<>(this.headers);
            var resp = restTemplate.exchange(server.getRoot() + url, HttpMethod.DELETE, requestEntity, tClass);
            validateExceptions(resp);
        } catch (Exception e) {
            printRequest(e);
            throw e;
        }
    }

    private String getCurrentScope() {
        return context.getEnv("dennis.systems.security.scope.id", "Default");
    }


}
