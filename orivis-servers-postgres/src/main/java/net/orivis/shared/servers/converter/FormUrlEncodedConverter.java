package net.orivis.shared.servers.converter;

import net.orivis.shared.exceptions.TransformException;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.lang.reflect.Field;

public class FormUrlEncodedConverter {

    public static <T> MultiValueMap<String, String> convertToFormUrlEncoded(T object) {
        MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();

        if (object == null) {
            return formData;
        }

        Field[] fields = object.getClass().getDeclaredFields();
        for (Field field : fields) {
            try {
                field.setAccessible(true);
                Object value = field.get(object);
                if (value != null) {
                    formData.add(field.getName(), value.toString());
                }
            } catch (IllegalAccessException e) {
                throw new TransformException("error_converting_to_form_urlencoded");
            }
        }
        return formData;
    }
}
