package net.orivis.shared.ui_settings.repository;


import org.springframework.data.mongodb.core.MongoTemplate;
import net.orivis.shared.mongo.repository.PaginationRepository;
import net.orivis.shared.ui_settings.model.FieldFormPosition;


public class FieldFormPositionRepository  extends PaginationRepository<FieldFormPosition> {
    public FieldFormPositionRepository(MongoTemplate mongoTemplate) {
        super(mongoTemplate);
    }
}
