package net.orivis.shared.ui_settings.repository;


import org.springframework.data.mongodb.core.MongoTemplate;
import net.orivis.shared.mongo.repository.PaginationRepository;
import net.orivis.shared.ui_settings.model.UserFormSettingsModel;

public class UserFormSettingsRepo extends PaginationRepository<UserFormSettingsModel> {

    public UserFormSettingsRepo(MongoTemplate mongoTemplate) {
        super(mongoTemplate);
    }
}
