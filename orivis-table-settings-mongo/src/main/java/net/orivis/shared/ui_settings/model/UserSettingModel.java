package net.orivis.shared.ui_settings.model;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.orivis.shared.mongo.model.OrivisAssignableEntity;

import java.util.TimeZone;

@Data
@Entity(name = "setting")
@NoArgsConstructor
@AllArgsConstructor
public class UserSettingModel extends OrivisAssignableEntity {

    @Column(name = "language")
    private String language;

    @Column(name = "time_zone")
    private TimeZone timeZone;

    @Override
    public String asValue() {
        return language ;
    }
}
