package net.orivis.shared.ui_settings.form;

import jakarta.persistence.OneToMany;
import lombok.Data;
import net.orivis.shared.annotations.ObjectByIdPresentation;
import net.orivis.shared.mongo.form.OrivisPojo;
import net.orivis.shared.ui_settings.model.FieldFormPosition;

import java.util.List;

;

@Data
public class UserFormSettingsForm implements OrivisPojo {

    private String id;
    @OneToMany
    @ObjectByIdPresentation
    private List<FieldFormPosition> positions;
    private String entityType;

    @Override
    public String asValue() {
        return entityType + " " + id;
    }
}
