package net.orivis.shared.ui_settings.model;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import lombok.Data;
import net.orivis.shared.annotations.ObjectByIdPresentation;
import net.orivis.shared.mongo.model.OrivisAssignableEntity;

import java.util.List;

@Data
@Entity
public class UserFormSettingsModel extends OrivisAssignableEntity {

    private String entityType;
    @OneToMany
    @ObjectByIdPresentation
    private List<FieldFormPosition> positions;

    @Override
    public String asValue() {
        return entityType;
    }
}
