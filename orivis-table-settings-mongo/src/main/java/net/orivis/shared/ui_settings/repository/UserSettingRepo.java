package net.orivis.shared.ui_settings.repository;


import org.springframework.data.mongodb.core.MongoTemplate;
import net.orivis.shared.mongo.repository.PaginationRepository;
import net.orivis.shared.ui_settings.model.UserSettingModel;

public class UserSettingRepo extends PaginationRepository<UserSettingModel> {

    public UserSettingRepo(MongoTemplate mongoTemplate) {
        super(mongoTemplate);
    }
}
