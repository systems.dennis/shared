package net.orivis.shared.ui_settings.repository;


import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;
import net.orivis.shared.mongo.repository.PaginationRepository;
import net.orivis.shared.ui_settings.model.TableSettingModel;

import java.io.Serializable;

@Repository
public class TableSettingRepo<ID_TYPE extends Serializable> extends PaginationRepository<TableSettingModel> {
    public TableSettingRepo(MongoTemplate mongoTemplate) {
        super(mongoTemplate);
    }
}
