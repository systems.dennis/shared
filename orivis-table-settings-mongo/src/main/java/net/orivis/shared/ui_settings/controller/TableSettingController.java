package net.orivis.shared.ui_settings.controller;

import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.web.bind.annotation.*;
import net.orivis.shared.annotations.security.Id;
import net.orivis.shared.annotations.security.SelfOnlyRole;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.exceptions.ItemForAddContainsIdException;
import net.orivis.shared.mongo.controller.AddItemController;
import net.orivis.shared.mongo.controller.DeleteItemController;
import net.orivis.shared.mongo.controller.ListItemController;
import net.orivis.shared.ui_settings.form.TableSettingForm;
import net.orivis.shared.ui_settings.model.TableSettingModel;
import net.orivis.shared.ui_settings.service.TableSettingService;
import net.orivis.shared.utils.security.DefaultIdChecker;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v2/shared/table_setting")
@OrivisController(value = TableSettingService.class)
@CrossOrigin
public class TableSettingController
        extends OrivisContextable
        implements AddItemController<TableSettingModel, TableSettingForm>,
        ListItemController<TableSettingModel, TableSettingForm>,
        net.orivis.shared.mongo.controller.DeleteItemController<TableSettingModel> {

    public TableSettingController(OrivisContext context) {
        super(context);
    }

    @GetMapping("/list_all")
    @WithRole
    public List<TableSettingForm> findByUserAndTopic(@RequestParam(required = true) String topic) {
        return getService().findByUserAndTopic(topic).stream().map(this::toForm).collect(Collectors.toList());
    }


    @DeleteMapping(value = "/delete/{id}",  consumes = {"application/json", "application/json;charset=UTF-8"}, produces = {"application/json", "application/json;charset=UTF-8"})
    @ResponseBody
    @WithRole
    @SelfOnlyRole ()
    @Override
    public void delete(@Id(checker = DefaultIdChecker.class) @PathVariable String id) throws ItemForAddContainsIdException {
        DeleteItemController.super.delete(id);
    }

    @Override
    public TableSettingService getService() {
        return getBean(TableSettingService.class);
    }


}
