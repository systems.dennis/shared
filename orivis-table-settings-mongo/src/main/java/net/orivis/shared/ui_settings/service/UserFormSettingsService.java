package net.orivis.shared.ui_settings.service;

import lombok.extern.slf4j.Slf4j;
import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.repository.OrivisFilter;
import org.springframework.stereotype.Service;
import net.orivis.shared.annotations.security.ISecurityUtils;
import net.orivis.shared.exceptions.ItemForAddContainsIdException;
import net.orivis.shared.mongo.service.PaginationService;
import net.orivis.shared.ui_settings.form.UserFormSettingsForm;
import net.orivis.shared.ui_settings.model.UserFormSettingsModel;
import net.orivis.shared.ui_settings.repository.FieldFormPositionRepository;
import net.orivis.shared.ui_settings.repository.UserFormSettingsRepo;

import java.io.Serializable;
import java.util.Optional;

@Service
@Slf4j
@OrivisService(model = UserFormSettingsModel.class, form = UserFormSettingsForm.class, repo = UserFormSettingsRepo.class)
public class UserFormSettingsService<ID_TYPE extends Serializable> extends PaginationService<UserFormSettingsModel> {
    public UserFormSettingsService(OrivisContext holder) {
        super(holder);
    }

    @Override
    public UserFormSettingsModel preAdd(UserFormSettingsModel object) throws ItemForAddContainsIdException {

        Optional<UserFormSettingsModel> existing = getRepository().filteredFirst(createUserFormSpecification(object.getEntityType()));
        var positionBean =   getBean(FieldFormPositionRepository.class);
        existing.ifPresent(userFormSettingsModel ->
        {
            object.setId(userFormSettingsModel.getId());
            try {

                positionBean.deleteAll(existing.get().getPositions());
            } catch (Exception e){
                log.info("Cannot delete old items", e);
            }
        });

        positionBean.saveAll(object.getPositions());


        return super.preAdd(object);
    }

    @Override
    public UserFormSettingsRepo getRepository() {
        return super.getRepository();
    }

    public OrivisFilter<UserFormSettingsModel> createUserFormSpecification(String entityType) {
        OrivisFilter<UserFormSettingsModel> specification = getSelfCreatedItems(getBean(ISecurityUtils.class).getUserDataId(), false);
        var additionalSpecification = getFilterImpl().eq("entityType", entityType);
        return specification.and(additionalSpecification);
    }

}
