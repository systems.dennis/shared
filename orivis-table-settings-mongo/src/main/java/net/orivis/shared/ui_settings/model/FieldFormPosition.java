package net.orivis.shared.ui_settings.model;

import jakarta.persistence.Entity;
import lombok.Data;
import net.orivis.shared.mongo.model.OrivisAssignableEntity;

@Data
@Entity
public class FieldFormPosition extends OrivisAssignableEntity {
    private String fieldName;
    private Integer position;

    private String tabName;

    @Override
    public String asValue() {
        return fieldName + " " + position;
    }
}
