package net.orivis.shared.ui_settings.form;

import lombok.Data;
import net.orivis.shared.annotations.Validation;
import net.orivis.shared.mongo.form.OrivisPojo;
import net.orivis.shared.validation.ValueNotEmptyValidator;

import java.util.TimeZone;

@Data
public class UserSettingForm implements OrivisPojo {

    private String id;

    @Validation(ValueNotEmptyValidator.class)
    private String language;

    @Validation(ValueNotEmptyValidator.class)
    private TimeZone timeZone;
}
