package net.orivis.shared.model_history.repository;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import net.orivis.shared.model_history.model.DeleteHistoryModel;
import net.orivis.shared.mongo.repository.PaginationRepository;

@Repository
public class DeleteHistoryRepo extends PaginationRepository<DeleteHistoryModel> {

    public DeleteHistoryRepo(MongoTemplate mongoTemplate) {
        super(mongoTemplate);
    }

//    public Optional<DeleteHistoryModel> findFirstByIdDeletedObjectAndClassDeletedObject(String delId, String type){
//        MongoSpecification<DeleteHistoryModel> spec = MongoSpecification.get( new Criteria().andOperator(Criteria.where("idDeletedObject").is(delId), Criteria.where("classDeletedObject").is( type)));
//        return findFirst(spec);
//    }
//
//    public boolean existsByIdDeletedObjectAndClassDeletedObject(String delId, String type){
//        MongoSpecification<DeleteHistoryModel> spec = MongoSpecification.get( new Criteria().andOperator(Criteria.where("idDeletedObject").is(delId), Criteria.where("classDeletedObject").is( type)));
//        return count(spec) > 0;
//    }
//
//    public List<DeleteHistoryModel> findAllByClassDeletedObject(String type){
//        MongoSpecification<DeleteHistoryModel> spec =  MongoSpecification.get(Criteria.where("classDeletedObject").is( type));
//        return findAll(spec);
//    }

}
