package net.orivis.shared.model_history.service;

import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.config.OrivisContext;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import net.orivis.shared.model_history.form.EditHistoryForm;
import net.orivis.shared.model_history.model.EditHistoryModel;
import net.orivis.shared.model_history.repository.EditHistoryRepo;
import net.orivis.shared.mongo.repository.MongoSpecification;
import net.orivis.shared.mongo.service.PaginationService;

import java.util.Optional;

@Service
@OrivisService(model = EditHistoryModel.class, form = EditHistoryForm.class, repo = EditHistoryRepo.class)
public class EditHistoryService extends PaginationService<EditHistoryModel> {
    public EditHistoryService(OrivisContext holder) {
        super(holder);
    }


    public Optional<EditHistoryModel> findById(String id) {
        return getRepository().filteredOne(getFilterImpl().id( id));
    }


    public Optional<EditHistoryModel> findByEditedTypeAndId(String type, String id) {
        EditHistoryRepo repo = getRepository();
        return repo.filteredFirst(getFilterImpl().eq("editedTpe", type).and(getFilterImpl().eq("idEditedObject", id)));
    }

    public Page<EditHistoryModel> findByEditedType(String type) {
        EditHistoryRepo repo = getRepository();
        return repo.filteredData((MongoSpecification<EditHistoryModel>) getFilterImpl().eq("classEditedObject", type));

    }

    @Override
    public EditHistoryRepo getRepository() {
        return super.getRepository();
    }
}
