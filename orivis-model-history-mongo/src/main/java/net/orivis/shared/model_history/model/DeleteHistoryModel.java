package net.orivis.shared.model_history.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import net.orivis.shared.mongo.model.OrivisEntity;

import java.util.Date;

@Data
@Entity(name = "delete_history_object")
@AllArgsConstructor
@NoArgsConstructor
public class DeleteHistoryModel extends OrivisEntity {

    @Column(name = "deleted_id")
    private String idDeletedObject;

    private Date deletedDate = new Date();

    @Column(name = "object_type")
    private String classDeletedObject;

    @Column(columnDefinition = "text")
    private String content;

    @SneakyThrows
    public static DeleteHistoryModel from(OrivisEntity deleted) {
        ObjectMapper objectMapper = new ObjectMapper();
        String content = objectMapper.writeValueAsString(deleted);
        DeleteHistoryModel model = new DeleteHistoryModel();
        model.setIdDeletedObject(deleted.getId());
        model.setClassDeletedObject(deleted.getClass().getSimpleName());
        model.setContent(content);
        return model;
    }

    @Override
    public String asValue() {
        return classDeletedObject + getId();
    }
}
