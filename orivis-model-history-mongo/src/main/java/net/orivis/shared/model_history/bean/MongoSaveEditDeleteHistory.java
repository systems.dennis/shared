package net.orivis.shared.model_history.bean;

import lombok.extern.slf4j.Slf4j;
import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.mongo.model.OrivisEntity;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import net.orivis.shared.beans.AbstractEditDeleteHistoryBean;
import net.orivis.shared.exceptions.StatusException;
import net.orivis.shared.model_history.model.DeleteHistoryModel;
import net.orivis.shared.model_history.model.EditHistoryModel;
import net.orivis.shared.model_history.service.DeleteHistoryService;
import net.orivis.shared.model_history.service.EditHistoryService;;
import net.orivis.shared.exceptions.DeleteNotPossibleException;
import net.orivis.shared.exceptions.ItemWasDeletedException;

import java.util.Date;

import static net.orivis.shared.utils.Mapper.mapper;

@Slf4j @Service @Primary
public class MongoSaveEditDeleteHistory extends AbstractEditDeleteHistoryBean<OrivisEntity, String> {


    public MongoSaveEditDeleteHistory(OrivisContext context) {
        super(context);
    }

    @Override
    public void edit(OrivisEntity from, OrivisEntity to) {
        if (! getContext().getEnv("global.app.model_history.edit_allowed", true)){
            return;
        }
        var editableObject = EditHistoryModel.from(from, to);
        getContext().getBean(EditHistoryService.class).save(editableObject);
    }

    @Override
    public void delete(String id, OrivisEntity model) {
        if (! getContext().getEnv("global.app.model_history.delete_allowed", true)){
            return;
        }

        if (id == null || model == null){
            throw new DeleteNotPossibleException("id_or_model_are_null", id);
        }
        try {
            DeleteHistoryModel deleteHistoryModel = new DeleteHistoryModel();
            deleteHistoryModel.setDeletedDate(new Date());
            deleteHistoryModel.setContent(mapper.writeValueAsString(model));
            deleteHistoryModel.setClassDeletedObject(model.getClass().getSimpleName());

            var deletedObjectExists = getBean(DeleteHistoryService.class).existsByTypeAndDeletedId(id,
                    this.getClass().getAnnotation(OrivisService.class).model().getSimpleName());
            if (deletedObjectExists) {
                throw new ItemWasDeletedException(id, model.getClass());
            }
        } catch (Exception e){
            log.debug("cannot save object history: ", e);
            if (getContext().getEnv("global.app.model_history.delete_throw_error_on_exception", false)){
                throw new StatusException("global.app.delete_model_history.not_possible", HttpStatus.NOT_EXTENDED);
            }
        }

    }

    @Override
    public void throwIfDeleted(String id, Class model) {
        if (! getContext().getEnv("global.app.model_history.delete_allowed", true)){
            return;
        }
        if (model == null || id == null) return;
        var deletedObjectExists = getBean(DeleteHistoryService.class).existsByTypeAndDeletedId(id,
                model.getSimpleName());
        if (deletedObjectExists) {
            throw new ItemWasDeletedException(id, model.getClass().getSuperclass());
        }
    }
}
