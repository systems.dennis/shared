package net.orivis.shared.model_history.controller;

import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import net.orivis.shared.annotations.security.SelfOnlyRole;

import net.orivis.shared.exceptions.HistoryObjectNotFoundException;
import net.orivis.shared.model_history.form.DeleteHistoryForm;
import net.orivis.shared.model_history.model.DeleteHistoryModel;
import net.orivis.shared.model_history.service.DeleteHistoryService;
import net.orivis.shared.mongo.controller.ListItemController;


import java.util.List;
import java.util.stream.Collectors;


@RequestMapping("/api/v2/deleted/history")
@OrivisController(value = DeleteHistoryService.class)
public class DeleteHistoryController extends OrivisContextable implements ListItemController<DeleteHistoryModel, DeleteHistoryForm> {


    public DeleteHistoryController(OrivisContext context) {
        super(context);
    }

    @GetMapping("/find/{type}/{id}")
    @SelfOnlyRole
    public DeleteHistoryForm findByDeletedIdAndType(@PathVariable String type, @PathVariable String id) {
        DeleteHistoryService service = getService();
        return toForm(service.findByTypeAndDeletedId(type, id).orElseThrow(() -> new HistoryObjectNotFoundException(id, type)));
    }

    @GetMapping("/find/{type}")
    public List<DeleteHistoryForm> findByType(@PathVariable String type) {
        DeleteHistoryService service = getService();
        return service.findByType(type)
                .stream()
                .map(this::toForm)
                .collect(Collectors.toList());
    }
}
