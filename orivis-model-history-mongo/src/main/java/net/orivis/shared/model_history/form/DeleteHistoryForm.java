package net.orivis.shared.model_history.form;

import lombok.Data;
import net.orivis.shared.mongo.form.OrivisPojo;

import java.util.Date;

@Data
public class DeleteHistoryForm implements OrivisPojo {
    private String id;

    private Long idDeletedObject;

    private Date deletedDate;

    private String classDeletedObject;

    private String content;
}
