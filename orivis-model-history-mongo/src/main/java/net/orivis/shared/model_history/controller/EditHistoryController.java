package net.orivis.shared.model_history.controller;

import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.annotations.security.OrivisSecurityValidator;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.controller.orivis.ConfigDrivenPermissionCondition;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.annotations.security.SelfOnlyRole;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.exceptions.EditableObjectNotFoundException;
import net.orivis.shared.model_history.form.EditHistoryForm;
import net.orivis.shared.model_history.model.EditHistoryModel;
import net.orivis.shared.model_history.service.EditHistoryService;
import net.orivis.shared.mongo.controller.ListItemController;

import java.util.List;
import java.util.stream.Collectors;


@RequestMapping("/api/v2/history/edited")
@RestController
@Secured
@OrivisController(value = EditHistoryService.class)
public class EditHistoryController extends OrivisContextable
        implements ListItemController<EditHistoryModel, EditHistoryForm> {

    public EditHistoryController(OrivisContext context) {
        super(context);
    }

    @SelfOnlyRole
    @GetMapping("/find/{type}/{id}")
    public EditHistoryForm findByEditedTypeAndId(@PathVariable String type, @PathVariable String id) {
        EditHistoryService service = getService();
        return toForm(service.findByEditedTypeAndId(type, id)
                .orElseThrow(() -> new EditableObjectNotFoundException(type, id)));
    }

    @GetMapping("/find/{type}")
    @WithRole(value = "ROLE_ADMIN", ignoreOnCondition = @OrivisSecurityValidator(value = ConfigDrivenPermissionCondition.class, parameters = {"global.app.config.view.edit.history", "{\"type\":\"role\", \"values\": [\"ROLE_ADMIN\"]}"}))
    public List<EditHistoryForm> findAllByType(@PathVariable String type) {
        EditHistoryService service = getService();
        return service.findByEditedType(type)
                .stream()
                .map(this::toForm)
                .collect(Collectors.toList());
    }


}
