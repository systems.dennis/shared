package net.orivis.shared.model_history.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import net.orivis.shared.mongo.model.OrivisEntity;
import org.springframework.data.mongodb.core.index.Indexed;

import java.util.Date;

@Data
@Entity(name = "edit_history_object")
@NoArgsConstructor
public class EditHistoryModel extends OrivisEntity {

    @Column(name = "edited_id")
    private String idEditedObject;

    private Date editedDate = new Date();

    @Indexed
    @Column(name = "object_type")
    private String classEditedObject;

    @Column(columnDefinition = "text")
    private String oldContent;

    @Column(columnDefinition = "text")
    private String newContent;

    @SneakyThrows
    public static EditHistoryModel from(OrivisEntity original, OrivisEntity edited) {
        ObjectMapper objectMapper = new ObjectMapper();
        String oldContent = objectMapper.writeValueAsString(original);
        String newContent = objectMapper.writeValueAsString(edited);

        EditHistoryModel model = new EditHistoryModel();
        model.setIdEditedObject(original.getId());
        model.setClassEditedObject(original.getClass().getSimpleName());
        model.setOldContent(oldContent);
        model.setNewContent(newContent);
        return model;
    }

    @Override
    public String asValue() {
        return classEditedObject + getId();
    }
}
