package net.orivis.shared.model_history.service;

import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.config.OrivisContext;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import net.orivis.shared.model_history.form.DeleteHistoryForm;
import net.orivis.shared.model_history.model.DeleteHistoryModel;
import net.orivis.shared.model_history.repository.DeleteHistoryRepo;
import net.orivis.shared.mongo.repository.MongoSpecification;
import net.orivis.shared.mongo.service.PaginationService;
import net.orivis.shared.repository.AbstractRepository;


import java.util.Optional;

@Service
@OrivisService(model = DeleteHistoryModel.class, form = DeleteHistoryForm.class, repo = DeleteHistoryRepo.class)
public class DeleteHistoryService extends PaginationService<DeleteHistoryModel> {
    public DeleteHistoryService(OrivisContext holder) {
        super(holder);
    }


    public Optional<DeleteHistoryModel> findById(String id) {
        return getRepository().findById(id);
    }

    public boolean existsByTypeAndDeletedId(String id, String type) {
        DeleteHistoryRepo repo = getRepository();



        return repo.filteredCount(getFilterImpl().eq("deletedId", id).and(getFilterImpl().eq("classDeletedObject", type))) > 0;
    }

    public Optional<DeleteHistoryModel> findByTypeAndDeletedId(String type, String id) {
        DeleteHistoryRepo repo = getRepository();
        return repo.filteredFirst(getFilterImpl().eq("type", type).and(getFilterImpl().eq("deletedId", id)));



    }

    public Page<DeleteHistoryModel> findByType(String type) {
        DeleteHistoryRepo repo = getRepository();
        return repo.filteredData((MongoSpecification<DeleteHistoryModel>) getFilterImpl().eq("type", type));

    }

    @Override
    public <F extends AbstractRepository<DeleteHistoryModel, String>> F getRepository() {
        return super.getRepository();
    }
}
