package net.orivis.shared.model_history.repository;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;
import net.orivis.shared.model_history.model.EditHistoryModel;
import net.orivis.shared.mongo.repository.PaginationRepository;

@Repository
public class EditHistoryRepo extends PaginationRepository<EditHistoryModel> {

    public EditHistoryRepo(MongoTemplate mongoTemplate) {
        super(mongoTemplate);
    }
}
