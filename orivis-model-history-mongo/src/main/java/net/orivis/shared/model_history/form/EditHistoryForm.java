package net.orivis.shared.model_history.form;

import lombok.Data;
import net.orivis.shared.mongo.form.OrivisPojo;

import java.util.Date;

@Data
public class EditHistoryForm implements OrivisPojo {

    private String id;

    private String idEditedObject;

    private Date editedDate;

    private String classEditedObject;

    private String oldContent;

    private String newContent;
}
