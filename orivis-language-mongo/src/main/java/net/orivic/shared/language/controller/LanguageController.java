package net.orivic.shared.language.controller;

import net.orivic.shared.language.form.LanguageForm;
import net.orivic.shared.language.model.LanguageModel;
import net.orivic.shared.language.service.LanguageService;
import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.controller.SearchEntityApi;
import net.orivis.shared.controller.SearcherInfo;
import net.orivis.shared.mongo.controller.AddItemController;
import net.orivis.shared.mongo.controller.DeleteItemController;
import net.orivis.shared.mongo.controller.EditItemController;
import net.orivis.shared.mongo.controller.ListItemController;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v2/shared/language")
@OrivisController(LanguageService.class)
@CrossOrigin
@Secured (roles = "ROLE_ADMIN")
public class LanguageController
        extends OrivisContextable
        implements AddItemController<LanguageModel, LanguageForm>,
        EditItemController<LanguageModel, LanguageForm>,
        ListItemController<LanguageModel, LanguageForm>,
        DeleteItemController<LanguageModel> {

    static {
        SearchEntityApi.registerSearch("language", new SearcherInfo("name", LanguageService.class));
    }

    public LanguageController(OrivisContext context) {
        super(context);
    }
}
