package net.orivic.shared.language.repo;

import net.orivic.shared.language.model.LanguageModel;
import net.orivis.shared.mongo.repository.PaginationRepository;
import org.springframework.data.mongodb.core.MongoTemplate;

public class LanguageRepo extends PaginationRepository<LanguageModel> {

    public LanguageRepo(MongoTemplate mongoTemplate) {
        super(mongoTemplate);
    }

}
