package net.orivic.shared.language.form;

import lombok.Data;
import net.orivis.shared.annotations.Validation;
import net.orivis.shared.mongo.form.OrivisPojo;
import net.orivis.shared.pojo_form.Checkable;
import net.orivis.shared.pojo_form.PojoFormElement;
import net.orivis.shared.pojo_view.UIAction;
import net.orivis.shared.pojo_view.list.PojoListView;
import net.orivis.shared.pojo_view.list.PojoListViewField;
import net.orivis.shared.pojo_view.list.Remote;
import net.orivis.shared.validation.FieldIsUniqueValidator;

import static net.orivis.shared.pojo_view.DEFAULT_TYPES.*;

@Data
@PojoListView(actions = {"download", "new", "settings"})
public class LanguageForm implements OrivisPojo {

    @PojoFormElement(type = HIDDEN)
    private String id;

    @PojoFormElement(required = true)
    @PojoListViewField(searchable = true)
    @Validation(FieldIsUniqueValidator.class)
    private String name;
    @PojoFormElement(required = true, type = FILE, remote = @Remote(searchType = FILE))
    @PojoListViewField(searchable = false, sortable = false)
    @Validation(FieldIsUniqueValidator.class)
    private String icon;

    @PojoFormElement(required = true)
    @PojoListViewField(searchable = true)
    private String code;

    @PojoFormElement (checked = @Checkable, type = CHECKBOX, remote = @Remote(searchType = CHECKBOX))
    @PojoListViewField (type = CHECKBOX, remote = @Remote(searchType = CHECKBOX))
    private Boolean active;

    @PojoListViewField(actions = {@UIAction(component = "edit"), @UIAction(component = "delete")}, sortable = false)
    @PojoFormElement(available = false)
    private Integer action;

    @Override
    public String asValue() {
        return name;
    }
}
