package net.orivis.shared.dbupdater.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class DbUpdaterModel {
    List<DbInjection> dbInjectionList;
}