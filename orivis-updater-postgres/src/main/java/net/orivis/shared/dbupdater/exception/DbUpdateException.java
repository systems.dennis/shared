package net.orivis.shared.dbupdater.exception;

import net.orivis.shared.exceptions.StandardException;

public class DbUpdateException extends StandardException {
    public DbUpdateException(String text){
        super("db_updter", text);
    }
}
