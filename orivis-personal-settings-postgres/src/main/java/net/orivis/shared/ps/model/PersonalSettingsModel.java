package net.orivis.shared.ps.model;


import jakarta.persistence.Entity;
import lombok.Data;
import net.orivis.shared.postgres.model.LongAssignableEntity;

@Data
@Entity
public class PersonalSettingsModel extends LongAssignableEntity {
    public static final String PERSONAL_SETTING_NAME_FIELD = "name" ;
    private String name;
    private String value;
    private String type;

    @Override
    public String asValue() {
        return name;
    }
}
