package net.orivis.shared.ps.controller;

import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.web.bind.annotation.*;
import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.postgres.controller.AddItemController;
import net.orivis.shared.postgres.controller.DeleteItemController;
import net.orivis.shared.postgres.controller.ListItemController;
import net.orivis.shared.ps.form.PersonalSettingsForm;
import net.orivis.shared.ps.model.PersonalSettingsModel;
import net.orivis.shared.ps.service.PersonalSettingsService;

import java.io.Serializable;

@RestController
@RequestMapping("/api/v2/shared/personal_settings")
@OrivisController(value = PersonalSettingsService.class)
@CrossOrigin
@Secured
public class PersonalSettingsController<ID_TYPE extends Serializable>
        extends OrivisContextable
        implements AddItemController<PersonalSettingsModel, PersonalSettingsForm>,
        ListItemController<PersonalSettingsModel, PersonalSettingsForm>,
        DeleteItemController<PersonalSettingsModel> {

    public PersonalSettingsController(OrivisContext context) {
        super(context);
    }


    @GetMapping("/by_name")
    public PersonalSettingsForm getByName(@RequestParam("name") String name) {
        return toForm(getService().findFirstByName(name));
    }

    @DeleteMapping("/by_name")
    public PersonalSettingsForm deleteByName(@RequestParam("name") String name) {
        var item = getByName(name);
        getService().delete(item.getId());
        return item;
    }

    @Override
    public PersonalSettingsService getService() {
        return (PersonalSettingsService) getContext().getBean(getServiceClass());
    }
}
