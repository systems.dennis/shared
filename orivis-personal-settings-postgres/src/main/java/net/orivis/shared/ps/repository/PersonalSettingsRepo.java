package net.orivis.shared.ps.repository;


import net.orivis.shared.postgres.repository.OrivisRepository;
import org.springframework.stereotype.Repository;
import net.orivis.shared.ps.model.PersonalSettingsModel;

import java.io.Serializable;

@Repository
public interface PersonalSettingsRepo<ID_TYPE extends Serializable> extends OrivisRepository<PersonalSettingsModel> {
}
