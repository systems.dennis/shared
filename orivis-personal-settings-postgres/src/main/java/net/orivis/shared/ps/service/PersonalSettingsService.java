package net.orivis.shared.ps.service;

import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.repository.OrivisFilter;
import org.springframework.stereotype.Service;
import net.orivis.shared.annotations.DeleteStrategy;
import net.orivis.shared.annotations.security.ISecurityUtils;
import net.orivis.shared.exceptions.ItemForAddContainsIdException;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.postgres.service.PaginationService;
import net.orivis.shared.ps.form.PersonalSettingsForm;
import net.orivis.shared.ps.model.PersonalSettingsModel;
import net.orivis.shared.ps.repository.PersonalSettingsRepo;

import static net.orivis.shared.annotations.DeleteStrategy.DELETE_STRATEGY_PROPERTY;

@Service
@OrivisService(model = PersonalSettingsModel.class, form = PersonalSettingsForm.class, repo = PersonalSettingsRepo.class)
@DeleteStrategy(value  = DELETE_STRATEGY_PROPERTY, selfOnly = true)
public class PersonalSettingsService extends PaginationService<PersonalSettingsModel> {
    public PersonalSettingsService(OrivisContext context){
        super(context);

    }

    public PersonalSettingsModel findFirstByName(String name) {

        OrivisFilter<PersonalSettingsModel> filter = getFilterImpl().eq(PersonalSettingsModel.PERSONAL_SETTING_NAME_FIELD, name)
                .and(getBean(ISecurityUtils.class).belongsToMeSpecification(PersonalSettingsModel.class, getContext()));



        return getRepository().filteredOne(filter).orElseThrow(() -> new ItemNotFoundException(name));
    }

    @Override
    public PersonalSettingsModel preAdd(PersonalSettingsModel object) throws ItemForAddContainsIdException {



        try {
            var res = findFirstByName(object.getName());
            object.setId(res.getId());
        } catch (Exception exception){
            //ignored all ok
        }
        return super.preAdd(object);
    }

    @Override
    public OrivisFilter<PersonalSettingsModel> getAdditionalSpecification() {
        return getBean(ISecurityUtils.class).belongsToMeSpecification(PersonalSettingsModel.class,  getContext()).and(getNotDeletedQuery());
    }
}
