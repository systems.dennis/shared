package net.orivis.shared.dbupdater.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.Transient;
import lombok.Data;
import lombok.ToString;
import net.orivis.shared.mongo.model.OrivisAssignableEntity;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data

@Document(collection = "db_injection")
@ToString
public class DbInjection  extends OrivisAssignableEntity {

    @Transient
    public boolean root;

    @Field("text")
    private String sql;

    private String name;

    @JsonProperty ("always_to_run")
    private boolean alwaysToRun;

    @JsonProperty ("fail_on_execute")
    private boolean failOnExecute;

    @JsonProperty ("restart_on_fail")
    private boolean restartOnFail = false;

    private String script;

    @Field("if_sql")
    @JsonProperty ("if-not-sql")
    private String ifSql;

    @Field("db")
    @JsonProperty ("db")
    private String db;

    private boolean result;

    private String message;

    private String profile;

    @Transient
    private boolean isolated;

    @Override
    public boolean equals(Object o){
        if (getName() == null){
            return false;
        }
        if (o instanceof String){
            return getName().equals(o);
        } else if (o instanceof DbInjection){
            return getName() != null &&this.getName().equals(((DbInjection) o).getName());
        }

        return false;
    }

    @Override
    public int hashCode() {
        return getName() == null ? 0 : getName().hashCode();
    }

    @Override
    public String asValue() {
        return name;
    }
}
