package net.orivis.shared.dbupdater.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import net.orivis.shared.dbupdater.model.DbInjection;
import net.orivis.shared.mongo.repository.PaginationRepository;

import java.util.List;

@Repository
public class UpdateRepository extends PaginationRepository<DbInjection> {

    private final MongoTemplate mongoTemplate;

    public UpdateRepository(MongoTemplate mongoTemplate) {
        super(mongoTemplate);
        this.mongoTemplate = mongoTemplate;
    }


    public List<DbInjection> getFirstByNameAndProfile(String name, String profile, Pageable pageable) {
        Query query = new Query();
        query.addCriteria(Criteria.where("name").is(name)
                .orOperator(Criteria.where("profile").is(""), Criteria.where("profile").is(profile)));

        query.with(pageable);

        return mongoTemplate.find(query, DbInjection.class);
    }
}
