package net.orivis.shared.sequences.model;

import lombok.Data;
import net.orivis.shared.mongo.model.OrivisAssignableEntity;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class SequenceModel extends OrivisAssignableEntity {
    private Long value;
    private String type;

    @Override
    public String asValue() {
        return "";
    }
}
