package net.orivis.shared.sequences.controller;

import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.annotations.security.OrivisSecurityValidator;
import net.orivis.shared.controller.orivis.AllowAllCondition;
import org.springframework.web.bind.annotation.*;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.sequences.service.SequenceService;

@OrivisController(SequenceService.class)
@RestController
@RequestMapping("/api/v2/shared/sequence")
@CrossOrigin
public class SequenceController {
    private final SequenceService sequenceService;

    public SequenceController(SequenceService sequenceService) {
        this.sequenceService = sequenceService;
    }

    @GetMapping("/next/{type}")
    @WithRole(ignoreOnCondition = @OrivisSecurityValidator(AllowAllCondition.class))
    public synchronized @ResponseBody Long next(@PathVariable String type){
        return sequenceService.next(type, null);
    }

}
