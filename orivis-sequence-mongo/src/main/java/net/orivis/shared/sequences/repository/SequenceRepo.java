package net.orivis.shared.sequences.repository;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;
import net.orivis.shared.mongo.repository.PaginationRepository;
import net.orivis.shared.sequences.model.SequenceModel;

@Repository
public class SequenceRepo extends PaginationRepository<SequenceModel> {

    public SequenceRepo(MongoTemplate mongoTemplate) {
        super(mongoTemplate);
    }

}
