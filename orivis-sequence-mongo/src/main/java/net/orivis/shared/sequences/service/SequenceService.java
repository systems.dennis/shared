package net.orivis.shared.sequences.service;

import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.config.OrivisContext;
import org.springframework.data.domain.Sort;

import org.springframework.stereotype.Service;
import net.orivis.shared.mongo.service.PaginationService;
import net.orivis.shared.sequences.form.SequenceForm;
import net.orivis.shared.sequences.model.SequenceModel;
import net.orivis.shared.sequences.repository.SequenceRepo;

@Service
@OrivisService(repo = SequenceRepo.class, model =  SequenceModel.class, form = SequenceForm.class)
public class SequenceService extends PaginationService<SequenceModel> {
    public SequenceService(OrivisContext holder) {
        super(holder);
    }

    public synchronized Long next(String type, Long initValue) {
        var item = filteredFirst(getFilterImpl().eq("type", type), Sort.by(Sort.Direction.DESC,"value")).orElseGet(()-> createNewSequence(type, initValue));
        item.setValue(item.getValue() +1);
        save(item);
        return item.getValue();
    }

    private SequenceModel createNewSequence(String type, Long initValue) {

        if (initValue == null){
            initValue =getContext().getEnv("global.app.sequence_init_" + type, 0L);
        }


        SequenceModel sequence = new SequenceModel();
        sequence.setValue(initValue);
        sequence.setType(type);

        return sequence;
    }
}
