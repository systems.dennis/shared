package net.orivis.shared.sequences.form;

import lombok.Data;
import net.orivis.shared.mongo.form.OrivisPojo;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class SequenceForm implements OrivisPojo {
    private String id;
    private Long value;
    private String type;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String asValue() {
        return type + "->" + value;
    }
}
