package net.orivis.apptodb.repository;


import net.orivis.apptodb.model.AppSettingsModel;
import net.orivis.shared.mongo.repository.PaginationRepository;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class AppSettingsRepo extends PaginationRepository<AppSettingsModel> {

    public AppSettingsRepo(MongoTemplate mongoTemplate) {
        super(mongoTemplate);
    }
}
