package net.orivis.apptodb.model;

import jakarta.persistence.Transient;
import lombok.Data;
import net.orivis.shared.mongo.model.OrivisAssignableEntity;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class AppSettingsModel extends OrivisAssignableEntity {
    public static String DEFAULT_TYPE = "STRING";

    private String key;
    private String value;
    private String valueClassType;

    @Transient
    private Object realValue;

    @Override
    public String asValue() {
        return key + " " + value;
    }
}
