package net.orivis.apptodb.converters;


import net.orivis.shared.converters.ValueTypeIdentifier;
import org.springframework.stereotype.Component;

import static net.orivis.shared.converters.SettingsValueType.*;

@Component
public class AppSettingsValueTypeIdentifier implements ValueTypeIdentifier<String, String> {

    public static final String KEY_PASSWORD = "password";
    public static final String KEY_TOKEN = "token";

    public static final String REGEX_NUMBER = "[1-9][0-9]+";

    @Override
    public String identify(String key, String value) {
        value = value.trim();
        if (checkInteger(value)) {
            return INTEGER.name();
        } else if (checkBoolean(value)) {
            return BOOLEAN.name();
        } else if(checkPassword(key)) {
            return PASSWORD.name();
        } else {
            return STRING.name();
        }
    }


    private boolean checkBoolean(String value) {
        return value.equalsIgnoreCase("false") || value.equalsIgnoreCase("true");
    }

    private boolean checkInteger(String value) {
        if (value.equals("0")) {
            return true;
        }
        return value.matches(REGEX_NUMBER);
    }

    private boolean checkPassword(String key) {
        return key.contains(KEY_TOKEN) || key.contains(KEY_PASSWORD);
    }
}

