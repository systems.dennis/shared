package net.orivis.apptodb.controller;

import net.orivis.apptodb.form.AppSettingsForm;
import net.orivis.apptodb.model.AppSettingsModel;
import net.orivis.apptodb.service.AppSettingsService;
import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.exceptions.ItemDoesNotContainsIdValueException;
import net.orivis.shared.exceptions.ItemForAddContainsIdException;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.exceptions.ItemNotUserException;
import net.orivis.shared.exceptions.UnmodifiedItemSaveAttemptException;
import net.orivis.shared.mongo.controller.AddItemController;
import net.orivis.shared.mongo.controller.DeleteItemController;
import net.orivis.shared.mongo.controller.EditItemController;
import net.orivis.shared.mongo.controller.ListItemController;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api/v2/shared/app_settings")
@OrivisController(value = AppSettingsService.class)
@CrossOrigin
@Secured (roles = "ROLE_ADMIN")
public class AppSettingsController
        extends OrivisContextable

        implements
        DeleteItemController<AppSettingsModel>,
        ListItemController<AppSettingsModel, AppSettingsForm>,
        EditItemController<AppSettingsModel, AppSettingsForm>,
        AddItemController<AppSettingsModel, AppSettingsForm>
       {

    public AppSettingsController(OrivisContext context) {
        super(context);
    }

    @Override
    @GetMapping("/list")
    public ResponseEntity<Page<Map<String, Object>>> get( Integer limit, Integer page) {
        return ListItemController.super.get( limit, page);
    }

    @Override
    @PutMapping( "/edit/{id}")

    public ResponseEntity<AppSettingsForm> edit(AppSettingsForm form) throws ItemForAddContainsIdException, ItemDoesNotContainsIdValueException, UnmodifiedItemSaveAttemptException, ItemNotFoundException {
        return EditItemController.super.edit(form);
    }
    @Override
    @PutMapping("/add")

    public ResponseEntity<AppSettingsForm> add(AppSettingsForm form) throws ItemForAddContainsIdException, ItemDoesNotContainsIdValueException, UnmodifiedItemSaveAttemptException, ItemNotFoundException {
        return EditItemController.super.edit(form);
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable String id) throws ItemNotUserException, ItemNotFoundException {
        DeleteItemController.super.delete(id);
    }
}
