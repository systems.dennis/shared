v.0.2.x -> Now Bean copier uses ModelCache to operate with objects methods
@OrivisAutditable annotation present to modify objects before or after operations

v. 0.1.1 -> LocalWebContext support is ended, User OrivisContext instead, further rebranding to Orivis

v. 0.1.0 -> Rename @WebFormSupport to OrivisController and @OrivisService to @OrivisSerivce

v. 0.0.4 -> secured annotation has now flag prioritiesField

v. 0.0.2 -> parent dependency upgraded to avoid transitive dependency problem

----------> rewrite evaluate() method in SimpleEvaluator from recursion to while loop

v. 0.0.1 -> initial migration from sd-shared
Clean up

