package net.orivis.shared.controller.items;

import net.orivis.shared.config.OrivisContext;

public interface Contextable {
    OrivisContext getContext();
}
