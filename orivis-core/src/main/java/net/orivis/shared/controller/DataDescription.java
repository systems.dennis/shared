package net.orivis.shared.controller;

import net.orivis.shared.pojo_view.list.PojoListView;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor

public class DataDescription {
    private String title;
    private String component_id;
    private Boolean isSearchable;
    private Boolean showId;

    public static DataDescription of(String titleKey, String id, Class<?> pojo) {
        PojoListView view = pojo.getAnnotation(PojoListView.class);
        boolean isSearchable = view == null || view.enableSearching();
        boolean showId= view != null && view.showId();

        return new DataDescription(titleKey, id, isSearchable, showId );
    }
}
