package net.orivis.shared.controller.generated;


import net.orivis.shared.controller.items.OnEmptyResult;
import net.orivis.shared.controller.items.OrivisTransformable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.controller.forms.OrivisServiceable;
import net.orivis.shared.controller.orivis.MagicList;
import net.orivis.shared.form.AbstractOrivisForm;
import net.orivis.shared.model.OrivisIDPresenter;
import net.orivis.shared.repository.OrivisFilter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * List  object interface. Implementing this interface, will automatically create ../list/
 *
 * @param <DB_TYPE>       Entity which is managed by this interface and Service
 */
@Secured
public interface AbstractSelfItemsListController<DB_TYPE extends OrivisIDPresenter<ID_TYPE>, FORM extends AbstractOrivisForm<ID_TYPE>, ID_TYPE extends Serializable>

        extends OrivisServiceable<DB_TYPE, ID_TYPE>, MagicList<DB_TYPE, ID_TYPE>, OrivisTransformable< DB_TYPE, FORM> {

    @GetMapping("/list")
    @ResponseBody
    @WithRole
    default ResponseEntity<Page<FORM>> getData(@RequestParam (value = "limit", required = false) Integer limit,
                                               @RequestParam(value = "page", required = false) Integer page) {

       var specification =modifySearchSpecification(getContext().getFilterProvider().ofUser(getModel(), getContext().getCurrentUser()));
        var data = getService().find(specification, limit, page);
        List<FORM> result= new ArrayList<>();


        if (data.isEmpty()){

            result = (List<FORM>) onEmpty().doOnEmptyResults();
        }else {
            for (var item : data) {
                result.add(toForm(item));
            }
        }

        Page<FORM> request = new PageImpl<>(result, PageRequest.of(page, limit), result.size());
        return ResponseEntity.ok(request);
    }

    default OnEmptyResult onEmpty(){
        return OnEmptyResult.INSTANCE;
    }

    default OrivisFilter<DB_TYPE> modifySearchSpecification(OrivisFilter specification){
        return specification;
    }


}
