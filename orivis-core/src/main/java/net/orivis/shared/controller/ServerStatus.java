package net.orivis.shared.controller;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ServerStatus {
    private final String status;
    private final List<ServerErrorResponse> errors;

    public static ServerStatus ok() {
        return new ServerStatus("OK", new ArrayList<>());
    }

    public static ServerStatus failed(List<ServerErrorResponse> errors) {
        return new ServerStatus("FAILED", errors);
    }
}
