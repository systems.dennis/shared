package net.orivis.shared.controller;

import lombok.Data;
import net.orivis.shared.utils.PojoListField;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
public class DefaultFormDetailsContainer {
    private Map<String,  Object> data;

    private List<PojoListField> fields = new ArrayList<>();
}
