package net.orivis.shared.controller.generated;

import lombok.SneakyThrows;
import net.orivis.shared.controller.items.OrivisTransformable;
import net.orivis.shared.exceptions.ItemNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.controller.forms.OrivisServiceable;
import net.orivis.shared.form.AbstractOrivisForm;
import net.orivis.shared.model.OrivisIDPresenter;

import java.io.Serializable;

@Secured
public interface AbstractGetOrCreateByIdController<DB_TYPE extends OrivisIDPresenter<ID_TYPE>, FORM extends AbstractOrivisForm<ID_TYPE>, ID_TYPE extends Serializable> extends OrivisServiceable<DB_TYPE, ID_TYPE>, OrivisTransformable<DB_TYPE, FORM> {
    @SneakyThrows
    @GetMapping("/id/{id}")
    @ResponseBody
    default ResponseEntity<FORM> get(@PathVariable("id") String id, @RequestParam(required = false) String param) {

        DB_TYPE type = fetchById(id);
        if (type == null) {
            var res = getService().save(fromForm(createNew(id, param)));
            return ResponseEntity.ok(toForm(res));
        }
        return ResponseEntity.ok(toForm(type));
    }

    FORM createNew(String id, String ownerId);

    default DB_TYPE fetchById(Serializable id){
        return  getService().findById(id).orElse(null);
    };

}
