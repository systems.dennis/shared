package net.orivis.shared.controller;

import lombok.extern.slf4j.Slf4j;
import net.orivis.shared.annotations.OrivisService;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.annotations.security.OrivisSecurityValidator;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.beans.IdValidator;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.entity.AbstractOrivisEntity;
import net.orivis.shared.exceptions.SearchException;
import net.orivis.shared.form.AbstractOrivisForm;
import net.orivis.shared.service.AbstractService;
import net.orivis.shared.utils.OrivisContextable;
import net.orivis.shared.utils.bean_copier.BeanCopier;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/api/v1/search/type/")
@CrossOrigin
public class SearchEntityApi extends OrivisContextable {

    private static Map<String, SearcherInfo> searchBeans = new HashMap<>();

    public static void registerSearch(String type, SearcherInfo info) {
        searchBeans.putIfAbsent(type, info);
    }

    public SearchEntityApi(OrivisContext context) {
        super(context);
    }

    public static Class<? extends AbstractService> findServiceByType(String searchName) {
        return searchBeans.get(searchName).getSearchClass();
    }


    @GetMapping(value = "/toString/{type}/{id}", produces = "text/plain;charset=UTF-8")
    @WithRole(ignoreOnCondition = @OrivisSecurityValidator(AllowOnPublicSearch.class))
    @ResponseBody
    public ResponseEntity<String> findById(@PathVariable String type, @PathVariable Serializable id) {

        if (getBean(IdValidator.class).isIdSet(id)) {
            return ResponseEntity.ok("search_select");
        }

        var bean = getBean(searchBeans.get(type).getSearchClass());

        Class<? extends AbstractOrivisForm> form;
        OrivisService OrivisService = bean.getClass().getAnnotation(OrivisService.class);

        if (OrivisService != null) {
            form = OrivisService.form();
        } else {
            form = bean.getClass().getAnnotation(OrivisService.class).form();
        }

        return ResponseEntity.ok(getContext().getBean(BeanCopier.class).copy(getBean(searchBeans.get(type).getSearchClass()).findById(id).orElseThrow(), form).asValue());
    }

    @GetMapping(value = "{type}", produces = "application/json")
    @WithRole(ignoreOnCondition = @OrivisSecurityValidator(AllowOnPublicSearch.class))
    public PageImpl<Map<String, Object>> GET(@PathVariable("type") String type, @RequestParam(required = false, value = "sub_type") String subType,
                                             @RequestParam(value = "page", required = false) Integer page, @RequestParam(required = false) Integer limit,
                                             @RequestParam(value = "s", required = false) String what, @RequestParam(required = false, value = "ids") Serializable[] additionalIds) {


        var bean = getBean(searchBeans.get(type).getSearchClass());


        if (bean == null) {
            throw new SearchException(type);
        }
        bean.preSearch(what, type);

        if (page == null || page < 0) {
            page = 0;
        }

        var searchBean = searchBeans.get(type);

        var res = bean.search(searchBean.getField(), subType, what, page, limit == null ? 10 : limit, additionalIds);

        var pageList = new ArrayList<Map<String, Object>>();
        for (int i = 0; i < res.getContent().size(); i++) {
            AbstractOrivisEntity<?> model = (AbstractOrivisEntity<?>) res.getContent().get(i);
            AbstractOrivisForm form = (AbstractOrivisForm) getBean(BeanCopier.class).copy(model, bean.getForm());
            pageList.add(BeanCopier.values(form, model, getContext()));
        }
        return new PageImpl<>(pageList, res.getPageable(), res.getTotalElements());


    }


}
