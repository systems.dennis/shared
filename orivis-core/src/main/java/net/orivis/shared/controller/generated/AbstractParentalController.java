package net.orivis.shared.controller.generated;

import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.controller.forms.OrivisServiceable;
import net.orivis.shared.controller.items.OrivisTransformable;
import net.orivis.shared.controller.orivis.MagicList;
import net.orivis.shared.form.AbstractOrivisForm;
import net.orivis.shared.form.CountResponse;
import net.orivis.shared.model.OrivisIDPresenter;
import net.orivis.shared.service.AbstractParentalService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Secured
public interface AbstractParentalController<DB_TYPE extends OrivisIDPresenter<ID_TYPE>, FORM extends AbstractOrivisForm<ID_TYPE>, ID_TYPE extends Serializable> extends OrivisTransformable<DB_TYPE, FORM>, OrivisServiceable<DB_TYPE, ID_TYPE>, MagicList<DB_TYPE, ID_TYPE> {

    @GetMapping("/tree/count/parent/{id}")
    @ResponseBody
    @WithRole
    default ResponseEntity<CountResponse> countByParent(@PathVariable("id") ID_TYPE id) {
        AbstractParentalService<DB_TYPE, ID_TYPE> bean = getService();
        long count = bean.countByParent(id);

        CountResponse response = new CountResponse();
        response.setCount(count);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/tree/root")
    @ResponseBody
    @WithRole
    default ResponseEntity<Page<Map<String, Object>>> findRootElements(
            @RequestParam(value = "withCount", required = false, defaultValue = "false") Boolean withCount,
            @RequestParam(value = "limit", required = false) Integer limit,
            @RequestParam(value = "page", required = false) Integer page) {
        AbstractParentalService<DB_TYPE, ID_TYPE> bean = getService();
        Page<DB_TYPE> elements = bean.findRootElements(page, limit);

        return ResponseEntity.ok(addCountToData(toFormPage(elements), withCount));
    }

    @GetMapping("/tree/parent/{id}")
    @ResponseBody
    @WithRole
    default ResponseEntity<Page<Map<String, Object>>> findByParent(@PathVariable("id") ID_TYPE id,
                                                                   @RequestParam(value = "withCount", required = false, defaultValue = "false") Boolean withCount,
                                                                   @RequestParam(value = "limit", required = false) Integer limit,
                                                                   @RequestParam(value = "page", required = false) Integer page) {
        AbstractParentalService<DB_TYPE, ID_TYPE> bean = getService();
        Page<DB_TYPE> elements = bean.findByParent(id, limit, page);

        return ResponseEntity.ok(addCountToData(toFormPage(elements), withCount));
    }

    default Page<Map<String, Object>> addCountToData(Page<Map<String, Object>> items, Boolean withCount) {

        if (!withCount){
            return  items;
        }

        try {
            List<Map<String, Object>> data = items.getContent().stream()
                    .peek((item) -> item.put("ctn", this.countByParent((ID_TYPE) item.get("id")))).toList();

            return new PageImpl<>(data, items.getPageable(), items.getTotalElements());
        } catch (Exception e) {
            return items;
        }
    }

}
