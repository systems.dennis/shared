package net.orivis.shared.controller;

import net.orivis.shared.annotations.security.validator;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.service.AbstractService;

public class AllowOnPublicSearch implements validator {
    @Override
    public boolean checkIgnorePermission(Object[] values, Object target, OrivisContext context, String [] params) {
        return ((AbstractService<?, ?>)target).isPublicSearchEnabled();
    }
}
