package net.orivis.shared.controller.orivis;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.HttpStatus;
import net.orivis.shared.annotations.security.ISecurityUtils;
import net.orivis.shared.annotations.security.validator;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.exceptions.StatusException;

import java.util.List;

import static net.orivis.shared.utils.Mapper.mapper;

/**
 * Allows to config access threw settings.
 *
 * Parameters of ConfigDrivenPermissionCondition should have 2 parameters:
 * [0] name of the property to take from (string)
 * [1] default value if config not found as Json string: {"type":"scope", values: ["Default"]}
 * <b><i>! important</i> config should have a correct data for both of parameters</b>
 *
 *
 */
public class ConfigDrivenPermissionCondition  implements validator {
    public static final String TYPE_SCOPE = "scope";
    public static final String TYPE_USER = "user";
    public static final String TYPE_ROLE = "ROLE";
    @Override
    public boolean checkIgnorePermission(Object[] values, Object target, OrivisContext context,
                                         String[] parameters) {

        if (parameters.length <2){
            throw new StatusException("global.app.configuration.error.should_have_two_parameter_type_and_values", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        String config = context.getEnv(parameters[0], parameters[1]);
        DynamicPermissionItem item;
        try {
             item = mapper.readValue(config, DynamicPermissionItem.class);
        } catch (JsonProcessingException e) {
            throw new StatusException("global.app.configuration.error.incorrect_or_not_json_config", HttpStatus.INTERNAL_SERVER_ERROR);

        }

        if (values == null){
            throw new StatusException("global.app.configuration.error.incorrect_values_should_not_be_null", HttpStatus.INTERNAL_SERVER_ERROR);

        }

        var type =  item.getType();
        var checkParameters = List.of(item.getValues());

        if (TYPE_USER.equalsIgnoreCase(type)){
            return checkParameters.contains(String.valueOf(context.getCurrentUser()));
        }

        if (TYPE_ROLE.equalsIgnoreCase(type)){
            return context.getBean(ISecurityUtils.class).hasAnyRole(item.getValues());
        }

        if (TYPE_SCOPE.equalsIgnoreCase(type)){
            return   checkParameters.contains(context.getBean(ISecurityUtils.class).getScope());
        }

        throw new StatusException("global.app.configuration.error.unsupported_type_in_config", HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
