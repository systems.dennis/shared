package net.orivis.shared.controller.orivis;

import net.orivis.shared.annotations.security.validator;
import net.orivis.shared.config.OrivisContext;

public class AllowAllCondition implements validator {
    @Override
    public boolean checkIgnorePermission(Object[] values, Object target, OrivisContext context, String[] params) {
        return true;
    }
}
