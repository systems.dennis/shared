package net.orivis.shared.controller.generated;

import lombok.SneakyThrows;
import net.orivis.shared.controller.items.OrivisTransformable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.controller.forms.OrivisServiceable;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.form.AbstractOrivisForm;
import net.orivis.shared.model.OrivisIDPresenter;

import java.io.Serializable;

@Secured
public interface AbstractGetByIdController<DB_TYPE extends OrivisIDPresenter<ID_TYPE>, FORM extends AbstractOrivisForm<ID_TYPE>, ID_TYPE extends Serializable> extends OrivisServiceable<DB_TYPE, ID_TYPE>, OrivisTransformable<DB_TYPE, FORM> {
    @SneakyThrows
    @GetMapping("/id/{id}")
    @ResponseBody
    default ResponseEntity<FORM> get(@PathVariable ("id") ID_TYPE id) {
        DB_TYPE type = getService().findById(id).orElseThrow(() -> ItemNotFoundException.fromId(id));
        if (type == null){
            throw ItemNotFoundException.fromId(id);
        }
        return ResponseEntity.ok(toForm(type));
    }


}
