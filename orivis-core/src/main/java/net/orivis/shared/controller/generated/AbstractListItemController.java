package net.orivis.shared.controller.generated;


import net.orivis.shared.controller.items.OrivisTransformable;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.controller.forms.OrivisServiceable;
import net.orivis.shared.controller.orivis.MagicList;
import net.orivis.shared.form.AbstractOrivisForm;
import net.orivis.shared.model.OrivisIDPresenter;

import java.io.Serializable;
import java.util.Map;

/**
 * List  object interface. Implementing this interface, will automatically create ../list/
 *
 * @param <DB_TYPE> Entity which is managed by this interface and Service
 *
 * @since 3.1.2 version returns page, and uses Additional specification strictly
 */
@Secured
public interface AbstractListItemController<DB_TYPE extends OrivisIDPresenter<ID_TYPE>, FORM extends AbstractOrivisForm<ID_TYPE>, ID_TYPE extends Serializable> extends OrivisTransformable<DB_TYPE, FORM>, OrivisServiceable<DB_TYPE, ID_TYPE>, MagicList<DB_TYPE, ID_TYPE> {

    @GetMapping("/list")
    @ResponseBody
    @WithRole
    default ResponseEntity<Page<Map<String, Object>>> get(
                                                          @RequestParam(value = "limit", required = false) Integer limit,
                                                          @RequestParam(value = "page", required = false) Integer page) {

       var specification = getService().getAdditionalSpecification();

        return  ResponseEntity.ok(toFormPage(getService().find(specification, limit, page)));

    }
}
