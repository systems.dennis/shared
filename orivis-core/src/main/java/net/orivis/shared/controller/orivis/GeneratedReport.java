package net.orivis.shared.controller.orivis;

import lombok.Data;

import java.io.Serializable;

@Data
public class GeneratedReport implements Serializable {
    private String pathToDownload;
    private String type;
}
