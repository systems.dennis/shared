package net.orivis.shared.controller.orivis;

import jakarta.servlet.http.HttpServletRequest;
import lombok.SneakyThrows;
import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.controller.items.magic.UserSettingsFormInterface;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import net.orivis.shared.annotations.AbstractFormModifier;
import net.orivis.shared.annotations.FormTransient;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.controller.forms.OrivisServiceable;
import net.orivis.shared.controller.forms.ValidateForm;
import net.orivis.shared.controller.items.OrivisTransformable;
import net.orivis.shared.entity.ExtendedOrivisForm;
import net.orivis.shared.form.AbstractOrivisForm;
import net.orivis.shared.pojo_form.PojoForm;
import net.orivis.shared.pojo_form.PojoFormElement;
import net.orivis.shared.pojo_view.list.PojoListView;
import net.orivis.shared.form.GeneratedPojoForm;
import net.orivis.shared.utils.PojoFormField;
import net.orivis.shared.utils.bean_copier.BeanCopier;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.*;

public interface MagicForm<FORM extends AbstractOrivisForm> extends ValidateForm<FORM> {

    Map<String, GeneratedPojoForm> pojoFormCache = new HashMap<>();

    @GetMapping(value = "/root/fetch/form", produces = "application/json")
    @ResponseBody
    @WithRole
    default GeneratedPojoForm fetchForm(HttpServletRequest request, @RequestParam(required = false) Serializable id) {

        Class<? extends AbstractOrivisForm> formClass = AbstractOrivisForm.class;
        Class<? extends AbstractFormModifier> formModifier = AbstractFormModifier.class;

        OrivisController OrivisController = getClass().getAnnotation(OrivisController.class);

        if (OrivisController != null) {
            OrivisService OrivisService = OrivisController.value().getAnnotation(OrivisService.class);
            formClass = OrivisService.form();
            formModifier = OrivisService.formModifier();

        }

        var receivedForm = getPojoForm(formClass, id);
        GeneratedPojoForm changedForm = null;
        receivedForm.setAction(getClass().getAnnotation(RequestMapping.class).value()[0] + (id == null ? "/add" : "/edit"));
        receivedForm.setMethod(id == null ? HttpMethod.POST.name() : HttpMethod.PUT.name());

        if (getContext().getEnv("app.global.allow_form_change", true)) {
            var bean = getContext().getBean(UserSettingsFormInterface.class);
            bean.configForm(getContext(), receivedForm, formClass);
        }

        if (id != null) {
            setPojoFormValue(id, changedForm == null ?  receivedForm : changedForm, getContext());
        }
        var res =  changedForm == null ? receivedForm : changedForm ;

        if (formModifier != AbstractFormModifier.class) {
            AbstractFormModifier modifier = null;
            try {
                modifier = formModifier.newInstance();
                modifier.modify(getContext(),request, res);
            } catch (Exception e) {
                //ignored
            }
        }

        return  res;
    }



    default GeneratedPojoForm getPojoForm(Class<? extends AbstractOrivisForm> formClass, Serializable id) {

        if (pojoFormCache.containsKey(formClass.getName())) {
            var res = pojoFormCache.get(formClass.getName());
            res = getContext().getBean(BeanCopier.class).clone(res);
            res.setFieldList(new ArrayList<>(res.getFieldList()));
            return  res;
        } else {
            GeneratedPojoForm generatedPojoForm = new GeneratedPojoForm();

            PojoListView ann = formClass.getAnnotation(PojoListView.class);
            if (Objects.equals(ann.favoriteType(), "")) {
                generatedPojoForm.setObjectType(formClass.getSimpleName());
            } else {
                generatedPojoForm.setObjectType(ann.favoriteType());
            }

            if (formClass.getAnnotation(PojoForm.class) != null) {
                copyTo(formClass.getAnnotation(PojoForm.class), generatedPojoForm);
            } else {
                generatedPojoForm.setTitle(getClass().getSimpleName().toLowerCase());
                generatedPojoForm.setCommitText("global.submit");
                generatedPojoForm.setShowTitle(true);
            }


            BeanCopier.withEach(formClass, field -> getDescription(field, generatedPojoForm));

            pojoFormCache.put(formClass.getName(), generatedPojoForm);

            generatedPojoForm.getFieldList().sort(Comparator.comparing(PojoFormField::getOrder));

            return getContext().getBean(BeanCopier.class).clone(generatedPojoForm);
        }
    }

    @SneakyThrows
    default void setPojoFormValue(Serializable id, GeneratedPojoForm pojoForm, OrivisContext context) {
        var service = ((OrivisServiceable) this).getService();

        Class<? extends AbstractFormModifier> formModifier = AbstractFormModifier.class;

        OrivisController OrivisController = getClass().getAnnotation(OrivisController.class);

        if (OrivisController != null) {
            OrivisService OrivisService = OrivisController.value().getAnnotation(OrivisService.class);
            formModifier = OrivisService.formModifier();

        }

        var model = service.findByIdOrThrow(id);
        var form = ((OrivisTransformable) this).toForm(model);
        pojoForm.setValue(BeanCopier.values(form, model, context));

        if (form instanceof ExtendedOrivisForm){
            if (formModifier != AbstractFormModifier.class) {
                var valueFetcherClass = formModifier.newInstance();
                pojoForm.getFieldList().forEach(x -> {
                    if (x.getCustomized() != null && x.getCustomized()) {
                        pojoForm.getValue().put(x.getField(),valueFetcherClass.getValue(pojoForm, x.getField(), model)  );
                    }
                });
            }
        }

    }

    default void copyTo(PojoForm annotation, GeneratedPojoForm generatedPojoForm) {
        generatedPojoForm.setTitle(annotation.title().title());
        generatedPojoForm.setShowTitle(annotation.title().show());
        generatedPojoForm.setCommitText(annotation.commitButtonText());
    }

    @SneakyThrows
    default void getDescription(Field x, GeneratedPojoForm generatedForm) {

        if (x.getAnnotation(FormTransient.class) == null) {
            if (x.getAnnotation(PojoFormElement.class) != null && x.getAnnotation(PojoFormElement.class).available()) {
                generatedForm.getFieldList().add(PojoFormField.from(x, getContext()));
            }
        }
    }
}