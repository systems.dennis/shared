package net.orivis.shared.controller.orivis;

import lombok.Data;
import net.orivis.shared.repository.OrivisFilter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Data
/**
 * This is a basic request to the fetch data api, that will be transformed to the DB query before the result set is returned
 */
public class OrivisRequest implements Serializable   {
    private Integer page = 0;
    private Integer limit = 20;

    private List<OrivisFilter> cases = new ArrayList<>();
    private List<OrivisOrder> sort = Collections.singletonList(OrivisOrder.ID_DESC);

    private List<OrivisQuery> query = Collections.emptyList();
}
