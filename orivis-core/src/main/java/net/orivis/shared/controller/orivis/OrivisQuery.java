package net.orivis.shared.controller.orivis;

import lombok.Data;
import lombok.SneakyThrows;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.controller.SearchEntityApi;
import net.orivis.shared.entity.OrivisID;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.exceptions.OperationNotAllowedException;
import net.orivis.shared.model.OrivisIDPresenter;
import net.orivis.shared.repository.OrivisFilter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.*;

import static net.orivis.shared.pojo_view.DEFAULT_TYPES.*;

@Data
public class OrivisQuery {

    public static final SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
    private String field;
    private String type;
    private Object value;

    private String searchName;

    private String dataType;

    private String logicalOperator = "AND";

    @SneakyThrows
    public OrivisFilter toQuery(OrivisContext context) {

        OrivisFilter provider = null;
        if (dataType.equals("checkbox")) {
            if (value.equals("null")) {
                return context.getFilterProvider().isNull(field);
            }
            value = Boolean.valueOf(value.toString());

        }
        if (dataType.equalsIgnoreCase(DOUBLE)){
            //
            value =new BigDecimal(value + "");
        }

        if (dataType.startsWith("date")) {
            if (value.equals("null")) {
                return context.getFilterProvider().isNull(field);
            }

            if (Date.class.equals(value.getClass())) {
                this.value = value;
            } else if (String.class.equals(value.getClass())){
                this.value = parseStringToDate((String) value);
            } else if (Long.class.equals(value.getClass())) {
                this.value = new Date((Long) value);
            }
        }


        if (dataType.equals("object_chooser")) {


            if (value != null && !value.toString().trim().equals("")) {
                if (!OrivisID.class.isAssignableFrom(value.getClass())) {
                    try {
                        value = context.getBean(SearchEntityApi.findServiceByType(searchName)).findById((Serializable) value).orElseThrow(() -> ItemNotFoundException.fromId(String.valueOf(value)));
                    } catch (Exception e) {
                        System.out.println(value);
                    }
                }
            }
        }

        if (Objects.equals(this.type, "=")) {
            if (value == null || value.toString().isBlank()) {
                //todo ?? why?
                return context.getFilterProvider().isNull(field);
            }
            return context.getFilterProvider().eq(field, value);
        }

        if (Objects.equals(this.type, "!=")) {
            if (value == null || value.toString().isBlank()) {
                return context.getFilterProvider().notNull(field);
            }
            return context.getFilterProvider().notEq(field, value);
        }

        if (Objects.equals(this.type, ">=")) {
            return context.getFilterProvider().greatEQ(field, value);
        }

        if (Objects.equals(this.type, ">")) {
            return context.getFilterProvider().greater(field, value);
        }

        if (Objects.equals(this.type, "<=")) {
            return context.getFilterProvider().lessEQ(field, value);
        }

        if (Objects.equals(this.type, "<")) {
            return context.getFilterProvider().less(field, value);
        }

        if (Objects.equals(this.type, "%...%")) {
            return context.getFilterProvider().setInsensitive(true).contains(field, String.valueOf(value));
        }
        if (Objects.equals(this.type, "!%...%")) {
            return context.getFilterProvider().setInsensitive(true).notContains(field, String.valueOf(value));
        }

        if (Objects.equals(this.type, "%...")) {
            return context.getFilterProvider().setInsensitive(true).startsWith(field, String.valueOf(value));
        }
        if (Objects.equals(this.type, "!%...")) {
            return context.getFilterProvider().setInsensitive(true).notEndsWith(field, String.valueOf(value));
        }

        if (Objects.equals(this.type, "...%")) {
            return context.getFilterProvider().setInsensitive(true).endsWith(field, String.valueOf(value));
        }
        if (Objects.equals(this.type, "!...%")) {
            return context.getFilterProvider().setInsensitive(true).notEndsWith(field, String.valueOf(value));
        }

        if (Objects.equals(this.type, "is_null")) {
            return context.getFilterProvider().isNull(field);
        }

        if (Objects.equals(this.type, "not_null")) {
            return context.getFilterProvider().notNull(field);
        }

        if (Objects.equals(this.type, "in") && Objects.equals(this.dataType, OBJECT_SEARCH)) {
            List<OrivisIDPresenter<?>> sourceList = new ArrayList<>();
            if (value != null) {
                try {
                    OrivisIDPresenter<?> model;
                    var service = context.getBean(SearchEntityApi.findServiceByType(searchName));

                    for (Serializable integer : (List<Serializable>) value) {
                        try {
                            Serializable id = integer;
                            model = (OrivisIDPresenter<?>) service.findById(context.getFilterProvider().getIdValue(id)).orElseThrow(() -> ItemNotFoundException.fromId(id));
                            sourceList.add(model);
                        } catch (Exception e) {
                            System.out.println(value);
                        }
                    }
                } catch (Exception e) {
                    return context.getFilterProvider().iN(field, (List<Serializable>) value);
                }
            }
            return context.getFilterProvider().iN(field, sourceList);
        }
        if (Objects.equals(this.type, "not_in") && Objects.equals(this.dataType, OBJECT_SEARCH) ) {

            List<OrivisIDPresenter<?>> sourceList = new ArrayList<>();
            if (value != null) {
                try {
                    OrivisIDPresenter<?> model;
                    var service = context.getBean(SearchEntityApi.findServiceByType(searchName));

                    for (Serializable idValue : (List<Serializable>) value) {
                        try {

                            model = (OrivisIDPresenter<?>) service.findById(context.getFilterProvider().getIdValue(idValue)).orElseThrow(() -> ItemNotFoundException.fromId(idValue));
                            sourceList.add(model);
                        } catch (Exception e) {
                            System.out.println(value);
                        }
                    }
                } catch (Exception e) {
                    return context.getFilterProvider().notIN(field, (List<Serializable>) value);
                }
            }
            return context.getFilterProvider().notIN(field, sourceList);
        }

        if (Objects.equals(this.type, "not_in") && (this.dataType.equals(REFERENCED_ID) || this.dataType.equals(REFERENCED_IDS) || this.dataType.equals(TEXT))){
            return context.getFilterProvider().notIN(field, (List<?>) value);
        }
        if (Objects.equals(this.type, "in") && (this.dataType.equals(REFERENCED_ID) || this.dataType.equals(REFERENCED_IDS) || this.dataType.equals(TEXT))){
            return context.getFilterProvider().iN(field, (List<?>) value);
        }

        throw new OperationNotAllowedException("Do not know operator send to query");
    }

    public static Date trim(Date date) {
        if (date == null) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);

        return calendar.getTime();
    }

    private Date parseStringToDate(String value) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        return dateFormat.parse(value, new ParsePosition(0));
    }
}
