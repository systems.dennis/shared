package net.orivis.shared.controller.orivis;

import lombok.Data;

@Data
public class DynamicPermissionItem {
    private String type;
    private String[] values;
}
