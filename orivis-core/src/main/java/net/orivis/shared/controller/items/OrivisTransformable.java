package net.orivis.shared.controller.items;

import lombok.SneakyThrows;
import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.audit.AuditProcessor;
import net.orivis.shared.exceptions.DeveloperException;
import net.orivis.shared.model.OrivisIDPresenter;
import net.orivis.shared.service.AbstractPaginationService;
import net.orivis.shared.utils.bean_copier.ModelCache;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.controller.forms.OrivisServiceable;
import net.orivis.shared.entity.AbstractOrivisEntity;
import net.orivis.shared.entity.OrivisID;
import net.orivis.shared.entity.KeyValue;
import net.orivis.shared.form.AbstractOrivisForm;
import net.orivis.shared.utils.bean_copier.BeanCopier;
import net.orivis.shared.utils.bean_copier.OrivisTranformer;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface OrivisTransformable<DB_TYPE extends OrivisIDPresenter, FORM extends AbstractOrivisForm> {

    @SneakyThrows
    default DB_TYPE fromForm(FORM form) {
        OrivisController OrivisController = getClass().getAnnotation(OrivisController.class);

        if (OrivisController != null) {
            var cl = OrivisServiceable.findDeclaredClass(getClass(), OrivisController.value().getAnnotation(OrivisService.class)).model();
            return afterTransformToDBType((DB_TYPE) getContext().getBean(BeanCopier.class).copy(form, cl), form);
        }

        OrivisController orivisController = getClass().getAnnotation(OrivisController.class);
        var cl = OrivisServiceable.findDeclaredClass(getClass(), orivisController.value().getAnnotation(OrivisService.class)).model();
        return afterTransformToDBType((DB_TYPE) getContext().getBean(BeanCopier.class).copy(form, cl), form);

    }

    @SneakyThrows
    default FORM toForm(DB_TYPE model) {
        OrivisController OrivisController = getClass().getAnnotation(OrivisController.class);

        if (OrivisController != null) {
            var cl = OrivisServiceable.findDeclaredClass(getClass(), OrivisController.value().getAnnotation(OrivisService.class)).form();
            model = (DB_TYPE) getContext().getBean(AuditProcessor.class).processAudit(getContext().clone(model), null, AbstractPaginationService.ORIVIS_OPERATION_PRE_TRANSFORM );
            var res = getContext().getBean(BeanCopier.class).copy(model, cl);
            return afterTransformToForm((FORM) res, model);
        }

        OrivisService orivisService = getClass().getAnnotation(OrivisService.class);
        var cl = OrivisServiceable.findDeclaredClass(getClass(), orivisService).form();
        model = (DB_TYPE) getContext().getBean(AuditProcessor.class).processAudit(getContext().clone(model), null, AbstractPaginationService.ORIVIS_OPERATION_PRE_TRANSFORM );
        var res = getContext().getBean(BeanCopier.class).copy(model, cl);
        return afterTransformToForm((FORM) res, model);
    }

    @SneakyThrows
    default KeyValue transformValueToTargetType(KeyValue keyValue, Class<?> targetClass,
                                                Class<? extends AbstractOrivisForm> sourceClass) {

        Field editField = ModelCache.get(sourceClass).findAny(keyValue.getKey()).getField();
        OrivisTranformer transformer = editField.getAnnotation(OrivisTranformer.class);

        Object transformableValue = keyValue.getValue();

        if (transformer != null) {
            var converter = transformer.transformWith().newInstance();
            if (OrivisID.class.isAssignableFrom(transformableValue.getClass())) {
                transformableValue = converter.transform(transformableValue, transformer, ((OrivisID) transformableValue).getId().getClass(), getContext());
            } else {
                transformableValue = converter.transform(transformableValue, transformer, targetClass, getContext());
            }
        }

        return new KeyValue(keyValue.getKey(), transformableValue);
    }

    @SneakyThrows
    default KeyValue transformValueToModelType(KeyValue keyValue) {

        var description = getClassDescription();
        Class<?> targetClass;
        Class<? extends AbstractOrivisForm> sourceClass;
        targetClass = description.model();
        sourceClass = description.form();
        return transformValueToTargetType(keyValue, targetClass, sourceClass);
    }

    @SneakyThrows
    default KeyValue transformValueToFormType(KeyValue keyValue) {

        var description = getClassDescription();
        Class<?> sourceClass;
        Class<? extends AbstractOrivisForm> targetClass;

        targetClass = description.form();
        sourceClass = description.model();
        return transformValueToTargetType(keyValue, sourceClass, targetClass);
    }

    default OrivisService getClassDescription() {
        OrivisController OrivisController = getClass().getAnnotation(OrivisController.class);
        ;

        if (OrivisController != null) {
            return OrivisController.value().getAnnotation(OrivisService.class);
        }
        throw new DeveloperException("No @OrivisController annotation for the class ", this.getClass().getName());
    }

    default FORM afterTransformToForm(FORM ob, DB_TYPE origin) {
        return ob;
    }

    default DB_TYPE afterTransformToDBType(DB_TYPE ob, FORM form) {
        return ob;
    }

    default Page<Map<String, Object>> toFormPage(Page<DB_TYPE> page) {
        BeanCopier beanCopier = getContext().getBean(BeanCopier.class);

        OrivisController orivisController = getClass().getAnnotation(OrivisController.class);

        Class<? extends AbstractOrivisForm> cl;

        cl = OrivisServiceable.findDeclaredClass(getClass(), orivisController.value().getAnnotation(OrivisService.class)).form();

        List<Map<String, Object>> data = new ArrayList<>();
        for (int i = 0; i < page.getContent().size(); i++) {
            OrivisIDPresenter<?> model = page.getContent().get(i);
            model = getContext().getBean(AuditProcessor.class).processAudit(getContext().clone(model), null, AbstractPaginationService.ORIVIS_OPERATION_PRE_TRANSFORM );
            var form = beanCopier.copy(model, cl);
            data.add(BeanCopier.values(form, model, getContext()));
        }
        return new PageImpl<>(data, page.getPageable(), page.getTotalElements());
    }

    OrivisContext getContext();
}
