package net.orivis.shared.controller.forms;

import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.entity.AbstractOrivisEntity;
import net.orivis.shared.exceptions.StandardException;
import net.orivis.shared.form.AbstractOrivisForm;
import net.orivis.shared.model.OrivisIDPresenter;
import net.orivis.shared.service.AbstractService;

import java.io.Serializable;

@SuppressWarnings("ALL")
public interface OrivisServiceable<DB_TYPE extends OrivisIDPresenter<ID_TYPE>, ID_TYPE extends Serializable> {

    default <T extends AbstractService<DB_TYPE, ID_TYPE>> T getService() {
        return (T) getContext().getBean(getServiceClass());
    }

    default <E extends AbstractOrivisEntity> Class<? extends AbstractService> getServiceClass() {
        OrivisController OrivisController = getClass().getAnnotation(OrivisController.class);
        return getClass().getAnnotation(OrivisController.class).value();
    }

    OrivisContext getContext();


    //todo why we need model which is what we are searching for?
    static OrivisService findDeclaredClass(Class c, OrivisService serviceClass) {

        if (serviceClass == null) {
            OrivisController ser = (OrivisController) c.getAnnotation(OrivisController.class);
            if (ser == null) {
                throw new StandardException("global.app.errors.no_controller_annotation", c.getSimpleName());
            }
            var serviceClassHolder = ser.value();

            serviceClass = serviceClassHolder.getAnnotation(OrivisService.class);


            if (serviceClass == null) {
                throw new UnsupportedOperationException("@OrivisService annotation is not present on " + c + " and not present at service : " + ser.value());
            }
        }

        return serviceClass;
    }

    default <T extends AbstractOrivisForm> Class<T> getForm() {
        OrivisService OrivisService = getClass().getAnnotation(OrivisService.class);
        return (Class<T>) findDeclaredClass(getClass(), getClass().getAnnotation(OrivisService.class)).form();


    }

    default <T extends AbstractOrivisForm> Class<T> getModel() {
        OrivisService OrivisService = getClass().getAnnotation(OrivisService.class);
        var res = findDeclaredClass(getClass(), getClass().getAnnotation(OrivisService.class));
        return (Class<T>) res.model();
    }
}
