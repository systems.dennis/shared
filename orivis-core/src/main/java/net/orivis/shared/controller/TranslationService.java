package net.orivis.shared.controller;

import org.springframework.web.bind.annotation.*;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.beans.LocaleBean;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.entity.KeyValue;
import net.orivis.shared.utils.OrivisContextable;

import java.util.List;

@RestController
@CrossOrigin()
@RequestMapping("/api/v1/translation")
public class TranslationService extends OrivisContextable {

    private LocaleBean localeBean;

    public TranslationService(OrivisContext context, LocaleBean localeBean) {
        super(context);
        this.localeBean = localeBean;
    }

    @GetMapping("/{text}/{lang}")
    public String translate(@PathVariable("text") String text, @PathVariable("lang") String lang) {
        return getContext().getMessageTranslation(text, lang);
    }

    @GetMapping("/cache/clean")
    @WithRole("ROLE_ADMIN")
    public void cleanCache() {
        localeBean.clearCache();
    }

    @GetMapping(value = "/list/{lang}", produces = "application/json")
    public List<KeyValue> translations(@PathVariable("lang") String lang) {
        return localeBean.getMessageTranslations(lang, getContext());
    }

}
