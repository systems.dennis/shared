package net.orivis.shared.controller.forms;

import lombok.SneakyThrows;
import net.orivis.shared.annotations.Validation;
import net.orivis.shared.annotations.ValidationContent;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.entity.KeyValue;
import net.orivis.shared.exceptions.ValidationFailedException;
import net.orivis.shared.form.AbstractOrivisForm;
import net.orivis.shared.pojo_form.ValidationContext;
import net.orivis.shared.pojo_form.ValidationResult;
import net.orivis.shared.service.AbstractService;
import net.orivis.shared.utils.bean_copier.BeanCopier;
import net.orivis.shared.validation.ValueValidator;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.*;


public interface ValidateForm<FORM extends AbstractOrivisForm> {

    @SneakyThrows
    default void validate(KeyValue keyValue) {
        var formClass = getClassOfForm();
        var field = BeanCopier.findField(keyValue.getKey(), formClass);

        Map<String, List<ValidationResult>> validationResult = new HashMap<>();
        validateField(keyValue, field, true, validationResult, keyValue.getValue(),  ((OrivisServiceable) this).getServiceClass() );

        checkValidation(validationResult);
    }

    @SneakyThrows
    default void validate(FORM form, boolean edit) {
        validate(form, edit, ((OrivisServiceable) this).getServiceClass() );
    }
    @SneakyThrows
    default void validate(FORM form, boolean edit, Class<? extends AbstractService> serviceable) {
        var validatedFields = BeanCopier.findAnnotatedFields(form.getClass(), Validation.class);

        Map<String, List<ValidationResult>> validationResult = new HashMap<>();
        for (Field field : validatedFields) {
            validateField(form, field, edit, validationResult, BeanCopier.readValue(form, field), serviceable);
        }

        checkValidation(validationResult);
    }

    default void validateField(Object objectOfForm, Field field, boolean edit,
                               Map<String, List<ValidationResult>> validation, Object valueOfField, Class<? extends AbstractService> serviceClass)
            throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {


        var fieldValidation = field.getAnnotation(Validation.class);

        if (fieldValidation != null) {
            List<ValidationResult> validationResults = new ArrayList<>();

            for (Class<? extends ValueValidator> cl : fieldValidation.value()) {
                var validationClass = cl.getConstructor().newInstance();
                field.setAccessible(true);

                ValidationContent validationContent = new ValidationContent();
                validationContent.setServiceClass(serviceClass);
                validationContent.setField(field.getName());
                validationContent.setValidation(fieldValidation);
                validationContent.setEdit(edit);
                validationContent.setContext(getContext());

                var validationResult = validationClass
                        .validate(objectOfForm, valueOfField, validationContent);

                validationResults.add(validationResult);
                field.setAccessible(false);
            }
            validation.put(field.getName(), validationResults);
        }
    }

    default void checkValidation(Map<String, List<ValidationResult>> validationResult) {
        ValidationContext context = new ValidationContext();
        context.setData(validationResult);

        if (!context.getValidationMessages().isEmpty()) {
            throw new ValidationFailedException(context);
        }
    }

    default Class<? extends AbstractOrivisForm> getClassOfForm() {
        return ((OrivisServiceable) this).getForm();
    }

    OrivisContext getContext();
}
