package net.orivis.shared.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.orivis.shared.service.AbstractService;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearcherInfo {
    private String field;
    private Class <? extends AbstractService> searchClass;
}
