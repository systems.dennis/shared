package net.orivis.shared.controller;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ServerErrorResponse {
    public static String MAIL_AUTHENTICATION_ERROR = "Mail Authentication Error";

    private int statusCode;
    private String errorType;
    private String errorMessage;
}
