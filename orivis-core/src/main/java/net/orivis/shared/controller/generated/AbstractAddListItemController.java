package net.orivis.shared.controller.generated;

import net.orivis.shared.controller.items.OrivisTransformable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.controller.forms.OrivisServiceable;
import net.orivis.shared.controller.orivis.MagicForm;
import net.orivis.shared.entity.KeyValue;
import net.orivis.shared.exceptions.ListValidationFailedException;
import net.orivis.shared.exceptions.ValidationFailedException;
import net.orivis.shared.form.AbstractOrivisForm;
import net.orivis.shared.model.OrivisIDPresenter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Secured
public interface AbstractAddListItemController<DB_TYPE extends OrivisIDPresenter<ID_TYPE>, FORM extends AbstractOrivisForm<ID_TYPE>, ID_TYPE extends Serializable>
        extends OrivisServiceable<DB_TYPE, ID_TYPE>, OrivisTransformable<DB_TYPE, FORM>, MagicForm<FORM> {

    /**
     * Performs addAll procedure.
     *
     * @param forms list of form objects to add
     * @return list of saved objects
     * @throws ListValidationFailedException if any form in the list fails validation
     */
    @PostMapping(value = "/add/list", consumes = {"application/json", "application/json;charset=UTF-8"}, produces = {"application/json", "application/json;charset=UTF-8"})
    @ResponseBody
    @WithRole
    default ResponseEntity<List<FORM>> addList(@RequestBody List<FORM> forms) throws ListValidationFailedException {
        validateListSize(forms);

        Map<Integer, List<KeyValue>> validationErrors = new HashMap<>();
        List<DB_TYPE> modelsToSave = new ArrayList<>();

        for (int i = 0; i < forms.size(); i++) {
            FORM form = forms.get(i);

            try {
                validate(form, form.getId() != null);
                DB_TYPE model = fromForm(form);
                modelsToSave.add(model);
            } catch (ValidationFailedException e) {
                validationErrors.put(i, e.getErrorMessages());
            }
        }

        if (!validationErrors.isEmpty()) {
            throw new ListValidationFailedException(validationErrors);
        }

        List<DB_TYPE> savedModels = getService().addAll(modelsToSave);

        List<FORM> savedForms = new ArrayList<>();
        for (DB_TYPE model : savedModels) {
            savedForms.add(toForm(model));
        }

        return ResponseEntity.ok(savedForms);
    }

    private void validateListSize(List<FORM> forms) {
        Integer maxListSize = getContext().getEnv("global.app.max.add.list.size", 100);
        if(forms.size() > maxListSize) {
            throw new ListValidationFailedException("global.list_too_large_for_add");
        }
    }
}
