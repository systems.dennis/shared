package net.orivis.shared.controller;

import org.springframework.stereotype.Component;
import org.springframework.web.context.request.WebRequest;

import jakarta.servlet.http.HttpServletResponse;

@Component
public interface ExternalControllerAdvice {
    Object onException(Class<?> exception, Exception e, HttpServletResponse response, WebRequest request, OrivisExceptionController parent);
}
