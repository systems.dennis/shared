package net.orivis.shared.controller.generated;

import lombok.SneakyThrows;
import net.orivis.shared.controller.items.OrivisTransformable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.controller.forms.OrivisServiceable;
import net.orivis.shared.form.AbstractOrivisForm;
import net.orivis.shared.model.OrivisIDPresenter;

import java.io.Serializable;


public interface AbstractGetOrCreateItemController<DB_TYPE extends OrivisIDPresenter<ID_TYPE>, FORM extends AbstractOrivisForm<ID_TYPE>, ID_TYPE extends Serializable>  extends OrivisServiceable<DB_TYPE, ID_TYPE>, OrivisTransformable<DB_TYPE, FORM> {
    @SneakyThrows
    @GetMapping("/get_or_create/{id}")
    default FORM getItemByIdOrCreate(@PathVariable("id") ID_TYPE id) {
        return toForm( getService().filteredFirst(getService().getFilterImpl().eq(getCompareField(), id)).orElseGet(()-> createNew(id)));

    }

    @Override
    OrivisContext getContext() ;

    String getCompareField();

    default DB_TYPE createNew(ID_TYPE id){
        DB_TYPE val = createElement(id);
        return getService().save(val);

    }

    DB_TYPE createElement(ID_TYPE id);
}
