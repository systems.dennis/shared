package net.orivis.shared.controller.generated;

import net.orivis.shared.controller.items.OrivisTransformable;
import net.orivis.shared.controller.items.UpdateFieldResponse;
import net.orivis.shared.exceptions.EditFieldException;
import net.orivis.shared.exceptions.FieldNotFoundException;
import net.orivis.shared.exceptions.ItemDoesNotContainsIdValueException;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.exceptions.UnmodifiedItemSaveAttemptException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.controller.forms.OrivisServiceable;
import net.orivis.shared.controller.orivis.MagicForm;
import net.orivis.shared.entity.KeyValue;
import net.orivis.shared.form.AbstractOrivisForm;
import net.orivis.shared.model.OrivisIDPresenter;
import net.orivis.shared.utils.bean_copier.BeanCopier;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;

/**
 *
 * Update object interface. Implementing this interface, will automatically create ../edit_field/field/{field}
 *
 * @param <DB_TYPE> Entity which is managed by this interface and Service
 *
 * @param <FORM> form to be present
 * @param <ID_TYPE> a {@link Serializable} id type
 */


@Secured
public interface AbstractEditFieldItemController<DB_TYPE extends OrivisIDPresenter<ID_TYPE>, FORM extends AbstractOrivisForm<ID_TYPE>, ID_TYPE extends Serializable> extends OrivisServiceable<DB_TYPE, ID_TYPE>, OrivisTransformable<DB_TYPE, FORM>, MagicForm<FORM> {

    @WithRole
    @PutMapping(value = "/edit_field/id/{id}")
    @ResponseBody
    default ResponseEntity<UpdateFieldResponse<ID_TYPE>> editField(@PathVariable ID_TYPE id, @RequestBody KeyValue keyValue)
            throws ItemDoesNotContainsIdValueException, UnmodifiedItemSaveAttemptException, ItemNotFoundException, IllegalAccessException, InvocationTargetException {
        if (keyValue.getKey() == null) {
            throw new EditFieldException(keyValue);
        }

        var field = BeanCopier.findField(keyValue.getKey(), getForm());
        if (field == null) {
            throw new FieldNotFoundException(keyValue.getKey());
        }

        var type = field.getType();
        Object value = keyValue.getValue();

        if (value == null){
            KeyValue keyValueFromModel = getService().editField(id, keyValue);
            return ResponseEntity.ok(new UpdateFieldResponse<>(id, keyValueFromModel));
        }
        if (type == Long.class) {
            value = ((Integer) value).longValue();
        }

        validate(keyValue);

        var transformedToModelTypeKeyValue = transformValueToModelType(new KeyValue(keyValue.getKey(), value));
        KeyValue keyValueFromModel = getService().editField(id, transformedToModelTypeKeyValue);

        var transformedToFormTypeKeyValue = transformValueToFormType(keyValueFromModel);

        var response = new UpdateFieldResponse<>(id, transformedToFormTypeKeyValue);
        return ResponseEntity.ok(response);
    }

}
