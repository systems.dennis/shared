package net.orivis.shared.controller.orivis;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrivisOrder {

    public static final OrivisOrder ID_DESC = new OrivisOrder("id", true);


    private String field;
    private Boolean desc;
}
