package net.orivis.shared.controller.generated;

import net.orivis.shared.annotations.security.Id;
import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.controller.forms.OrivisServiceable;
import net.orivis.shared.exceptions.DeleteNotPossibleException;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.exceptions.ItemNotUserException;
import org.springframework.web.bind.annotation.*;
import net.orivis.shared.exceptions.ItemWasDeletedException;
import net.orivis.shared.model.OrivisIDPresenter;
import net.orivis.shared.utils.security.DefaultIdChecker;

import java.io.Serializable;
import java.util.List;

/**
 * Delete object interface. Implementing this interface, will automatically create ../delete/{id}
 */
@Secured
public interface AbstractDeleteItemController< DB_TYPE extends OrivisIDPresenter< ID_TYPE>, ID_TYPE extends Serializable> extends OrivisServiceable<DB_TYPE, ID_TYPE> {


    @WithRole
    @RequestMapping(value="/delete/{id}", method=RequestMethod.DELETE)
    default void delete(@Id(checker = DefaultIdChecker.class) @PathVariable ID_TYPE id) throws ItemNotUserException, ItemNotFoundException {
        try {
            getService().delete(id);
        } catch (ItemWasDeletedException | ItemNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new DeleteNotPossibleException();
        }
    }


    @RequestMapping(value="/delete/deleteItems", method=RequestMethod.DELETE)
    default void deleteItems(@RequestParam List<ID_TYPE> ids) throws ItemNotUserException, ItemNotFoundException {
        for (ID_TYPE id : ids) {
            delete(id);
        }
    }

}
