package net.orivis.shared.controller.generated;

import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.controller.items.OrivisTransformable;
import net.orivis.shared.exceptions.ItemForAddContainsIdException;
import net.orivis.shared.form.AbstractOrivisForm;
import net.orivis.shared.importer.Importer;
import net.orivis.shared.model.OrivisIDPresenter;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.List;

@Secured
@CrossOrigin
public interface AbstractImportController<DB_TYPE extends OrivisIDPresenter<ID_TYPE>, FORM extends AbstractOrivisForm, ID_TYPE extends Serializable> extends Importer<DB_TYPE, FORM, ID_TYPE>, OrivisTransformable<DB_TYPE, FORM> {
    @PostMapping(value = {"/import","/import/" })
    @ResponseBody
    @WithRole
    default ResponseEntity<List<FORM>> startImport() throws ItemForAddContainsIdException {

        var res = this.fetchAndStore(getContext());
        return ResponseEntity.ok(res);
    }

    OrivisContext getContext();
}
