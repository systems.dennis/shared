package net.orivis.shared.controller.items;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.orivis.shared.entity.KeyValue;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateFieldResponse<ID_TYPE extends Serializable> implements Serializable {
    private ID_TYPE id;
    private KeyValue keyValue;
}
