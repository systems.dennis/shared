package net.orivis.shared.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController ()
@RequestMapping("/__health")
public class HealthCheck {

    public static ArrayList<ServerErrorResponse> errors = new ArrayList<>();

    @GetMapping
    public String ok(){
        return "OK";
    }

    @GetMapping(value = "/check", produces = "application/json")

    public ServerStatus checkHealth(){
        if (errors.isEmpty()) {
            return ServerStatus.ok();
        } else {
            return ServerStatus.failed(errors);
        }
    }
}
