package net.orivis.shared.controller.generated;

import net.orivis.shared.controller.items.OrivisTransformable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.annotations.security.SelfOnlyRole;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.controller.forms.OrivisServiceable;
import net.orivis.shared.controller.orivis.MagicForm;
import net.orivis.shared.exceptions.ItemDoesNotContainsIdValueException;
import net.orivis.shared.exceptions.ItemForAddContainsIdException;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.exceptions.UnmodifiedItemSaveAttemptException;
import net.orivis.shared.form.AbstractOrivisForm;
import net.orivis.shared.model.OrivisIDPresenter;

import java.io.Serializable;

/**
 * Update object interface. Implementing this interface, will automatically create ../edit/
 *
 * @param <DB_TYPE> Entity which is managed by this interface and Service
 */
@Secured
public interface AbstractEditItemController <DB_TYPE extends OrivisIDPresenter<ID_TYPE>,
        FORM extends AbstractOrivisForm<ID_TYPE>, ID_TYPE extends Serializable> extends OrivisServiceable<DB_TYPE, ID_TYPE>, OrivisTransformable<DB_TYPE, FORM>,
        MagicForm<FORM>,
        AbstractEditFieldItemController<DB_TYPE, FORM, ID_TYPE> {


    @PutMapping(value = "/edit", consumes = {"application/json", "application/json;charset=UTF-8"}, produces = {"application/json", "application/json;charset=UTF-8"})
    @ResponseBody
    @SelfOnlyRole
    @WithRole
    default ResponseEntity<FORM> edit(@RequestBody FORM form) throws ItemForAddContainsIdException, ItemDoesNotContainsIdValueException, UnmodifiedItemSaveAttemptException, ItemNotFoundException {
        validate(form, true);
        return ResponseEntity.ok(toForm((DB_TYPE) getService().edit(fromForm(form))));
    }

}
