package net.orivis.shared.controller.generated;


import net.orivis.shared.controller.items.OrivisTransformable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.controller.forms.OrivisServiceable;
import net.orivis.shared.controller.orivis.MagicForm;
import net.orivis.shared.exceptions.ItemForAddContainsIdException;
import net.orivis.shared.form.AbstractOrivisForm;
import net.orivis.shared.model.OrivisIDPresenter;

import java.io.Serializable;

/**
 * An interface which adds possibility to add data
 * <p>
 * requires a service, to be processed, which is accessed threw the getService method.
 */
@Secured
public interface AbstractAddItemController<DB_TYPE extends OrivisIDPresenter<ID_TYPE>, FORM extends AbstractOrivisForm<ID_TYPE>, ID_TYPE extends Serializable> extends OrivisServiceable<DB_TYPE, ID_TYPE>, OrivisTransformable<DB_TYPE, FORM>, MagicForm<FORM> {

    /**
     * Performs add procedure
     *
     * @param form form object to add
     * @return an object which had been saved in DB with it's ID
     * @throws ItemForAddContainsIdException - this exception, means that object is tried to be updated instead of added. To devide add and edit is necessary as two different operations with different privileges
     */
    @PostMapping(value = "/add",  consumes = {"application/json", "application/json;charset=UTF-8"}, produces = {"application/json", "application/json;charset=UTF-8"})
    @ResponseBody
    @WithRole
    default ResponseEntity<FORM> add(@RequestBody FORM form) throws ItemForAddContainsIdException {
        validate(form, form.getId() != null);
        DB_TYPE savedModel = getService().save(fromForm(form));
        afterSaveForm(form, savedModel);
        return ResponseEntity.ok(toForm((savedModel)));
    }

    default void afterSaveForm(FORM form, DB_TYPE model) {};



}
