package net.orivis.shared.controller.orivis;

import jakarta.servlet.http.HttpServletRequest;
import lombok.SneakyThrows;
import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.model.OrivisIDPresenter;
import net.orivis.shared.service.AbstractService;
import org.springframework.core.io.UrlResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import net.orivis.shared.annotations.AbstractFormModifier;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.beans.OnApplicationStart;
import net.orivis.shared.controller.DefaultFormDetailsContainer;
import net.orivis.shared.controller.forms.OrivisServiceable;
import net.orivis.shared.controller.items.Contextable;
import net.orivis.shared.controller.items.OrivisTransformable;
import net.orivis.shared.entity.AbstractOrivisEntity;
import net.orivis.shared.entity.ExtendedOrivisForm;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.form.AbstractOrivisForm;
import net.orivis.shared.pojo_view.DEFAULT_TYPES;
import net.orivis.shared.pojo_view.list.PojoListView;
import net.orivis.shared.pojo_view.list.PojoListViewField;
import net.orivis.shared.pojo_view.list.Remote;
import net.orivis.shared.pojo_view.GeneratedPojoList;
import net.orivis.shared.utils.PojoListField;
import net.orivis.shared.utils.WebAction;
import net.orivis.shared.utils.bean_copier.BeanCopier;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

import static net.orivis.shared.pojo_view.DEFAULT_TYPES.TEXT;

public interface MagicList<T extends AbstractOrivisEntity<ID_TYPE>, ID_TYPE extends Serializable> extends Contextable {

    Map<String, GeneratedPojoList> pojoListCache = new HashMap<>();

    @GetMapping(value = {"/root/fetch/list", "/root/fetch/list/"}, produces = "application/json")
    @ResponseBody
    @WithRole
    default GeneratedPojoList fetchForm(HttpServletRequest request) {

        Class<? extends AbstractOrivisForm> formClass;
        Class<? extends AbstractFormModifier> formModifier;

        OrivisController OrivisController = getClass().getAnnotation(OrivisController.class);

        if (OrivisController != null) {
            OrivisService OrivisService = OrivisController.value().getAnnotation(OrivisService.class);
            formClass = OrivisService.form();
            formModifier = OrivisService.formModifier();

        } else {
            OrivisService orivisService = getClass().getAnnotation(OrivisController.class).value().getAnnotation(OrivisService.class);
            formClass = orivisService.form();
            formModifier = orivisService.formModifier();
        }

        var form = pojoListCache.containsKey(formClass.getName()) ? pojoListCache.get(formClass.getName()) : getPojoList(formClass);

        var res = getContext().getBean(BeanCopier.class).clone(form);
        res.setFields(new ArrayList<>(res.getFields()));

        if (formModifier != AbstractFormModifier.class) {
            AbstractFormModifier modifier = null;
            try {
                modifier = formModifier.newInstance();
                modifier.modify(getContext(), res, request);
            } catch (Exception e) {
                //ignored
            }
        }
        return res;
    }

    @SneakyThrows
    default GeneratedPojoList getPojoList(Class<? extends AbstractOrivisForm> formClass) {
        GeneratedPojoList generatedPojoList = new GeneratedPojoList();

        if (formClass.getAnnotation(PojoListView.class) != null) {
            copyTo(formClass.getAnnotation(PojoListView.class), generatedPojoList, formClass);
        } else {
            generatedPojoList.setShowTitle(true);
            generatedPojoList.setObjectType(formClass.getSimpleName());
        }
        generatedPojoList.setTableTitle(getClass().getSimpleName() + "_list".toLowerCase());
        generatedPojoList.setDefaultField(getDefaultField());

        BeanCopier.withEach(formClass, field -> getDescription(field, generatedPojoList));

        generatedPojoList.getFields().sort(Comparator.comparing(PojoListField::getOrder));
        generatedPojoList.setDefaultSorting(formClass.getConstructor().newInstance().defaultSearchOrderField());
        pojoListCache.put(formClass.getName(), generatedPojoList);
        return generatedPojoList;
    }

    default String getDefaultField() {
        return "name";
    }

    @SneakyThrows
    @GetMapping(value = {"/root/fetch/details/{id}", "/root/fetch/details/{id}/"}, produces = "application/json")
    @WithRole
    default DefaultFormDetailsContainer objectDetails(@PathVariable("id") ID_TYPE id, HttpServletRequest request) {

        var bean = ((OrivisServiceable) this).getService();
        var res = (OrivisIDPresenter<ID_TYPE>) bean.findById(id).orElseThrow(() -> ItemNotFoundException.fromId(id));

        DefaultFormDetailsContainer container = new DefaultFormDetailsContainer();
        container.setData(BeanCopier.values(((OrivisTransformable) this).toForm(res), res, getContext()));
        container.setFields(fetchForm(request).getFields());

        return container;
    }


    @PostMapping(value = {"/root/fetch/data", "/root/fetch/data/"}, produces = "application/json", consumes = "application/json")
    @ResponseBody
    @WithRole
    default Page<Map<String, Object>> fetchData(@RequestBody OrivisRequest request) {
        AbstractService service;
        Class<? extends AbstractFormModifier> formModifier;

        OrivisController OrivisController = getClass().getAnnotation(OrivisController.class);
        if (OrivisController != null) {
            var serviceModel = OrivisController.value();
            service = getContext().getBean(serviceModel);
            formModifier = serviceModel.getAnnotation(OrivisService.class).formModifier();
        } else {
            var serviceModel = getClass().getAnnotation(OrivisController.class).value();
            service = getContext().getBean(serviceModel);
            formModifier = serviceModel.getAnnotation(OrivisService.class).formModifier();
        }

        Page<OrivisIDPresenter> res = service.search(request);

        var pageList = new ArrayList<Map<String, Object>>();
        for (int i = 0; i < res.getContent().size(); i++) {
            OrivisIDPresenter model = res.getContent().get(i);
            var form = ((OrivisTransformable) this).toForm(model);
            var parcedFields = BeanCopier.values(form, model, getContext());
            pageList.add(parcedFields);
            if (form instanceof ExtendedOrivisForm) {
                if (formModifier != AbstractFormModifier.class) {
                    try {
                        var valueFetcherClass = formModifier.newInstance();
                        if (valueFetcherClass.hasCustomValues(model)) {
                            valueFetcherClass.appendToValues(parcedFields, model);
                        }

                    } catch (Exception e) {
                        //ignored
                    }
                }
            }


        }
        return new PageImpl<>(pageList, res.getPageable(), res.getTotalElements());
    }


    @SneakyThrows
    @PostMapping(value = {"/root/download/data", "/root/download/data/",}, consumes = "application/json")
    @ResponseBody
    @WithRole
    default GeneratedReport download(@RequestBody OrivisRequest request) {

        AbstractService service;

        OrivisController OrivisController = getClass().getAnnotation(OrivisController.class);
        if (OrivisController != null) {
            service = getContext().getBean(OrivisController.value());
        } else {
            service = getContext().getBean(getClass().getAnnotation(OrivisController.class).value());
        }

        UrlResource generatedReportPath = service.download(request);

        GeneratedReport report = new GeneratedReport();

        report.setPathToDownload(generatedReportPath.getFilename());
        report.setType("application/oset-stram");

        return report;

    }

    @SneakyThrows
    @GetMapping(value = {"/root/download/data/{name}", "/root/download/data/{name}/",})
    @ResponseBody

    default ResponseEntity<UrlResource> fetchFile(@PathVariable String name) {
        AbstractService service;

        OrivisController OrivisController = getClass().getAnnotation(OrivisController.class);
        if (OrivisController != null) {
            service = getContext().getBean(OrivisController.value());
        } else {
            service = getContext().getBean(getClass().getAnnotation(OrivisController.class).value());
        }

        UrlResource generatedReportPath = service.getInterNalResource(name);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + name + "\"").body(generatedReportPath);
    }

    default void getDescription(Field f, GeneratedPojoList generatedPojoList) {

        var el = f.getAnnotation(PojoListViewField.class);

        PojoListField field = new PojoListField();

        if (el == null) {
            field.setOrder(0);
            field.setSearchType(TEXT);
            field.setRemoteType("");
            field.setSearchable(false);
            field.setShowContent(true);
            field.setSortable(true);

            field.setType(TEXT);
            field.setVisible(true);
            field.setGroup(null);
            field.setFilterable(false);
        } else {
            if (!el.available()) {
                return;
            }
            field.setOrder(el.order());
            field.setSearchType(el.remote().searchType());
            field.setActions(Arrays.stream(el.actions()).map(x -> new WebAction().from(x)).collect(Collectors.toList()));

            field.setSearchable(el.searchable());
            field.setShowContent(el.showContent());
            field.setSortable(el.sortable());
            field.setFormat(el.format());
            field.setType(el.type());
            field.setGroup(el.group());
            field.setFilterable(el.filterable());

            if (!"".equals(el.remote().fetcher())) {
                field.setType(DEFAULT_TYPES.OBJECT_SEARCH);
                field.setSearchType(DEFAULT_TYPES.OBJECT_SEARCH);
            }

            if (!"".equals(el.remote().searchName())) {
                field.setSearchField(el.remote().searchField());
                field.setSearchName(el.remote().searchName());
            }
            if (!Remote.class.equals(el.remote().controller())) {
                field.setSearchName(OnApplicationStart.getSearchName(el.remote().controller()));
                field.setSearchField(OnApplicationStart.getFieldName(el.remote().controller()));
                if (DEFAULT_TYPES.DEFAULT_TYPE.equals(el.type() )){
                    field.setType(DEFAULT_TYPES.OBJECT_SEARCH);
                    field.setRemoteType(DEFAULT_TYPES.OBJECT_SEARCH);
                } else {
                    field.setType(el.type());
                    field.setRemoteType(el.type());
                }
            }
            field.setVisible(el.visible());
            field.setRemoteType(el.remote().fetcher());
        }
        field.setField(f.getName());
        field.setTranslation(f.getDeclaringClass().getSimpleName() + "." + f.getName().toLowerCase());
        generatedPojoList.getFields().add(field);
    }

    default void copyTo(PojoListView annotation, GeneratedPojoList generatedPojoList, Class<? extends AbstractOrivisForm> formClass) {
        generatedPojoList.setShowTitle(annotation.enableTitle());
        generatedPojoList.setSearchEnabled(annotation.enableSearching());
        generatedPojoList.setObjectType(annotation.favoriteType().isEmpty() ? formClass.getSimpleName() : annotation.favoriteType());
        generatedPojoList.setListActions(Arrays.asList(annotation.actions()));
        if (annotation.enableTitle() && !Objects.equals(annotation.title(), "")) {
            generatedPojoList.setTableTitle(annotation.title());
        } else {
            generatedPojoList.setTableTitle(this.getClass().getSimpleName() + ".list.title");
        }
    }


}
