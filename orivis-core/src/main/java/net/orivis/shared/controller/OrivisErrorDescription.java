package net.orivis.shared.controller;

import lombok.Data;

import java.util.Date;

@Data
public class OrivisErrorDescription {
    private String errorMessage;
    private Date date = new Date();
    private String url;

    public static OrivisErrorDescription of(Exception e, String res) {
        OrivisErrorDescription description = new OrivisErrorDescription();
        description.setErrorMessage(e.getMessage());
        description.setUrl(res);
        return description;
    }
}
