package net.orivis.shared.config;

import jakarta.servlet.http.HttpServletRequest;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.orivis.shared.beans.OrivisFilterProvider;
import net.orivis.shared.model.OrivisIDPresenter;
import net.orivis.shared.repository.OrivisFilter;
import net.orivis.shared.utils.bean_copier.BeanCopier;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import net.orivis.shared.annotations.security.ISecurityUtils;
import net.orivis.shared.beans.OrivisSettingsResolver;
import net.orivis.shared.beans.IdToAuthorizationIdBean;
import net.orivis.shared.beans.LocaleBean;
import net.orivis.shared.entity.TokenData;
import net.orivis.shared.exceptions.SettingKeyNotFoundException;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

@Data
@Service
@Slf4j
public class OrivisContext {
    public static final Map<String, Object> data = new HashMap<>();
    private HttpServletRequest request;

    private MessageResourceSource messages;

    private ApplicationContext applicationContext;

    private Environment environment;

    private LocaleBean localeBean;


    private final IdToAuthorizationIdBean id2id;
    private static OrivisSettingsResolver resolver;
    private static OrivisSettingsResolver EMPTY_RESOLVER = new OrivisSettingsResolver() {
        @Override
        public boolean hasSetting(String setting) {
            return false;
        }

        @Override
        public String getEnv(String key) {
            return null;
        }

        @Override
        public String getEnv(String key, Object def) {
            return null;
        }
    };

    public OrivisContext(HttpServletRequest request,
                         MessageResourceSource messages, ApplicationContext applicationContext,
                         Environment environment,
                         LocaleBean localeBean,
                         IdToAuthorizationIdBean id2id) {
        this.request = request;
        this.messages = messages;
        this.applicationContext = applicationContext;
        this.environment = environment;
        this.localeBean = localeBean;
        this.id2id = id2id;
    }


    static OrivisSettingsResolver getResolver(OrivisContext context){
        if (resolver == null || Objects.equals(resolver , EMPTY_RESOLVER )){
            return null;
        }
        resolver = context.getBean(OrivisSettingsResolver.class, EMPTY_RESOLVER);
        return  resolver;
    }

    @SneakyThrows
    public <T> T getEnv(String key) {
        var resolver = getBean(OrivisSettingsResolver.class, null);
        if (resolver == null){
            return (T) environment.getProperty(key);
        }

        return (T) resolver.getEnv(key);
    }

    public Serializable getCurrentUser(){
        return getBean(ISecurityUtils.class).getUserDataId();
    }



    public OrivisFilter<?> getFilterProvider(){
        return  this.getBean(OrivisFilterProvider.class).get();
    }

    /**
     * Short alias for getFilterProvider()
     * @return
     */
    public OrivisFilter<?> fp (){
        return this.getFilterProvider();
    }

    public <T> T getEnv(String key, T def) {
        try {
            T env = getEnv(key);
            if (Objects.isNull(env)) {
                return def;
            }
            return env;

        } catch(SettingKeyNotFoundException e) {
            return def;
        }
    }

    public TokenData getToken() {
        return getBean(ISecurityUtils.class).getToken();
    }



    public String getMessageTranslation(String header, String lang) {
        messages.resolveCode(header, new Locale(localeBean.transform(lang, this)));
        final String message = messages.getMessage(
                header.toLowerCase(),
                new Object[]{},
                "?_" + header + "_?",
                new Locale(localeBean.transform(lang, this))); //--> en_EN -> en_US

        if (message == null || (message.startsWith("?_") && message.endsWith("_?"))) {
            log.warn(header + " not found !");
        }
        return message;

    }

    private String getMessageTranslation(String header, String lang, Object... params) {
        return getMessageTranslation(header, new Locale(localeBean.transform(lang, this)), params);
    }

    private String getMessageTranslation(String header, Locale locale, Object... params) {
        final String message = messages.getMessage(
                header.toLowerCase(),
                params,
                "?_" + header + "_?",
                locale);

        if (message == null || (message.startsWith("?_") && message.endsWith("_?"))) {
            log.warn(header + " not found !");
        }
        return message;

    }

    public <T extends Serializable> T currentUser() {
        return (T) getBean(ISecurityUtils.class).getUserDataId();
    }

    private ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public <T> T getBean(Class<T> value) {
        if (value == HttpServletRequest.class) {
            return (T) request;
        }
        return getApplicationContext().getBean(value);
    }
    public <T> T getBean(Class<T> value,T def) {
        try {
            return getApplicationContext().getBean(value);
        } catch (BeansException e){
            return def;
        }
    }

    public OrivisIDPresenter<?> clone(OrivisIDPresenter<?> model) {
        return getBean(BeanCopier.class).clone(model);
    }
    public <T>T copy(OrivisIDPresenter<?> model, Class<T> cl) {
        return getBean(BeanCopier.class).copy(model, cl);
    }
}
