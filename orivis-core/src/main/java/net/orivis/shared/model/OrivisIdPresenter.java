package net.orivis.shared.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Transient;
import lombok.Data;
import net.orivis.shared.annotations.FormTransient;
import net.orivis.shared.entity.AbstractOrivisEntity;
import net.orivis.shared.exceptions.NotImplementedException;

import java.io.Serializable;

@Data
@MappedSuperclass
public abstract class OrivisIDPresenter<ID_TYPE extends Serializable> implements AbstractOrivisEntity<ID_TYPE> {

    @FormTransient
    private Boolean hidden;

    @JsonIgnore
    public  boolean isIdSet(){
        throw new NotImplementedException();
    }
    @Transient
    @Override
    @JsonIgnore
    public Boolean getHidden() {
        return hidden;
    }


}
