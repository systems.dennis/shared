package net.orivis.shared.exceptions;

import lombok.Data;

import java.io.Serializable;

@Data
public class EditableObjectNotFoundException extends StandardWithIdException {
    private Serializable id;
    private String target;

    public EditableObjectNotFoundException(String type, Serializable id) {
        super(id, type, "global.exceptions.item_not_found_in_history_edited_objects");
        this.id = id;
        this.target = type;
    }
}
