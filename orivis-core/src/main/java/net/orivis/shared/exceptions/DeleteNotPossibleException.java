package net.orivis.shared.exceptions;

import lombok.Data;

import java.io.Serializable;

@Data
public class DeleteNotPossibleException extends StandardException {

    public DeleteNotPossibleException() {
        super(null, "global.delete.not.possible");
    }

    public DeleteNotPossibleException(String s, Serializable target){
        super(target, s);
    }
}
