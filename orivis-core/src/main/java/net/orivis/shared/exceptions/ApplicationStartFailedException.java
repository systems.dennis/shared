package net.orivis.shared.exceptions;

public class ApplicationStartFailedException extends StandardException {
    public ApplicationStartFailedException(Class<?> model, String s) {
        super( model.getName(),s);
    }
}
