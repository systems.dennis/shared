package net.orivis.shared.exceptions;

import lombok.Data;
import net.orivis.shared.entity.KeyValue;
import net.orivis.shared.pojo_form.ValidationContext;

import java.util.ArrayList;
import java.util.List;

@Data
public class ValidationFailedException extends RuntimeException {
    private List<KeyValue> errorMessages = new ArrayList<>();
    public ValidationFailedException(ValidationContext context) {
        errorMessages =  context.getFieldErrors();
    }
}
