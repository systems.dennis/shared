package net.orivis.shared.exceptions;

import lombok.Data;
import net.orivis.shared.entity.KeyValue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class ListValidationFailedException extends RuntimeException {
    private Map<String, List<KeyValue>> errorMessages = new HashMap<>();

    public ListValidationFailedException(Map< ? , List<KeyValue>> context) {
        context.forEach((key, value) -> errorMessages.put(String.valueOf(key), value));
    }

    public ListValidationFailedException(String errorMessage) {
        super(errorMessage);
    }

    @Override
    public String getMessage() {
        if (!errorMessages.isEmpty()) {
            StringBuilder message = new StringBuilder();
            errorMessages.forEach((key, value) -> {
                message.append(key).append(": ").append(value).append(" ");
            });
            return message.toString();
        } else {
            return super.getMessage();
        }
    }
}
