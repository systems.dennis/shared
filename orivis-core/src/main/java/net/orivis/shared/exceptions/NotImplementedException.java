package net.orivis.shared.exceptions;

/**
 * This exception is throw when Implementation of the object should be done, but still not
 */
public class NotImplementedException extends StandardException {
}
