package net.orivis.shared.exceptions;

import lombok.Data;
import net.orivis.shared.form.AbstractOrivisForm;

import java.io.Serializable;

@Data
public class ItemNotFoundException extends StandardWithIdException {

    private Serializable id;
    private String target;

    private ItemNotFoundException(){}

    public static <T extends Serializable>ItemNotFoundException fromId(T id){
        var exception =  new ItemNotFoundException();
        exception.setId(id);
        exception.setTarget("global.exceptions.not_found");
        return  exception;
    }

    public <T extends AbstractOrivisForm> ItemNotFoundException(AbstractOrivisForm object) {
        super(object.getId(), object.getClass().getSimpleName(), "global.exceptions.not_found");
        this.target = object.getClass().getSimpleName();
        this.id = object.getId();
    }

    public ItemNotFoundException(String object) {
        super(null, object, "global.exceptions.not_found");
        this.target = object;
    }
}
