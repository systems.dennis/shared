package net.orivis.shared.exceptions;

import lombok.Data;

@Data
public class SettingKeyNotFoundException extends StandardException {

    public SettingKeyNotFoundException(String settingKey, String message) {
        super(settingKey, message);
    }
}
