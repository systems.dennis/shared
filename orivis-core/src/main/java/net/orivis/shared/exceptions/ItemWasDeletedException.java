package net.orivis.shared.exceptions;

import lombok.Data;

import java.io.Serializable;

@Data
public class ItemWasDeletedException extends StandardWithIdException {
    private Serializable id;
    private String target;

    public ItemWasDeletedException(Serializable id, Class cl) {
        super(id, cl.getSimpleName(), "global.exceptions.item_was_deleted");
        this.id = id;
        this.target = cl.getSimpleName();
    }
}
