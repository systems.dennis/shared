package net.orivis.shared.exceptions;

import lombok.Data;

@Data
public class SettingValueTypeNotFoundException extends StandardException {

    public SettingValueTypeNotFoundException(String valueType, String message) {

        super(valueType, message);
    }
}
