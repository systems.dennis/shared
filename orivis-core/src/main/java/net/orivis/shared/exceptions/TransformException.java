package net.orivis.shared.exceptions;

public class TransformException extends ItemNotFoundException {

    public TransformException(String object) {
        super(object);
    }
}
