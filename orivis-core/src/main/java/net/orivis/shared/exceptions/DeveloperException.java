package net.orivis.shared.exceptions;

/**
 * Developer exception is invoked when it is clear that exception cames from the development process (for example:
 * missing config, missing annotations, etc
 * )
 */
public class DeveloperException extends StandardException {
    public DeveloperException(String target, String message) {
        super(target, message);
    }
}
