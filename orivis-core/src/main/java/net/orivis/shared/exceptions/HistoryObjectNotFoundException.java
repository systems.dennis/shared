package net.orivis.shared.exceptions;

import lombok.Data;

import java.io.Serializable;

@Data
public class HistoryObjectNotFoundException extends StandardWithIdException {
    private Serializable id;
    private String target;

    public HistoryObjectNotFoundException(Serializable id, String type) {
        super(id, type, "global.exceptions.item_not_found_in_uninstall_history");
        this.id = id;
        this.target = type;
    }
}
