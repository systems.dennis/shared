package net.orivis.shared.exceptions;

public class BeanCopierException extends RuntimeException {
    public BeanCopierException(String message) {
        super(message);
    }
}
