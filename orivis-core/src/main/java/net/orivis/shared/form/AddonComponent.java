package net.orivis.shared.form;

import java.io.Serializable;

public interface AddonComponent extends Serializable {
    Object getAddon();
}
