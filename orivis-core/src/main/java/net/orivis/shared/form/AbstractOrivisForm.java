package net.orivis.shared.form;

import org.springframework.data.domain.Sort;
import net.orivis.shared.entity.OrivisID;
import net.orivis.shared.entity.KeyValue;

import java.io.Serializable;

public interface AbstractOrivisForm<ID_TYPE extends Serializable> extends OrivisID<ID_TYPE> {
    String ID_FIELD = "id";


    String asValue();

    default KeyValue defaultSearchOrderField(){
        return  new KeyValue(ID_FIELD,  Sort.Direction.DESC);
    }
}
