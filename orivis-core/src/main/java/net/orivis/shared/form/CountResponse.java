package net.orivis.shared.form;

import lombok.Data;

@Data
public class CountResponse {
    private Long count;
}
