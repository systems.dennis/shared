package net.orivis.shared.form;

import lombok.Data;
import net.orivis.shared.annotations.forms_and_entities.B;
import net.orivis.shared.annotations.forms_and_entities.S;
import net.orivis.shared.annotations.forms_and_entities.SA;
import net.orivis.shared.annotations.forms_and_entities.ServerColumn;
import net.orivis.shared.pojo_view.UIAction;
import net.orivis.shared.utils.PojoFormField;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static net.orivis.shared.pojo_view.DEFAULT_TYPES.*;

@Data
public class GeneratedPojoForm {

    public static GeneratedPojoForm INSTANCE;

    @ServerColumn(
            searchable = @B(value = true),
            type = @S(value = TEXT, onForm = TEXT_AREA),
            actions = @SA({UIAction.ACTION_DELETE, UIAction.ACTION_EDIT}))
    private String method;
    private String action;
    private String title;
    private Boolean showTitle;
    private String commitText;

    private Map<String, Object> value;

    private String objectType;


    private List<PojoFormField> fieldList = new ArrayList<>();
}
