package net.orivis.shared.converters;

public interface ValueTypeIdentifier<FROM, TO> {
   TO identify(FROM key, FROM value);

}
