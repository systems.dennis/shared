package net.orivis.shared.converters;

import com.fasterxml.jackson.core.JsonProcessingException;
import jakarta.persistence.AttributeConverter;
import org.springframework.stereotype.Component;
import net.orivis.shared.controller.orivis.OrivisRequest;
import net.orivis.shared.exceptions.StandardException;

import static net.orivis.shared.utils.Mapper.mapper;

@Component
public class MagicQueryConverter implements AttributeConverter<OrivisRequest, String> {

    @Override
    public String convertToDatabaseColumn(OrivisRequest magicQuery) {
        try {
            return mapper.writeValueAsString(magicQuery);
        } catch (JsonProcessingException e) {
            throw new StandardException(e, "JSON writing error");
        }
    }

    @Override
    public OrivisRequest convertToEntityAttribute(String value) {
        try {
            return mapper.readValue(value, OrivisRequest.class);
        } catch (JsonProcessingException e) {
            throw new StandardException(e, "JSON reading error");
        }
    }
}
