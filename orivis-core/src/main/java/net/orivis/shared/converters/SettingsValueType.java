package net.orivis.shared.converters;

public enum SettingsValueType {
    STRING,
    INTEGER,
    BOOLEAN,
    PASSWORD
}
