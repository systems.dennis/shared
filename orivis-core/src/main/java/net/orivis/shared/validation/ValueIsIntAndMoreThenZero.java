package net.orivis.shared.validation;

import net.orivis.shared.annotations.ValidationContent;
import net.orivis.shared.pojo_form.ValidationResult;

public class ValueIsIntAndMoreThenZero implements ValueValidator {
    public static ValueIsIntAndMoreThenZero DEFAULT = new ValueIsIntAndMoreThenZero();
    private String field;

    @Override
    public ValidationResult validate(Object element, Object value, ValidationContent content) {
        this.field = field;
        ValidationResult result = new ValidationResult();
        result.setResult(value != null && !String.valueOf(value).isEmpty());

        if (value == null) {
            result.setErrorMessage(element.getClass().getSimpleName().toLowerCase() + ".value.is.empty" + field);
            result.setResult(false);
        }

        try {
            var res = Integer.parseInt(value.toString());
            if (res < 1) {
                result.setErrorMessage(element.getClass().getSimpleName().toLowerCase() + ".value.not.less.one." + field);
                result.setResult(false);
            } else {
                result.setResult(true);
            }
        } catch (Exception e) {
            result.setErrorMessage(element.getClass().getSimpleName().toLowerCase() + ".value.not.integer." + field);
            result.setResult(false);
        }
        return result;
    }

    public void setField(String field) {
        this.field = field;
    }
}
