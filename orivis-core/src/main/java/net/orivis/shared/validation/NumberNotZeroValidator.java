package net.orivis.shared.validation;

import net.orivis.shared.annotations.ValidationContent;
import net.orivis.shared.pojo_form.ValidationResult;

public class NumberNotZeroValidator implements ValueValidator {

    @Override
    public ValidationResult validate(Object element, Object value, ValidationContent content) {

        if (value == null) {
            return ValidationResult.fail(element.getClass().getSimpleName().toLowerCase() + ".value.should.not.be.null");
        }

        if (value instanceof Number) {
            Number numberValue = (Number) value;
            if (numberValue.longValue() <= 0) {
                return ValidationResult.fail(element.getClass().getSimpleName().toLowerCase() + ".value.should.be.positive");
            }
        }
        return ValidationResult.PASSED;
    }
}
