package net.orivis.shared.validation;

import net.orivis.shared.annotations.ValidationContent;
import net.orivis.shared.annotations.security.ISecurityUtils;
import net.orivis.shared.pojo_form.ValidationResult;


import java.util.Objects;
import java.util.regex.Pattern;

public class EmailValidator implements ValueValidator<Object, String> {
    private static final String SPACE = " ";

    private final Pattern regexPattern =  Pattern.compile("^(?=.{1,64}@)[A-Za-z0-9]+([._-]?[A-Za-z0-9]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$");

    @Override
    public ValidationResult validate(Object element, String value,
                                     ValidationContent content) {

        if (Objects.isNull(value) || value.trim().isEmpty()) {
            return ValidationResult.fail(element.getClass().getSimpleName().toLowerCase() + ".empty.email");
        }

        if (value.contains(SPACE)) {
            return ValidationResult.fail(element.getClass().getSimpleName().toLowerCase() + ".email.contains.spaces");
        }

        if (content.getContext().getBean(ISecurityUtils.class).isAdmin()){
            return ValidationResult.PASSED;
        }

        value = value.trim();
        var ok = regexPattern
                .matcher(value)
                .matches();

        if (!ok) {
            return ValidationResult.fail(element.getClass().getSimpleName().toLowerCase() + ".incorrect.email");
        }

        return ValidationResult.PASSED;
    }
}
