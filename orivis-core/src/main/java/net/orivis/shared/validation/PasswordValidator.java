package net.orivis.shared.validation;

import lombok.extern.slf4j.Slf4j;
import net.orivis.shared.annotations.ValidationContent;
import net.orivis.shared.exceptions.BeanCopierException;
import net.orivis.shared.pojo_form.ValidationResult;
import net.orivis.shared.utils.bean_copier.BeanCopier;

import java.util.regex.Pattern;

@Slf4j
public class PasswordValidator implements ValueValidator<Object, String> {
    private final Pattern regex =Pattern.compile( "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!*()_{};:,<.>/\\\\-])" +
            "[0-9a-zA-Z@#$%^&+=!*()_{};:,<.>/\\\\-]{8,}$");

    @Override
    public ValidationResult validate(Object element, String value, ValidationContent content) {
        value = value.trim();
        var ok = regex
                .matcher(value)
                .matches();

        if (!ok){
            return ValidationResult.fail(element.getClass().getSimpleName().toLowerCase() + ".incorrect.password");
        }

        try {
            if (!value.equals(BeanCopier.readValue(element, "repeatPassword"))) {
                return ValidationResult.fail(element.getClass().getSimpleName().toLowerCase() + ".password_does_not_match");
            }
        } catch (BeanCopierException e){
            return ValidationResult.PASSED;
        } catch (Exception e){
            log.error("Something wrong with validation ", e);
        }
       return ValidationResult.PASSED;
    }
}
