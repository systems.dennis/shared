package net.orivis.shared.validation;

import net.orivis.shared.annotations.ValidationContent;
import net.orivis.shared.pojo_form.ValidationResult;

public interface ValueValidator<FIELD_CLASS, VALUE_CLASS> {

    ValidationResult validate(FIELD_CLASS element, VALUE_CLASS value, ValidationContent content);

}
