package net.orivis.shared.validation;

import net.orivis.shared.annotations.ValidationContent;
import net.orivis.shared.pojo_form.ValidationResult;

import java.util.regex.Pattern;

public class PhoneNumberValidator implements ValueValidator {
    private final Pattern ptrn = Pattern.compile("^(\\+\\d{1,3}( )?)?((\\(\\d{3}\\))|\\d{3})[- .]?\\d{3}[- .]?\\d{4}$");

    @Override
    public ValidationResult validate(Object element, Object value, ValidationContent content) {
        var res= ptrn.matcher(String.valueOf(value)).matches();
        if (res){
            return ValidationResult.PASSED;
        } else {
            return ValidationResult.fail(element.getClass().getSimpleName().toLowerCase() + ".phone.is.incorrect");
        }
    }
}
