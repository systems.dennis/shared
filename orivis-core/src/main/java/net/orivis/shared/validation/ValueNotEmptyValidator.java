package net.orivis.shared.validation;

import net.orivis.shared.annotations.ValidationContent;
import net.orivis.shared.pojo_form.ValidationResult;

import java.util.Collection;
import java.util.Objects;


public class ValueNotEmptyValidator implements ValueValidator {
    public static ValueNotEmptyValidator DEFAULT = new ValueNotEmptyValidator();

    @Override
    public ValidationResult validate(Object element, Object value, ValidationContent content) {

        if (Objects.isNull(value)) {
            return ValidationResult.fail(element.getClass().getSimpleName().toLowerCase() + ".value.not.set." + content.getField() );
        }

        Class<?> valueClass = value.getClass();

        if (valueClass.equals(String.class) && ((String) value).trim().isEmpty()) {
            return ValidationResult.fail(element.getClass().getSimpleName().toLowerCase() + ".value.not.set." + content.getField() );
        }

        if (Collection.class.isAssignableFrom(valueClass) && ((Collection<?>) value).isEmpty()) {
            return ValidationResult.fail(element.getClass().getSimpleName().toLowerCase() + ".value.not.set." + content.getField() );
        }

        return ValidationResult.PASSED;
    }
}
