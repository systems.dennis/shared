package net.orivis.shared.validation;

import net.orivis.shared.annotations.ValidationContent;
import net.orivis.shared.form.AbstractOrivisForm;
import net.orivis.shared.pojo_form.ValidationResult;
import net.orivis.shared.service.AbstractService;

public class FieldIsUniqueValidator implements ValueValidator<Object, Object> {
    @Override
    public ValidationResult validate(Object element, Object value, ValidationContent content) {
        boolean isValueEmpty = value == null || value.toString().isEmpty();

        if (isValueEmpty) {
            return ValidationResult.PASSED;
        }

        var provider = content.getContext().getFilterProvider().eq(content.getField(), value);

        if (content.isEdit()) {
            provider.and(content.getContext().getFilterProvider().idNot(((AbstractOrivisForm) element).getId()));
        }

        AbstractService service = (AbstractService<?, ?>) content.getContext().getBean(content.getServiceClass());
        if (service.count( provider) > 0) {
            return ValidationResult.fail(element.getClass().getSimpleName().toLowerCase() + ".field." + content.getField() + ".not.unique");
        }


        return ValidationResult.PASSED;
    }
}
