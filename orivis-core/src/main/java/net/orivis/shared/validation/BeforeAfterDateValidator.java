package net.orivis.shared.validation;

import net.orivis.shared.annotations.ValidationContent;
import net.orivis.shared.pojo_form.ValidationResult;
import net.orivis.shared.utils.OrivisDate;
import net.orivis.shared.utils.bean_copier.BeanCopier;

import java.util.Date;

public class BeforeAfterDateValidator implements ValueValidator<Object, Object> {

    @Override
    public ValidationResult validate(Object element, Object value, ValidationContent content) {

        String[] params = content.getValidation().params();

        if (params.length < 2) {
            return ValidationResult.fail(element.getClass().getSimpleName().toLowerCase() + ".not.enough.parameters.for.date.validation");
        }

        String startDateField = params[0];
        String endDateField = params[1];

        OrivisDate startValue = BeanCopier.readValue(element, startDateField);
        OrivisDate endValue = BeanCopier.readValue(element, endDateField);

        return validateDateRange(element, startValue.getDate(), endValue.getDate());

    }

    private ValidationResult validateDateRange(Object element, Date startValue, Date endValue) {
        if (startValue == null && endValue == null) {
            return ValidationResult.PASSED;
        }
        if (startValue == null){
            return ValidationResult.fail(element.getClass().getSimpleName().toLowerCase() + ".date.validation.start_date_null_and_end_date_not");
        }
        if (startValue.after(endValue)) {
            return ValidationResult.fail(element.getClass().getSimpleName().toLowerCase() + ".date.validation.start_date_after_end_date");
        }
        return ValidationResult.PASSED;
    }
}
