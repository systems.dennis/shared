package net.orivis.shared.beans;

import lombok.extern.slf4j.Slf4j;
import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.service.AbstractService;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.aop.support.AopUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMapping;
import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.annotations.entity.CreatedBy;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.controller.SearchEntityApi;
import net.orivis.shared.controller.SearcherInfo;
import net.orivis.shared.exceptions.ApplicationStartFailedException;
import net.orivis.shared.utils.bean_copier.BeanCopier;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Configuration
@Slf4j
public class OnApplicationStart {


    @Bean
    public CommandLineRunner init(OrivisContext context, ApplicationContext applicationContext, OnOrivisStart onAppStart) {
        return args -> {
            //since 3.1.2 No more forced elements to start only what is defined by application
            //since 3.3.0 here is added scanning services for SearchEntityApi

            try {
                Map<String, Object> webFormsBeans = applicationContext.getBeansWithAnnotation(OrivisController.class);
                Map<String, Object> orivisControllerBeans = applicationContext.getBeansWithAnnotation(OrivisController.class);

                Map<String, Object> combinedBeans = new HashMap<>(webFormsBeans);
                for (Map.Entry<String, Object> entry : orivisControllerBeans.entrySet()) {
                    combinedBeans.putIfAbsent(entry.getKey(), entry.getValue());
                }

                for (var controller : combinedBeans.values()) {
                    var targetClass =  AopUtils.getTargetClass(controller);
                    Class<? extends AbstractService> serviceClass;

                    OrivisController orivisController = targetClass.getAnnotation(OrivisController.class);

                    serviceClass = orivisController.value();


                    var name = getSearchName(targetClass);
                    var fieldName =  getFieldName(targetClass);
                    if (fieldName != null && name != null) {
                        SearchEntityApi.registerSearch(name, new SearcherInfo(fieldName, serviceClass));
                    }
                    //since version 3.3.1 @CreatedBy annotation is now replacing UserAssignableEntity
                    //to avoid multiple fetching annotation data /searching for annotation a map is now created
                    //also check that double @CreatedBy by model is not defined

                        checkForCreatedAnnotations(orivisController, targetClass, context);


                }
            } catch (Exception e) {
                log.info("error during automatic SearchEntity filling: ", e);
            }

            if (onAppStart != null) {
                onAppStart.afterRun(context);
            }
        };
    }

    private void checkForCreatedAnnotations(Annotation annotation, Class<?> targetClass, OrivisContext context) {
        Class<?> model;

        if (annotation.annotationType() == OrivisController.class) {
            model = ((OrivisController) annotation).value().getAnnotation(OrivisService.class).model();
        } else if (annotation.annotationType() == OrivisController.class) {
            model = ((OrivisController) annotation).value().getAnnotation(OrivisService.class).model();
        } else {
            throw new IllegalArgumentException("Unsupported annotation type: " + annotation.annotationType());
        }

        var createdFields = BeanCopier.findAnnotatedFields(model, CreatedBy.class, BeanCopier.TYPE_ALL_FIELDS);
        if (createdFields.size() > 1){
            throw new ApplicationStartFailedException(model, "global.few.created_annotations");
        }

        if (createdFields.size() == 1){
            OrivisContextable.CREATED_FIELDS_MAP.put(model, createdFields.get(0));
        }
    }


    public static String getFieldName(Class<?> controller) {
        Class<?> model;

        OrivisController orivisController = controller.getAnnotation(OrivisController.class);

        if (orivisController != null) {

            model = controller.getAnnotation(OrivisController.class).value().getAnnotation(OrivisService.class).model();


            for (var field : model.getDeclaredFields()) {
                if (!java.lang.reflect.Modifier.isStatic(field.getModifiers())) {
                    if (field.getType() == String.class) {
                        return field.getName();
                    }
                }

            }
        }


        return null;
    }

    public static String getSearchName(Class<?> controller) {
        String name = null;

        OrivisController support = controller.getAnnotation(OrivisController.class);

        if (support != null) {
                name = retrieveNameFromPath(controller);

        }

        return name;
    }

    public static String retrieveNameFromPath(Class<?> controller) {
        var mapping = controller.getAnnotation(RequestMapping.class).value();

        if (mapping.length == 0) {
            return null;
        }

        var mappingControllerPath = mapping[0];

        if (mappingControllerPath.endsWith("/")) {
            mappingControllerPath = mappingControllerPath.substring(0, mappingControllerPath.length() -1);
        }
        var name = mappingControllerPath.split("/");
        return name[name.length - 1];


    }


}
