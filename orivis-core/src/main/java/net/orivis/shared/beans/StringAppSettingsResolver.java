package net.orivis.shared.beans;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.OrivisContextable;

@Service
public class StringAppSettingsResolver extends OrivisContextable implements OrivisSettingsResolver {
    private Environment environment;

    public StringAppSettingsResolver(OrivisContext context, Environment environment) {
        super(context);
        this.environment = environment;
    }

    @Override
    public boolean hasSetting(String setting) {
        try {
            environment.getProperty(setting);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public <T> T getEnv(String key) {
        var value = environment.getProperty(key);

        if (value == null){
            return null;
        }
        try {
            return (T) Integer.valueOf(value);
        } catch (Exception eint) {
            try {
                return (T) Long.valueOf(value);
            } catch (Exception elong) {

                if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false")) {
                    return (T) Boolean.valueOf(value);
                }
                return (T) value;
            }
        }
    }


    @Override
    public <T> T getEnv(String key, Object def) {
        try {
            return getEnv(key);
        } catch (ClassCastException e) {
            throw e;
        } catch (Exception e) {
            return (T) def;
        }
    }
}
