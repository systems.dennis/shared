package net.orivis.shared.beans;

public interface OrivisSettingsResolver {
    boolean hasSetting(String setting);
    <T>T getEnv(String key);
    <T>T getEnv(String key, Object def);
}
