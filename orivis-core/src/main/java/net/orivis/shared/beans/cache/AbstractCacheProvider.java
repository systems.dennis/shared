package net.orivis.shared.beans.cache;

import net.orivis.shared.controller.orivis.OrivisRequest;
import net.orivis.shared.entity.OrivisID;
import net.orivis.shared.exceptions.DeveloperException;
import net.orivis.shared.exceptions.StandardException;
import net.orivis.shared.model.OrivisIDPresenter;
import net.orivis.shared.repository.OrivisFilter;
import net.orivis.shared.utils.DataSupplier;
import net.orivis.shared.utils.Supplier;
import org.springframework.data.domain.Page;

import java.io.Serializable;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public interface AbstractCacheProvider {
    Map<String, AbstractCacheProvider> instances = new HashMap<>();
    <T extends OrivisID<? extends Serializable>> Optional<T> get(String group, Serializable id, Class <?> c,
                                                                 DataSupplier<Optional<T>> or,
                                                                 CacheElementChecker<T> checker, Duration duration);
    Page<? extends OrivisID<? extends Serializable>> get(String group, String request);
    void addToCache(String group, OrivisID<? extends Serializable> entity, Duration duration);
    void addToCache(String group, String request, Page<? extends OrivisID<? extends Serializable>> cache, Duration duration);
    void removeFromCache( OrivisID<? extends Serializable> cache);
    void updateCache(String group, OrivisID<? extends Serializable> cache);
    String buildDataCacheKey(String modelType, OrivisFilter<?> filter,
                                Integer page, Integer limit, OrivisRequest request);

    void cleanGarbage();

    static <T extends  AbstractCacheProvider > T getInstance(Class<? extends AbstractCacheProvider> provider){
        if (provider == null){
            throw new DeveloperException("provider.is_null", "");
        }
        if (instances.containsKey(provider.getSimpleName()) ) {
            return (T) instances.get(provider.getSimpleName());
        }

        try {
            var instance = provider.getConstructor().newInstance();
            instances.put(provider.getSimpleName(), instance);
            return (T) instance;
        } catch (Exception e){
            return null; // not able to get provider!
        }


    }
}
