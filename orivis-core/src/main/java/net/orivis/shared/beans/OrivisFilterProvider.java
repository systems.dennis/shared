package net.orivis.shared.beans;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import net.orivis.shared.model.OrivisIDPresenter;
import net.orivis.shared.repository.OrivisFilter;

@Service
@Scope("prototype")
public interface OrivisFilterProvider {
    <T extends OrivisIDPresenter<?>> OrivisFilter<T> get();

}
