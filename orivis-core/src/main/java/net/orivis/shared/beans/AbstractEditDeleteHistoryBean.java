package net.orivis.shared.beans;

import net.orivis.shared.entity.AbstractOrivisEntity;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.OrivisContextable;

import java.io.Serializable;

/**
 * When item is deleted or edited this bean is called to save delete history
 * Annotation @Primary is required on the implementation
 * <b>This is a default nothing happening implementation! </b>
 * @param <T> Object type extended normally from {@link AbstractOrivisEntity}
 * @param <ID_TYPE> ID type of String/Long or any other
 */
@Service
@Scope("singleton")
public class AbstractEditDeleteHistoryBean<T, ID_TYPE extends Serializable> extends OrivisContextable {

    private Boolean enabled;

    public AbstractEditDeleteHistoryBean(OrivisContext context) {
        super(context);
    }

    public void edit(T from, T to){

    }
    public void delete(ID_TYPE id, T from){

    }

    public void throwIfDeleted(ID_TYPE id, Class object){

    }


    public boolean isEnabled(){
        if (enabled != null){
            return  enabled;
        }
        return this.enabled = getContext().getEnv("global.save.versions", false);

    }
}
