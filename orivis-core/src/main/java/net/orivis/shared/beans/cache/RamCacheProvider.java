package net.orivis.shared.beans.cache;

import net.orivis.shared.beans.cache.exceptions.CacheNotInGroupException;
import net.orivis.shared.controller.orivis.OrivisOrder;
import net.orivis.shared.controller.orivis.OrivisRequest;
import net.orivis.shared.entity.OrivisID;
import net.orivis.shared.repository.OrivisFilter;
import net.orivis.shared.utils.DataSupplier;
import org.springframework.data.domain.Page;

import java.io.Serializable;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;

public class RamCacheProvider implements AbstractCacheProvider {

    private static final Map<Serializable, Map<Serializable, OrivisID<? extends Serializable> >> cache = new ConcurrentHashMap<>();
    private static final Map<Serializable, Page<? extends OrivisID<? extends Serializable>>> dataCache = new ConcurrentHashMap<>();
    private static final Map<Serializable, Set<String>> cacheIndex = new ConcurrentHashMap<>();
    private static final Map<String, Map<Serializable, List<Serializable>>> groups = new ConcurrentHashMap<>();

    private final ConcurrentSkipListMap<Long, GarbageItem> operateRemove = new ConcurrentSkipListMap<>();

    @Override
    public <T extends OrivisID<? extends Serializable>> Optional<T> get(String group, Serializable id, Class<?> c,
                                                                        DataSupplier<Optional<T>> or, CacheElementChecker<T> checker,
                                                                        Duration duration)  {
        var data = cache.get(c.getSimpleName());

        if (data != null && data.containsKey(id)) {
            if (groups.get(c.getSimpleName()) != null && groups.get(c.getSimpleName()).get(group) != null && groups.get(c.getSimpleName()).get(group).contains(id.toString())) {
                return Optional.of( checker.check((T) data.get(id)));
            }
        }

        Optional<T> fetchedItem = or.get();

        if (fetchedItem.isPresent()) {

            addToCache(group, fetchedItem.get(), duration);
            return fetchedItem;
        }

        return Optional.empty();
    }

    @Override
    public Page<? extends OrivisID<? extends Serializable>> get(String group, String request) {
        if (request == null) {
            return null;
        }


        Page<? extends OrivisID<? extends Serializable>> cachedData = dataCache.get(request);

        if (cachedData == null || groups.get(request) == null ||
                (groups.get(request) != null && !groups.get(request).get(group).contains(request))) {
            return null;
        }

        groups.computeIfAbsent(request, k -> new ConcurrentHashMap<>())
                .computeIfAbsent(group, k -> new ArrayList<>())
                .add(request);

        return cachedData;
    }

    @Override
    public void addToCache(String group, OrivisID<? extends Serializable> entity, Duration duration) {
        if (entity == null || duration == null){
            return;
        }

        Map<Serializable,  OrivisID<? extends Serializable>> cacheSector =  cache.putIfAbsent(entity.getClass().getSimpleName(), new ConcurrentHashMap<>());

        if (cacheSector == null){
            cacheSector = cache.get(entity.getClass().getSimpleName());
        }
        cacheSector.put(entity.getId(), entity);

        if (duration != Duration.ZERO) {
            operateRemove.put(new Date().getTime() + duration.toMillis(), new GarbageItem(entity.getClass().getSimpleName(), entity.getId()));
        }
        groups.computeIfAbsent(entity.getClass().getSimpleName(), k -> new ConcurrentHashMap<>())
                .computeIfAbsent(group, k -> new ArrayList<>())
                .add(entity.getId().toString());;
    }

    @Override
    public void addToCache(String group, String request, Page<? extends OrivisID<? extends Serializable>> items, Duration duration) {
        if (request == null || duration == null || items == null || items.isEmpty()) {
            return;
        }

        dataCache.put(request, items);

        groups.computeIfAbsent(request, k -> new ConcurrentHashMap<>())
                .computeIfAbsent(group, k -> new ArrayList<>())
                .add(request);

        for (OrivisID<? extends Serializable> item : items) {
            cacheIndex.computeIfAbsent(item.getId(), k -> new HashSet<>()).add(request);
        }

        if (!duration.isZero()) {
            operateRemove.put(
                    new Date().getTime() + duration.toMillis(),
                    new GarbageItem(request, request)
            );
        }
    }


    @Override
    public void removeFromCache(OrivisID<? extends Serializable> item) {
        var cacheSector = cache.get(item.getClass().getSimpleName());
        if (cacheSector != null) {
            cacheSector.remove(item.getId());

            removeFromGroup(item.getClass().getSimpleName(), item.getId());
        }

        Set<String> keysToRemove = cacheIndex.remove(item.getId());
        if (keysToRemove != null) {
            for (String key : keysToRemove) {
                dataCache.remove(key);
            }
        }
    }

    @Override
    public void updateCache(String group, OrivisID<? extends Serializable> item) {

        addToCache(group, item, Duration.ZERO);

        Set<String> keysToRemove = cacheIndex.remove(item.getId());
        if (keysToRemove != null) {
            for (String key : keysToRemove) {
                dataCache.remove(key);
            }
        }
    }

    @Override
    public void cleanGarbage() {
        var items = operateRemove.headMap(new Date().getTime());
        for (var item : items.keySet()){
            var toRemove = operateRemove.get(item);
            var cleanCache = cache.get(toRemove.getType());
            var cleanDataCache = dataCache.get(toRemove.getType());

            if (cleanCache != null) {
                cache.get(toRemove.getType()).remove(toRemove.getId());
                removeFromGroup(toRemove.getType(), toRemove.getId());
            }

            if (cleanDataCache != null) {
                dataCache.remove(toRemove.getId());
                removeFromGroup(toRemove.getId().toString(), toRemove.getId());
            }
        }

        items.clear();
    }

    @Override
    public String buildDataCacheKey(String modelType, OrivisFilter<?> filter,
                                 Integer page, Integer limit, OrivisRequest request) {

        StringBuilder key = new StringBuilder();

        key.append("model=").append(modelType);

        if (filter != null) {
            key.append(";filter=").append(filter.toString());
        }

        if (page != null) {
            key.append(";page=").append(page);
        }

        if (limit != null) {
            key.append(";limit=").append(limit);
        }

        if (request != null) {
            List<OrivisOrder> sortOrders = request.getSort();
            if (sortOrders != null && !sortOrders.isEmpty()) {
                key.append(";sort=[");
                for (OrivisOrder order : sortOrders) {
                    key.append(order.getField()).append(order.getDesc() ? "_desc" : "_asc").append(",");
                }
                key.deleteCharAt(key.length() - 1);
                key.append("]");
            }
        }

        return key.toString();
    }

    private void removeFromGroup(String type, Serializable id) {
        Map<Serializable, List<Serializable>> groupEntries = groups.get(type);
        if (groupEntries != null) {
            for (List<Serializable> values : groupEntries.values()) {
                values.remove(id);
            }
        }
    }
}
