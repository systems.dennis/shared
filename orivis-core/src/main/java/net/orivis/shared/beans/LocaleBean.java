package net.orivis.shared.beans;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.entity.KeyValue;
import net.orivis.shared.exceptions.TranslationException;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

@Slf4j
@Service

public class LocaleBean {

    public static final String GLOBAL_USE_CACHE_TRANSLATION = "global.use_cache_translation";
    public static final String GLOBAL_MESSAGE_PATH = "global.messages.path";

    private Map<String, List<KeyValue>> cache = new HashMap<>();



    public String transform(String localeName, OrivisContext context) {

        File directory = new File(getMessagesPath(context));
        if(directory == null || !directory.isDirectory()) throw new TranslationException(localeName);

        List<File> filesFromDirectory = Arrays.asList(directory.listFiles());


        for(File file : filesFromDirectory) {

            if(file.getName().contains(".properties")) {

                String fileNameWithoutProp = List.of(file.getName().split("\\.")).get(0);
                List<String> fileParts = List.of(fileNameWithoutProp.split("_"));
                if(fileParts.get(0).equals("messages") && fileParts.get(1).equals(localeName) && fileParts.size() == 3) {
                    return localeName + "_" + fileParts.get(2);
                }

            }

        }
        throw new TranslationException(localeName);

    }

    public List<KeyValue> getMessageTranslations(String lang, OrivisContext context) {
        if (lang == null || lang.isBlank()) {
            throw new TranslationException(lang);
        }

        if (isUseCache(context) && cache.containsKey(lang)) {
            return cache.get(lang);
        } else {

            var i18n = getMessagesPath(context);

            if (i18n != null && Files.exists(Path.of(i18n))) {
                var langToLoad = transform(lang, context);
                i18n = i18n + File.separator + "messages_" + langToLoad + ".properties";

                try {
                    Properties properties = new Properties();
                    properties.load(new InputStreamReader(new FileInputStream(i18n), StandardCharsets.UTF_8));

                    List<KeyValue> res = new ArrayList<>();
                    var items = properties.keys();

                    while (items.hasMoreElements()) {
                        var key = items.nextElement().toString();
                        res.add(new KeyValue(key, properties.getProperty(key)));
                    }
                    cache.put(lang, res);
                    return res;
                } catch (Exception e) {
                    if (isUseCache(context)) {
                        cache.put(lang, Collections.singletonList(new KeyValue("error", "lang_not_supported")));
                    }
                }
            }
            return null;
        }
    }

    public void clearCache() {
        cache.clear();
    }

    public boolean isUseCache(OrivisContext context) {
        return context.getEnv(GLOBAL_USE_CACHE_TRANSLATION, true);
    }

    public String getMessagesPath(OrivisContext context) {
        return context.getEnv(GLOBAL_MESSAGE_PATH, "./i18n");
    }
}
