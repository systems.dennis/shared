package net.orivis.shared.beans.cache;

import net.orivis.shared.entity.OrivisID;

import java.io.Serializable;

public interface CacheElementChecker<T extends OrivisID<? extends Serializable>> {
    T check(T entity);
}
