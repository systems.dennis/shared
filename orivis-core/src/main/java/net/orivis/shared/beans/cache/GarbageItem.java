package net.orivis.shared.beans.cache;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GarbageItem {
    private String type;
    private Serializable id;
}

