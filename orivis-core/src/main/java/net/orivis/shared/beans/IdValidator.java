package net.orivis.shared.beans;

import net.orivis.shared.exceptions.StandardException;

import java.io.Serializable;


public class IdValidator<ID_TYPE extends Serializable> {
    public boolean isIdSet(ID_TYPE id){
        throw new StandardException("id.check.not_implemented", String.valueOf(id));
    }
}
