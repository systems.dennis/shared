package net.orivis.shared.beans;

import org.springframework.stereotype.Component;
import net.orivis.shared.config.OrivisContext;

@Component
public interface OnOrivisStart {
    public void afterRun(OrivisContext context);
}
