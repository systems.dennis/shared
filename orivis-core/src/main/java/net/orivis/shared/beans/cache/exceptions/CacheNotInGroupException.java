package net.orivis.shared.beans.cache.exceptions;

import net.orivis.shared.exceptions.AccessDeniedException;

public class CacheNotInGroupException extends AccessDeniedException {
}
