package net.orivis.shared.beans;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Scope("singleton")
@Component
public class IdToAuthorizationIdBean {
    public Serializable idToAuthorizationId(final Serializable id) {
        return id;
    }
}
