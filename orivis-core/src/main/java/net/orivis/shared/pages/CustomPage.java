package net.orivis.shared.pages;

import lombok.Data;

import java.util.List;

@Data
public class CustomPage<T> {

    private int totalPages;
    private int totalElements;
    private int number;
    private int size;
    private int numberOfElements;
    private boolean hasPreviousPage;
    private boolean hasNextPage;
    private List<T> content;
    private boolean hasContent;

}
