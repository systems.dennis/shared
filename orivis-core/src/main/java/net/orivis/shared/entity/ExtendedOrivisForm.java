package net.orivis.shared.entity;

import java.io.Serializable;
import java.util.List;

public interface ExtendedOrivisForm<ID_TYPE extends Serializable> {
    ID_TYPE getId();

    List<KeyValue> getAdditionalValues();

    void setId(ID_TYPE id);

    void setAdditionalValues(List<KeyValue> keyValues);
}
