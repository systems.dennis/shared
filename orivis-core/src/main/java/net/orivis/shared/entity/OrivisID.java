package net.orivis.shared.entity;

import java.io.Serializable;

public interface OrivisID<ID_TYPE extends Serializable> {
      ID_TYPE getId();
}
