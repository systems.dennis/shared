package net.orivis.shared.entity;

import org.springframework.data.domain.Sort;
import net.orivis.shared.form.AbstractOrivisForm;

import java.io.Serializable;

public interface AbstractOrivisEntity<ID_TYPE extends Serializable> extends OrivisID<ID_TYPE> {
    String ID_FIELD = AbstractOrivisForm.ID_FIELD;

     void setId(ID_TYPE id);
    String asValue();

    boolean isIdSet();

    default KeyValue defaultSearchOrderField(){
        return  new KeyValue(ID_FIELD,  Sort.Direction.DESC);
    }

    Boolean getHidden();
}
