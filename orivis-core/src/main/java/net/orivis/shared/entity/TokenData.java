package net.orivis.shared.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TokenData {
   private String scope;
   private String token;


}
