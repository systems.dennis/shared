package net.orivis.shared.entity;

import lombok.Data;

@Data
public class TreeNode {
   private Long id;
   private String name;
   private Long cnt;
}
