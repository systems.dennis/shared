package net.orivis.shared.entity;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.BeanDeserializer;
import com.fasterxml.jackson.databind.deser.BeanDeserializerBuilder;
import com.fasterxml.jackson.databind.deser.SettableBeanProperty;
import com.fasterxml.jackson.databind.deser.impl.BeanPropertyMap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;

public class ExtendedFormDeserializer extends BeanDeserializer {


    public ExtendedFormDeserializer(BeanDeserializerBuilder builder, BeanDescription beanDesc, BeanPropertyMap properties, Map<String, SettableBeanProperty> backRefs, HashSet<String> ignorableProps, boolean ignoreAllUnknown, boolean hasViews) {
        super(builder, beanDesc, properties, backRefs, ignorableProps, ignoreAllUnknown, hasViews);
    }

    @Override
    public Object deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException, JsonProcessingException {
        ExtendedOrivisForm res = (ExtendedOrivisForm) super.deserialize(jp, ctxt);
        JsonNode node = jp.getCodec().readTree(jp);
        int id = (Integer) node.get("id").numberValue();
        res.setId((long) id);
        var arrNode = node.get("additionalValues");
        if (arrNode != null) {
            ArrayList<KeyValue> keyValue = new ArrayList<>();
            for (final JsonNode objNode : arrNode) {
                KeyValue object = new KeyValue();
                object.setKey(objNode.get("key").textValue());
                object.setValue(objNode.get("value").textValue());
                keyValue.add(object);
            }
            res.setAdditionalValues(keyValue);
        }


        return res;


    }
}
