package net.orivis.shared.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.TYPE})
/**
 * Indicates that The result of the method will never be null and developer can relay on it!
 * @since 3.0.5
 */
public @interface NeverNullResponse {
    /**
     * A string description when result is never null
     * @return
     */
    String on() default "always";
}
