package net.orivis.shared.annotations.entity;

public @interface CloneParameters {
    boolean disableCache() default false;
    int depth() default 1;
}
