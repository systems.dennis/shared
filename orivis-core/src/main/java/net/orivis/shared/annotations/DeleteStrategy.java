package net.orivis.shared.annotations;



import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface DeleteStrategy {

    public static final int DELETE_STRATEGY_DB = 1;
    public static final int DELETE_STRATEGY_PROPERTY = 2;
    public static final String DELETE_PROPERTY = "hidden";

    int value() default DELETE_STRATEGY_DB;
    String field() default DELETE_PROPERTY;

    boolean selfOnly() default false;

    boolean storeDeleteEventHistory() default  false;
}
