package net.orivis.shared.annotations;

import net.orivis.shared.beans.cache.AbstractCacheProvider;
import net.orivis.shared.beans.cache.RamCacheProvider;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface CacheStrategy {

    boolean enabled() default false;
    int age() default -1;

    Class<? extends AbstractCacheProvider> cacheProvider() default RamCacheProvider.class;
}
