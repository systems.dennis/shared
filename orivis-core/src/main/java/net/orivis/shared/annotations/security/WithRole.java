package net.orivis.shared.annotations.security;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, ElementType.ANNOTATION_TYPE})
public @interface WithRole {
    String value() default "ROLE_ANY";
    String or() default "";
    OrivisSecurityValidator ignoreOnCondition() default @OrivisSecurityValidator();
}
