package net.orivis.shared.annotations.security;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface OrivisSecurityValidator {
    Class<? extends validator> value() default NoValidation.class;
    String[] parameters() default  {};

}
