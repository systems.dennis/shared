package net.orivis.shared.annotations.entity;

import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.entity.AbstractOrivisEntity;

import java.lang.reflect.Field;

public interface OrivisAuditableService {

    void process(Field field, OrivisContext context, int type, AbstractOrivisEntity<?> origin, AbstractOrivisEntity<?> entity);

}
