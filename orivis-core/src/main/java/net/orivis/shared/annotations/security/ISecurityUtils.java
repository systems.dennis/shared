package net.orivis.shared.annotations.security;

import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.entity.TokenData;
import net.orivis.shared.repository.OrivisFilter;

import java.io.Serializable;
import java.util.List;

public interface ISecurityUtils<T extends Serializable> {
    T getUserDataId();


    default void isMy(Object o){ };

    TokenData getToken();

    boolean isAdmin();

    boolean hasRole(String role);

    String getUserLanguage();

    boolean hasAnyRole(String[] role);

    List<String> getRolesList();

     static OrivisFilter belongsToMeQuery(Class cl, Serializable userId, OrivisContext context){
        return context.getFilterProvider().ofUser(cl, userId);
    }

    default OrivisFilter belongsToMeQuery(Class cl, OrivisContext context){
        return  belongsToMeQuery(cl, getUserDataId(), context);
    }
    default OrivisFilter belongsToMeSpecification(Class cl, OrivisContext context){
        return  belongsToMeQuery(cl, getUserDataId(), context);
    }

    void validateToken(TokenData token);

    String getLogin();

    default String getScope(){
         try {
             return getToken().getScope();
         } catch (Exception e){
             return "Default";
         }
    }
}
