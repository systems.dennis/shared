package net.orivis.shared.annotations.security.selfchecker;

import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.security.AbstractAuthorizationProvider;

public class NoChecker implements AbstractSelfChecker {

    public NoChecker(){}

    @Override
    public void check(AbstractAuthorizationProvider dto, Object object, OrivisContext context) {

    }
}
