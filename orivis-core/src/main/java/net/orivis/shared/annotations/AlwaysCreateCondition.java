package net.orivis.shared.annotations;

import net.orivis.shared.config.OrivisContext;

public class AlwaysCreateCondition implements AutoCreationCheck {

    @Override
    public boolean checkAutoCreation(Object[] values, Object target, OrivisContext context) {
        return true;
    }
}
