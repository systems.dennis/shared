package net.orivis.shared.annotations.forms_and_entities;

public @interface B {
    boolean value() default false;
    boolean onForm() default false;
}
