package net.orivis.shared.annotations.forms_and_entities;

import net.orivis.shared.pojo_view.list.Remote;

import static net.orivis.shared.annotations.forms_and_entities.SameClass.NO_VALUE;
import static net.orivis.shared.pojo_view.DEFAULT_TYPES.TEXT;

public @interface ServerColumn {
    B searchable() default @B(value = true, onForm = true);
    B visible() default @B(value = true, onForm = true);
    B available() default @B(value = true, onForm = true);
    S type() default @S(value = TEXT);

    S customName() default @S(value = NO_VALUE);
    S group() default @S(value = NO_VALUE);

    R remote() default @R(@Remote);
    I order() default @I();

    boolean required() default false;

    boolean showPlaceHolder() default false;

    String fieldNote() default NO_VALUE;

    String placeHolder() default NO_VALUE;

    SA actions() default @SA() ;

}

