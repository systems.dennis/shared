package net.orivis.shared.annotations.forms_and_entities;

import static net.orivis.shared.annotations.forms_and_entities.SameClass.NO_VALUE;

public @interface SA {
    String[] value() default NO_VALUE;
    String[] onForm() default SameClass.SAME_VALUE;
}
