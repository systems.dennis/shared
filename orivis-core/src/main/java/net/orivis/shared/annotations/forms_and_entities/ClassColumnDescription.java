package net.orivis.shared.annotations.forms_and_entities;

public @interface ClassColumnDescription {
    Class<?> value() default NoValue.class;
    Class<?> onForm() default SameClass.class;
}