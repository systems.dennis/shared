package net.orivis.shared.annotations.entity;


import net.orivis.shared.annotations.AlwaysCreateCondition;
import net.orivis.shared.annotations.AutoCreationCheck;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)

public @interface OrivisAuditable {

    Class<? extends OrivisAuditableService>[] value() default OrivisAuditableService.class;
}
