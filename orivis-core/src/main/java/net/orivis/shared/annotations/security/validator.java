package net.orivis.shared.annotations.security;

import net.orivis.shared.config.OrivisContext;

public interface validator {
    boolean checkIgnorePermission(Object[] values, Object target, OrivisContext context, String [] parameters);
}
