package net.orivis.shared.annotations.security;

import net.orivis.shared.config.OrivisContext;

public class NoValidation implements validator {

    @Override
    public boolean checkIgnorePermission(Object[] values, Object target, OrivisContext context, String[] params) {
        return false;
    }
}
