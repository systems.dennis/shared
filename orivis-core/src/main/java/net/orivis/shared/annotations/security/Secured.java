package net.orivis.shared.annotations.security;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE})
public @interface Secured {
    String[] roles() default {};

    /**
     * This field defines if security annotation has priority under @WithRole annotation
     *
     * @return true if level of ROLES is over @WithRole(roles) false other vise
     *
     *
     * ex. @Security ("ROLE_USER"), WithRole("ROLE_ADMIN")
     *
     * if (@Security ("ROLE_USER", true){
     *     ROLE_ADMIN will be ignored
     * }
     */
    boolean prioritiesField() default true;
}
