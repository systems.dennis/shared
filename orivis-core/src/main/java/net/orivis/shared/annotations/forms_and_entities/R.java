package net.orivis.shared.annotations.forms_and_entities;

import net.orivis.shared.pojo_view.list.Remote;


public @interface R {
    Remote value();
    Remote onForm()  default @Remote(same = true);
}
