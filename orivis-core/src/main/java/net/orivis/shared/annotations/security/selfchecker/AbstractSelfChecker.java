package net.orivis.shared.annotations.security.selfchecker;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.security.AbstractAuthorizationProvider;

public interface AbstractSelfChecker<T> {
    void check(AbstractAuthorizationProvider dto, T object, OrivisContext context);
}
