package net.orivis.shared.annotations;

import lombok.Data;
import net.orivis.shared.config.OrivisContext;

@Data
public class ValidationContent {

    private Class<?> serviceClass;

    private String field;

    private Validation validation;

    private boolean edit;

    private OrivisContext context;
}
