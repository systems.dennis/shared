package net.orivis.shared.annotations;

import net.orivis.shared.service.AbstractService;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface OrivisController {
    String NO_SECURE = "";
    /**
     * A service that will work with data
     * @return A class of the service that will be taken from context
     */
    Class<? extends AbstractService> value() ;
    String[] securedBy() default NO_SECURE;



}
