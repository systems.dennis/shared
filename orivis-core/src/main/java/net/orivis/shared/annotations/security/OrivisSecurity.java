package net.orivis.shared.annotations.security;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * OrivisSecurity allows to setup access level for the method
 *
 * When primary role is selected the method will check for roles of this value, and if it doesn't pass it also checks additional
 * validators, that can also throw StatusException if the check fails
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, ElementType.ANNOTATION_TYPE})
public @interface OrivisSecurity {
    String primaryRole() default "ROLE_ANY";
    OrivisSecurityValidator validator() default @OrivisSecurityValidator();
}
