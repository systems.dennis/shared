package net.orivis.shared.annotations;

import jakarta.servlet.http.HttpServletRequest;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.entity.AbstractOrivisEntity;

import net.orivis.shared.form.GeneratedPojoForm;
import net.orivis.shared.pojo_view.GeneratedPojoList;

import java.io.Serializable;
import java.util.Map;

public interface AbstractFormModifier <ID_TYPE> {
    void modify(OrivisContext context, HttpServletRequest request, GeneratedPojoForm form);

    <T>Object getValue( GeneratedPojoForm form, String field, T object );

    <T extends Serializable>Object getValue(String key, AbstractOrivisEntity<T> model);


    void modify(OrivisContext context, GeneratedPojoList res, HttpServletRequest request);
    boolean hasCustomValues(Object model);

    void appendToValues(Map<String, Object> values, Object model);
}
