package net.orivis.shared.annotations;

import net.orivis.shared.validation.ValueValidator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Validation {
    Class<? extends ValueValidator>[] value() default {} ;

    String[] params() default {};
}
