package net.orivis.shared.annotations.entity;

public interface PrimaryDateSetter {
    <T>T getNewDateValue();
}
