package net.orivis.shared.annotations;

import net.orivis.shared.entity.AbstractOrivisEntity;
import net.orivis.shared.form.AbstractOrivisForm;
import net.orivis.shared.repository.AbstractRepository;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface OrivisService {
    Class<? extends AbstractOrivisEntity> model();
    Class<? extends AbstractOrivisForm> form();
    Class<? extends AbstractRepository> repo();
    Class<? extends AbstractFormModifier> formModifier() default AbstractFormModifier.class;
    CacheStrategy cacheStrategy() default  @CacheStrategy(enabled = false);
}
