package net.orivis.shared.annotations;

import net.orivis.shared.config.OrivisContext;

public interface AutoCreationCheck {

    boolean checkAutoCreation(Object[] values, Object target, OrivisContext context);
}
