package net.orivis.shared.annotations.forms_and_entities;

public @interface I {
    int SAME = -9999999;
    int value() default 0;
    int onForm() default SAME;

}
