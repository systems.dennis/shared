package net.orivis.shared.utils.bean_copier;

import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.model.OrivisIDPresenter;

import java.io.Serializable;

public class BeanCopierTransformer<ID_TYPE extends Serializable> implements OrivisDataTransofrmer<ID_TYPE> {

    @Override
    public <DB_TYPE extends OrivisIDPresenter<ID_TYPE>> Object transform(Object object, OrivisTranformer transformerDescription, Class<?> toType, OrivisContext context) {
        if (context == null){
            return null;
        }
        return context.getBean(BeanCopier.class).copy(object, toType);
    }
}
