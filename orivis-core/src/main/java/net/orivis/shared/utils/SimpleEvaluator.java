package net.orivis.shared.utils;


import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class SimpleEvaluator {

    private final Environment environment;

    public SimpleEvaluator(Environment environment) {
        this.environment = environment;
    }

    public String evaluate(String sql, int position, Map<String, String> params) {
        while (sql.contains("${")) {
            int newPosition = sql.indexOf("${", position);

            if (newPosition == -1) {
                break;
            }

            int newEndPosition = sql.indexOf("}", newPosition);
            if (newEndPosition == -1) {
                throw new IllegalArgumentException("unmatched placeholder: missing '}' for '${' in: " + sql);
            }

            String expr = sql.substring(newPosition, newEndPosition + 1);
            String key = expr.substring(2, expr.length() - 1);

            String tagValue = params.get(key);
            if (tagValue == null) {
                tagValue = environment.getProperty(key);
            }

            if (tagValue != null) {
                sql = sql.substring(0, newPosition) + tagValue + sql.substring(newEndPosition + 1);
            } else {
                throw new IllegalArgumentException("expression : " + expr + " not found in Properties");
            }
        }
        return sql.replace("::", "\\:\\:");
    }
}
