package net.orivis.shared.utils.bean_copier;

import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.model.OrivisIDPresenter;
import net.orivis.shared.utils.OrivisDate;

import java.io.Serializable;
import java.util.Date;

public class DateToUTCConverter  implements OrivisDataTransofrmer<Serializable> {
    @Override
    public <DB_TYPE extends OrivisIDPresenter<Serializable>> Object transform(Object object, OrivisTranformer transformerDescription, Class<?> toType, OrivisContext context) {
        if (object == null){
            return  null;
        }

        Object result;

        if (object.getClass() == OrivisDate.class){
            result = ((OrivisDate)object).UTC().getDate();
            //transform to date from TSDATE
        } else {
            result = OrivisDate.of((Date) object).UTC();
            //transform to TS date from Date
        }

        return result;
    }
}
