package net.orivis.shared.utils.bean_copier;

import lombok.Data;
import net.orivis.shared.entity.AbstractOrivisEntity;
import net.orivis.shared.form.AddonComponent;

import java.io.Serializable;

@Data
public class ObjectDefinition<ID_TYPE extends Serializable> implements Serializable {
    private ID_TYPE key;

    private String value;
    public ObjectDefinition(AbstractOrivisEntity<ID_TYPE> entity){
        if (entity == null){
            return;
        }
        this.key = entity.getId();
        this.value = entity.asValue();

        if (entity instanceof AddonComponent){
            this.additional = ((AddonComponent) entity).getAddon();
        }
    }
    private Object additional;
}
