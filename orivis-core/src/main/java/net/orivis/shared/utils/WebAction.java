package net.orivis.shared.utils;

import lombok.Data;
import net.orivis.shared.pojo_view.UIAction;

@Data
public class WebAction {
    private String name;
    private boolean allowOnMultiSelect;


    public WebAction from (UIAction action){
        this.name = action.component() + action.value();
        this.allowOnMultiSelect = action.allowOnMultipleRows();
        return  this;
    }
}
