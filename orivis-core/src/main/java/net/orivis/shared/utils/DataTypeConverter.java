package net.orivis.shared.utils;

import org.springframework.stereotype.Component;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.controller.SearchEntityApi;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.exceptions.TransformException;
import net.orivis.shared.pojo_view.DEFAULT_TYPES;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Objects;

@Component
public class DataTypeConverter extends OrivisContextable {

    public DataTypeConverter(OrivisContext context) {
        super(context);
    }

    public Object convert(String type, Object value, String searchName) {
        if (Objects.isNull(value)) {
            return null;
        }
        if (type.equals(DEFAULT_TYPES.OBJECT_SEARCH)) {
            return convertToObjectSearch(value, searchName);
        }
        if (type.equals(DEFAULT_TYPES.CHECKBOX)) {
            return convertToCheckbox(value);
        }
        if (type.equals(DEFAULT_TYPES.DATE)) {
            return convertToDate(value);
        }
        if (type.equals(DEFAULT_TYPES.INTEGER)) {
            return convertToInteger(value);
        }
        return value;
    }

    private Object convertToObjectSearch(Object value, String searchName) {
        try {
            Long id = Long.valueOf(value.toString());
            return getBean(SearchEntityApi.findServiceByType(searchName)).getRepository().findById(id).get();
        } catch (Exception e) {
            throw new ItemNotFoundException(value.toString());
        }
    }

    private Object convertToCheckbox(Object value) {
        return Boolean.valueOf(value.toString());
    }

    private Object convertToDate(Object value) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm");
            return dateFormat.parse(value.toString());
        } catch (ParseException e) {
            throw new TransformException("failed_to_convert_date");
        }
    }

    private Object convertToInteger(Object value) {
        return Integer.valueOf(value.toString());
    }
}
