package net.orivis.shared.utils.security;

import net.orivis.shared.annotations.security.selfchecker.AbstractSelfChecker;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.exceptions.ItemNotUserException;
import net.orivis.shared.utils.bean_copier.BeanCopier;

import java.util.Objects;

public class DefaultIdChecker implements AbstractSelfChecker {

    @Override
    public void check(AbstractAuthorizationProvider provider, Object object, OrivisContext context) {
        Object userDataId = BeanCopier.readValue(object, "userDataId");

        if (!Objects.equals(context.getCurrentUser(), userDataId)) {
            throw new ItemNotUserException();
        }
    }
}
