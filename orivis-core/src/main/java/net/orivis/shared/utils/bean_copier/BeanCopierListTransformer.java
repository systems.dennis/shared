package net.orivis.shared.utils.bean_copier;

import lombok.extern.slf4j.Slf4j;
import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.entity.AbstractOrivisEntity;
import net.orivis.shared.model.OrivisIDPresenter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class BeanCopierListTransformer<ID_TYPE extends Serializable> implements OrivisDataTransofrmer<ID_TYPE> {

    private Class<?> determineTargetClass(Class<?> cl, OrivisTranformer dataTransformer) {
        OrivisService OrivisServicen = dataTransformer.additionalClass().getAnnotation(OrivisService.class);

        if (AbstractOrivisEntity.class.isAssignableFrom(cl)) {
            if (OrivisServicen != null) {
                return dataTransformer.additionalClass().getAnnotation(OrivisService.class).form();
            }

            return dataTransformer.additionalClass().getAnnotation(OrivisService.class).form();
        }

        if (OrivisServicen != null) {
            return dataTransformer.additionalClass().getAnnotation(OrivisService.class).model();
        }
        return dataTransformer.additionalClass().getAnnotation(OrivisService.class).model();
    }

    @Override
    public <DB_TYPE extends OrivisIDPresenter<ID_TYPE>> Object transform(Object object, OrivisTranformer dataTransformer, Class<?> toType, OrivisContext context) {

        try {
            var beanCopier = context.getBean(BeanCopier.class);
            List<?> items = (List<?>) object;
            List result = new ArrayList<>();

            items.forEach(x -> result.add(beanCopier.copy(x, determineTargetClass(x.getClass(), dataTransformer))));

            return result;
        } catch (Exception e){
            log.debug("error: copying  " + object, e);
        }
        return  null;
    }
}
