package net.orivis.shared.utils;

public interface Supplier <T> {
    T onNull(Object item);
}

