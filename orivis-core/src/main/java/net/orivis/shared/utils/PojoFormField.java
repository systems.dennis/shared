package net.orivis.shared.utils;

import lombok.Data;
import lombok.SneakyThrows;
import net.orivis.shared.beans.OnApplicationStart;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.entity.KeyValue;
import net.orivis.shared.pojo_form.DataProvider;
import net.orivis.shared.pojo_form.PojoFormElement;
import net.orivis.shared.pojo_view.DEFAULT_TYPES;
import net.orivis.shared.pojo_view.list.Remote;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
public class PojoFormField implements Serializable {
    private String translation;
    private Integer order;
    private String field;
    private String type;
    private boolean required;
    private boolean visible;
    private boolean showPlaceHolder;
    private boolean checked;
    private boolean format;
    private boolean showLabel;
    List<KeyValue> dataProviders = new ArrayList<>();
    private String searchField;
    private String searchType;
    private String searchName;
    private String fieldNote;

    private String placeHolder;

    private String customName;

    private String remoteType;

    private String favType;
    private String subType;

    private String group;

    private String description;

    private Boolean customized = false;

    private boolean isId = false;

    @SneakyThrows
    public static PojoFormField from(Field x, OrivisContext context) {
        PojoFormField field = new PojoFormField();

        PojoFormElement element = x.getAnnotation(PojoFormElement.class);
        field.setField(x.getName());
        field.setTranslation(x.getDeclaringClass().getSimpleName() + "." + x.getName().toLowerCase());
        field.setOrder(element == null ? 0 : element.order());
        field.setRequired(element != null && element.required());
        field.setVisible(element != null && element.visible());
        field.setType(element == null ? "text" : element.type());
        field.setPlaceHolder(element == null ? "" : field.placeHolder);
        field.setShowPlaceHolder(element != null && element.showPlaceHolder());
        field.setChecked(element != null && element.checked().checked());
        field.setShowLabel(element == null || element.showLabel());
        field.setGroup(null);
        field.setDescription(element == null || Objects.equals(element.description(), PojoFormElement.NO_DESCRIPTION) ? null : element.description());


        //fix this is needed because magic ui may relay on it


        field.setId(element != null && element.ID());
        if (element != null) {
            field.setCustomName(element.group());
            field.setFavType(element.type());

            field.setRemoteType(element.remote().fetcher());
            field.setSearchType(element.remote().searchType());

            field.setSearchName(element.remote().searchName());
            field.setSearchField(element.remote().searchField());


            if (!Remote.class.equals(element.remote().controller()) ) {
                field.setSearchName(OnApplicationStart.getSearchName(element.remote().controller()));
                field.setSearchField(OnApplicationStart.getFieldName(element.remote().controller()));
                if (DEFAULT_TYPES.DEFAULT_TYPE.equals(element.type() )){
                    field.setType(DEFAULT_TYPES.OBJECT_SEARCH);
                    field.setRemoteType(DEFAULT_TYPES.OBJECT_SEARCH);
                } else {
                    field.setType(element.type());
                    field.setRemoteType(element.type());
                }
            }


            field.setFieldNote(element.fieldNote());
            field.setGroup(element.group());

            if (element.dataProvider() != null && element.dataProvider() != DataProvider.class) {
                var items = element.dataProvider().getConstructor().newInstance().getItems(context);
                field.getDataProviders().addAll(items);

            }
        }
        return field;
    }


    public boolean equals(Object o) {
        if (o == null) return false;
        if (o.getClass() != this.getClass()) {
            return false;
        }

        return Objects.equals(((PojoFormField) o).getField(), getField());
    }
}
