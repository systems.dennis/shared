package net.orivis.shared.utils;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import net.orivis.shared.config.OrivisContext;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

@Data
@Slf4j
public class OrivisContextable {

    private final OrivisContext context;


    public static  final Map<Class<?>, Field> CREATED_FIELDS_MAP = new HashMap<>();


    public <T> T getBean(Class<T> cl){
        return getContext().getBean(cl);
    }


    public OrivisContextable(OrivisContext context){
        this.context =  context;
    }

    public <T extends Serializable>T getCurrentUser(){
       return (T) getContext().getCurrentUser();
    }

    public static Logger getDefaultLogger() {
        return log;
    }
}
