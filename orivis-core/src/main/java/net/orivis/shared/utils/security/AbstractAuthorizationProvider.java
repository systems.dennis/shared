package net.orivis.shared.utils.security;

public interface AbstractAuthorizationProvider {
    String getLogin();
    String getPassword();
    String getToken();

}
