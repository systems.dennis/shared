package net.orivis.shared.utils.bean_copier;

import lombok.Data;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class ModelCopierDefinition {



    private final List<FieldDefinition> copyFields = new ArrayList<>();
    private final List<FieldDefinition> transientFields = new ArrayList<>();


    private final List<FieldDefinition> validationFields = new ArrayList<>();
    private final List<FieldDefinition> auditFields = new ArrayList<>();
    private final List<Field> allFields = new ArrayList<>();



    public FieldDefinition findField(String fieldName) {
        return copyFields.stream().filter(x-> x.getField().getName().equals(fieldName)).findFirst().orElse(null);
    }
    public FieldDefinition findTransient(String fieldName) {
        return transientFields.stream().filter(x-> x.getField().getName().equals(fieldName)).findFirst().orElse(null);
    }
    public FieldDefinition findAny(String fieldName) {
        var field = findField(fieldName);
        if (field == null) return findTransient(fieldName);
        return field;
    }

    public List<FieldDefinition> all() {
        var res = new  ArrayList<>(copyFields);
        res.addAll(transientFields);
        return res;
    }
    List<Field> allOfFields(){
        if (allFields.isEmpty()){
            allFields.addAll(transientFields.stream().map(FieldDefinition::getField).toList());
            allFields.addAll(copyFields.stream().map(FieldDefinition::getField).toList());
        }
        return  allFields;
    }
}
