package net.orivis.shared.utils.bean_copier;

import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.exceptions.AbstractTransformerException;
import net.orivis.shared.model.OrivisIDPresenter;

import java.io.Serializable;

public interface OrivisDataTransofrmer<ID_TYPE extends Serializable> {

    default boolean returnSameIfTypesAreEqual(Object from, Class<?> toType) {
        if (toType == null) {
            throw new AbstractTransformerException(getClass().getSimpleName());
        }
        return from == null || toType.isAssignableFrom(from.getClass());
    }

    <DB_TYPE extends OrivisIDPresenter< ID_TYPE>> Object transform(Object object, OrivisTranformer transformerDescription, Class<?> toType, OrivisContext context);
}
