package net.orivis.shared.utils;

@FunctionalInterface
public interface DataSupplier<T> {
    T get();
}
