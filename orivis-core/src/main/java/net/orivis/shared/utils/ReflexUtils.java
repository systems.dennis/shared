package net.orivis.shared.utils;

import net.orivis.shared.annotations.FormTransient;

import java.lang.reflect.Field;
import java.util.List;

public class ReflexUtils {
    public static final int TYPE_TRANSIENT_FIELDS = 1;
    public static final int TYPE_NON_TRANSIENT_FIELDS = 2;
    public static final int TYPE_ALL_FIELDS = 0;

    public static List<Field> getFields(Class<?> cl, List<Field> fields, int collectFieldType) {
        if (cl == null) {
            return null;
        }
        if (cl.getSuperclass().getSuperclass() != null) {

            getFields(cl.getSuperclass(), fields, collectFieldType);

        }

        var declaredFields = cl.getDeclaredFields();
        for (var field : declaredFields) {
            if (!java.lang.reflect.Modifier.isStatic(field.getModifiers())) {

                if (collectFieldType == TYPE_ALL_FIELDS) {
                    fields.add(field);
                }

                boolean isTransient = field.getAnnotation(FormTransient.class) != null ;
                boolean onlyTransient = collectFieldType == TYPE_TRANSIENT_FIELDS;
                if ((onlyTransient && isTransient) || (!onlyTransient && !isTransient)) {
                    fields.add(field);
                }
            }
        }
        return fields;
    }
}
