package net.orivis.shared.utils.bean_copier;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import net.orivis.shared.annotations.FormTransient;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.exceptions.DeveloperException;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Data
public class FieldDefinition {
    private Class сlazz;
    private Field field;
    private String id;

    private Method getter;
    private Method setter;
    private OrivisDataTransofrmer transformer;
    private OrivisTranformer transformerAnnotation;

    /**
     * Mates are the field definition which had been used to copy field to other object
     * So we skip cycling to fetch that fields again and again
     */
    private Map<String, FieldDefinition> mates = new HashMap<>();


    public FieldDefinition getMate(Class cl, MateFetcher fetcher) {
        var res = mates.get(cl.getSimpleName());

        if (res != null) {
            return res;
        }

        res = fetcher.fetch(cl);
        mates.put(cl.getSimpleName(), res);
        return res;
    }


    /**
     * Method may not set value, logging the cases in this case to TRACE!
     * @param from and object of class to copy from
     * @param to - an object to copy to
     * @param ignoreTransience - this will copy field even if it is marked as FormTransient
     * @param context - a standard orivis context
     */
    public void copy(Object from, Object to, boolean ignoreTransience, OrivisContext context) {


        if (to == null || from == null) {
            log.trace("to == null or from == null");
            return;
        }
        var fieldBuddy = getMate(to.getClass(), (cl)-> ModelCache.get(to.getClass()).findAny(this.field.getName()));
        if (fieldBuddy == null) {
            log.trace("No field {}  found in {}", field.getName(), to.getClass().getName());
            return;
        }

        if (fieldBuddy.isTransient() && !ignoreTransience) {
            log.trace("Skipping transient: {} -> {}", from, to);
            return;
        }
        //now we have a field buddy and need to perform setter and getter from each


        try {
            fieldBuddy.setter.invoke(to, this.getTransformedValue(context, from));
        } catch (Exception e) {
            log.trace("Cannot copy field: Stage: setter ", e);
        }
    }

    public Object getTransformedValue(OrivisContext context, Object from)  {
        try {
            FieldDefinition any = ModelCache.get(from.getClass()).findAny(this.field.getName());
            // need getter from from

            Object res = any.getter.invoke(from);
            if (this.transformer != null) {
                res = transformer.transform(res, transformerAnnotation, this.field.getType(), context);
            }
            return res;
        } catch (Exception e){
            log.trace("Cannot copy field Stage: getter ", e);
            return null;
        }

    }

    public void setValue(Object object, Object value){
        try {
            this.setter.invoke(object, value);
        } catch (Exception e) {
            throw new DeveloperException("Cannot set value for object", object.toString());
        }
    }

    boolean isTransient() {
        return field.getAnnotation(FormTransient.class) != null;
    }

    @Override
    public String toString() {
        return "FieldDefinition{" +
                "field=" + field +
                ", сlazz=" + сlazz +
                ", id='" + id + '\'' +
                ", getter=" + getter +
                ", setter=" + setter +
                ", transformer=" + transformer +
                ", transformerAnnotation=" + transformerAnnotation +
                '}';
    }

    public <T> T readValue(Object model) {
        try {
            return (T) this.getter.invoke(model);
        } catch (Exception e) {
            throw new DeveloperException("Cannot set value for object", model.toString());
        }
    }
}

interface MateFetcher {
    FieldDefinition fetch(Class cl);
}
