package net.orivis.shared.utils;

import lombok.Data;

import java.util.List;
import java.util.Objects;

@Data
public class PojoListField {


    private Boolean visible;
    private Boolean searchable;
    private Boolean filterable;
    private Boolean sortable;
    private Integer order;
    private List<WebAction> actions;
    private Boolean showContent;
    private String format;

    private String type;

    private String remoteType;

    private String searchField;
    private String searchName;

    private String searchType;
    private String subType;
    private String css;
    private String field;
    private String translation;
    private String group = null;
    private Boolean customized = false;

    public boolean equals(Object o) {
        if (o == null) return false;
        if (o.getClass() != this.getClass()) {
            return false;
        }

        return Objects.equals(((PojoListField) o).getField(), getField());
    }
}
