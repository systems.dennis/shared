package net.orivis.shared.utils.bean_copier;

import lombok.extern.slf4j.Slf4j;
import net.orivis.shared.annotations.Validation;
import net.orivis.shared.annotations.entity.*;
import net.orivis.shared.exceptions.DeveloperException;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;




@Slf4j
public class ModelCache {

    private static final Map<String, ModelCopierDefinition> cache = new HashMap<>();
    private static final Map<String, Object> singletones = new HashMap<>();



    public static ModelCopierDefinition get(Class cl) {
        if (isCacheEnabled(cl) && cache.containsKey(cl.getName())) {
            return cache.get(cl.getName());
        }


        var deifinition = createDefinition(cl);

        if (isCacheEnabled(cl)) cache.put(cl.getName(), deifinition);
        return deifinition;
    }

    public static <T> T getInstance(Class<T> c) {
        if (c == null) return null;
        if (!singletones.containsKey(c.getName())) {
            try {
                singletones.put(c.getName(), c.getConstructor().newInstance());
            } catch (Exception e) {

                throw new DeveloperException("Cannot instantiate object of class", c.getName());
            }
        }
        return (T) singletones.get(c.getName());

    }

    private static synchronized ModelCopierDefinition createDefinition(Class cl) {
        ModelCopierDefinition definition = new ModelCopierDefinition();

        var nonTransientFields = BeanCopier.getFields(cl, new ArrayList<>() , BeanCopier.TYPE_NON_TRANSIENT_FIELDS );

        definition.getCopyFields().addAll(nonTransientFields.stream().map(ModelCache::to).toList()) ;
        var transientFields = BeanCopier.getFields(cl, new ArrayList<>() , BeanCopier.TYPE_TRANSIENT_FIELDS );
        definition.getTransientFields().addAll(transientFields.stream().map(ModelCache::to).toList()) ;

        BeanCopier.withEach(cl, OrivisAuditable.class, (x)-> {definition.getAuditFields().add(ModelCache.to(x));});

        BeanCopier.withEach(cl, Created.class, (x)-> {definition.getAuditFields().add(ModelCache.to(x));});
        BeanCopier.withEach(cl, CreatedBy.class, (x)-> {definition.getAuditFields().add(ModelCache.to(x));});
        BeanCopier.withEach(cl, Updated.class, (x)-> {definition.getAuditFields().add(ModelCache.to(x));});
        BeanCopier.withEach(cl, UpdatedBy.class, (x)-> {definition.getAuditFields().add(ModelCache.to(x));});

        BeanCopier.withEach(cl, Validation.class, (x)-> {definition.getValidationFields().add(ModelCache.to(x));});
        return definition;
    }

    private static FieldDefinition to(Field field) {

        FieldDefinition definition = new FieldDefinition();

        definition.setField(field);
        definition.setСlazz(field.getDeclaringClass());
        fetchTransformer(field, definition);
        definition.setId(field.getName());

        definition.setGetter(BeanCopier.findFieldAssociatedMethod(field.getDeclaringClass(), field, BeanCopier.GETTER_PREFIX ));
        definition.setSetter(BeanCopier.findFieldAssociatedMethod(field.getDeclaringClass(), field, BeanCopier.SETTER_PREFIX ));
        return definition;
    }

    private static void fetchTransformer(Field field, FieldDefinition definition) {
        try {
            var annotation = field.getAnnotation(OrivisTranformer.class);
            if (annotation == null) {
                return;
            }
            definition.setTransformer(annotation.transformWith().getConstructor().newInstance());
            definition.setTransformerAnnotation(annotation);
        } catch (Exception e) {
            log.trace("error: fetching transformer for field {}", field.getName());

        }
    }

    private static boolean isCacheEnabled(Class cl) {
        CloneParameters params = (CloneParameters) cl.getAnnotation(CloneParameters.class);
        return params == null || !params.disableCache();
    }
}
