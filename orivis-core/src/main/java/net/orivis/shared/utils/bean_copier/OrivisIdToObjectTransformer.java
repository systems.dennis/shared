package net.orivis.shared.utils.bean_copier;

import lombok.SneakyThrows;
import org.apache.logging.log4j.util.Strings;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.entity.AbstractOrivisEntity;
import net.orivis.shared.exceptions.TransformException;
import net.orivis.shared.model.OrivisIDPresenter;
import net.orivis.shared.service.AbstractService;

import java.io.Serializable;

/**
 * Objects in the system for today can contain of String or Longs (more or less this is default behaviour)
 * In feature by request this can be transformed to the service implemented in each individual case separately
 * @param <ID_TYPE> - a type of ID of the object
 */
public class OrivisIdToObjectTransformer<ID_TYPE extends Serializable> implements OrivisDataTransofrmer<ID_TYPE> {

    @SneakyThrows
    @Override
    public <DB_TYPE extends OrivisIDPresenter<ID_TYPE>> Object transform(Object object, OrivisTranformer transformerDescription, Class<?> toType, OrivisContext context) {
        if (object == null) {
            return null;
        }

        if (object.getClass() == toType) {
            return object;
        }

        if (object instanceof Long && object.equals(0L)) {
            return null;
        }

        if (object.getClass() == String.class && Strings.isBlank((String) object)){
            return null;
        }

        if (isFormObjectTOId(toType)){
            return ((AbstractOrivisEntity<?>) object).getId();
        }
        //so get Object By id
        return  ((AbstractService<DB_TYPE , ID_TYPE>) context.getBean(transformerDescription.additionalClass()))
                .findById((ID_TYPE) object)
                .orElseThrow(()-> new TransformException(String.valueOf(object)));
    }

    public boolean isFormObjectTOId(Class<?> toType) {
        return toType == Long.class || toType == String.class;
    }
}
