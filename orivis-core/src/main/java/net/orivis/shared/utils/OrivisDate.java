package net.orivis.shared.utils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import net.orivis.shared.exceptions.StandardException;

import java.io.Serializable;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;

@Data
public class OrivisDate implements Serializable {

    public static final String CURRENT_TIME_ZONE = Calendar.getInstance().getTimeZone().getID();
    private Date date;
    private Long milliseconds;
    private String zone;

    private String template;

    @JsonIgnore
    private Calendar calendar;
    public static OrivisDate of (Long date){

       return new OrivisDate(ofDate(date));

    }

    private static Calendar ofDate(Long date){
        if (date == null) return  null;
        var res  =  Calendar.getInstance();
        res.setTimeInMillis(date);
        return res;
    }
    public static OrivisDate of (Date date){
        if (date == null) return  null;
        return  of(date.getTime());
    }

    public OrivisDate(){
        this(Calendar.getInstance());
    }

    public OrivisDate(Calendar calendar){
        if (calendar == null){
            throw new StandardException("", "calendar_has_no_data");
        }

        date = calendar.getTime();
        zone = CURRENT_TIME_ZONE;
        milliseconds = date.getTime();
    }

    public static OrivisDate now(){
        return  OrivisDate.of(new Date());
    }

    public OrivisDate ts(String timeZone){
        zone = timeZone;
        return  this;
    }
    public OrivisDate template(String template){
        this.template = template;
        return  this;
    }

    public OrivisDate UTC(){
        if (Objects.equals(zone, "UTC")){
            return this;
        }
        var calendar = Calendar.getInstance(TimeZone.getTimeZone(ZoneId.of("UTC")));
        calendar.setTimeInMillis(milliseconds);
        return new OrivisDate(calendar);
    }

    public Date get(){
        return  this.date;
    }


}
