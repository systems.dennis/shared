package net.orivis.shared.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;
import net.orivis.shared.model.OrivisIDPresenter;

import java.io.Serializable;
import java.util.Optional;

@NoRepositoryBean @Repository
public interface AbstractFilterRepo<ENTITY extends OrivisIDPresenter<ID_TYPE>, ID_TYPE extends Serializable> {


    /**
     * @deprecated use this method only when you are sure it is needed. Otherwise use Service analog method
     */
    @Deprecated
    Optional<ENTITY> filteredOne(OrivisFilter<?> one);

    /**
     * @deprecated use this method only when you are sure it is needed. Otherwise use Service analog method
     */
    @Deprecated
    Optional<ENTITY> filteredFirst(OrivisFilter<?> first);

    Optional<ENTITY> filteredFirst(OrivisFilter<?> first, Sort sort);
    /**
     * @deprecated use this method only when you are sure it is needed. Otherwise use Service analog method
     */
    @Deprecated
    Page<ENTITY> filteredData(OrivisFilter<?> specification);
    Page<ENTITY> filteredData(OrivisFilter<?> specification, Sort sort);
    /**
     * @deprecated use this method only when you are sure it is needed. Otherwise use Service analog method
     */
    @Deprecated
    Page<ENTITY> filteredData(OrivisFilter<?> specification, Pageable pageable);
    Page<ENTITY> filteredData(OrivisFilter<?> specification, Pageable pageable, Sort sort);
    /**
     * @deprecated use this method only when you are sure it is needed. Otherwise use Service analog method
     */
    @Deprecated
    default boolean exists(OrivisFilter<?> specification){
        return filteredCount(specification) > 0;
    }
    /**
     * @deprecated use this method only when you are sure it is needed. Otherwise use Service analog method
     */
    @Deprecated
    long filteredCount(OrivisFilter<?> filter);

    @Deprecated
    void deleteData(OrivisFilter<?> spec);
}
