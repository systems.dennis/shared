package net.orivis.shared.repository;

import net.orivis.shared.exceptions.StandardException;
import net.orivis.shared.model.OrivisIDPresenter;
import net.orivis.shared.utils.OrivisContextable;

import java.io.Serializable;
import java.util.List;

public interface OrivisFilter<T extends OrivisIDPresenter<?>> {

    String EQUALS_OPERATOR = "equals";
    String STARTS_WITH_OPERATOR = "startsWith";
    String NOT_STARTS_WITH_OPERATOR = "notStartsWith";
    String ENDS_WITH_OPERATOR = "endsWith";
    String NOT_ENDS_WITH_OPERATOR = "notEndsWith";
    String MORE_THEN = "gt";
    String LESS_THEN = "lt";
    String LESS_EQUALS = "le";
    String MORE_EQUALS = "ge";
    String CONTAINS_OPERATOR = "contains";
    String IN = "in";
    String NOT_IN = "not_in";
    String NOT_CONTAINS_OPERATOR = "_nc_";
    String NOT_EMPTY = "_ne_";
    String EMPTY = "_em_";
    String NOT_EQUALS_OPERATOR = "notEquals";
    String NULL_OPERATOR = "null";
    String NOT_NULL_OPERATOR = "notNull";

    <E extends OrivisFilter<T>>E operator(String field, Object value, String type);

    <E extends OrivisFilter<?>>E and(E filter);

    <E extends OrivisFilter<?>>E or(E filter);

    default <E extends OrivisFilter<T>>E  eq(String field, Object value){
        return (E) operator(field, value, EQUALS_OPERATOR);
    }

    default <E extends OrivisFilter<T>>E  id(Object value){
        return (E) operator(OrivisIDPresenter.ID_FIELD, value, EQUALS_OPERATOR);
    }

    default <E extends OrivisFilter<T>>E  idNot(Object value){
        return (E) operator(OrivisIDPresenter.ID_FIELD, value, NOT_EQUALS_OPERATOR);
    }

    /*
    since version 3.3.0 there is no more assignable predefined field name and field should be annotated by CreatedBy
     */
    default <E extends OrivisFilter<T>>E  ofUser(Class<?> cl, Serializable user){
        return (E) operator(getUserField(cl), user, EQUALS_OPERATOR);
    }

    default <E extends OrivisFilter<T>>E  notEq(String field, Object value){
        return (E) operator(field, value, NOT_EQUALS_OPERATOR);
    }
    default <E extends OrivisFilter<T>>E  notIN(String field, List<?> value){
        return (E) operator(field, value, NOT_IN);
    }
    default  <E extends OrivisFilter<T>>E  iN(String field, List<?> value){
        return (E) operator(field, value, IN);
    }

    default <E extends OrivisFilter<T>>E  contains(String field, String value){
        return (E) operator(field, value, CONTAINS_OPERATOR);
    }

    default <E extends OrivisFilter<T>>E  notContains(String field, String value){
        return (E) operator(field, value, NOT_CONTAINS_OPERATOR);
    }
    default <E extends OrivisFilter<T>>E  greater(String field, Object value){
        return (E) operator(field, value, MORE_THEN);
    }

    default <E extends OrivisFilter<T>>E  greatEQ(String field, Object value){
        return (E) operator(field, value, MORE_EQUALS);
    }

    default <E extends OrivisFilter<T>>E  lessEQ(String field, Object value){
        return (E) operator(field, value, LESS_EQUALS);
    }
    default <E extends OrivisFilter<T>>E  less(String field, Object value){
        return (E) operator(field, value, LESS_THEN);
    }

    default <E extends OrivisFilter<T>>E  startsWith(String field, String value){
        return (E) operator(field, value, STARTS_WITH_OPERATOR);
    }

    default <E extends OrivisFilter<T>>E  notStartsWith(String field, String value){
        return (E) operator(field, value, NOT_STARTS_WITH_OPERATOR);
    }
    default <E extends OrivisFilter<T>>E  endsWith(String field, String value){
        return (E) operator(field, value, ENDS_WITH_OPERATOR);
    }
    default <E extends OrivisFilter<T>>E  empty(String field){
        return (E) operator(field, null, EMPTY);
    }
    default <E extends OrivisFilter<T>>E  notEmpty(String field){
        return (E) operator(field, null, NOT_EMPTY);
    }
    default <E extends OrivisFilter<T>>E  isNull(String field){
        return (E) operator(field, null, NULL_OPERATOR);
    }
    default <E extends OrivisFilter<T>>E  notNull(String field){
        return (E) operator(field, null, NOT_NULL_OPERATOR);
    }
     default <E extends OrivisFilter<T>>E notEndsWith(String field, String value){
        return (E) operator(field, value, NOT_ENDS_WITH_OPERATOR);
    }
      <E extends OrivisFilter<T>>E comparasionType(Class<?> type);

    boolean isClosed();

    String getField();
    /**
     * When used, next query changes should replace this result
     * @return query
     */
    default    <E extends OrivisFilter<T>>E  empty(){
        return (E) operator("id", null, NOT_EMPTY);
    }

    OrivisFilter<T> setInsensitive(boolean insensitive);
    OrivisFilter<T> setComplex(boolean complex);
    OrivisFilter<T> setJoinOn(String on);

    Serializable getIdValue(Object id);
    
    boolean isEmpty();


    String getOperator();

    Object getValue();

    Class<?> getFieldClass();

    boolean isComplex();

    default String getUserField(Class cl){
        try {
            return OrivisContextable.CREATED_FIELDS_MAP.get(cl).getName();
        } catch (Exception e){
            throw new StandardException(cl.getName(), "global.app.field_is_not_able_to_be_found_as_creator");
        }
    }

    String getOn();

    <T>T getQueryRoot();
}
