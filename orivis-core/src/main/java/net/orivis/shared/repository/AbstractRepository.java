package net.orivis.shared.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;
import net.orivis.shared.model.OrivisIDPresenter;

import java.io.Serializable;


@Repository
@NoRepositoryBean
public interface AbstractRepository<ENTITY extends OrivisIDPresenter<ID_TYPE>, ID_TYPE extends Serializable> extends CrudRepository<ENTITY, ID_TYPE>, AbstractFilterRepo<ENTITY, ID_TYPE> {

    Page<ENTITY> findAll(Pageable pageRequest);



    /**
     * @param entity must not be {@literal null}.
     * @param <S>
     * @return
     * @deprecated Use getService() save method instead of repository. Using this method can provoke different errors and unexpected behaviour
     */
    @Deprecated
    <S extends ENTITY> S save(S entity);


}
