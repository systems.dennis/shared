package net.orivis.shared.importer.exception;

public class ImportException extends RuntimeException {
    public ImportException(String s) {
        super(s);
    }
}
