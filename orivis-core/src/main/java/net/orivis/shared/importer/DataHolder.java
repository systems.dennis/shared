package net.orivis.shared.importer;

import lombok.Data;

import java.util.List;

@Data
public class DataHolder <T> {
    private List<T> value;
}
