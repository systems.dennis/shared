package net.orivis.shared.importer;

import jakarta.persistence.Column;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.apache.logging.log4j.util.Strings;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.entity.AbstractOrivisEntity;
import net.orivis.shared.importer.exception.ImportException;
import net.orivis.shared.utils.bean_copier.BeanCopier;

import java.lang.reflect.Field;
import java.util.List;

@NoArgsConstructor
public class PojoByFieldChecker implements PojoExistsChecker {
    @SneakyThrows
    @Override
    public AbstractOrivisEntity exists(Import imp, AbstractOrivisEntity entity, OrivisContext context, String[] params) {

        check(entity, params);


        Field field = BeanCopier.findField(params[0], entity.getClass());

        field.setAccessible(true);

        Object value = field.get(entity);

        if (value == null) {
            throw new ImportException("Field " + field.getName() + " is seems to be empty, but it should not");
        }

        field.setAccessible(false);

        String fieldName = field.getName();
        var annName = field.getAnnotation(Column.class);
        if (annName != null && !Strings.isEmpty(annName.name())) {
            fieldName = field.getAnnotation(Column.class).name();
        }


        var cs = context.getFilterProvider().eq(fieldName, value).setInsensitive(true);

        List<? extends AbstractOrivisEntity> res = context.getBean(imp.pojoChecker().serviceClass()).find(cs, 1, 0).getContent();


        return res.size() > 0 ? res.get(0) : null;
    }

    private void check(AbstractOrivisEntity entity, String[] params) {
        if (entity == null) {
            throw new ImportException("Entity is null");
        }

        if (params == null || params.length < 1) {
            throw new ImportException("No params set for the PojoChecker " + getClass());
        }
    }
}
