package net.orivis.shared.importer;

import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.form.AbstractOrivisForm;

public interface AbstractConditionalImport<Form extends AbstractOrivisForm> {
    Boolean satisfy(Form form, OrivisContext context);
}
