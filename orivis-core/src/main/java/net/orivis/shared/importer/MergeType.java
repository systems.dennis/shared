package net.orivis.shared.importer;

public enum MergeType {
    UPDATE, SKIP, ADD_NEW
}
