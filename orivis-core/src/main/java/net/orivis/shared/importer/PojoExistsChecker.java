package net.orivis.shared.importer;

import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.entity.AbstractOrivisEntity;

import java.io.Serializable;

public interface PojoExistsChecker {
    <ID_TYPE extends Serializable> AbstractOrivisEntity<ID_TYPE> exists (Import imp, AbstractOrivisEntity<ID_TYPE> entity, OrivisContext context, String [] params);
}
