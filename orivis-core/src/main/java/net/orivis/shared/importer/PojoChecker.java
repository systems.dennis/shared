package net.orivis.shared.importer;


import net.orivis.shared.service.AbstractService;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface PojoChecker {

    PojoChecker NO_CHECKER = new PojoChecker(){
        @Override
        public Class<? extends Annotation> annotationType() {
            return PojoChecker.class;
        }

        @Override
        public Class<? extends AbstractService> serviceClass() {
            return null;
        }

        @Override
        public Class<? extends PojoExistsChecker> pojoChecker() {
            return null;
        }

        @Override
        public String[] params() {
            return new String[0];
        }
    };
    Class <? extends AbstractService> serviceClass();
    Class<? extends PojoExistsChecker> pojoChecker();
    String [] params();
}