package net.orivis.shared.importer;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import net.orivis.shared.annotations.OrivisService;
import org.springframework.core.env.Environment;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.controller.items.OrivisTransformable;
import net.orivis.shared.entity.ImportFilter;
import net.orivis.shared.form.AbstractOrivisForm;
import net.orivis.shared.importer.exception.ImportException;
import net.orivis.shared.model.OrivisIDPresenter;
import net.orivis.shared.service.AbstractService;

import java.io.Serializable;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.*;
import java.util.stream.Collectors;

import static net.orivis.shared.utils.Mapper.mapper;

public interface Importer<DB_TYPE extends OrivisIDPresenter<ID_TYPE>, FORM extends AbstractOrivisForm, ID_TYPE extends Serializable> extends OrivisTransformable<DB_TYPE, FORM> {
    Set<String> importedClasses = new HashSet<>();

    default List<FORM> fetchAndStore(OrivisContext context) {
        String currentClass = getClass().getSimpleName();

        String propertiesKey = "global.app.lock_same_import_in_the_same_time";
        Boolean isParallelImportForbidden = getContext().getEnv(propertiesKey, true);
        try {
            if (importedClasses.contains(currentClass) && isParallelImportForbidden) {
                throw new ImportException(" this class " + getClass().getSimpleName() + " is currently being imported");
            } else {
                importedClasses.add(currentClass);
                return storeData(fetchData(context), context);
            }
        } finally {
            importedClasses.remove(currentClass);
        }
    }

    @SneakyThrows
    default List<DB_TYPE> fetchData(OrivisContext context) {
        Import imp = getClass().getAnnotation(Import.class);
        if (imp == null) {
            throw new ImportException("Annotation @Import is not set for the class " + this.getClass());
        }

        ObjectMapper mapper = getObjectMapper();
        Environment env = context.getBean(Environment.class);

        var property = env.getProperty(imp.pathKey());
        property = modifyPath(property);
        if(property == null){
            throw new ImportException("There is no property for path" + imp.pathKey());
        }

        HttpClient client = HttpClient.newBuilder().authenticator(new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(
                        context.getEnv(imp.auth().login()),
                        ((String)context.getEnv(imp.auth().password())).toCharArray());
            }
        }).build();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(property))
                .GET()
                .build();

        var string = client.send(request, HttpResponse.BodyHandlers.ofString());
        var str = string.body();
        List<FORM> result;
        try {

            if (imp.dataHolder() != DataHolder.class) {
                for (String key : keys()) {
                    str = str.replaceAll("\"" + key + "\":", "\"" + keyToKey(key) + "\":");
                }
                result = mapper.readValue(str, imp.dataHolder()).getValue();
            } else {
                result = List.of((FORM[]) mapper.readValue(str, imp.returnType()));

            }
        } catch (Exception e) {
            e.printStackTrace();
            result = new ArrayList<>();
        }

        Class<? extends AbstractOrivisForm> formClass;

            formClass = getService().getClass().getAnnotation(OrivisService.class).form();

        if (ImportFilter.class.isAssignableFrom(formClass)){
            result = result.stream().filter(f -> {
                var element = (ImportFilter)f;
                return element.accept();
            }).collect(Collectors.toList());
        }

        result = filterImport(result);

        result = result.stream().map(this::beforeItemImported).collect(Collectors.toList());

        preImport();

        PojoExistsChecker exPojoExistsChecker = imp.pojoChecker().pojoChecker().getConstructor().newInstance();

        boolean toCheck = imp.conditional() != AbstractConditionalImport.class;
        List<DB_TYPE> subResult = null;
        if (toCheck) {
            AbstractConditionalImport<FORM> obj = null;
            try {
                obj = imp.conditional().getConstructor().newInstance();
            } catch (Exception e) {
                throw new ImportException("There is no constructor for class: " + this.getClass() + ": " + e);
            }
            AbstractConditionalImport<FORM> finalObj = obj;

            subResult = result.stream().filter(form -> finalObj.satisfy(form, getContext())).map(this::fromForm).collect(Collectors.toList());
        } else {
            subResult = result.stream().map(this::fromForm).collect(Collectors.toList());
        }

        List<DB_TYPE> endList = new ArrayList<>();

        for (var item : subResult) {

            if (imp.mergeType() != MergeType.ADD_NEW) {
                //do nothing
            } else if (imp.pojoChecker().pojoChecker().equals(PojoChecker.NO_CHECKER)) {
                endList.add(item);
            }

            if (imp.mergeType() == MergeType.UPDATE) {
                var existElement = exPojoExistsChecker.exists(imp, item, context, imp.pojoChecker().params());
                if (existElement != null) {
                    item.setId(existElement.getId());
                }
                endList.add(item);
            }

            if (imp.mergeType() == MergeType.SKIP) {
                var existElement = exPojoExistsChecker.exists(imp, item, context, imp.pojoChecker().params());

                if (existElement != null) {
                    //do nothing
                } else {
                    endList.add(item);
                }
            }

            if (imp.mergeType() == MergeType.ADD_NEW) {
                endList.add(item);
            }
        }
        return endList;
    }

    default String modifyPath(String path){
        return path;
    }

    default ObjectMapper getObjectMapper() {
        return mapper;
    }

    default List<FORM> storeData(List<DB_TYPE> data, OrivisContext context) {

        List<FORM> saved = new ArrayList<>();

        data.forEach(x -> {
            try {
                DB_TYPE si = getService().save(x);
                saved.add(toForm(si));
            } catch (Exception e) {
                getService().getLogger().error("Could not save item: " + x.asValue(), e);
            }
        });
        afterImport(saved, data);

        return saved;
    }

    default void afterImport(List<FORM> saved, List<DB_TYPE> types) {
    }

    <E, T extends AbstractService<DB_TYPE, ID_TYPE>> T getService();


    default Map<String, String> keyReplacement() {
        return Collections.emptyMap();
    }

    default String keyToKey(String value) {
        return keyReplacement().get(value);
    }

    default Set<String> keys() {
        return keyReplacement().keySet();
    }

    default FORM beforeItemImported(FORM item) {
        return item;
    }

    default void preImport(){}

    default List<FORM> filterImport(List<FORM> forms){
        return forms;
    }
}
