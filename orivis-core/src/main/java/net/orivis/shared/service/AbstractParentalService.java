package net.orivis.shared.service;

import org.springframework.data.domain.Page;
import net.orivis.shared.model.OrivisIDPresenter;
import net.orivis.shared.repository.OrivisFilter;

import java.io.Serializable;

public interface AbstractParentalService<DB_TYPE extends OrivisIDPresenter<ID_TYPE>, ID_TYPE extends Serializable> extends AbstractService<DB_TYPE, ID_TYPE> {

    default String getParentField() {
        return "parent";
    }


    default long countByParent(ID_TYPE id) {

        OrivisFilter<DB_TYPE> specification = getFilterImpl().eq(getParentField(), findByIdOrThrow(updateId(id)));
        return getRepository().filteredCount(specification.and(getAdditionalSpecification()));
    }

    default long countByRoot(DB_TYPE x) {
        OrivisFilter<DB_TYPE> specification = getFilterImpl().eq(getParentField(), x);
        return count(specification.and(getAdditionalSpecification()));
    }


    default Page<DB_TYPE> findRootElements(int page, int limit) {
        OrivisFilter<DB_TYPE> specification = getFilterImpl().isNull(getParentField());
        return find(specification.and(getAdditionalSpecification()),  limit, page);
    }

    default Page<DB_TYPE> findByParent(ID_TYPE parentId, int limit, int page) {
        OrivisFilter<DB_TYPE> specification = getFilterImpl().eq(getParentField(), findByIdOrThrow(updateId(parentId)));
        return find(specification.and(getAdditionalSpecification()),  limit, page);
    }
}
