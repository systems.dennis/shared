package net.orivis.shared.service;

import org.slf4j.Logger;
import net.orivis.shared.annotations.DeleteStrategy;
import net.orivis.shared.annotations.security.ISecurityUtils;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.exceptions.DeleteNotPossibleException;
import net.orivis.shared.model.OrivisIDPresenter;
import net.orivis.shared.repository.OrivisFilter;
import net.orivis.shared.utils.bean_copier.BeanCopier;

import java.io.Serializable;
import java.lang.reflect.Field;

import static net.orivis.shared.annotations.DeleteStrategy.*;

public interface DeleteObject<DB_TYPE extends OrivisIDPresenter<ID_TYPE>, ID_TYPE extends Serializable> {

    default DB_TYPE delete(DB_TYPE object, AbstractService<DB_TYPE, ID_TYPE> service) {

        if (getSelfOnly(service.getClass())){
            service.getContext().getBean(ISecurityUtils.class).isMy(object);
        }

        int strategy = getDeleteStrategy(service.getClass());


        if (strategy == DELETE_STRATEGY_DB) {
            service.getRepository().delete(object);
            saveDeleteHistoryIfRequired(object, service);
            return object;

        } else if (strategy ==  DELETE_STRATEGY_PROPERTY) {
            Field field = BeanCopier.findField(getDeleteField(getClass()), object.getClass());;
            try {
                field.setAccessible(true);
                field.set(object, Boolean.TRUE);
                field.setAccessible(false);
                service.getRepository().save(object);
                saveDeleteHistoryIfRequired(object, service);
                return object;
            } catch (Exception e) {
                throw new DeleteNotPossibleException(e.getMessage(), field.getName());
            }

        }



        throw new DeleteNotPossibleException("global.wrong.delete.strategy", strategy);
    }

    /**
     * Save object after it was deleted from DB
     * if "global.save.deleted=true"
     *
     * @param deletedObject deleted object with ID
     */
    default void saveDeleteHistoryIfRequired(DB_TYPE deletedObject, AbstractService<DB_TYPE, ID_TYPE> service) {
        try {
            //todo move
//            Boolean isRequired = getStoreHistoryRequired(service.getClass());
//            if (isRequired) {
//                var deleteHistoryModel = DeleteHistoryModel.from(deletedObject);
//                service.getContext().getBean(DeleteHistoryService.class).save(deleteHistoryModel);
//            }
        } catch (Exception e) {
            getLogger().error("Cannot save original version for object", e);
        }
    }

    Logger getLogger();

    default int getDeleteStrategy(Class<?> cl) {
        var ann = cl.getAnnotation(DeleteStrategy.class);
        if (ann == null) {
            return DELETE_STRATEGY_DB;
        }
        return ann.value();
    }
    default boolean getSelfOnly(Class<?> cl) {
        var ann = cl.getAnnotation(DeleteStrategy.class);
        if (ann == null) {
            return false;
        }
        return ann.selfOnly();
    }

    default String getDeleteField(Class<?> cl) {
        var ann = cl.getAnnotation(DeleteStrategy.class);
        if (ann == null) {
            return DELETE_PROPERTY;
        }
        return ann.field();
    }

    default boolean getStoreHistoryRequired(Class<?> cl) {
        var ann = cl.getAnnotation(DeleteStrategy.class);
        if (ann == null) {
            return false;
        }
        return ann.storeDeleteEventHistory();
    }

    /**
     * This method can be used as start point and will never produce null
     *
     * @return Query case
     */
    default OrivisFilter<DB_TYPE> getNotDeletedQuery() {

        var query =  getFilterImpl().notNull(OrivisIDPresenter.ID_FIELD);

       if (getDeleteStrategy(getClass()) == DELETE_STRATEGY_PROPERTY) {
            query = query.and(getFilterImpl().notEq(getDeleteField(getClass()), true));
        }
        return query;
    }

   OrivisFilter<DB_TYPE> getFilterImpl();

    OrivisContext getContext();
}
