package net.orivis.shared.service;

import java.io.Serializable;

public interface DeleteService {
    void deleteById(Serializable id);
}
