package net.orivis.shared.service;

import lombok.SneakyThrows;
import net.orivis.shared.annotations.NeverNullResponse;
import net.orivis.shared.annotations.CacheStrategy;
import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.annotations.security.ISecurityUtils;
import net.orivis.shared.audit.AuditProcessor;
import net.orivis.shared.beans.AbstractEditDeleteHistoryBean;
import net.orivis.shared.beans.IdValidator;
import net.orivis.shared.beans.OrivisFilterProvider;
import net.orivis.shared.beans.cache.AbstractCacheProvider;
import net.orivis.shared.beans.cache.CacheElementChecker;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.controller.orivis.OrivisRequest;
import net.orivis.shared.entity.AbstractOrivisEntity;
import net.orivis.shared.entity.KeyValue;
import net.orivis.shared.entity.OrivisID;
import net.orivis.shared.exceptions.*;
import net.orivis.shared.form.AbstractOrivisForm;
import net.orivis.shared.model.OrivisIDPresenter;
import net.orivis.shared.pojo_view.list.PojoListView;
import net.orivis.shared.repository.AbstractRepository;
import net.orivis.shared.repository.OrivisFilter;
import net.orivis.shared.utils.DataSupplier;
import net.orivis.shared.utils.ErrorSupplier;
import net.orivis.shared.utils.Supplier;
import net.orivis.shared.utils.bean_copier.BeanCopier;
import net.orivis.shared.utils.bean_copier.ObjectDefinition;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.springframework.core.io.UrlResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.time.Duration;
import java.util.*;

import static net.orivis.shared.annotations.DeleteStrategy.DELETE_STRATEGY_PROPERTY;
import static net.orivis.shared.controller.forms.OrivisServiceable.findDeclaredClass;
import static net.orivis.shared.service.AbstractPaginationService.ORIVIS_OPERATION_AFTER_DELETE;
import static net.orivis.shared.service.AbstractPaginationService.ORIVIS_OPERATION_PRE_DELETE;
import static net.orivis.shared.utils.bean_copier.BeanCopier.findField;

/**
 * Implementation should implement methods which are necessary for web service to work properly
 * In this implementation all methods, like add, edit, delete or list are in the same interface. Form more complex functionality, it can make sence to move all this logical functions to the separate service
 *
 * @param <DB_TYPE> A class of the entity to be saved. Should be persistant entity, in the best an implementation of {@link AbstractOrivisForm}
 */
public interface AbstractService<DB_TYPE extends OrivisIDPresenter<ID_TYPE>, ID_TYPE extends Serializable> extends DeleteObject<DB_TYPE, ID_TYPE> {
    /**
     * A simple fetch all implementation
     *
     * @return list of all records of T
     */
    List<DB_TYPE> find();

    /**
     * Updates an object in DB
     *
     * @param object - object to be updated, with ID
     * @return Updated T object
     * @throws ItemNotUserException                Exceptions on thrown when trying to update and object by user, not owner of the object
     * @throws ItemNotFoundException               When object cannot be found by id of the #object, this exception is thrown
     * @throws UnmodifiedItemSaveAttemptException  Exception says, that there are no changes in the object which should be updated
     * @throws ItemDoesNotContainsIdValueException Exception is throw when Objects has null ID
     */
    DB_TYPE edit(DB_TYPE object) throws ItemNotUserException, ItemNotFoundException, UnmodifiedItemSaveAttemptException, ItemDoesNotContainsIdValueException;

    KeyValue editField(ID_TYPE id, KeyValue keyValue) throws ItemNotUserException, ItemNotFoundException, UnmodifiedItemSaveAttemptException, ItemDoesNotContainsIdValueException, IllegalAccessException, InvocationTargetException;

    /**
     * Removes object from DB
     *
     * @param id - id of the object to be removed
     * @throws ItemNotUserException  Exceptions are thrown on trying to update and object by user, not owner of the object
     * @throws ItemNotFoundException When object cannot be found by id of the #object, this exception is thrown
     */
    default void delete(ID_TYPE id) throws ItemNotUserException, ItemNotFoundException {
        var object = findById(id).orElseThrow(() -> ItemNotFoundException.fromId(id));
        getContext().getBean(AuditProcessor.class).processAudit(object, null, ORIVIS_OPERATION_PRE_DELETE);
        preDelete(object);
        object = delete(object, this);
        afterDelete(object);
        var deleteHistory = getContext().getBean(AbstractEditDeleteHistoryBean.class);
        if (deleteHistory.isEnabled()) {
            deleteHistory.delete(id, object);
        }

        var provider = getCacheProvider();
        if (provider != null) {
            provider.removeFromCache(object);
        }
        getContext().getBean(AuditProcessor.class).processAudit(object, null, ORIVIS_OPERATION_AFTER_DELETE);
    }

    /**
     * Removes objects from DB
     *
     * @param ids - ids of objects to be removed
     * @throws ItemNotUserException  Exception is thrown when trying to update an object from a user, not owner of the object
     * @throws ItemNotFoundException When object cannot be found by id of the #object, this exception is thrown
     */
    void deleteItems(List<ID_TYPE> ids) throws ItemNotUserException, ItemNotFoundException;

    /**
     * Before object is saved in DB this method is called
     *
     * @param object   - an object variant to be updated
     * @param original - an original object from DB
     * @return - a final object to be updated
     * @throws ItemNotFoundException              When object cannot be found by id of the #object, this exception is thrown
     * @throws UnmodifiedItemSaveAttemptException Exception says, that there are no changes in the object which should be updated
     */
    default DB_TYPE preEdit(DB_TYPE object, DB_TYPE original) throws UnmodifiedItemSaveAttemptException, ItemNotFoundException {
        return object;
    }


    default void saveVersionIfRequired(DB_TYPE original, DB_TYPE object) {

        var historyVersion = getContext().getBean(AbstractEditDeleteHistoryBean.class);

        try {
            if (historyVersion.isEnabled()) {
                historyVersion.edit(original, object);
            }


        } catch (Exception e) {
            getLogger().error("Cannot save original version for object", e);
        }
    }

    /**
     * Before object is added this method is called
     *
     * @param object to be saved
     * @return a modified object before saving
     * @throws ItemForAddContainsIdException No id should be in edit object
     */
    default DB_TYPE preAdd(DB_TYPE object) throws ItemForAddContainsIdException {
        return object;
    }

    OrivisContext getContext();


    @SneakyThrows
    default Page<DB_TYPE> search(String field, String subtype, String value, int page, Integer size, Serializable[] additionalIds) {
        var model = getModel().getConstructor().newInstance();
        var res = getRepository().filteredData(getSearchRequestAbstractDataFilter(field, value, updateIds(additionalIds)), PageRequest.of(page, size, model.defaultSearchOrderField().getValue(), model.defaultSearchOrderField().getKey()));
        return res;
    }

    default OrivisFilter<DB_TYPE> getSearchRequestAbstractDataFilter(String field, String value, ID_TYPE[] additionalIds) {
        OrivisFilter<DB_TYPE> spec = getFilterImpl().contains(field, value).setInsensitive(true);
        OrivisFilter<DB_TYPE> additionalAbstractDataFilter = getAdditionalSpecification();
        if (additionalAbstractDataFilter != null) {
            spec = spec.and(additionalAbstractDataFilter);
        }

        return spec;
    }

    default OrivisFilter<DB_TYPE> getFilterImpl() {
        return getContext().getBean(OrivisFilterProvider.class).get();
    }

    default ID_TYPE[] updateIds(Object[] ids) {
        return (ID_TYPE[]) ids;
    }

    default ID_TYPE updateId(Object id) {
        return (ID_TYPE) id;
    }

    default Optional<DB_TYPE> filteredOne(OrivisFilter<?> one) {
        return getRepository().filteredOne(one.and(getAdditionalSpecification()));
    }

    default Optional<DB_TYPE> filteredFirst(OrivisFilter<?> first) {
        return filteredFirst(first, Sort.unsorted());
    }

    default Optional<DB_TYPE> filteredFirst(OrivisFilter<?> first, Sort sort) {
        return getRepository().filteredFirst(first.and(getAdditionalSpecification()), sort);
    }

    default Page<DB_TYPE> filteredData(OrivisFilter<?> specification) {
        return getRepository().filteredData(specification.and(getAdditionalSpecification()));
    }


    default Page<DB_TYPE> filteredData(OrivisFilter<?> specification, Pageable pageable) {
        return getRepository().filteredData(specification.and(getAdditionalSpecification()), pageable);
    }

    default boolean exists(OrivisFilter<?> specification) {
        return filteredCount(specification) > 0;
    }

    default long filteredCount(OrivisFilter<?> filter) {
        return getRepository().filteredCount(filter.and(getAdditionalSpecification()));
    }

    default long filteredCount() {
        return getRepository().filteredCount(getAdditionalSpecification());
    }

    default void deleteData(OrivisFilter<?> filter) {
        getRepository().deleteData(filter.and(getAdditionalSpecification()));
    }

    /**
     * What to do with object after it is stored in DB
     *
     * @param object Saved object with ID
     * @return modified object
     */
    default DB_TYPE afterAdd(DB_TYPE object) {
        return object;
    }

    /**
     * What to do with object before it is deleted from DB
     *
     * @param object deleted object with ID
     * @return deleted object
     */
    default DB_TYPE preDelete(DB_TYPE object) {
        return object;
    }

    /**
     * What to do with object after it is deleted from DB
     *
     * @param object deleted object with ID
     * @return deleted object
     */
    default DB_TYPE afterDelete(DB_TYPE object) {
        return object;
    }


    Logger getLogger();

    /**
     * Verifies whether object exist or not ( normally, by Id)
     *
     * @param object Object to check
     * @return true if object exists, false otherwise
     */
    boolean exists(DB_TYPE object);

    DB_TYPE save(DB_TYPE form);

    List<DB_TYPE> addAll(List<DB_TYPE> form);

    @SneakyThrows
    default <F extends DB_TYPE> F findByIdOrThrow(ID_TYPE id) {
        DB_TYPE res = findById(id).orElseThrow(() -> ItemNotFoundException.fromId(id));
        return (F) res;
    }

    default <T> T findById(ID_TYPE id, Supplier<T> orElse) {
        var res = findById(id);
        if (res.isEmpty()) {
            return orElse.onNull(id);
        }
        return (T) res.get();
    }

    default OrivisFilter<DB_TYPE> getSelfCreatedItems(Serializable currentUser) {
        return getSelfCreatedItems(currentUser, true);
    }

    @NeverNullResponse(on = "ignoreOnAdmin = false")
    default OrivisFilter<DB_TYPE> getSelfCreatedItems(Serializable currentUser, boolean ignoreOnAdmin) {
        return getSelfCreatedItemsQuery(currentUser, ignoreOnAdmin);
    }


    default OrivisFilter<DB_TYPE> getSelfCreatedItemsQuery(Serializable currentUser) {
        return getSelfCreatedItemsQuery(currentUser, true);
    }

    @NeverNullResponse(on = "ignoreOnAdmin = false")
    default OrivisFilter<DB_TYPE> getSelfCreatedItemsQuery(Serializable currentUser, boolean ignoreOnAdmin) {
        if (ignoreOnAdmin && getContext().getBean(ISecurityUtils.class).isAdmin()) {
            return getFilterImpl().notNull(OrivisIDPresenter.ID_FIELD);
        }
        return getFilterImpl().eq("userDataId", currentUser);
    }


    default Optional<DB_TYPE> findByIdClone(ID_TYPE id) {
        var el = findById(id);

        if (el.isPresent()) {
            return Optional.of(getContext().getBean(BeanCopier.class).clone(el.get()));
        } else {
            return el;
        }

    }

    default boolean isIdSet(ID_TYPE id) {
        return getContext().getBean(IdValidator.class).isIdSet(id);
    }

    default Optional<DB_TYPE> findById(Serializable idSerializable) {
        var id = updateId(idSerializable);
        if (!isIdSet(id)) {
            throw new StandardException(String.valueOf(id), "id.was.not_set.ty_find_by_id");
        }
        OrivisFilter<DB_TYPE> abstractDataFilter = getAdditionalSpecification();

        Optional<DB_TYPE> element;
        var provider = getCacheProvider();

        //since orivis data filter is never null

        if (provider != null) {
            element = provider.get(getGroup(), id,
                    getModel(),
                    () -> getRepository().filteredOne(abstractDataFilter.and(getFilterImpl().id(id))),
                    (CacheElementChecker<DB_TYPE>) this::verifyFromCache, Duration.ofMinutes(getConfigForClass("get")));

        }  else {
            element = getRepository().filteredOne(abstractDataFilter.and(getFilterImpl().id(id)));
        }


        getContext().getBean(AbstractEditDeleteHistoryBean.class).throwIfDeleted(id, getModel());

        return element;
    }

    default DB_TYPE verifyFromCache(DB_TYPE el) throws AccessDeniedException {
        return el;
    }

    /**
     * When object is recieved, we had to check that it's a correct object that we are allowed to pass forward
     *
     * @param obj and Optional of the current type
     *            <p>
     *            throws exception if validation is failed or false
     * @return
     */
    default boolean verifyObjectFromCache(Optional<DB_TYPE> obj) {
        return true;
    }

    default <T> T findById(ID_TYPE id, Exception els) {
        return findById(id, new ErrorSupplier<>(els));
    }

    default <T extends AbstractOrivisEntity> Class<T> getModel() {

        return (Class<T>) findDeclaredClass(getClass(), getClass().getAnnotation(OrivisService.class)).model();

    }

    default <T extends AbstractOrivisForm> Class<T> getForm() {
        return (Class<T>) findDeclaredClass(getClass(), getClass().getAnnotation(OrivisService.class)).form();
    }

    default AbstractCacheProvider getCacheProvider() {
        CacheStrategy cacheStrategy = getClass().getAnnotation(CacheStrategy.class);
        if (Objects.isNull(cacheStrategy) || !cacheStrategy.enabled()) return null;

        return AbstractCacheProvider.getInstance(cacheStrategy.cacheProvider());
    }


    /**
     * Returns all records from db having id more than from (or ignoring if null) and limited of limit
     * Default method to get all values should be used with care or better not to be used !
     *
     * @param limit - max
     * @param page  - a page to select
     * @return List of res
     */
    default Page<DB_TYPE> find(Integer limit, Integer page) {
        return find(null, limit, page);
    }


    default Page<DB_TYPE> find(OrivisFilter<DB_TYPE> searchAbstractDataFilter, Integer limit, Integer page) {

        if (limit == null || limit == -1) {
            limit = 200;
        }

        final PageRequest request = PageRequest.of(Objects.requireNonNullElse(page, 0), limit);

        if (searchAbstractDataFilter != null) {

            var provider = getCacheProvider();
            if (provider != null) {
                String cacheKey = provider.buildDataCacheKey(getModel().getSimpleName(), null, page, limit, null);

                return findWithCache(provider, cacheKey, () -> getRepository().filteredData(searchAbstractDataFilter, request), "list");
            }

            return getRepository().filteredData(searchAbstractDataFilter, request);
        } else {
            var provider = getCacheProvider();
            if (provider != null) {
                String cacheKey = provider.buildDataCacheKey(getModel().getSimpleName(), null, page, limit, null);

                return findWithCache(provider, cacheKey, () -> getRepository().findAll(request), "list");
            }

            return getRepository().findAll(request);
        }
    }

    <F extends AbstractRepository<DB_TYPE, ID_TYPE>> F getRepository();

    default Page<DB_TYPE> search(OrivisRequest request) {
        Page<DB_TYPE> data;
        Integer page = request.getPage();
        Integer limit = request.getLimit();

        if (request.getQuery().isEmpty() && request.getCases().isEmpty() && getAdditionalSpecification() == null) {
            var provider = getCacheProvider();
            if (provider != null) {
                String cacheKey = provider.buildDataCacheKey(getModel().getSimpleName(), null, page, limit, request);

                return findWithCache(provider, cacheKey, () -> getRepository().findAll(PageRequest.of(page, limit, createFromRequest(request))), "list");
            }

            return getRepository().findAll(PageRequest.of(page, limit, createFromRequest(request)));

        } else {
            OrivisFilter<DB_TYPE> finalAbstractDataFilter = prepareAbstractDataFilter(request);

            var provider = getCacheProvider();
            if (provider != null) {
                String cacheKey = provider.buildDataCacheKey(getModel().getSimpleName(), finalAbstractDataFilter, page, limit, request);
                return findWithCache(provider, cacheKey, () -> getRepository().filteredData(finalAbstractDataFilter,
                        PageRequest.of(page, limit, createFromRequest(request))), "list");
            }

            return getRepository().filteredData(finalAbstractDataFilter,
                    PageRequest.of(page, limit, createFromRequest(request)));
        }
    }

    default Page<DB_TYPE> findWithCache(AbstractCacheProvider provider,
                                        String cacheKey,
                                        DataSupplier<Page<DB_TYPE>> fetchFunction,
                                        String configKey) {

        Page<DB_TYPE> cachedData = (Page<DB_TYPE>) provider.get(getGroup(), cacheKey);
        if (Objects.nonNull(cachedData)) {
            return cachedData;
        }

        Page<DB_TYPE> data = fetchFunction.get();
        provider.addToCache(getGroup(), cacheKey, data, Duration.ofMinutes(getConfigForClass(configKey)));

        return data;
    }

    default OrivisFilter<DB_TYPE> prepareAbstractDataFilter(OrivisRequest request) {
        OrivisFilter<DB_TYPE> modifyQuery = getAdditionalSpecification();

        List<OrivisFilter<DB_TYPE>> orFilters = new ArrayList<>();

        for (var query : request.getQuery()) {
            OrivisFilter<DB_TYPE> condition = modifyQuery(query.toQuery(getContext()));

            if ("OR".equalsIgnoreCase(query.getLogicalOperator())) {
                orFilters.add(condition);
            } else {
                modifyQuery.and(condition);
            }
        }

        if (!orFilters.isEmpty()) {
            OrivisFilter<DB_TYPE> orFilterGroup = orFilters.get(0);
            for (int i = 1; i < orFilters.size(); i++) {
                orFilterGroup = orFilterGroup.or(orFilters.get(i));
            }
            modifyQuery.and(orFilterGroup);
        }

        for (var query : request.getCases()) {
            modifyQuery.and(modifyQuery(query));
        }

        return modifyQuery;

    }

    default Sort createFromRequest(OrivisRequest request) {
        if (request.getSort() == null || request.getSort().isEmpty()) {
            return Sort.unsorted();
        }

        List<Sort> sorts = new ArrayList<>();


        request.getSort().stream().filter(Objects::nonNull).forEach(x -> sorts.add(
                Sort.by(x.getDesc() ? Sort.Direction.DESC : Sort.Direction.ASC, getFieldOrder(x.getField()))));
        if (sorts.size() > 1) {

            var firstSort = sorts.get(0);
            for (int i = 1; i < sorts.size(); i++) {

                firstSort = firstSort.and(sorts.get(i));
            }

            return firstSort;
        } else {
            return sorts.get(0);
        }
    }

    default String getFieldOrder(String field) {
        try {
            var sortField = findField(field, getModel());

            if (AbstractOrivisForm.class.isAssignableFrom(sortField.getType())) {
                return sortField.getName() + "." + ((AbstractOrivisEntity) sortField.getType().getConstructor().newInstance()).defaultSearchOrderField().getKey();
            }
            return field;
        } catch (Exception e) {
            return field;
        }

    }

    default long count() {
        OrivisFilter<DB_TYPE> spec = getAdditionalSpecification();
        return getRepository().filteredCount(spec);
    }

    default boolean isEmpty(OrivisFilter AbstractDataFilter) {
        return count(AbstractDataFilter) == 0;
    }


    default boolean isEmpty() {
        return count() == 0;
    }

    default long count(OrivisFilter<DB_TYPE> spec) {
        OrivisFilter<DB_TYPE> customAbstractDataFilter = getAdditionalSpecification();

        if (customAbstractDataFilter != null) {
            return getRepository().filteredCount(customAbstractDataFilter.and(spec));
        }

        return getRepository().filteredCount(spec);
    }

    @SneakyThrows
    default UrlResource download(OrivisRequest request) {
        Page<DB_TYPE> data;
        var path = getContext().getEnv("global.download.path", "./");
        OrivisFilter<DB_TYPE> customAbstractDataFilter = getAdditionalSpecification();

        if (request.getQuery().isEmpty()) {
            if (customAbstractDataFilter != null) {
                data = getRepository().filteredData(customAbstractDataFilter, PageRequest.of(0, 100000, createFromRequest(request)));
            } else {
                data = getRepository().findAll(PageRequest.of(0, 100000, createFromRequest(request)));
            }
        } else {

            OrivisFilter<DB_TYPE> filter = getFilterImpl().empty();

            for (var query : request.getQuery()) {
                filter.and(query.toQuery(getContext()));
            }

            if (customAbstractDataFilter != null) {
                data = getRepository().filteredData(filter.and(customAbstractDataFilter),
                        PageRequest.of(0, 100000, createFromRequest(request)));
            } else {
                data = getRepository().filteredData(filter, (PageRequest.of(0, 100000, createFromRequest(request))));
            }
        }

        List<Map<String, Object>> res = new ArrayList<>();
        var bc = getContext().getBean(BeanCopier.class);

        for (DB_TYPE entity : data.getContent()) {
            var form = bc.copy(entity, getForm());
            Map<String, Object> values = BeanCopier.values(form, entity, getContext());
            res.add(values);
        }

        if (res.size() == 0) {
            res.add(Collections.singletonMap("res", " ---- NO DATA ---"));
        }


        Workbook workbook = null;

        String fileName = path + UUID.randomUUID().toString() + ".xls";

        if (fileName.endsWith("xlsx")) {
            workbook = new XSSFWorkbook();
        } else {
            workbook = new HSSFWorkbook();
        }

        Sheet sheet = workbook.createSheet("data");

        var headRow = 1;
        var cln = 0;
        Row row = sheet.createRow(headRow);
        for (String el : res.get(0).keySet()) {
            if ("action".equalsIgnoreCase(el)) {
                continue;
            }

            Cell cell0 = row.createCell(cln);
            cell0.setCellValue(el);
            cln++;

        }

        var dataRow = 2;
        var datacln = 0;

        for (Map<String, Object> element : res) {
            Row datarow = sheet.createRow(dataRow);


            for (Object el : element.values()) {
                Cell cell = datarow.createCell(datacln);
                if (el instanceof ObjectDefinition) {
                    el = ((ObjectDefinition) el).getValue();
                }

                if (el == null) {
                    el = "";
                }

                if (el.toString().length() > 5000) {
                    el = String.valueOf(el).substring(0, 5000);
                }

                cell.setCellValue(String.valueOf(el));
                datacln++;
            }
            datacln = 0;
            dataRow++;

        }

        FileOutputStream fos = new FileOutputStream(fileName);
        workbook.write(fos);
        fos.close();
        System.out.println(fileName + " written successfully");

        return new UrlResource(new File(fileName).toURI());

    }


    default String getItemFavoriteType() {
        if (getForm().getAnnotation(PojoListView.class) == null) {
            return getForm().getSimpleName();
        }

        return getForm().getAnnotation(PojoListView.class).favoriteType();
    }

    default UrlResource getInterNalResource(String resource) {

        var path = getContext().getEnv("global.download.path", "./");
        String fileName = path + resource;
        try {
            return new UrlResource(new File(fileName).toURI());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Since version 3.0.5 will never produce null
     *
     * @return not deleted query on notEmptyId query
     */
    default @NeverNullResponse OrivisFilter<DB_TYPE> getAdditionalSpecification() {
        return getNotDeletedQuery();
    }

    default void preSearch(String what, String type) {
    }

    default boolean isPublicSearchEnabled() {
        return false;
    }

    default void checkMy(DB_TYPE object) {
        getContext().getBean(ISecurityUtils.class).isMy(object);
    }

    default @NeverNullResponse OrivisFilter<DB_TYPE> modifyQuery(OrivisFilter<DB_TYPE> query) {
        return query;
    }

    default Boolean isObjectDeleted(DB_TYPE object) {
        int strategy = getDeleteStrategy(getClass());
        if (strategy == DELETE_STRATEGY_PROPERTY) {
            if (Objects.isNull(object.getHidden())) {
                return false;
            }
            return object.getHidden();
        }
        try {
            findById(object.getId());
            return false;
        } catch (ItemWasDeletedException e) {
            return true;
        }
    }

    default boolean getByIdAndUserDataId(ID_TYPE id, Serializable currentUser) {
        try {
            if (getContext().getBean(ISecurityUtils.class).isAdmin()) {
                return true;
            }
            OrivisFilter<DB_TYPE> AbstractDataFilter = getFilterImpl().id(id).and(getFilterImpl().ofUser(getModel(), currentUser));
            return count(AbstractDataFilter) > 0;
        } catch (Exception e) {
            return false;
        }
    }

    default int getConfigForClass(String type) {
        return getContext().getEnv(getCacheParameterKey(type), getDefaultCacheLifeTime());
    }

    default String getCacheParameterKey(String type) {
        return "app.global.caching.class_" + getModel().getSimpleName().toLowerCase() + "_" + type;
    }

    default int getDefaultCacheLifeTime() {
        return 120;
    }

    default String getGroup(){
        return this.getClass().getSimpleName();
    }
}
