package net.orivis.shared.service;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.audit.AuditProcessor;
import net.orivis.shared.exceptions.ItemDoesNotContainsIdValueException;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.exceptions.ItemNotUserException;
import net.orivis.shared.exceptions.UnmodifiedItemSaveAttemptException;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import net.orivis.shared.annotations.security.ISecurityUtils;
import net.orivis.shared.beans.IdToAuthorizationIdBean;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.controller.forms.QueryObject;
import net.orivis.shared.entity.KeyValue;
import net.orivis.shared.model.OrivisIDPresenter;
import net.orivis.shared.repository.OrivisFilter;
import net.orivis.shared.repository.AbstractRepository;
import net.orivis.shared.utils.OrivisContextable;
import net.orivis.shared.utils.bean_copier.BeanCopier;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Slf4j
public abstract class AbstractPaginationService<DB_TYPE extends OrivisIDPresenter<ID_TYPE>, ID_TYPE extends Serializable>
        extends OrivisContextable
        implements AbstractService<DB_TYPE, ID_TYPE> {

    public static final int ORIVIS_OPERATION_PRE_ADD = 0;
    public static final int ORIVIS_OPERATION_AFTER_ADD = 1;
    public static final int ORIVIS_OPERATION_PRE_DELETE = 2;
    public static final int ORIVIS_OPERATION_AFTER_DELETE = 3;
    public static final int ORIVIS_OPERATION_PRE_UPDATE = 4;
    public static final int ORIVIS_OPERATION_PRE_TRANSFORM = 5;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private IdToAuthorizationIdBean id2id;

    public AbstractPaginationService(OrivisContext holder) {
        super(holder);
    }


    @Override
    public Logger getLogger() {
        return log;
    }

    @Override
    public boolean exists(DB_TYPE object) {
        return getRepository().existsById(object.getId());
    }

    @SneakyThrows
    @Override
    public DB_TYPE save(DB_TYPE model) {

        if (model.getId() != null && !model.isIdSet()) {
            model.setId(null);
        }

        if (model.getId() != null) {
            if (entityManager != null) {
                Session session = entityManager.unwrap(Session.class);
                session.evict(model);
            }
            return edit(getBean(BeanCopier.class).clone(model));
        }

        getBean(AuditProcessor.class).processAudit(model, null, ORIVIS_OPERATION_PRE_ADD);

        preAdd(model);

        var res = getRepository().save(model);
        var provider = getCacheProvider();
        if (provider != null) {
            provider.addToCache(getGroup(), getBean(BeanCopier.class).clone(res), Duration.ofMinutes(getConfigForClass("add")));
        }
        getBean(AuditProcessor.class).processAudit(model, null, ORIVIS_OPERATION_AFTER_ADD);
        afterAdd(res);

        return res;
    }

    @SneakyThrows
    @Override
    @Transactional
    public List<DB_TYPE> addAll(List<DB_TYPE> models) {
        for (DB_TYPE model : models) {
            if (!model.isIdSet()) {
                model.setId(null);
            }

            getBean(AuditProcessor.class).processAudit(model, null, ORIVIS_OPERATION_PRE_ADD);

            preAdd(model);
        }

        List<DB_TYPE> savedModels = (List<DB_TYPE>) getRepository().saveAll(models);

        var provider = getCacheProvider();
        for (DB_TYPE savedModel : savedModels) {
            afterAdd(savedModel);
            if (provider != null) {
                provider.addToCache(getGroup(), getBean(BeanCopier.class).clone(savedModel), Duration.ofMinutes(getConfigForClass("add")));
            }
            getBean(AuditProcessor.class).processAudit(savedModel, null, ORIVIS_OPERATION_PRE_ADD);
        }

        return savedModels;
    }

    public void preFetchOriginal(DB_TYPE form) {

    }

    public OrivisFilter<DB_TYPE> getAdditionalCases(QueryObject<String> parameters) {
        return getFilterImpl().empty();
    }

    @Override
    public List<DB_TYPE> find() {
        return find(false);
    }

    public List<DB_TYPE> find(Boolean ignoreAdditionalSpecification) {
        List<DB_TYPE> result = new ArrayList<>();
        if (ignoreAdditionalSpecification) {
            getRepository().findAll().forEach(result::add);
        } else {
            getRepository().filteredData(getAdditionalSpecification()).forEach(result::add);
        }
        return result;
    }

    @Override
    public DB_TYPE edit(DB_TYPE model) throws ItemNotUserException, ItemNotFoundException, UnmodifiedItemSaveAttemptException, ItemDoesNotContainsIdValueException {
        ID_TYPE editedId = model.getId();
        DB_TYPE cloneOfOriginal = findByIdClone(editedId).orElseThrow(() -> ItemNotFoundException.fromId(editedId));
        //Here need to be cloned to avoid Hibernate to take original object from cache

        if (Objects.equals(model, cloneOfOriginal)) {
            throw new UnmodifiedItemSaveAttemptException("Object not changed");
        }

        var copier = getContext().getBean(BeanCopier.class);
        copier.copyTransientFields(model, cloneOfOriginal);

        getBean(AuditProcessor.class).processAudit(model, cloneOfOriginal, ORIVIS_OPERATION_PRE_UPDATE);

        model = preEdit(model, cloneOfOriginal); // change to retrieve data

        var res = getRepository().save(model);
        saveVersionIfRequired(cloneOfOriginal, model);

        var provider = getCacheProvider();
        if (provider != null) {
            provider.updateCache(getGroup(), getBean(BeanCopier.class).clone(res));
        }

        getBean(AuditProcessor.class).processAudit(model, cloneOfOriginal, ORIVIS_OPERATION_PRE_TRANSFORM);

        afterEdit(res, cloneOfOriginal); // afterEdit


        return res;
    }

    public void afterEdit(DB_TYPE object, DB_TYPE original) {

    }


    @Override
    public KeyValue editField(ID_TYPE id, KeyValue keyValue)
            throws ItemNotUserException, ItemNotFoundException, UnmodifiedItemSaveAttemptException, ItemDoesNotContainsIdValueException, IllegalAccessException, InvocationTargetException {

        DB_TYPE original = getRepository().findById(id).orElseThrow(() -> ItemNotFoundException.fromId(id));
        var cloneOfOriginal = getContext().getBean(BeanCopier.class).clone(original);

        var field = BeanCopier.findField(keyValue.getKey(), cloneOfOriginal.getClass());

        BeanCopier.setFieldValue(cloneOfOriginal, field, keyValue.getValue());

        cloneOfOriginal = edit(cloneOfOriginal);
        var provider = getCacheProvider();
        if (provider != null) {
            provider.updateCache(getGroup(), cloneOfOriginal);
        }
        var valueFromModel = BeanCopier.readValue(cloneOfOriginal, keyValue.getKey());
        return new KeyValue(keyValue.getKey(), valueFromModel);
    }


    @Override
    @Transactional
    public void deleteItems(List<ID_TYPE> ids) throws ItemNotUserException, ItemNotFoundException {
        ids.forEach(this::delete);
    }


    @Override
    public <F extends AbstractRepository<DB_TYPE, ID_TYPE>> F getRepository() {
        OrivisService OrivisService = getClass().getAnnotation(OrivisService.class);

        if (OrivisService != null) {
            log.debug("OrivisController is deprecated and will be removed in future versions.");
            return (F) getContext().getBean(OrivisService.repo());
        }

        return (F) getContext().getBean(getClass().getAnnotation(OrivisService.class).repo());
    }

    @SuppressWarnings("Unchecked")
    public <T extends ISecurityUtils> T getUtils() {
        return (T) getContext().getBean(ISecurityUtils.class);
    }

    public boolean isIdSet(Long id) {
        return id != null && id > 0;
    }
}
