package net.orivis.shared.audit;

import lombok.extern.slf4j.Slf4j;
import net.orivis.shared.annotations.AutoCreationCheck;
import net.orivis.shared.annotations.entity.*;
import net.orivis.shared.annotations.security.ISecurityUtils;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.model.OrivisIDPresenter;
import net.orivis.shared.service.AbstractPaginationService;
import net.orivis.shared.utils.OrivisContextable;
import net.orivis.shared.utils.bean_copier.BeanCopier;
import net.orivis.shared.utils.bean_copier.FieldDefinition;
import net.orivis.shared.utils.bean_copier.ModelCache;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.List;

@Slf4j
@Component
public class AuditProcessor extends OrivisContextable {

    public AuditProcessor(OrivisContext context) {
        super(context);
    }

    public <T extends OrivisIDPresenter > T processAudit(T model, OrivisIDPresenter origin, int operationType) {
        if (model.getClass().isAnnotationPresent(IgnoreAuditProcessing.class)) {
            return model;
        }

        var fields = getAllFields(model.getClass());

        for (var fieldDefinition : fields) {
            var field = fieldDefinition.getField();
            try {
                if (field.isAnnotationPresent(Created.class) && operationType == AbstractPaginationService.ORIVIS_OPERATION_PRE_ADD) {
                    handleCreated(field, model);
                }

                if (field.isAnnotationPresent(CreatedBy.class)&& operationType == AbstractPaginationService.ORIVIS_OPERATION_PRE_ADD) {
                    handleCreatedBy(field, model);
                }

                if (field.isAnnotationPresent(Updated.class)&& operationType == AbstractPaginationService.ORIVIS_OPERATION_PRE_ADD) {
                    handleUpdated(field, model);
                }

                if (field.isAnnotationPresent(UpdatedBy.class)&& operationType == AbstractPaginationService.ORIVIS_OPERATION_PRE_ADD) {
                    handleUpdatedBy(field, model);
                }

                if (field.isAnnotationPresent(OrivisAuditable.class)){
                    handleCustomAuditable(fieldDefinition, model, origin, operationType);
                }

            } catch (Exception e) {
                log.trace("Cannot set field to default: " + field.getName(), e);
            }
        }
        return model;
    }

    private void handleCustomAuditable(FieldDefinition fieldDefinition, OrivisIDPresenter model, OrivisIDPresenter origin, int operationType) {
        log.trace("Starting custom Auditable processor check");

        var field = fieldDefinition.getField();
        log.trace("Auditable --> field: " , field.getName());
        var elements = field.getAnnotation(OrivisAuditable.class).value();
        for (var element : elements){
            log.trace("Auditable class --> field: " , element.getName());
            ModelCache.getInstance(element).process(field, getContext(), operationType, origin, model);
        }
        log.trace("Custom Auditable processor check finished");
    }

    private void handleCreated(Field field, OrivisIDPresenter model) {
        try {
            Created createdAnnotation = field.getAnnotation(Created.class);
            AutoCreationCheck autoCreationCheck = createdAnnotation.ignoreOnCondition().getDeclaredConstructor().newInstance();

            if (!autoCreationCheck.checkAutoCreation(new Object[]{}, model, getContext())) {
                return;
            }


            BeanCopier.setFieldValue(model, field, getContext().getBean(PrimaryDateSetter.class).getNewDateValue(), true);
        } catch (Exception e) {
            log.trace("cannot set field to default:" + field.getName());
        }
    }

    private void handleCreatedBy(Field field, OrivisIDPresenter model) {
        try {
            CreatedBy createdAnnotation = field.getAnnotation(CreatedBy.class);
            AutoCreationCheck autoCreationCheck = createdAnnotation.ignoreOnCondition().getDeclaredConstructor().newInstance();

            if (!autoCreationCheck.checkAutoCreation(new Object[]{}, model, getContext())) {
                return;
            }

            BeanCopier.setFieldValue(model, field, getContext().getBean(ISecurityUtils.class).getUserDataId(), true);
        } catch (Exception e) {
            log.trace("cannot set field to default:" + field.getName());
        }
    }

    private void handleUpdated(Field field, OrivisIDPresenter model) {
        try {
            Updated createdAnnotation = field.getAnnotation(Updated.class);
            AutoCreationCheck autoCreationCheck = createdAnnotation.ignoreOnCondition().getDeclaredConstructor().newInstance();
            if (!autoCreationCheck.checkAutoCreation(new Object[]{}, model, getContext())) {
                return;
            }

            BeanCopier.setFieldValue(model, field, getContext().getBean(PrimaryDateSetter.class).getNewDateValue(), true);
        } catch (Exception e) {
            log.trace("cannot set field to default:" + field.getName());
        }
    }

    private void handleUpdatedBy(Field field, OrivisIDPresenter model) {
        try {
            UpdatedBy createdAnnotation = field.getAnnotation(UpdatedBy.class);
            AutoCreationCheck autoCreationCheck = createdAnnotation.ignoreOnCondition().getDeclaredConstructor().newInstance();
            if (!autoCreationCheck.checkAutoCreation(new Object[]{}, model, getContext())) {
                return;
            }

            BeanCopier.setFieldValue(model, field, getContext().getBean(ISecurityUtils.class).getUserDataId(), true);
        } catch (Exception e) {
            log.trace("cannot set field to default:" + field.getName());
        }
    }

    private List<FieldDefinition> getAllFields(Class<?> clazz) {
        return ModelCache.get(clazz).getAuditFields();
    }
}
