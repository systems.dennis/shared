package net.orivis.shared.pojo_form;

import net.orivis.shared.config.OrivisContext;

import java.util.ArrayList;

public interface DataProvider<LABEL_TYPE> {
    ArrayList<ItemValue<LABEL_TYPE>> getItems(OrivisContext context);
}
