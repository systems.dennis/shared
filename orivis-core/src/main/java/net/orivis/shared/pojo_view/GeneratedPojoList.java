package net.orivis.shared.pojo_view;

import lombok.Data;
import net.orivis.shared.entity.KeyValue;
import net.orivis.shared.utils.PojoListField;

import java.util.ArrayList;
import java.util.List;

@Data
public class GeneratedPojoList {
    public static GeneratedPojoList INSTANCE;
    private List<PojoListField> fields = new ArrayList<>();

    private List<String> searchFields;
    private boolean searchEnabled;
    private String tableTitle;
    private Boolean showTitle;

    private String defaultField;

    private KeyValue defaultSorting;

    private List<String> listActions;

    private String objectType;


}
