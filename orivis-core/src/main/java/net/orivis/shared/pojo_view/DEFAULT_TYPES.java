package net.orivis.shared.pojo_view;

public class DEFAULT_TYPES {

    /**
     * Used to indicate for ui that the time should be present in date with time or date only
     */
    public static final String DATE_FORMAT_FULL = "date_time";
    public static final String DATE_FORMAT_SHORT = "date";

    public static final String TEXT="text";
    public static final String HIDDEN="hidden";
    public static final String DATE="date";
    public static final String DATE_TIME = "datetime";
    public static final String NUMBER="number";
    public static final String DOUBLE="double";
    public static final String INTEGER="integer";
    public static final String COLLECTION="collections";
    public static final String MULTI_VALUE="multi";
    public static final String DROP_DOWN="drop-down";
    public static final String DICTIONARY ="dictionary";
    public static final String CHECKBOX="checkbox";
    public static final String STATUS="status";
    public static final String LABEL="label";
    public static final String PASSWORD="password";
    public static final String TEXT_AREA="text_area";
    public static final String FILE = "file";
    public static final String FILES = "files";
    public static final String DEFAULT_TYPE=TEXT;
    public static final String OBJECT_SEARCH = "object_chooser";
    public static final String REFERENCED_ID = "referenced_id";
    public static final String REFERENCED_IDS = "referenced_ids";
    public static final String OBJECT_LIST = "object_chooser_list";
    public static final String OBJECT_TYPE = "auto_complete";

    public static final String DEFAULT_EMPTY_VALUE ="";
}
