package net.orivis.shared.pojo_view;

import net.orivis.shared.config.OrivisContext;

public interface DefaultDataConverter<T, E> {
    default <F>F convert(T object, E data, OrivisContext context){
        return (F) object;
    }
}
