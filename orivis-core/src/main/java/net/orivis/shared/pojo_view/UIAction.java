package net.orivis.shared.pojo_view;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(value = ElementType.FIELD)
public @interface UIAction {



    static final String ACTION_NEW = "new";
    static final String ACTION_SETTINGS = "settings";
    static final String ACTION_DOWNLOAD = "download";
    static final String ACTION_DELETE = "delete";
    static final String ACTION_EDIT = "edit";


    int order() default 0;
    String component() default "";
    boolean allowOnMultipleRows() default false;
    UIActionParameter [] parameters() default {};

    @AliasFor("component")
    String value() default "";

}
