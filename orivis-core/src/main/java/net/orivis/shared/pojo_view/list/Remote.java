package net.orivis.shared.pojo_view.list;

import static net.orivis.shared.pojo_view.DEFAULT_TYPES.TEXT;

public @interface Remote {
    /**
     * deprecate since version 3.3. use controller field instead
     * Now controller value allows automatic find relative parameter
     * @return String
     *
     * searchType is not required to be set in this case!
     */
    @Deprecated
    String searchField() default "";

    /**
     * deprecate since version 3.3. use controller field instead
     * Now controller value allows automatic find relative parameter
     * @return String
     */
    @Deprecated
    String searchName() default "";

    String searchType() default TEXT;

    String fetcher() default "";

    boolean editable() default true;

    Class<?> controller() default Remote.class;

    boolean same() default false;
}
