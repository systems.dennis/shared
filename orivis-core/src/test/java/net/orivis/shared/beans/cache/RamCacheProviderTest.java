package net.orivis.shared.beans.cache;

import lombok.extern.slf4j.Slf4j;
import net.orivis.shared.utils.bean_copier.TestConfig;
import net.orivis.shared.utils.bean_copier.entity.TestCacheEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Duration;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfig.class)
public class RamCacheProviderTest {
    private RamCacheProvider cacheProvider;
    private TestCacheEntity testEntity;
    private Page<TestCacheEntity> testPage;

    @BeforeEach
    void setUp() {
        cacheProvider = new RamCacheProvider();

        testEntity = new TestCacheEntity();
        testEntity.setId(1L);
        testEntity.setName("test");
        cacheProvider.addToCache("seller", testEntity, Duration.ofMinutes(1));

        List<TestCacheEntity> entities = Collections.singletonList(testEntity);
        String request = "test";
        testPage = new PageImpl<>(entities);
        cacheProvider.addToCache("seller", request, testPage, Duration.ofMinutes(1));
    }

    @Test
    public void testGetEntityFromCacheFoundElement() {
        Optional<TestCacheEntity> cachedEntity = cacheProvider.get(
                "seller", 1L, TestCacheEntity.class, Optional::empty, t -> t, Duration.ofMinutes(5));
        assertTrue(cachedEntity.isPresent());
        assertEquals(testEntity, cachedEntity.get());
    }

    @Test
    public void testGetEntityFromCacheNotFoundElementInCorrectGroup() {
        Optional<TestCacheEntity> cachedEntity = cacheProvider.get(
                "seller", 2L, TestCacheEntity.class, Optional::empty, t -> t, Duration.ofMinutes(5));
        assertFalse(cachedEntity.isPresent());
    }

    @Test
    public void testGetEntityFromCacheNotFoundElementInIncorrectCorrectGroup() {
        Optional<TestCacheEntity> cachedEntity = cacheProvider.get(
                "buyer", 1L, TestCacheEntity.class, Optional::empty, t -> t, Duration.ofMinutes(5));
        assertFalse(cachedEntity.isPresent());
    }

    @Test
    public void testAddEntityToCache() {
        TestCacheEntity newElement = new TestCacheEntity();
        newElement.setId(3L);
        newElement.setName("test");

        cacheProvider.addToCache("seller", newElement, Duration.ofMinutes(5));

        Optional<TestCacheEntity> cachedEntity = cacheProvider.get(
                "seller", 3L, TestCacheEntity.class, Optional::empty, t -> t, Duration.ofMinutes(5));
        assertTrue(cachedEntity.isPresent());
        assertEquals(newElement, cachedEntity.get());
    }

    @Test
    public void testAddListToCache() {
        TestCacheEntity newElement = new TestCacheEntity();
        newElement.setId(3L);
        newElement.setName("test");

        List<TestCacheEntity> newElements = Collections.singletonList(newElement);
        String request = "request";
        Page<TestCacheEntity> page = new PageImpl<>(newElements);

        cacheProvider.addToCache("seller", request, page, Duration.ofMinutes(5));

        Page<?> cachedEntity = cacheProvider.get("seller", request);
        assertEquals(page, cachedEntity);
    }

    @Test
    public void testGetListFromCacheFoundElement() {
        String request = "request";
        Page<?> cachedEntity = cacheProvider.get("seller", request);
        assertEquals(testPage, cachedEntity);
    }

    @Test
    public void testGetListFromCacheNotFoundElementInCorrectGroup() {
        String request = "requestIncorrect";
        Page<?> cachedPage = cacheProvider.get("seller", request);
        assertNull(cachedPage);
    }

    @Test
    public void testGetListFromCacheNotFoundElementInIncorrectCorrectGroup() {
        String request = "request";
        Page<?> cachedPage = cacheProvider.get("buyer", request);
        assertNull(cachedPage);
    }

    @Test
    public void testRemoveFromCache() {
        cacheProvider.removeFromCache(testEntity);

        Optional<TestCacheEntity> cachedEntity = cacheProvider.get(
                "seller", 1L, TestCacheEntity.class, Optional::empty, t -> t, Duration.ofMinutes(1));
        assertFalse(cachedEntity.isPresent());
    }

    @Test
    public void testUpdateCache() {
        testEntity.setName("updatedTest");
        cacheProvider.updateCache("seller", testEntity);

        Optional<TestCacheEntity> cachedEntity = cacheProvider.get(
                "seller", 1L, TestCacheEntity.class, Optional::empty, t -> t, Duration.ofMinutes(1));
        assertTrue(cachedEntity.isPresent());
        assertEquals("updatedTest", cachedEntity.get().getName());
    }

    @Test
    public void testCleanGarbageRemovesFromCacheAndGroups() {
        cacheProvider.addToCache("employee", testEntity, Duration.ofMillis(1));

        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        Optional<TestCacheEntity> cachedEntityBefore = cacheProvider.get(
                "employee", 1L, TestCacheEntity.class, Optional::empty, t -> t, Duration.ofMinutes(1));
        assertTrue(cachedEntityBefore.isPresent());

        cacheProvider.cleanGarbage();

        Optional<TestCacheEntity> cachedEntityAfter = cacheProvider.get(
                "employee", 1L, TestCacheEntity.class, Optional::empty, t -> t, Duration.ofMinutes(1));

        assertFalse(cachedEntityAfter.isPresent());

    }

    @Test
    public void testCleanGarbageRemovesFromDataCacheAndGroups() {
        cacheProvider.addToCache("employee", "test_request", testPage, Duration.ofMillis(1));

        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        Page<?> cachedEntityBefore = cacheProvider.get("employee", "test_request");
        assertEquals(cachedEntityBefore, testPage);
        cacheProvider.cleanGarbage();
        Page<?>  cachedEntityAfter = cacheProvider.get("employee", "test_request");
        assertNull(cachedEntityAfter);
    }
}
