package net.orivis.shared.utils.bean_copier;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.orivis.shared.utils.bean_copier.entity.bean_copier.AnnotatedTestClass;
import net.orivis.shared.utils.bean_copier.entity.bean_copier.AnnotatedTestClassChild;
import net.orivis.shared.utils.bean_copier.entity.bean_copier.AnnotatedTestClassCopy;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfig.class)
public class FieldDefinitionTest {

    private final AnnotatedTestClassChild annotatedChild = new AnnotatedTestClassChild();
    private AnnotatedTestClassCopy annotatedFieldsObjectCopy = new AnnotatedTestClassCopy();

    {
        annotatedChild.setStringField("String");
        annotatedChild.setIntegerField(111);
        annotatedChild.setLongField(100L);
        AnnotatedTestClass.doubleField = 2.5;
        annotatedChild.setByteField((byte) 1);
        annotatedChild.setShortField((short) 2);
        AnnotatedTestClassChild.floatField = 1.5F;
        annotatedChild.setCharacterField('&');
    }

    @Test
    @SneakyThrows
    public void testCopyTransientFields() {
        FieldDefinition fieldDefinition = new FieldDefinition();

        Field shortField = AnnotatedTestClassChild.class.getDeclaredField("shortField");
        Method getter = AnnotatedTestClassChild.class.getDeclaredMethod("getShortField");
        Method setter = AnnotatedTestClassChild.class.getDeclaredMethod("setShortField", Short.class);

        fieldDefinition.setField(shortField);
        fieldDefinition.setGetter(getter);
        fieldDefinition.setSetter(setter);

        fieldDefinition.copy(annotatedChild, annotatedFieldsObjectCopy, true, null);
        assertEquals(Short.valueOf((short) 2), annotatedFieldsObjectCopy.getShortField());
    }

    @Test
    @SneakyThrows
    public void testCopyTransientFieldsShouldIgnoreWhenFlagIsTrue() {
        FieldDefinition fieldDefinition = new FieldDefinition();

        Field shortField = AnnotatedTestClassChild.class.getDeclaredField("shortField");
        Method getter = AnnotatedTestClassChild.class.getDeclaredMethod("getShortField");
        Method setter = AnnotatedTestClassCopy.class.getDeclaredMethod("setShortField", Short.class);

        fieldDefinition.setField(shortField);
        fieldDefinition.setGetter(getter);
        fieldDefinition.setSetter(setter);

        fieldDefinition.copy(annotatedChild, annotatedFieldsObjectCopy, false, null);
        assertNull(annotatedFieldsObjectCopy.getShortField());
    }


}
