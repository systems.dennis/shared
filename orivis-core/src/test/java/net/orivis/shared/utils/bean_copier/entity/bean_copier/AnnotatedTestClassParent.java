package net.orivis.shared.utils.bean_copier.entity.bean_copier;

import lombok.Data;
import org.springframework.stereotype.Component;

import jakarta.persistence.Column;

@Data
@Component
public class AnnotatedTestClassParent extends OrivisEntity {

    @Column
    private Character characterField;

    @Override
    public String asValue() {
        return "";
    }
}
