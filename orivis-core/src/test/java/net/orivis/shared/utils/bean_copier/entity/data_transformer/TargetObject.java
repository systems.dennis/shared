package net.orivis.shared.utils.bean_copier.entity.data_transformer;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class TargetObject {
//    @DataTransformer(transFormWith = DateAndStringConverter.class, params = "dd.MM.yyyy")
    private Date fromStringToDateType;
//    @DataTransformer(transFormWith = DateAndStringConverter.class, params = "dd.MM.yyyy")
    private String fromDateToStringType;
}
