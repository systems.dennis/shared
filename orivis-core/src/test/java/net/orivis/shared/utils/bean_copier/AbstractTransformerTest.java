package net.orivis.shared.utils.bean_copier;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import net.orivis.shared.exceptions.AbstractTransformerException;
import net.orivis.shared.utils.bean_copier.entity.abstract_transformer.AbstractTransformerTestClass;
import net.orivis.shared.utils.bean_copier.entity.abstract_transformer.AbstractTransformerTestClassChild;
import net.orivis.shared.utils.bean_copier.entity.abstract_transformer.AbstractTransformerTestClassNotChild;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfig.class)
class AbstractTransformerTest {

    @Mock
    private OrivisDataTransofrmer<Long> transformer;

    @Autowired
    private AbstractTransformerTestClassChild testClassChild;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);

        Mockito.when(transformer.returnSameIfTypesAreEqual(Mockito.any(), Mockito.any())).thenCallRealMethod();
    }

    @Test
    public void testReturnSameIfTypesAreEqualMethodReturnsFalseWhenToTypeIsNotAssignableFromObjectFrom() {
        log.info("Method testReturnSameIfTypesAreEqualMethodReturnsFalseWhenToTypeIsNotAssignableFromObjectFrom() " +
                "started in class {}", getClass().getName());
        assertFalse(transformer.returnSameIfTypesAreEqual(testClassChild, AbstractTransformerTestClassNotChild.class));
    }

    @Test
    public void testReturnSameIfTypesAreEqualMethodReturnsTrueWhenToTypeIsAssignableFromObjectFrom() {
        log.info("Method testReturnSameIfTypesAreEqualMethodReturnsTrueWhenToTypeIsAssignableFromObjectFrom() " +
                "started in class {}", getClass().getName());
        assertTrue(transformer.returnSameIfTypesAreEqual(testClassChild, AbstractTransformerTestClassChild.class));
        assertTrue(transformer.returnSameIfTypesAreEqual(testClassChild, AbstractTransformerTestClass.class));
    }

    @Test
    public void testReturnSameIfTypesAreEqualMethodReturnsTrueWhenObjectFromIsNull() {
        log.info("Method testReturnSameIfTypesAreEqualMethodReturnsTrueWhenObjectFromIsNull() started in class {}",
                getClass().getName());
        assertTrue(transformer.returnSameIfTypesAreEqual(null, Class.class));
    }

    @Test
    public void testReturnSameIfTypesAreEqualMethodThrowsExceptionWhenClassToTypeIsNull() {
        log.info("Method testReturnSameIfTypesAreEqualMethodThrowsExceptionWhenClassToTypeIsNull() started in class {}",
                getClass().getName());
        assertThrows(AbstractTransformerException.class, () -> transformer.returnSameIfTypesAreEqual(Class.class, null));
    }

}