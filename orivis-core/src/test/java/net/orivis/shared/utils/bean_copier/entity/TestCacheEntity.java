package net.orivis.shared.utils.bean_copier.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import net.orivis.shared.utils.bean_copier.entity.bean_copier.OrivisEntity;

@Data
public class TestCacheEntity extends OrivisEntity {
    private String name;

    @Override
    public String asValue() {
        return "";
    }
}
