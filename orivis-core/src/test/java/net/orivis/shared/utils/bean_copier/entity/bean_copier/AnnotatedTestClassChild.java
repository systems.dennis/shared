package net.orivis.shared.utils.bean_copier.entity.bean_copier;

import lombok.Data;
import net.orivis.shared.annotations.FormTransient;
import org.springframework.stereotype.Component;

import jakarta.persistence.Column;

@Data
@Component
public class AnnotatedTestClassChild extends AnnotatedTestClass {

    @Column
    private Byte byteField;

    @Column
    @FormTransient
    private Short shortField;

    @Column
    public static Float floatField;
}
