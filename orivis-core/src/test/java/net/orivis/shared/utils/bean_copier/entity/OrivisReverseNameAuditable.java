package net.orivis.shared.utils.bean_copier.entity;

import net.orivis.shared.annotations.entity.OrivisAuditableService;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.entity.AbstractOrivisEntity;
import net.orivis.shared.utils.bean_copier.BeanCopier;

import java.lang.reflect.Field;

public class OrivisReverseNameAuditable implements OrivisAuditableService {

    public OrivisReverseNameAuditable() {}
    @Override
    public void process(Field field, OrivisContext context, int type, AbstractOrivisEntity<?> origin, AbstractOrivisEntity<?> entity) {
        var value  = BeanCopier.readValue(entity ,  field);
        BeanCopier.setFieldValue(entity, field, new StringBuilder(value.toString()).reverse().toString());
    }
}
