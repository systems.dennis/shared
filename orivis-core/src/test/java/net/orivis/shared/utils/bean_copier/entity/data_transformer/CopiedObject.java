package net.orivis.shared.utils.bean_copier.entity.data_transformer;

import lombok.Data;
import lombok.NoArgsConstructor;
import net.orivis.shared.utils.bean_copier.OrivisTranformer;
import net.orivis.shared.utils.bean_copier.DateToUTCConverter;

import java.util.Date;


@Data
@NoArgsConstructor
public class CopiedObject {
    @OrivisTranformer(transformWith = DateToUTCConverter.class)
    private String fromStringToDateType;
    @OrivisTranformer(transformWith = DateToUTCConverter.class)
    private Date fromDateToStringType;
}
