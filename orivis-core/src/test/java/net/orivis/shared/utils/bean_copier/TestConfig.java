package net.orivis.shared.utils.bean_copier;

import jakarta.servlet.http.HttpServletRequest;
import net.orivis.shared.beans.IdToAuthorizationIdBean;
import net.orivis.shared.beans.LocaleBean;
import net.orivis.shared.config.MessageResourceSource;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.bean_copier.entity.abstract_transformer.AbstractTransformerTestClassChild;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.core.env.StandardEnvironment;
import org.springframework.mock.web.MockHttpServletRequest;

@TestConfiguration
public class TestConfig {

    @Bean
    public BeanCopier beanCopier(OrivisContext webContext) {
        return new BeanCopier(webContext);
    }

    @Bean
    public OrivisContext webContext(HttpServletRequest request, Environment environment, ApplicationContext applicationContext) {
        return new OrivisContext(request, new MessageResourceSource(), applicationContext, environment, new LocaleBean()
                , new IdToAuthorizationIdBean());
    }

    @Bean
    public MessageResourceSource messageResourceSource() {
        return new MessageResourceSource();
    }

    @Bean
    public HttpServletRequest httpServletRequest() {
        return new MockHttpServletRequest();
    }

    @Bean
    public LocaleBean localeBean() {
        return new LocaleBean();
    }

    @Bean
    public IdToAuthorizationIdBean idToAuthorizationIdBean() {
        return new IdToAuthorizationIdBean();
    }

    @Bean
    public Environment environment() {
        return new StandardEnvironment();
    }

    @Bean
    public AbstractTransformerTestClassChild abstractTransformerTestClassChild() {
        return new AbstractTransformerTestClassChild();
    }
}
