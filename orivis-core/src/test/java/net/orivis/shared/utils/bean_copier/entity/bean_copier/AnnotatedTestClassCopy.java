package net.orivis.shared.utils.bean_copier.entity.bean_copier;

import lombok.Data;
import net.orivis.shared.annotations.FormTransient;
import org.springframework.stereotype.Component;

@Data
@Component
public class AnnotatedTestClassCopy extends OrivisEntity {

    private String stringField;

    private Integer integerField;

    @FormTransient
    private Long longField;

    public static Double doubleField;

    private Byte byteField;

    @FormTransient
    private Short shortField;

    public static Float floatField;

    private Character characterField;

    @Override
    public String asValue() {
        return "";
    }
}
