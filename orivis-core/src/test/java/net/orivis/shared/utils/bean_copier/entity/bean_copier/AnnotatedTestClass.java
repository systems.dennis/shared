package net.orivis.shared.utils.bean_copier.entity.bean_copier;

import lombok.Data;
import net.orivis.shared.annotations.FormTransient;
import net.orivis.shared.utils.bean_copier.OrivisTranformer;
import net.orivis.shared.utils.bean_copier.DateToUTCConverter;
import org.springframework.stereotype.Component;

import jakarta.persistence.Column;

@Data
@Component
public class AnnotatedTestClass extends AnnotatedTestClassParent{
    @Column
    private String stringField;

    @Column
    @OrivisTranformer(transformWith = DateToUTCConverter.class)
    private Integer integerField;

    @Column
    @FormTransient
    private Long longField;

    @Column
    public static double doubleField;

}
