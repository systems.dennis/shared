package net.orivis.shared.utils.bean_copier.entity;

import lombok.extern.slf4j.Slf4j;
import net.orivis.shared.utils.bean_copier.ModelCache;
import net.orivis.shared.utils.bean_copier.TestConfig;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.Assert.assertEquals;

@Slf4j
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfig.class)
public class ModelCacheTest {
    @Test
    public void testAllAnnotationsAreFetchedOnModelCacheGetMethod(){

        var def = ModelCache.get(ComplexClass.class);

        assertEquals( 6 , def.all().size());
        assertEquals( 2 , def.getValidationFields().size());
        assertEquals( 2 , def.getAuditFields().size());
        assertEquals( 2 , def.getTransientFields().size());
        assertEquals( 4 , def.getCopyFields().size());
    }

    @Test
    public void testAuditablePerformedCorrectly(){
        var complex = new ComplexClass();

        complex.setName("complex");

        complex.setIdid("Test");
        var field = ModelCache.get(ComplexClass.class).findAny("name").getField();

        ModelCache.getInstance(OrivisReverseNameAuditable.class).process(
                field, null, 0, new ComplexClass(), complex
        );

        assertEquals("xelpmoc", complex.getName());
    }
}

