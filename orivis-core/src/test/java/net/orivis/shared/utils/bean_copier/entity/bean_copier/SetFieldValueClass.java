package net.orivis.shared.utils.bean_copier.entity.bean_copier;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.orivis.shared.annotations.FormTransient;
import net.orivis.shared.form.AbstractOrivisForm;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SetFieldValueClass implements AbstractOrivisForm<Long> {
    private Long id;

    private String value;

    @FormTransient
    private String trans;

    @Override
    public String asValue() {
        return "";
    }
}
