package net.orivis.shared.utils.bean_copier;

import lombok.extern.slf4j.Slf4j;
import net.orivis.shared.exceptions.DeveloperException;
import net.orivis.shared.model.OrivisIDPresenter;
import net.orivis.shared.utils.bean_copier.entity.bean_copier.AnnotatedTestClassChild;
import net.orivis.shared.utils.bean_copier.entity.bean_copier.AnnotatedTestClassCopy;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import net.orivis.shared.exceptions.BeanCopierException;
import net.orivis.shared.utils.bean_copier.entity.bean_copier.*;
import net.orivis.shared.utils.bean_copier.entity.data_transformer.CopiedObject;

import jakarta.persistence.Column;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfig.class)
class BeanCopierTest {
    public static String DEFAULT_DATE_TEMPLATE = "yyyy-MM-dd HH:mm:ss";

    @Autowired
    private BeanCopier beanCopier;
    private final AnnotatedTestClassChild annotatedChild = new AnnotatedTestClassChild();
    private AnnotatedTestClassCopy annotatedFieldsObjectCopy = new AnnotatedTestClassCopy();

    {
        annotatedChild.setStringField("String");
        annotatedChild.setIntegerField(111);
        annotatedChild.setLongField(100L);
        AnnotatedTestClass.doubleField = 2.5;
        annotatedChild.setByteField((byte) 1);
        annotatedChild.setShortField((short) 2);
        AnnotatedTestClassChild.floatField = 1.5F;
        annotatedChild.setCharacterField('&');
    }


    @Test
    public void testCopyTransientFieldsMethodReturnsCopyForTransientFieldsTargetClassWhichNotStatic() {
        log.info("Method testCopyTransientFieldsMethodReturnsCopyForTransientFieldsTargetClassWhichNotStatic() " +
                "started in class {}", getClass().getName());
        AnnotatedTestClassChild child = new AnnotatedTestClassChild();

        beanCopier.copyTransientFields(child, annotatedChild);
        assertEquals(annotatedChild.getShortField(), child.getShortField());
        assertEquals(annotatedChild.getLongField(), child.getLongField());
    }

    @Test
    public void testCopyTransientFieldsMethodDontReturnCopyForNotTransientFieldsTargetClass() {
        log.info("Method testCopyTransientFieldsMethodDontReturnCopyForNotTransientFieldsTargetClass() " +
                "started in class {}", getClass().getName());
        beanCopier.copyTransientFields( annotatedFieldsObjectCopy, annotatedChild);
        assertNotEquals(annotatedChild.getIntegerField(), annotatedFieldsObjectCopy.getIntegerField());
        assertNotEquals(annotatedChild.getByteField(), annotatedFieldsObjectCopy.getByteField());
    }


    @Test
    public void testCopyMethodReturnsNullWhenObjectFromIsNull() {
        log.info("Method testCopyMethodReturnsNullWhenObjectFromIsNull() started in class {}",
                getClass().getName());
        assertNull(beanCopier.copy(null, Class.class));
    }

    @Test
    public void testCopyMethodThrowsExceptionWhenTargetClassIsNull() {
        log.info("Method testCopyMethodThrowsExceptionWhenTargetClassIsNull() started in class {}",
                getClass().getName());
        assertThrows(DeveloperException.class, () -> beanCopier.copy(annotatedChild, null));
    }

    @Test
    public void testCopyMethodReturnsCopyForFieldsTargetClassWhichNotStaticAndWithoutTransient() {
        log.info("Method testCopyMethodReturnsCopyForFieldsTargetClassWhichNotStaticAndWithoutTransient() started " +
                "in class {}", getClass().getName());
        annotatedFieldsObjectCopy = beanCopier.copy(annotatedChild, AnnotatedTestClassCopy.class);
        assertEquals(annotatedChild.getByteField(), annotatedFieldsObjectCopy.getByteField());
    }

    @Test
    public void testCopyMethodReturnsCopyForFieldsTargetTransient() {
        log.info("Method testCopyMethodReturnsCopyForFieldsTargetTransient() started " +
                "in class {}", getClass().getName());
        annotatedFieldsObjectCopy = beanCopier.copy(annotatedChild, AnnotatedTestClassCopy.class);
        assertNull(annotatedFieldsObjectCopy.getShortField());
    }

    @Test
    public void testCopyMethodReturnsNoCopyForStaticFields() {
        log.info("Method testCopyMethodReturnsNoCopyForStaticFields() started " +
                "in class {}", getClass().getName());
        beanCopier.copyTransientFields( annotatedFieldsObjectCopy, annotatedChild);
        assertNull(AnnotatedTestClassCopy.doubleField);
        assertNull(AnnotatedTestClassCopy.floatField);
    }

    @Test
    public void testCopyMethodReturnsNoCopyForTransientAnnotatedFields() {
        log.info("Method testCopyMethodReturnsNoCopyForTransientAnnotatedFields() started " +
                "in class {}", getClass().getName());
        annotatedFieldsObjectCopy = beanCopier.copy(annotatedChild, AnnotatedTestClassCopy.class);
        assertNull(annotatedFieldsObjectCopy.getLongField());
        assertNull(annotatedFieldsObjectCopy.getShortField());
    }

    @Test
    public void testCopyMethodReturnsCopyForFieldsTargetClassAndAllItParents() {
        log.info("Method testCopyMethodReturnsNoCopyForTransientAnnotatedFields() started " +
                "in class {}", getClass().getName());
        annotatedFieldsObjectCopy = beanCopier.copy(annotatedChild, AnnotatedTestClassCopy.class);
        assertEquals(annotatedChild.getStringField(), annotatedFieldsObjectCopy.getStringField());
    }

    @Test
    public void testFindFieldMethodReturnsNullWhenNameOfFieldIsNull() {
        log.info("Method testFindFieldMethodReturnsNullWhenNameOfFieldIsNull() started " +
                "in class {}", getClass().getName());
        assertNull(BeanCopier.findField(null, Class.class));
    }

    @Test
    public void testFindFieldMethodReturnsNullWhenSourceClassIsNull() {
        log.info("Method testFindFieldMethodReturnsNullWhenSourceClassIsNull() started " +
                "in class {}", getClass().getName());
        String fieldName = "String";
        assertNull(BeanCopier.findField(fieldName, null));
    }

    @Test
    public void testFindFieldMethodReturnsTargetFieldWhenClassContainsIt() {
        log.info("Method testFindFieldMethodReturnsTargetFieldWhenClassContainsIt() started " +
                "in class {}", getClass().getName());
        String fieldName = AnnotatedTestClass.class.getDeclaredFields()[0].getName();
        var actualFieldName = BeanCopier.findField(fieldName, AnnotatedTestClass.class).getName();
        assertEquals(fieldName, actualFieldName);
    }

    @Test
    public void testFindFieldMethodReturnsTargetFieldWhenParentsClassContainsIt() {
        log.info("Method testFindFieldMethodReturnsTargetFieldWhenParentsClassContainsIt() started " +
                "in class {}", getClass().getName());
        String fieldName = AnnotatedTestClassParent.class.getDeclaredFields()[0].getName();
        var actualFieldName = BeanCopier.findField(fieldName, AnnotatedTestClassChild.class).getName();
        assertEquals(fieldName, actualFieldName);
    }

    @Test
    public void testFindFieldMethodThrowsExceptionWhenDoNotContainIt() {
        log.info("Method testFindFieldMethodThrowsExceptionWhenDoNotContainIt() started " +
                "in class {}", getClass().getName());
        String unknownField = "Unknown";
        assertThrows(BeanCopierException.class, () -> BeanCopier.findField(unknownField, AnnotatedTestClass.class));
    }

    @Test
    public void testWithEachMethodReturnsFieldsListWhenParametersAreCorrect() {
        log.info("Method testWithEachMethodReturnsFieldsListWhenParametersAreCorrect() started " +
                "in class {}", getClass().getName());
        var fieldsList = new ArrayList<String>();
        BeanCopier.withEach(AnnotatedTestClassParent.class, entity -> fieldsList.add(entity.getName()));
        int actualLength = fieldsList.size();
        int expectedLength = AnnotatedTestClassParent.class.getDeclaredFields().length +
                OrivisEntity.class.getDeclaredFields().length + OrivisIDPresenter.class.getDeclaredFields().length;

        assertEquals(expectedLength, actualLength);
    }

    @Test
    public void testWithEachMethodReturnsFieldsListWithoutStaticAndTransientParameters() {
        log.info("Method testWithEachMethodReturnsFieldsListWithoutStaticAndTransientParameters() started " +
                "in class {}", getClass().getName());
        var fieldsList = new ArrayList<String>();
        BeanCopier.withEach(AnnotatedTestClassCopy.class, entity -> fieldsList.add(entity.getName()));
        int actualLength = fieldsList.size();
        int numberStaticAndTransientFields = 2;
        int expectedLength = AnnotatedTestClassCopy.class.getDeclaredFields().length - numberStaticAndTransientFields +
                OrivisEntity.class.getDeclaredFields().length + OrivisIDPresenter.class.getDeclaredFields().length;
        assertEquals(expectedLength, actualLength);
    }

    @Test
    public void testFindAnnotatedFieldsMethodReturnsEmptyListWhenClassIsNull() {
        log.info("Method testFindAnnotatedFieldsMethodReturnsEmptyListWhenClassIsNull() started " +
                "in class {}", getClass().getName());
        var annotatedFieldsList = BeanCopier.findAnnotatedFields(null, Column.class);
        assertTrue(annotatedFieldsList.isEmpty());
    }

    @Test
    public void testFindAnnotatedFieldsMethodReturnsEmptyListWhenAnnotationIsNull() {
        log.info("Method testFindAnnotatedFieldsMethodReturnsEmptyListWhenAnnotationIsNull() started " +
                "in class {}", getClass().getName());
        var annotatedFieldsList = BeanCopier.findAnnotatedFields(Class.class, null);
        assertTrue(annotatedFieldsList.isEmpty());
    }

    @Test
    public void testFindAnnotatedFieldsMethodReturnsFieldsListWhenClassAndAnnotationAreCorrect() {
        log.info("Method testFindAnnotatedFieldsMethodReturnsFieldsListWhenClassAndAnnotationAreCorrect() started " +
                "in class {}", getClass().getName());
        var fieldsList = BeanCopier.findAnnotatedFields(AnnotatedTestClassParent.class, Column.class);
        int expectedFieldsNumber = AnnotatedTestClassParent.class.getDeclaredFields().length;
        assertEquals(expectedFieldsNumber, fieldsList.size());
    }

    @Test
    public void testFindAnnotatedFieldsMethodReturnsEmptyListWhenThereIsNoFieldsWithThisAnnotation() {
        log.info("Method testFindAnnotatedFieldsMethodReturnsEmptyListWhenThereIsNoFieldsWithThisAnnotation() started " +
                "in class {}", getClass().getName());
        var fieldsList = BeanCopier.findAnnotatedFields(AnnotatedTestClassParent.class, OrivisTranformer.class);
        int realFieldsNumber = AnnotatedTestClassParent.class.getDeclaredFields().length;
        assertNotEquals(realFieldsNumber, fieldsList.size());
    }

    @Test
    public void testSetValueFromNotNullToNullIsCorrect() throws InvocationTargetException, IllegalAccessException {
        log.info("Method testReadValueMethodGetValueWhenFieldWasFound() started " +
                "in class {}", getClass().getName());
        var obj = new SetFieldValueClass(1L, "Old", "Default");


        BeanCopier.setFieldValue(obj, BeanCopier.findField("value", SetFieldValueClass.class), null);
        assertNull(obj.getValue());
        BeanCopier.setFieldValue(obj, BeanCopier.findField("value", SetFieldValueClass.class), "50");
        assertEquals("50", obj.getValue());
    }

    private final CopiedObject copiedObject = new CopiedObject();
    private final SimpleDateFormat formatForDateNow = new SimpleDateFormat(DEFAULT_DATE_TEMPLATE);

    {
        copiedObject.setFromStringToDateType("09.08.2020");
        copiedObject.setFromDateToStringType(new Date());
    }
}