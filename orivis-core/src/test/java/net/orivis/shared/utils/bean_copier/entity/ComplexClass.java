package net.orivis.shared.utils.bean_copier.entity;

import lombok.Data;
import net.orivis.shared.annotations.FormTransient;
import net.orivis.shared.annotations.Validation;
import net.orivis.shared.annotations.entity.OrivisAuditable;
import net.orivis.shared.utils.bean_copier.entity.bean_copier.OrivisEntity;
import net.orivis.shared.validation.ValueNotEmptyValidator;

@Data
public class ComplexClass extends OrivisEntity {
    @Validation(ValueNotEmptyValidator.class)
    @OrivisAuditable(OrivisReverseNameAuditable.class)
    private String idid;

    @OrivisAuditable (OrivisReverseNameAuditable.class)
    private String name;


    private String description;

    @FormTransient
    @Validation (ValueNotEmptyValidator.class)
    private String transientField;

    @Override
    public String asValue() {
        return "";
    }
}
