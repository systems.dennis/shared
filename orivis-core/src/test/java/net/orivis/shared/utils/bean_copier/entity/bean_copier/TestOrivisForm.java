package net.orivis.shared.utils.bean_copier.entity.bean_copier;

import lombok.Data;
import net.orivis.shared.form.AbstractOrivisForm;

@Data
public class TestOrivisForm implements AbstractOrivisForm<Long> {
    private Long id;
    private String name;
    private Boolean aBoolean = true;

    @Override
    public String asValue() {
        return "";
    }
}
