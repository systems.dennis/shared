2025 -> The end of shared support. Product migrated to orivis
*-Version 3.6.3*
GetOrCreateItemController -> controller to get or crate a new element
*-Version 3.6.2*
Added class information to validation error keys
*-Version 3.5.6*
Allow to get count of children on parent control on direct request 
*-Version 3.5.7*
ignore on Condition is now @. the @ object has 2 parameter value: a class ignore on condition, and parameters param 
*-Version 3.5.*
Delete by query possibility added
*-Version 3.4.*
Added sorting to the abstract Filter
REFERENCED_ID type present
*-Version 3.4.3
All default repo methods are wrapped in AbstractService layer 
some minor bug fixes
*-Version 3.4.0
Delete is now only DELETE method, not as it was before POST
*-Version 3.4.0
Get by id or Create interface is added, critical issues fixed

*-Version 3.3.1
Fully removed functions assignUser
Use @CreatedBy annotation instead

*-Version 3.3.0
No more Search Entity required
Searches are created automatically
Override method search if you need to disallow it or use allowEntitySearch of WebFormSupportAnnotation
*-Version 3.2.6-*
Fixes for the pom xml to avoid log4j dependency problem
non critical issues related to core
@Created and CreatedBy / Updated / UpdatedBy annotations presented
*-Version 3.2.x-*
Mongo support fully allowed by shared-web-db-mongo component
Created Updated CreatedBy UpdatedBy annotations
*-Version 3.1.x-*
Migration to spring boot 3.0.7
No more spring security allowed
ID is no longer Long strict, can be also String
Db updater is now separate project
Default Repo service is not supported anymore
Added support for different ID types
*-Version 3.0.x-*
No more support of thymeleaf (bye bye)
Spring 3.0.6 migration
BeanCopier introduced

sonar fixes

*-Version 2.2.4.2-*
Web details support
Edit field support



*-Version 2.2.4-*
Full form data support. 
Allows to get data about forms and list through json format 
Implementation is MagicList And MagicForm
Also includes JS support as magic.list.js and magic.js

Includes solution for Searching and column sorting ordering, displaying

Import calls now after import method and can be overridden by  implementation class
*-Version 2.1.3-*
Fixed problems with Validation context
added default language config

*-Version 2.1.2.3-*

Pagination fix. Independend dif
No pages shown when only 1 page is present


-Version 2.1.2-*

Added ImportController support

*-Version 2.1.0-*

 - [x] All default controllers are now returning FORM TYPE
 - [x] All DB_TYPE is now extended from BaseEntity
 - [x] DefaultForm is now Renamed to DefaultForm
 - [x] All FORM_TYPE is now extended from DefaultForm
 - [x] Import functional first implementation 
 - [x] DeleteFormController is not DELETED 
 - [x] API description is now strict DB_TYPE, FORM for all subsystems
