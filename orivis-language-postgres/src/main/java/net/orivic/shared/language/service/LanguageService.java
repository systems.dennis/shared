package net.orivic.shared.language.service;

import net.orivic.shared.language.form.LanguageForm;
import net.orivic.shared.language.model.LanguageModel;
import net.orivic.shared.language.repo.LanguageRepo;
import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.postgres.service.PaginationService;
import org.springframework.stereotype.Service;


@Service
@OrivisService(model = LanguageModel.class, form = LanguageForm.class, repo = LanguageRepo.class)
public class LanguageService extends PaginationService<LanguageModel> {

    public LanguageService(OrivisContext holder) {
        super(holder);
    }
}
