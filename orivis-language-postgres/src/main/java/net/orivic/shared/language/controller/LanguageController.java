package net.orivic.shared.language.controller;

import net.orivic.shared.language.form.LanguageForm;
import net.orivic.shared.language.model.LanguageModel;
import net.orivic.shared.language.service.LanguageService;
import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.controller.SearchEntityApi;
import net.orivis.shared.controller.SearcherInfo;
import net.orivis.shared.controller.orivis.OrivisRequest;
import net.orivis.shared.postgres.controller.AddItemController;
import net.orivis.shared.postgres.controller.DeleteItemController;
import net.orivis.shared.postgres.controller.EditItemController;
import net.orivis.shared.postgres.controller.ListItemController;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/v2/shared/language")
@OrivisController(LanguageService.class)
@CrossOrigin
@Secured (roles = "ROLE_ADMIN")
public class LanguageController
        extends OrivisContextable
        implements AddItemController<LanguageModel, LanguageForm>,
        EditItemController<LanguageModel, LanguageForm>,
        ListItemController<LanguageModel, LanguageForm>,
        DeleteItemController<LanguageModel> {

    static {
        SearchEntityApi.registerSearch("language", new SearcherInfo("name", LanguageService.class));
    }

    @GetMapping("/list")
    @ResponseBody
    @Override
    public ResponseEntity<Page<Map<String, Object>>> get(
            @RequestParam(value = "limit", required = false) Integer limit,
            @RequestParam(value = "page", required = false) Integer page) {
        return ListItemController.super.get( limit, page);
    }

    @PostMapping(value = {"/root/fetch/data", "/root/fetch/data/"}, produces = "application/json", consumes = "application/json")
    @ResponseBody
    @Override
    public Page<Map<String, Object>> fetchData(@RequestBody OrivisRequest request) {
        return ListItemController.super.fetchData(request);
    }

    public LanguageController(OrivisContext context) {
        super(context);
    }

}
