package net.orivic.shared.language.model;

import jakarta.persistence.Entity;
import lombok.Data;
import net.orivis.shared.postgres.model.OrivisEntity;


@Data
@Entity
public class LanguageModel extends OrivisEntity {

    private String name;
    private String icon;

    private String code;

    private Boolean active;

    @Override
    public String asValue() {
        return name;
    }
}
