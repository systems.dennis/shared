package net.orivic.shared.language.repo;

import net.orivic.shared.language.model.LanguageModel;
import net.orivis.shared.postgres.repository.OrivisRepository;

import java.util.Optional;

public interface LanguageRepo extends OrivisRepository<LanguageModel> {

    Optional<LanguageModel> findByCode(String code);
}
