package net.orivis.shared.reactions.controller;

import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.mongo.model.OrivisAssignableEntity;
import net.orivis.shared.repository.OrivisFilter;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.controller.SearchEntityApi;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.exceptions.ItemNotUserException;
import net.orivis.shared.reactions.exception.ReactionException;
import net.orivis.shared.reactions.form.ReactionItemForm;
import net.orivis.shared.reactions.model.ReactionItemModel;
import net.orivis.shared.reactions.service.ReactionItemService;
import net.orivis.shared.mongo.controller.AddItemController;
import net.orivis.shared.mongo.controller.DeleteItemController;
import net.orivis.shared.mongo.controller.ListItemController;
import net.orivis.shared.utils.bean_copier.BeanCopier;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static net.orivis.shared.form.AbstractOrivisForm.ID_FIELD;

@RestController
@RequestMapping("/api/v2/shared/reaction")
@OrivisController(value = ReactionItemService.class)
@CrossOrigin
@Secured
public class ReactionItemController<ID_TYPE extends Serializable> extends OrivisContextable
        implements AddItemController<ReactionItemModel, ReactionItemForm>, ListItemController<ReactionItemModel, ReactionItemForm>,
        DeleteItemController<ReactionItemModel> {

    public ReactionItemController(OrivisContext context) {
        super(context);
    }

    @GetMapping("/enabled")
    @ResponseBody
    @WithRole
    public ResponseEntity<ReactionResponse> enabled() {

        return ResponseEntity.ok(new ReactionResponse(getService().getIsReactionEnabled(), ""));
    }


    @GetMapping("/byName")
    @ResponseBody
    @WithRole
    public ResponseEntity<ReactionResponse> byName(@RequestParam("name") String name) {

        return ResponseEntity.ok(new ReactionResponse(getService().getIsReactionEnabled(), getService().findBySearchName(name)));
    }


    @PostMapping("/is_reaction")
    @ResponseBody
    @WithRole
    public ResponseEntity<ReactionResponse> isReaction(@RequestBody ReactionItemForm form) {
        return ResponseEntity.ok(new ReactionResponse(getService().isReactionForObject(form), ""));
    }


    @PostMapping("/delete")

    public ReactionResponse delete(@RequestBody ReactionItemForm id) throws ItemNotUserException, ItemNotFoundException {
        getService().delete(id);
        return  new ReactionResponse(true, "");
    }

    @GetMapping("/list/{type}/{searchName}")
    @ResponseBody
    @WithRole
    public ResponseEntity<Page<Map<String, Object>>> findByType(@PathVariable String type, @PathVariable("searchName") String name,
                                                             @RequestParam(value = "limit", required = false) Integer limit,
                                                             @RequestParam(value = "page", required = false) Integer page) {

        if (!getService().getIsReactionEnabled()){
            return ResponseEntity.ok(Page.empty());
        }

        var searchBean = SearchEntityApi.findServiceByType(name);

        if (searchBean == null){
            throw new ReactionException("global.exception.reaction.no_search_type_present");
        }

        OrivisFilter<ReactionItemModel> specification = getService().getFilterImpl().eq("userDataId", getContext().getCurrentUser())
                .and(getService().getFilterImpl().eq("type", type));
        Page<ReactionItemModel> models = getContext().getBean(ReactionItemService.class).find(specification, limit, page);

        List<String> ids = models.stream().map(ReactionItemModel::getModelId).collect(Collectors.toList());

        var bean = getContext().getBean(searchBean);



        return ResponseEntity.ok(toFormPage(bean.find(getService().getFilterImpl().iN(ID_FIELD, ids),-1, 0), searchBean));
    }


    public Page<Map<String, Object>> toFormPage(Page<?extends OrivisAssignableEntity> page, Class<?> c) {
        var ann = c.getAnnotation(OrivisService.class);
        var copier = getBean(BeanCopier.class);
        List<Map<String, Object>> data = new ArrayList<>();
        for (OrivisAssignableEntity entity : page.getContent()){

            data.add(BeanCopier.values(copier.copy(entity, ann.form()), entity, getContext()));
        }

        return new PageImpl<>(data, page.getPageable(), data.size());
    }

    @Override
    public ReactionItemService getService() {
        return  AddItemController.super.getService();
    }
}
