package net.orivis.shared.reactions.exception;

import org.springframework.http.HttpStatus;
import net.orivis.shared.exceptions.StatusException;

public class ReactionException extends StatusException {
    public ReactionException(String s) {
        super(s, HttpStatus.BAD_REQUEST);
    }
}
