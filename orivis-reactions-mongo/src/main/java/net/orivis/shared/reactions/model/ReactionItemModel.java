package net.orivis.shared.reactions.model;

import jakarta.persistence.Entity;
import lombok.Data;
import net.orivis.shared.mongo.model.OrivisAssignableEntity;

@Data
@Entity
public class ReactionItemModel extends OrivisAssignableEntity {

    private String modelId;

    private String type;

    private String reactionType;

    @Override
    public String asValue() {
        return type + "model_" +  getId()  + super.getUserDataId();
    }
}
