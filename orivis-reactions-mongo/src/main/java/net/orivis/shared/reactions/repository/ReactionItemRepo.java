package net.orivis.shared.reactions.repository;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;
import net.orivis.shared.reactions.model.ReactionItemModel;
import net.orivis.shared.mongo.repository.PaginationRepository;

@Repository
public class ReactionItemRepo extends PaginationRepository<ReactionItemModel> {

    public ReactionItemRepo(MongoTemplate mongoTemplate) {
        super(mongoTemplate);
    }
}
