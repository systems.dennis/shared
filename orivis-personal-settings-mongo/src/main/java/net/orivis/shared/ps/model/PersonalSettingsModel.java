package net.orivis.shared.ps.model;

import lombok.Data;
import net.orivis.shared.mongo.model.OrivisAssignableEntity;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class PersonalSettingsModel extends OrivisAssignableEntity {
    public static final String PERSONAL_SETTING_NAME_FIELD = "name" ;
    private String name;
    private String value;
    private String type;

    @Override
    public String asValue() {
        return name;
    }
}
