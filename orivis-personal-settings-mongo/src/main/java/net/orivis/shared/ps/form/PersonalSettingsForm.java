package net.orivis.shared.ps.form;

import lombok.Data;
import net.orivis.shared.annotations.Validation;
import net.orivis.shared.mongo.form.OrivisPojo;
import net.orivis.shared.pojo_form.PojoFormElement;
import net.orivis.shared.pojo_view.UIAction;
import net.orivis.shared.pojo_view.list.PojoListView;
import net.orivis.shared.pojo_view.list.PojoListViewField;
import net.orivis.shared.validation.ValueNotEmptyValidator;

import static net.orivis.shared.pojo_view.DEFAULT_TYPES.HIDDEN;


@Data
@PojoListView (actions = {"settings"}, enableSearching = true)
public class PersonalSettingsForm implements OrivisPojo {

    @PojoFormElement (type = HIDDEN)
    private String id;

    @Validation(ValueNotEmptyValidator.class)
    @PojoFormElement (required = true)
    @PojoListViewField (searchable = true)
    private String name;
    @PojoListViewField(visible = true)
    @PojoFormElement(type = HIDDEN)
    private String type;

    @PojoFormElement (required = true)
    @PojoListViewField (searchable = true, visible = true)
    @Validation (ValueNotEmptyValidator.class)
    private String value;

    @PojoListViewField (visible = false, actions = {  @UIAction(component = "delete") } )
    private int actions;

    @Override
    public String asValue() {
        return name;
    }

}
