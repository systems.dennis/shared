package net.orivis.shared.ps.repository;


import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;
import net.orivis.shared.mongo.repository.PaginationRepository;
import net.orivis.shared.ps.model.PersonalSettingsModel;

import java.io.Serializable;

@Repository
public class PersonalSettingsRepo<ID_TYPE extends Serializable> extends PaginationRepository<PersonalSettingsModel> {

    public PersonalSettingsRepo(MongoTemplate mongoTemplate) {
        super(mongoTemplate);
    }
}
