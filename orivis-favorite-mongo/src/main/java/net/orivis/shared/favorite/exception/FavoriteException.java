package net.orivis.shared.favorite.exception;

import org.springframework.http.HttpStatus;
import net.orivis.shared.exceptions.StatusException;

public class FavoriteException extends StatusException {
    public FavoriteException(String s) {
        super(s, HttpStatus.BAD_REQUEST);
    }
}
