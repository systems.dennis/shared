package net.orivis.shared.favorite.controller;

import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.mongo.model.OrivisAssignableEntity;
import net.orivis.shared.repository.OrivisFilter;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.controller.SearchEntityApi;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.exceptions.ItemNotUserException;
import net.orivis.shared.favorite.exception.FavoriteException;
import net.orivis.shared.favorite.form.FavoriteItemForm;
import net.orivis.shared.favorite.model.FavoriteItemModel;
import net.orivis.shared.favorite.service.FavoriteItemService;
import net.orivis.shared.mongo.controller.AddItemController;
import net.orivis.shared.mongo.controller.DeleteItemController;
import net.orivis.shared.mongo.controller.ListItemController;
import net.orivis.shared.utils.bean_copier.BeanCopier;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static net.orivis.shared.form.AbstractOrivisForm.ID_FIELD;

@RestController
@RequestMapping("/api/v2/shared/favorite")
@OrivisController(value = FavoriteItemService.class)
@CrossOrigin
@Secured
public class FavoriteItemController<ID_TYPE extends Serializable> extends OrivisContextable
        implements AddItemController<FavoriteItemModel, FavoriteItemForm>, ListItemController<FavoriteItemModel, FavoriteItemForm>,
        DeleteItemController<FavoriteItemModel> {

    public FavoriteItemController(OrivisContext context) {
        super(context);
    }

    @GetMapping("/enabled")
    @ResponseBody
    @WithRole
    public ResponseEntity<FavoriteResponse> enabled() {

        return ResponseEntity.ok(new FavoriteResponse(getService().getIsFavoriteEnabled(), ""));
    }


    @GetMapping("/byName")
    @ResponseBody
    @WithRole
    public ResponseEntity<FavoriteResponse> byName(@RequestParam("name") String name) {

        return ResponseEntity.ok(new FavoriteResponse(getService().getIsFavoriteEnabled(), getService().findBySearchName(name)));
    }


    @PostMapping("/is_favorite")
    @ResponseBody
    @WithRole
    public ResponseEntity<FavoriteResponse> isFavorite(@RequestBody FavoriteItemForm form) {
        return ResponseEntity.ok(new FavoriteResponse(getService().isFavoriteForObject(form), ""));
    }


    @PostMapping("/delete")

    public FavoriteResponse delete(@RequestBody FavoriteItemForm id) throws ItemNotUserException, ItemNotFoundException {
        getService().delete(id);
        return  new FavoriteResponse(true, "");
    }

    @GetMapping("/list/{type}/{searchName}")
    @ResponseBody
    @WithRole
    public ResponseEntity<Page<Map<String, Object>>> findByType(@PathVariable String type, @PathVariable("searchName") String name,
                                                             @RequestParam(value = "limit", required = false) Integer limit,
                                                             @RequestParam(value = "page", required = false) Integer page) {

        if (!getService().getIsFavoriteEnabled()){
            return ResponseEntity.ok(Page.empty());
        }

        var searchBean = SearchEntityApi.findServiceByType(name);

        if (searchBean == null){
            throw new FavoriteException("global.exception.favorite.no_search_type_present");
        }

        OrivisFilter<FavoriteItemModel> specification = getService().getFilterImpl().eq("userDataId", getContext().getCurrentUser())
                .and(getService().getFilterImpl().eq("type", type));
        Page<FavoriteItemModel> models = getContext().getBean(FavoriteItemService.class).find(specification, limit, page);

        List<String> ids = models.stream().map(FavoriteItemModel::getModelId).collect(Collectors.toList());

        var bean = getContext().getBean(searchBean);



        return ResponseEntity.ok(toFormPage(bean.find(getService().getFilterImpl().iN(ID_FIELD, ids),-1, 0), searchBean));
    }


    public Page<Map<String, Object>> toFormPage(Page<?extends OrivisAssignableEntity> page, Class<?> c) {
        var ann = c.getAnnotation(OrivisService.class);
        var copier = getBean(BeanCopier.class);
        List<Map<String, Object>> data = new ArrayList<>();
        for (OrivisAssignableEntity entity : page.getContent()){

            data.add(BeanCopier.values(copier.copy(entity, ann.form()), entity, getContext()));
        }

        return new PageImpl<>(data, page.getPageable(), data.size());
    }

    @Override
    public FavoriteItemService getService() {
        return  AddItemController.super.getService();
    }
}
