package net.orivis.shared.favorite.repository;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;
import net.orivis.shared.favorite.model.FavoriteItemModel;
import net.orivis.shared.mongo.repository.PaginationRepository;

@Repository
public class FavoriteItemRepo extends PaginationRepository<FavoriteItemModel> {

    public FavoriteItemRepo(MongoTemplate mongoTemplate) {
        super(mongoTemplate);
    }
}
