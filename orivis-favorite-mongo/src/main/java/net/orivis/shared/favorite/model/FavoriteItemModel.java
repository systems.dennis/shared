package net.orivis.shared.favorite.model;

import jakarta.persistence.Entity;
import lombok.Data;
import net.orivis.shared.mongo.model.OrivisAssignableEntity;

@Data
@Entity
public class FavoriteItemModel extends OrivisAssignableEntity {

    private String modelId;

    private String type;

    @Override
    public String asValue() {
        return type + "model_" +  getId()  + super.getUserDataId();
    }
}
