package net.orivis.shared.favorite.form;

import lombok.Data;
import net.orivis.shared.annotations.Validation;
import net.orivis.shared.favorite.validation.FavoriteStoreLimitValidator;
import net.orivis.shared.mongo.form.OrivisPojo;
import net.orivis.shared.pojo_form.PojoFormElement;
import net.orivis.shared.pojo_view.UIAction;
import net.orivis.shared.pojo_view.list.PojoListView;
import net.orivis.shared.pojo_view.list.PojoListViewField;

import static net.orivis.shared.pojo_view.DEFAULT_TYPES.HIDDEN;

@Data
@PojoListView(actions = {})
public class FavoriteItemForm implements OrivisPojo {

    @PojoFormElement(type = HIDDEN)
    private String id;

    private String modelId;

    @PojoFormElement(required = true)
    @PojoListViewField(searchable = true)
    private String type;

    @PojoFormElement (available = false)
    @PojoListViewField( available = false)
    @Validation(FavoriteStoreLimitValidator.class)
    private String userDataId;

    @PojoListViewField(actions = {@UIAction(component = "delete")})
    @PojoFormElement(available = false)
    private Integer action;

    @Override
    public String asValue() {
        return type + " "  + userDataId + " " + modelId;
    }
}
