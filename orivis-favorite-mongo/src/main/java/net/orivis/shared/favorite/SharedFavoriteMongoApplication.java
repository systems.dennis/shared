package net.orivis.shared.favorite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SharedFavoriteMongoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SharedFavoriteMongoApplication.class, args);
	}

}
