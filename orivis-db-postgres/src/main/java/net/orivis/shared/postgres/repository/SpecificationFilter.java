package net.orivis.shared.postgres.repository;

import jakarta.persistence.criteria.Predicate;
import lombok.Data;
import net.orivis.shared.exceptions.StandardException;
import net.orivis.shared.model.OrivisIDPresenter;
import net.orivis.shared.postgres.repository.query_processors.AbstractClassProcessor;
import net.orivis.shared.repository.OrivisFilter;
import org.springframework.data.jpa.domain.Specification;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class SpecificationFilter<T extends OrivisIDPresenter<?>> implements OrivisFilter<T> {

    boolean empty = true;
    private Specification<T> root;
    private boolean complex;
    private String on;
    private Class<?> type;
    boolean calculated = false;
    private boolean insensitive;

    private boolean closed;
    private String operationType;
    private Object value;
    private String field;

    private List<SpecificationFilter<T>> or = new ArrayList<>();

    private List<SpecificationFilter<T>> and = new ArrayList<>();
    @Override
    public SpecificationFilter<T> operator(String field, Object value, String type) {

        if (this.isClosed()){
            throw new StandardException("query_was_already_closed", "only add/or functions are now available. " +
                    "Please check that 'operation' is performed after additional parameter' ");
        }

        if (field != null && type != null){
            setEmpty(false);
        }

        this.field = field;
        this.value = value;
        this.operationType = type;


        root = (root, query, criteriaBuilder) -> {
            var qq = AbstractClassProcessor.processor(SpecificationFilter.this, root);

            List<Predicate> predicates = new ArrayList<>();
            if (!qq.isNotNullCase()){
                qq.addToNullOrNotNullPredicate(criteriaBuilder, root, predicates);
            } else {
                qq.processDefault(criteriaBuilder, predicates);
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };

        this.closed = true;
        return this;
    }

    private Specification<T> getRoot(){
        return  root;
    }


    @Override
    public <E extends OrivisFilter<?>> E and(E filter) {
        if (isCalculated()) {
            throw new StandardException("query_was_already_closed", "due to limitations of Query you cannot use Query after you had already called method 'getCriteriaRoot()'");
        }


        and.add((SpecificationFilter<T> ) filter);

        this.empty = false;
        return (E) this;
    }

    @Override
    public <E extends OrivisFilter<?>> E or(E filter) {
        if (isCalculated()) {
            throw new StandardException("query_was_already_closed", "due to limitations of Query you cannot use Query after you had already called method 'getCriteriaRoot()'");
        }


        or.add((SpecificationFilter<T> ) filter);

        this.empty = false;
        return (E) this;
    }

    @Override
    public <E extends OrivisFilter<T>> E comparasionType(Class<?> type) {
        this.type = type;
        return (E) this;
    }

    @Override
    public SpecificationFilter<T> setInsensitive(boolean insensitive) {
        this.insensitive = insensitive;
        return this;
    }

    @Override
    public SpecificationFilter<T> setComplex(boolean complex) {
        this.complex = complex;
        return this;
    }

    @Override
    public SpecificationFilter<T> setJoinOn(String on) {
        this.on = on;
        setComplex(true);
        return this;
    }

    @Override
    public Serializable getIdValue(Object id) {
        if (id instanceof Integer) {
            return Long.valueOf((Integer) id);
        }

        if (id instanceof String) {
            return Long.valueOf((String) id);
        }

        return (long) id;
    }

    @Override
    public boolean isEmpty() {
        return empty;
    }

    @Override
    public String getOperator() {
        return operationType;
    }

    boolean isEmpty(OrivisFilter<?> filter){
        return  filter == null || filter.isEmpty();
    }

    public Class<?> getFieldClass() {
        try {

            return type == null ? getValue().getClass() : type;
        } catch (Exception e){
            return  String.class;
        }
    }

    @Override
    public <E> E getQueryRoot() {

        if (calculated) return (E) root;

        if (!or.isEmpty()) {

            or.forEach((x) -> {
                x.getQueryRoot();
                root = root.or(x.getRoot());
            });
        }
        if (!and.isEmpty()) {

            and.forEach((x) -> {
                x.getQueryRoot();
                root = root.and(x.getRoot());
            });
        }
        calculated = true;

        return (E) root;
    }

    public boolean getInsensitive() {
        return this.insensitive;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();

        sb.append("field=").append(this.field != null ? this.field : "null");
        sb.append(";operationType=").append(this.operationType != null ? this.operationType : "null");
        sb.append(";value=").append(this.value != null ? this.value.toString() : "null");
        sb.append(";insensitive=").append(this.insensitive);
        sb.append(";complex=").append(this.complex);

        if (!this.or.isEmpty()) {
            sb.append(";or=[");
            for (SpecificationFilter<T> spec : this.or) {
                sb.append(spec.toString()).append(",");
            }
            sb.deleteCharAt(sb.length() - 1);
            sb.append("]");
        }

        if (!this.and.isEmpty()) {
            sb.append(";and=[");
            for (SpecificationFilter<T> spec : this.and) {
                sb.append(spec.toString()).append(",");
            }
            sb.deleteCharAt(sb.length() - 1);
            sb.append("]");
        }

        return sb.toString();
    }
}
