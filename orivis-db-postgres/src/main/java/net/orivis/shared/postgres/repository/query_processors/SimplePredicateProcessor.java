package net.orivis.shared.postgres.repository.query_processors;

import jakarta.persistence.criteria.Root;
import net.orivis.shared.repository.OrivisFilter;

public class SimplePredicateProcessor extends AbstractClassProcessor {
    public SimplePredicateProcessor(OrivisFilter queryCase, Root root) {
        super(queryCase, root);
    }

    @Override
    public Object getValue(Object value) {
        return value;
    }
}
