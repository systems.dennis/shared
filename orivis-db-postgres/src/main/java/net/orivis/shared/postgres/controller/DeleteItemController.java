package net.orivis.shared.postgres.controller;

import net.orivis.shared.annotations.security.Id;
import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.controller.generated.AbstractDeleteItemController;
import net.orivis.shared.exceptions.ItemForAddContainsIdException;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.exceptions.ItemNotUserException;
import net.orivis.shared.postgres.model.OrivisEntity;
import net.orivis.shared.utils.security.DefaultIdChecker;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Secured
public interface DeleteItemController<T extends OrivisEntity> extends AbstractDeleteItemController<T,Long> {


    @DeleteMapping(value = "/delete/{id}",  consumes = {"application/json", "application/json;charset=UTF-8"}, produces = {"application/json", "application/json;charset=UTF-8"})
    @ResponseBody
    @WithRole
    @Override
    default void delete(@Id(checker = DefaultIdChecker.class) @PathVariable Long id) throws ItemForAddContainsIdException {
        AbstractDeleteItemController.super.delete(id);
    }

    @Override
    @WithRole
    @RequestMapping(value="/delete/deleteItems", method= RequestMethod.DELETE)
    default void deleteItems(@RequestParam List<Long> ids) throws ItemNotUserException, ItemNotFoundException {
        AbstractDeleteItemController.super.deleteItems(ids);
    }
}
