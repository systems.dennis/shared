package net.orivis.shared.postgres.controller;

import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.controller.generated.AbstractAddItemController;
import net.orivis.shared.exceptions.ItemForAddContainsIdException;
import net.orivis.shared.postgres.form.OrivisPojo;
import net.orivis.shared.postgres.model.OrivisEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
@Secured
public interface AddItemController<T extends OrivisEntity,E extends OrivisPojo> extends AbstractAddItemController<T,E, Long> {


    @PostMapping(value = "/add",  consumes = {"application/json", "application/json;charset=UTF-8"}, produces = {"application/json", "application/json;charset=UTF-8"})
    @ResponseBody
    @WithRole
    @Override
    default ResponseEntity<E> add(@RequestBody E form) throws ItemForAddContainsIdException {
        return AbstractAddItemController.super.add(form);
    }
}
