package net.orivis.shared.postgres.bean;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import net.orivis.shared.annotations.entity.PrimaryDateSetter;

import java.time.LocalDate;
import java.util.Date;

@Component @Scope("singleton")
public class PostgresDateInstance implements PrimaryDateSetter {
    @Override
    public <T> T getNewDateValue() {
        return (T) new Date();
    }
}
