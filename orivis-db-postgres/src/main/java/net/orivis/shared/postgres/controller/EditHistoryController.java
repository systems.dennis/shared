package net.orivis.shared.postgres.controller;

import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.annotations.security.SelfOnlyRole;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.exceptions.EditableObjectNotFoundException;
import net.orivis.shared.postgres.form.EditHistoryForm;
import net.orivis.shared.postgres.model.EditHistoryModel;
import net.orivis.shared.postgres.service.EditHistoryService;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v2/edited/history")
@OrivisController(value = EditHistoryService.class)
public class EditHistoryController extends OrivisContextable
        implements ListItemController<EditHistoryModel, EditHistoryForm> {

    public EditHistoryController(OrivisContext context) {
        super(context);
    }

    @SelfOnlyRole
    @GetMapping("/find/{type}/{id}")
    public EditHistoryForm findByEditedTypeAndId(@PathVariable String type, @PathVariable Long id) {
        EditHistoryService service = getService();
        return toForm(service.findByEditedTypeAndId(type, id)
                .orElseThrow(() -> new EditableObjectNotFoundException(type, id)));
    }

    @SelfOnlyRole
    @GetMapping("/find/{type}")
    public List<EditHistoryForm> findAllByType(@PathVariable String type) {
        EditHistoryService service = getService();
        return service.findByEditedType(type)
                .stream()
                .map(this::toForm)
                .collect(Collectors.toList());
    }


}
