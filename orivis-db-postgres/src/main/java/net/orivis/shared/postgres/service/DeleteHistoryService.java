package net.orivis.shared.postgres.service;

import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.postgres.form.DeleteHistoryForm;
import net.orivis.shared.postgres.model.DeleteHistoryModel;
import net.orivis.shared.postgres.repository.DeleteHistoryRepo;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Service
@OrivisService(model = DeleteHistoryModel.class, form = DeleteHistoryForm.class, repo = DeleteHistoryRepo.class)
public class DeleteHistoryService extends PaginationService<DeleteHistoryModel> {
    public DeleteHistoryService(OrivisContext holder) {
        super(holder);
    }

    @Override
    public Optional<DeleteHistoryModel> findById(Serializable id) {
        return getRepository().findById(updateId(id));
    }

    public boolean existsByTypeAndDeletedId(Long id, String type) {
        DeleteHistoryRepo repo = getRepository();
        return repo.existsByIdDeletedObjectAndClassDeletedObject(id, type);
    }

    public Optional<DeleteHistoryModel> findByTypeAndDeletedId(String type, Long id) {
        DeleteHistoryRepo repo = getRepository();
        return repo.findFirstByIdDeletedObjectAndClassDeletedObject(id, type);
    }

    public List<DeleteHistoryModel> findByType(String type) {
        DeleteHistoryRepo repo = getRepository();
        return repo.findAllByClassDeletedObject(type);
    }
}
