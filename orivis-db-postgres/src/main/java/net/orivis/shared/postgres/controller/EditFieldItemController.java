package net.orivis.shared.postgres.controller;

import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.controller.generated.AbstractEditFieldItemController;
import net.orivis.shared.controller.items.UpdateFieldResponse;
import net.orivis.shared.entity.KeyValue;
import net.orivis.shared.exceptions.ItemDoesNotContainsIdValueException;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.exceptions.UnmodifiedItemSaveAttemptException;
import net.orivis.shared.postgres.form.OrivisPojo;
import net.orivis.shared.postgres.model.OrivisEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.lang.reflect.InvocationTargetException;

@Secured
public interface EditFieldItemController<T extends OrivisEntity, E extends OrivisPojo> extends AbstractEditFieldItemController<T,E, Long> {

    @PutMapping(value = "/edit_field/id/{id}")
    @ResponseBody
    @WithRole
    @Override
    default ResponseEntity<UpdateFieldResponse<Long>> editField(@PathVariable Long id, @RequestBody KeyValue keyValue) throws ItemDoesNotContainsIdValueException, UnmodifiedItemSaveAttemptException, ItemNotFoundException, IllegalAccessException, InvocationTargetException {
        return AbstractEditFieldItemController.super.editField(id, keyValue);
    }

}
