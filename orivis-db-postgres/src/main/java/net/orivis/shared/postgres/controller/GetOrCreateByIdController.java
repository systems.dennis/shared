package net.orivis.shared.postgres.controller;

import lombok.SneakyThrows;
import net.orivis.shared.controller.generated.AbstractGetOrCreateByIdController;
import net.orivis.shared.postgres.form.OrivisPojo;
import net.orivis.shared.postgres.model.OrivisEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;

public interface GetOrCreateByIdController<DB_TYPE extends OrivisEntity, FORM extends OrivisPojo> extends AbstractGetOrCreateByIdController<DB_TYPE, FORM,  Long > {
    @SneakyThrows
    @GetMapping("/get_or_create/{id}")
    @ResponseBody
    default ResponseEntity<FORM> get(@PathVariable("id") String id, @RequestParam(required = false) String param) {
        DB_TYPE type;

        if (isNullId(id)) {
            type = null;
        } else {
            type = fetchById(id);
        }

        if (type == null) {
            var res = getService().save(fromForm(createNew(id, param)));
            return ResponseEntity.ok(toForm(res));
        }
        return ResponseEntity.ok(toForm(type));
    }

    /**
     * Id can be If ID type or else depending on what {@link #fetchById(Serializable)} returns
     * @param id
     * Long id is null if it is &lt;= 0;
     * It is highly recommended to override this method to avoid double type check here!
     * @return
     */
    default boolean isNullId(Serializable id){
        var res =  id == null;

        if (!res) {
            try {
                return Long.parseLong(String.valueOf(id)) <= 0;
            } catch (Exception e) {
                //ignored
            }
        }
        return res;
    }
}
