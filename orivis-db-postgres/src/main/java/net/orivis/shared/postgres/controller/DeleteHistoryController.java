package net.orivis.shared.postgres.controller;

import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.annotations.security.SelfOnlyRole;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.exceptions.HistoryObjectNotFoundException;
import net.orivis.shared.postgres.form.DeleteHistoryForm;
import net.orivis.shared.postgres.model.DeleteHistoryModel;
import net.orivis.shared.postgres.service.DeleteHistoryService;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v2/deleted/history")
@OrivisController(value = DeleteHistoryService.class)
public class DeleteHistoryController extends OrivisContextable implements ListItemController<DeleteHistoryModel, DeleteHistoryForm> {


    public DeleteHistoryController(OrivisContext context) {
        super(context);
    }

    @GetMapping("/find/{type}/{id}")
    @SelfOnlyRole
    public DeleteHistoryForm findByDeletedIdAndType(@PathVariable String type, @PathVariable Long id) {
        DeleteHistoryService service = getService();
        return toForm(service.findByTypeAndDeletedId(type, id).orElseThrow(() -> new HistoryObjectNotFoundException(id, type)));
    }

    @GetMapping("/find/{type}")
    @SelfOnlyRole
    public List<DeleteHistoryForm> findByType(@PathVariable String type) {
        DeleteHistoryService service = getService();
        return service.findByType(type)
                .stream()
                .map(this::toForm)
                .collect(Collectors.toList());
    }
}
