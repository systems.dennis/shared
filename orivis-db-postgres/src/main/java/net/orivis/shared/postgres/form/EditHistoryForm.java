package net.orivis.shared.postgres.form;

import lombok.Data;

import java.util.Date;

@Data
public class EditHistoryForm implements OrivisPojo {

    private Long id;

    private Long idEditedObject;

    private Date editedDate;

    private String classEditedObject;

    private String oldContent;

    private String newContent;
}
