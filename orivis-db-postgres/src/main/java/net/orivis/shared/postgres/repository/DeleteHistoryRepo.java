package net.orivis.shared.postgres.repository;

import org.springframework.stereotype.Repository;
import net.orivis.shared.postgres.model.DeleteHistoryModel;

import java.util.List;
import java.util.Optional;

@Repository
public interface DeleteHistoryRepo extends OrivisRepository<DeleteHistoryModel> {

    Optional<DeleteHistoryModel> findFirstByIdDeletedObjectAndClassDeletedObject(Long delId, String type);

    boolean existsByIdDeletedObjectAndClassDeletedObject(Long delId, String type);

    List<DeleteHistoryModel> findAllByClassDeletedObject(String type);
}
