package net.orivis.shared.postgres.repository.query_processors;

import jakarta.persistence.criteria.*;
import lombok.Data;
import net.orivis.shared.postgres.repository.SpecificationFilter;
import net.orivis.shared.repository.OrivisFilter;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;


@Data
public abstract class AbstractClassProcessor {
    private OrivisFilter filter;
    private Root root;

    private Class<?> type = String.class;

    private Object parameter = null;

    public AbstractClassProcessor (OrivisFilter AbstractDataFilter, Root root){

        this.filter = AbstractDataFilter;
        this.root = root;

        if (AbstractDataFilter.getFieldClass() == null && AbstractDataFilter.getValue() != null){
            type = AbstractDataFilter.getValue().getClass();
        }

    }

    public boolean isNotNullCase() {
        return !filter.NOT_NULL_OPERATOR.equalsIgnoreCase(filter.getOperator())
                && !filter.NULL_OPERATOR.equalsIgnoreCase(filter.getOperator());
    }
    public void addToNullOrNotNullPredicate(CriteriaBuilder criteriaBuilder, Root root, List<Predicate> predicates) {
        if (Objects.equals(getFilter().getOperator(), filter.NULL_OPERATOR)) {
            predicates.add(criteriaBuilder.isNull(getPath(root)));
        } else {
            predicates.add(criteriaBuilder.isNotNull(getPath(root)));
        }
    }
    public  <T> Expression<T> getPath(Root root) {
        if (!getFilter().isComplex()) {
            return root.get(getFilter().getField());
        }

        String[] paths = getFilter().getOn().split("\\.");
        Join join = null;
        for (String j : paths) {
            if (join == null) {
                join = root.join(j);
            } else {
                join = join.join(j);
            }
        }

        return join.get(getFilter().getField());
    }

    public void processDefault( CriteriaBuilder criteriaBuilder,  List<Predicate> predicates) {

        Object value = getValue(filter.getValue());

        if (value == null) {
            throw new ArithmeticException("Item is wrong type");
        }
        if (filter.LESS_THEN.equalsIgnoreCase(filter.getOperator())) {
            predicates.add(criteriaBuilder.lessThan(getPath(root), (Date) value));
        }
        if (filter.MORE_THEN.equalsIgnoreCase(filter.getOperator())) {
            predicates.add(criteriaBuilder.greaterThan(getPath(root), (Date) value));
        }
        if (filter.LESS_EQUALS.equalsIgnoreCase(filter.getOperator())) {
            predicates.add(criteriaBuilder.lessThanOrEqualTo(getPath(root), (Date) value));
        }
        if (filter.MORE_EQUALS.equalsIgnoreCase(filter.getOperator())) {
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(getPath(root), (Date) value));
        }


        if (filter.NOT_NULL_OPERATOR.equalsIgnoreCase(filter.getOperator())) {
            predicates.add(criteriaBuilder.isNotNull(getPath(root)));
        }
        if (filter.NULL_OPERATOR.equalsIgnoreCase(filter.getOperator())) {
            predicates.add(criteriaBuilder.isNull(getPath(root)));
        }
        if (filter.EQUALS_OPERATOR.equalsIgnoreCase(filter.getOperator()) && !String.class.isAssignableFrom(filter.getValue().getClass())) {
            predicates.add(criteriaBuilder.equal(getPath(root), value));
        }

        //There is a bug that excludes elements with a value of null from the search results
        if (filter.NOT_EQUALS_OPERATOR.equalsIgnoreCase(filter.getOperator())) {
            addIsNullOrNotEqualPredicate(criteriaBuilder, predicates, value);
        }
        if (filter.IN.equalsIgnoreCase(filter.getOperator())) {
            CriteriaBuilder.In inClause = criteriaBuilder.in(getPath(root));

            Collection cValue = (Collection) value;
            cValue.forEach((x)-> inClause.value(x) );
            predicates.add(inClause);
        }



    }

    protected void addIsNullOrNotEqualPredicate(CriteriaBuilder criteriaBuilder, List<Predicate> predicates, Object value) {
        predicates.add(
                criteriaBuilder.or(
                        criteriaBuilder.isNull(getPath(root)),
                        criteriaBuilder.notEqual(getPath(root), value)));
    }

    public abstract Object getValue(Object value);

    public static AbstractClassProcessor processor(SpecificationFilter filter, Root root){
        if (filter.getFieldClass() != null && Collection.class.isAssignableFrom(filter.getFieldClass())){
            return new CollectionPredicateProcessor(filter, root);
        }
        if (BigDecimal.class.equals(filter.getFieldClass())){
            return new BigDecimalProcessor(filter, root);
        }
        if (Date.class.equals(filter.getFieldClass()) && !Date.class.isAssignableFrom(filter.getValue().getClass())){
            return new DatePredicateProcessor(filter, root);
        }

        if  (filter.getFieldClass() != null && (Number.class.isAssignableFrom(filter.getFieldClass()) || isPrimitiveNumber(filter.getFieldClass()))) {
            return new NumberPredicateProcessor(filter, root);
        }

        if (filter.getFieldClass() != null && (String.class.isAssignableFrom(filter.getFieldClass()))){
            return new StringPredicateProcessor(filter, root);
        } else {
            return new SimplePredicateProcessor(filter, root);
        }


    }

    private static boolean isPrimitiveNumber(Class c) {
        return int.class == c || long.class == c || short.class == c || double.class == c || float.class == c ;
    }

}
