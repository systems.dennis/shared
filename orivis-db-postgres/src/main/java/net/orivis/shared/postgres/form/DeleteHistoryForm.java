package net.orivis.shared.postgres.form;

import lombok.Data;

import java.util.Date;

@Data
public class DeleteHistoryForm implements OrivisPojo {
    private Long id;

    private Long idDeletedObject;

    private Date deletedDate;

    private String classDeletedObject;

    private String content;
}
