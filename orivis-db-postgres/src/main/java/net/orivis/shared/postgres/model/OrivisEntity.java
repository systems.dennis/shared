package net.orivis.shared.postgres.model;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import lombok.Data;
import net.orivis.shared.model.OrivisIDPresenter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;


@Data
@MappedSuperclass
public abstract class OrivisEntity extends OrivisIDPresenter<Long> {
    private static final LongIdValidatorImpl validator = new LongIdValidatorImpl();

    @Id
    @GeneratedValue(generator = "entity-aware-generator", strategy = GenerationType.SEQUENCE)
    @GenericGenerator(name = "entity-aware-generator", parameters = {
            @org.hibernate.annotations.Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "initial_value", value = "100")

    }, strategy = "net.orivis.shared.postgres.model.ByEntityNameSequenceGenerator")
    private Long id;

    @Override
    public boolean isIdSet() {
        return validator.isIdSet(this.id);
    }
}
