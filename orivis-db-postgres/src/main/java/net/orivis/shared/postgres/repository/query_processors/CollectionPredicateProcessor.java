package net.orivis.shared.postgres.repository.query_processors;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import net.orivis.shared.postgres.repository.SpecificationFilter;
import net.orivis.shared.repository.OrivisFilter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CollectionPredicateProcessor extends AbstractClassProcessor{

    public CollectionPredicateProcessor(SpecificationFilter queryCase, Root root) {
        super(queryCase, root);
    }


    @Override
    public void processDefault(CriteriaBuilder criteriaBuilder, List<Predicate> predicates) {
        Expression<Collection<String>> collectionExpression = getPath(getRoot());
        var queryCase = getFilter();
        if (OrivisFilter.CONTAINS_OPERATOR.equalsIgnoreCase(queryCase.getOperator())) {
            predicates.add(criteriaBuilder.isMember(String.valueOf(queryCase.getValue()), collectionExpression));
        }
        if (OrivisFilter.NOT_CONTAINS_OPERATOR.equalsIgnoreCase(queryCase.getOperator())) {
            predicates.add(criteriaBuilder.isNotMember(String.valueOf(queryCase.getValue()), collectionExpression));
        }
        if (OrivisFilter.NOT_EMPTY.equalsIgnoreCase(queryCase.getOperator())) {
            predicates.add(criteriaBuilder.isNotEmpty(collectionExpression));
        }
        if (OrivisFilter.EMPTY.equalsIgnoreCase(queryCase.getOperator())) {
            predicates.add(criteriaBuilder.isEmpty(collectionExpression));
        }
        if (OrivisFilter.IN.equalsIgnoreCase(queryCase.getOperator())) {
            CriteriaBuilder.In inClause = criteriaBuilder.in(getPath(getRoot()));

            Collection cValue = (Collection) queryCase.getValue();
            cValue.forEach((x)-> inClause.value(x) );
            predicates.add(inClause);
        }
        if (OrivisFilter.NOT_IN.equalsIgnoreCase(queryCase.getOperator())) {
            CriteriaBuilder.In inClause = criteriaBuilder.in(getPath(getRoot()));

            Collection cValue = (Collection) queryCase.getValue();
            cValue.forEach((x)-> inClause.value(x) );
            predicates.add(criteriaBuilder.not(inClause));
        }
        if (OrivisFilter.NOT_EQUALS_OPERATOR.equalsIgnoreCase(queryCase.getOperator())) {
            Collection cValue = (Collection) queryCase.getValue();

            List<Predicate> notEqualPredicates = new ArrayList<>();

            for (Object value : cValue) {
                Predicate notEqualPredicate = criteriaBuilder.notEqual(collectionExpression, value);
                notEqualPredicates.add(notEqualPredicate);
            }
            Predicate combinedNotEqualPredicates = criteriaBuilder.and(notEqualPredicates.toArray(new Predicate[notEqualPredicates.size()]));
            predicates.add(combinedNotEqualPredicates);
        }

    }

    @Override
    public Object getValue(Object value) {
        return value;
    }
}
