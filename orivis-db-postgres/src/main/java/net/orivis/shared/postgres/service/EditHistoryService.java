package net.orivis.shared.postgres.service;

import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.postgres.form.EditHistoryForm;
import net.orivis.shared.postgres.model.EditHistoryModel;
import net.orivis.shared.postgres.repository.EditHistoryRepo;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Service
@OrivisService(model = EditHistoryModel.class, form = EditHistoryForm.class, repo = EditHistoryRepo.class)
public class EditHistoryService extends PaginationService<EditHistoryModel> {
    public EditHistoryService(OrivisContext holder) {
        super(holder);
    }

    @Override
    public Optional<EditHistoryModel> findById(Serializable id) {
        return getRepository().filteredFirst(getFilter().id( id));
    }


    public Optional<EditHistoryModel> findByEditedTypeAndId(String type, Long id) {
       return  getRepository().filteredFirst(getFilter().eq("classEditedObject", type).and(getFilter().eq("idEditedObject", id)));

    }

    public List<EditHistoryModel> findByEditedType(String type) {
        EditHistoryRepo repo = getRepository();
        return repo.findAllByClassEditedObject(type);
    }

    @Override
    public EditHistoryRepo getRepository() {
        return super.getRepository();
    }
}
