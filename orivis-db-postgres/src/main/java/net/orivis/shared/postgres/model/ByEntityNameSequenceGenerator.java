package net.orivis.shared.postgres.model;

import org.hibernate.boot.registry.classloading.spi.ClassLoaderService;
import org.hibernate.id.enhanced.SequenceStyleGenerator;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.Type;
import org.springframework.data.mapping.MappingException;
import net.orivis.shared.annotations.SequenceParams;

import java.util.Properties;

public class ByEntityNameSequenceGenerator extends SequenceStyleGenerator {
    public static final String NAME = ByEntityNameSequenceGenerator.class.getName();

    public ByEntityNameSequenceGenerator(){
        super();
    }
    @Override
    public void configure(Type type, Properties params, ServiceRegistry serviceRegistry) throws MappingException {
        String entityName = params.getProperty(ENTITY_NAME);
        if (entityName == null) {
            throw new IllegalStateException("Entity name must not be null");
        }

        Class<?> entityClass = serviceRegistry.requireService(ClassLoaderService.class)
                .classForName(entityName);


        var ann = entityClass.getAnnotation(SequenceParams.class);
        if (ann != null){
            params.setProperty("increment_size", String.valueOf(ann.increment()));
            params.setProperty("initial_value", String.valueOf(ann.init()));
        }

        params.setProperty(SEQUENCE_PARAM, entityClass.getSimpleName() + "_seq");

        super.configure(type, params, serviceRegistry);
    }
}
