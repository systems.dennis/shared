package net.orivis.shared.postgres.form;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import net.orivis.shared.entity.ExtendedOrivisForm;
import net.orivis.shared.entity.KeyValue;
import net.orivis.shared.pojo_form.PojoFormElement;
import net.orivis.shared.pojo_view.list.PojoListViewField;

import java.util.ArrayList;
import java.util.List;

import static net.orivis.shared.pojo_view.DEFAULT_TYPES.HIDDEN;

@Data
public class ExtendedDefaultForm implements ExtendedOrivisForm<Long>, OrivisPojo {
    @PojoFormElement(type = HIDDEN)
    @JsonProperty("id")
    private Long id;
    @JsonProperty("additionalValues")
    @PojoListViewField (available = false)
    private List<KeyValue> additionalValues = new ArrayList<>();
}
