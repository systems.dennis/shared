package net.orivis.shared.postgres.model;


import jakarta.persistence.MappedSuperclass;
import lombok.Data;
import net.orivis.shared.annotations.FormTransient;
import net.orivis.shared.annotations.entity.CreatedBy;
import net.orivis.shared.pojo_view.list.PojoListViewField;

@MappedSuperclass
@Data
public abstract class LongAssignableEntity extends OrivisEntity {


    @PojoListViewField(visible = false, searchable = false)
    @CreatedBy
    @FormTransient
    private Long userDataId;

}
