package net.orivis.shared.postgres.repository.query_processors;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.extern.slf4j.Slf4j;
import net.orivis.shared.repository.OrivisFilter;

import java.math.BigInteger;
import java.util.Collection;
import java.util.List;

@Slf4j
public class NumberPredicateProcessor extends AbstractClassProcessor{

    public NumberPredicateProcessor(OrivisFilter queryCase, Root root) {
        super(queryCase, root);
    }

    @Override
    public Object getValue(Object value) {
        try {
            if (value instanceof Number) {
                return getNumberFromString( String.valueOf(value), getFilter().getFieldClass());
            } else {
                return null;
            }

        } catch (Exception e) {
            log.warn("Search is not able to parse date: " + value, e);
            return null;
        }
    }

    @Override
    public void processDefault(CriteriaBuilder criteriaBuilder, List<Predicate> predicates) {
        var filter = getFilter();
        var root = getRoot();
        Object value = getValue(filter.getValue());
        if (value == null) {
            throw new ArithmeticException("Item is wrong type");
        }
        if (filter.LESS_THEN.equalsIgnoreCase(filter.getOperator())) {
            predicates.add(criteriaBuilder.lessThan(getPath(root), ((Number) value).longValue()));
        }
        if (filter.MORE_THEN.equalsIgnoreCase(filter.getOperator())) {
            predicates.add(criteriaBuilder.greaterThan(getPath(root), ((Number) value).longValue()));
        }
        if (filter.LESS_EQUALS.equalsIgnoreCase(filter.getOperator())) {
            predicates.add(criteriaBuilder.lessThanOrEqualTo(getPath(root), ((Number) value).longValue()));
        }
        if (filter.MORE_EQUALS.equalsIgnoreCase(filter.getOperator())) {
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(getPath(root), ((Number) value).longValue()));
        }


        if (filter.NOT_NULL_OPERATOR.equalsIgnoreCase(filter.getOperator())) {
            predicates.add(criteriaBuilder.isNotNull(getPath(root)));
        }
        if (filter.NULL_OPERATOR.equalsIgnoreCase(filter.getOperator())) {
            predicates.add(criteriaBuilder.isNull(getPath(root)));
        }
        if (filter.EQUALS_OPERATOR.equalsIgnoreCase(filter.getOperator()) && !String.class.isAssignableFrom(filter.getValue().getClass())) {
            predicates.add(criteriaBuilder.equal(getPath(root), value));
        }

        //There is a bug that excludes elements with a value of null from the search results
        if (filter.NOT_EQUALS_OPERATOR.equalsIgnoreCase(filter.getOperator())) {
            addIsNullOrNotEqualPredicate(criteriaBuilder, predicates, value);
        }
        if (filter.IN.equalsIgnoreCase(filter.getOperator())) {
            CriteriaBuilder.In inClause = criteriaBuilder.in(getPath(root));

            Collection cValue = (Collection) value;
            cValue.forEach((x)-> inClause.value(x) );
            predicates.add(inClause);
        }

    }

    public static Number getNumberFromString(String value, Class<?> fieldClass) {

        if (int.class.equals(fieldClass)) {
            return Integer.valueOf(value);
        } if (Integer.class.equals(fieldClass)) {
            return Integer.valueOf(value);
        }
        if (Double.class.equals(fieldClass)) {
            return Double.valueOf(value);
        }  if (double.class.equals(fieldClass)) {
            return Double.valueOf(value);
        }
        if (long.class.equals(fieldClass)) {
            return Long.valueOf(value);
        }
        if (Long.class.equals(fieldClass)) {
            return Long.valueOf(value);
        }
        if (Short.class.equals(fieldClass)) {
            return Short.valueOf(value);
        }
        if (short.class.equals(fieldClass)) {
            return Short.valueOf(value);
        }
        if (Float.class.equals(fieldClass)) {
            return Short.valueOf(value);
        }
        if (float.class.equals(fieldClass)) {
            return Short.valueOf(value);
        }

        if (BigInteger.class.equals(fieldClass)) {
            return BigInteger.valueOf(Long.parseLong(value));
        }

        throw new IllegalArgumentException(" cannot transform String to number! ");
    }

}
