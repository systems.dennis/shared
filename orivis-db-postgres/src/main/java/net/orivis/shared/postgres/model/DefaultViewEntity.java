package net.orivis.shared.postgres.model;

import jakarta.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class DefaultViewEntity extends OrivisEntity {
    private  static int nextVal = 0;

    public DefaultViewEntity() {
        super.setId(random());
    }

    private static long random() {
        return ++nextVal;
    }

    public void setId(Long id){
        super.setId(id);
    }

    public Long getId(){
        return super.getId();
    }
}
