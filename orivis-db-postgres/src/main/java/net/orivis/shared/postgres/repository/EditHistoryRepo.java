package net.orivis.shared.postgres.repository;

import org.springframework.stereotype.Repository;
import net.orivis.shared.postgres.model.EditHistoryModel;

import java.util.List;
import java.util.Optional;

@Repository
public interface EditHistoryRepo extends OrivisRepository<EditHistoryModel> {

    Optional<EditHistoryModel> findFirstByClassEditedObjectAndIdEditedObject(String type, Long editedId);

    List<EditHistoryModel> findAllByClassEditedObject(String type);
}
