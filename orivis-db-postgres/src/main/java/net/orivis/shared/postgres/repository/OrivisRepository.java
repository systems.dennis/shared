package net.orivis.shared.postgres.repository;

import net.orivis.shared.postgres.model.OrivisEntity;
import net.orivis.shared.repository.AbstractFilterRepo;
import net.orivis.shared.repository.AbstractRepository;

public interface OrivisRepository<DB_TYPE extends OrivisEntity>  extends AbstractRepository<DB_TYPE, Long>, AbstractFilterRepo<DB_TYPE, Long> {
}
