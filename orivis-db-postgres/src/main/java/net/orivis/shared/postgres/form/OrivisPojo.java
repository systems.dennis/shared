package net.orivis.shared.postgres.form;

import net.orivis.shared.entity.KeyValue;
import net.orivis.shared.form.AbstractOrivisForm;
import org.springframework.data.domain.Sort;

public interface OrivisPojo extends AbstractOrivisForm<Long> {
    String ID_FIELD= "id";
    Long getId();


    default KeyValue defaultSearchOrderField(){
        return  new KeyValue(ID_FIELD,  Sort.Direction.DESC);
    }
    default String asValue(){
        return toString();
    }
}
