package net.orivis.shared.postgres.controller;

import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.controller.generated.AbstractEditItemController;
import net.orivis.shared.exceptions.ItemForAddContainsIdException;
import net.orivis.shared.postgres.form.OrivisPojo;
import net.orivis.shared.postgres.model.OrivisEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Secured
public interface EditItemController<T extends OrivisEntity, E extends OrivisPojo> extends AbstractEditItemController<T,E, Long> {


    @PutMapping(value = "/edit",  consumes = {"application/json", "application/json;charset=UTF-8"}, produces = {"application/json", "application/json;charset=UTF-8"})
    @ResponseBody
    @WithRole
    @Override
    default ResponseEntity<E> edit(@RequestBody E form) throws ItemForAddContainsIdException {
        return AbstractEditItemController.super.edit(form);
    }

}
