package net.orivis.shared.postgres.model;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import net.orivis.shared.beans.IdValidator;

@Service
public class LongIdValidatorImpl extends IdValidator<Long> {
    @Override
    public boolean isIdSet(Long id) {
        return id != null && id > 0;
    }
}
