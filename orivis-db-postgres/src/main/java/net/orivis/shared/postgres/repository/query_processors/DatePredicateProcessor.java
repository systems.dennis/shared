package net.orivis.shared.postgres.repository.query_processors;

import jakarta.persistence.criteria.Root;
import lombok.extern.slf4j.Slf4j;
import net.orivis.shared.postgres.repository.SpecificationFilter;

import java.text.SimpleDateFormat;
import java.util.Date;


@Slf4j
public class DatePredicateProcessor extends AbstractClassProcessor {
    public static final SimpleDateFormat formatter = new SimpleDateFormat("dd:MM:yyyy HH:mm:ss");

    public DatePredicateProcessor(SpecificationFilter queryCase, Root root) {
        super(queryCase, root);
    }

    @Override
    public Object getValue(Object value) {
        var strValue = String.valueOf(getFilter().getValue());
        try {
            value = formatter.parse(String.valueOf(getFilter().getValue()));
        } catch (Exception e) {
            //if date becomes as long perhaps?
            try {
                value = new Date(NumberPredicateProcessor.getNumberFromString(strValue, getFilter().getFieldClass()).longValue());
            } catch (Exception ignored) {}
            log.warn("Search is not able to parse date: " + value, e);
            return null;
        }

        return value;
    }
}


