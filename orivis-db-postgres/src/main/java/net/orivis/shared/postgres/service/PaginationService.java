package net.orivis.shared.postgres.service;

import net.orivis.shared.annotations.NeverNullResponse;
import net.orivis.shared.beans.OrivisFilterProvider;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.postgres.model.OrivisEntity;
import net.orivis.shared.repository.OrivisFilter;
import net.orivis.shared.service.AbstractPaginationService;

public class PaginationService<DB_TYPE extends OrivisEntity> extends AbstractPaginationService<DB_TYPE, Long> {
    public PaginationService(OrivisContext holder) {
        super(holder);
    }

    OrivisFilter<DB_TYPE> getFilter() {
        return getBean(OrivisFilterProvider.class).get();
    }

    @Override
    public Long[] updateIds(Object[] ids) {
        if (ids == null){
            return  null;
        }
        Long[] res = new Long[ids.length];
        for (int i = 0; i < ids.length; i++) {
            var id = ids[i];
            res[i] = id == null ? null : toLong(id);
        }


        return res;
    }

    @Override
    public Long updateId(Object id) {
        if (id == null){
            return  null;
        }
        return toLong(id);
    }

    private @NeverNullResponse Long toLong(Object id) {
        return  id.getClass() == Long.class ? (Long) id : Long.valueOf(id.toString());
    }
}
