package net.orivis.shared.postgres.repository;

import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import net.orivis.shared.postgres.model.OrivisEntity;
import net.orivis.shared.repository.AbstractFilterRepo;
import net.orivis.shared.repository.OrivisFilter;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;
import java.util.Optional;


@NoRepositoryBean
public class PaginationRepositoryImpl<ENTITY extends OrivisEntity> extends SimpleJpaRepository<ENTITY, Long> implements AbstractFilterRepo<ENTITY, Long> {

    public PaginationRepositoryImpl(JpaEntityInformation<ENTITY, ?> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
    }

    @Override
    public Optional<ENTITY> filteredOne(OrivisFilter<?> one) {
        return super.findOne((Specification<ENTITY>) one.getQueryRoot());
    }



    @Override
    public Optional<ENTITY> filteredFirst(OrivisFilter<?> first) {
        var res =  super.findAll((Specification<ENTITY>) first.getQueryRoot(), Pageable.ofSize(1));
        return res.isEmpty() ? Optional.empty() : Optional.of(res.getContent().get(0));
    }

    @Override
    public Optional<ENTITY> filteredFirst(OrivisFilter<?> first, Sort sort) {
        var res =  super.findAll((Specification<ENTITY>) first.getQueryRoot(), PageRequest.of(0, 1, sort));
        return res.isEmpty() ? Optional.empty() : Optional.of(res.getContent().get(0));
    }

    @Override
    public Page<ENTITY> filteredData(OrivisFilter<?> specification){

        return findAll((Specification<ENTITY>)specification.getQueryRoot(), Pageable.unpaged());
    }

    @Override
    public Page<ENTITY> filteredData(OrivisFilter<?> specification, Sort sort) {
        List<ENTITY> list = findAll((Specification<ENTITY>) specification.getQueryRoot(), sort);
        return new PageImpl<>(list, Pageable.unpaged(), list.size());
    }

    @Override
    public Page<ENTITY> filteredData(OrivisFilter<?> specification, Pageable pageable) {
        return findAll((Specification<ENTITY>) specification.getQueryRoot(), pageable);
    }

    @Override
    public Page<ENTITY> filteredData(OrivisFilter<?> specification, Pageable pageable, Sort sort) {

        return findAll((Specification<ENTITY>) specification.getQueryRoot(), PageRequest.of(pageable.getPageSize(), pageable.getPageSize(), sort));
    }

    @Override
    public long filteredCount(OrivisFilter<?> filter) {
        return count((Specification<ENTITY>) filter.getQueryRoot());
    }

    @Override
    @Transactional
    public void deleteData(OrivisFilter<?> spec) {
        List<ENTITY> entitiesToDelete = findAll((Specification<ENTITY>) spec.getQueryRoot());
        deleteAll(entitiesToDelete);
    }
}
