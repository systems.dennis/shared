package net.orivis.shared.postgres.bean;

import lombok.extern.slf4j.Slf4j;
import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.beans.AbstractEditDeleteHistoryBean;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.exceptions.DeleteNotPossibleException;
import net.orivis.shared.exceptions.ItemWasDeletedException;
import net.orivis.shared.postgres.model.DeleteHistoryModel;
import net.orivis.shared.postgres.model.EditHistoryModel;
import net.orivis.shared.postgres.model.OrivisEntity;
import net.orivis.shared.postgres.service.DeleteHistoryService;
import net.orivis.shared.postgres.service.EditHistoryService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.Date;

import static net.orivis.shared.utils.Mapper.mapper;

@Slf4j @Service @Primary
public class PostgresSaveEditDeleteHistory extends AbstractEditDeleteHistoryBean<OrivisEntity, Long> {
    public PostgresSaveEditDeleteHistory(OrivisContext context) {
        super(context);
    }

    @Override
    public void edit(OrivisEntity from, OrivisEntity to) {
        var editableObject = EditHistoryModel.from(from, to);
        getContext().getBean(EditHistoryService.class).save(editableObject);
    }

    @Override
    public void delete(Long id, OrivisEntity model) {

        if (id == null || model == null){
            throw new DeleteNotPossibleException("id_or_model_are_null", id);
        }
        try {
            DeleteHistoryModel deleteHistoryModel = new DeleteHistoryModel();
            deleteHistoryModel.setDeletedDate(new Date());
            deleteHistoryModel.setContent(mapper.writeValueAsString(model));
            deleteHistoryModel.setClassDeletedObject(model.getClass().getSimpleName());

            var deletedObjectExists = getBean(DeleteHistoryService.class).existsByTypeAndDeletedId(id,
                    this.getClass().getAnnotation(OrivisService.class).model().getSimpleName());
            if (deletedObjectExists) {
                throw new ItemWasDeletedException(id, model.getClass());
            }
        } catch (Exception e){
            log.debug("cannot save object history: ", e);
        }

    }

    @Override
    public void throwIfDeleted(Long id, Class model) {
        if (model == null || id == null) return;
        var deletedObjectExists = getBean(DeleteHistoryService.class).existsByTypeAndDeletedId(id,
                model.getSimpleName());
        if (deletedObjectExists) {
            throw new ItemWasDeletedException(id, model.getClass().getSuperclass());
        }
    }
}
