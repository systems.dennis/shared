package net.orivis.shared.postgres.repository.query_processors;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import net.orivis.shared.postgres.repository.SpecificationFilter;
import net.orivis.shared.repository.OrivisFilter;

import java.util.List;

public class StringPredicateProcessor extends AbstractClassProcessor{

    public StringPredicateProcessor(SpecificationFilter queryCase, Root root) {
        super(queryCase, root);
    }


    @Override
    public void processDefault( CriteriaBuilder criteriaBuilder, List<Predicate> predicates) {
        boolean selectInsensitive = true;
        var queryCase = getFilter();
        try {
            selectInsensitive = (boolean) getParameter();
        } catch (Exception ignored) {

        }
        String valueEx = String.valueOf(queryCase.getValue());

        if (OrivisFilter.CONTAINS_OPERATOR.equalsIgnoreCase(queryCase.getOperator())) {

            if (!selectInsensitive) {
                predicates.add(criteriaBuilder.like(getPath(getRoot()), "%" + queryCase.getValue() + "%"));
            } else {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(getPath(getRoot())), "%" + queryCase.getValue().toString().toLowerCase() + "%"));
            }
        }
        if (OrivisFilter.NOT_CONTAINS_OPERATOR.equalsIgnoreCase(queryCase.getOperator())) {

            if (!selectInsensitive) {
                predicates.add(criteriaBuilder.notLike(criteriaBuilder.lower(getPath(getRoot())), "%" + queryCase.getValue().toString().toLowerCase() + "%"));
            } else {
                predicates.add(criteriaBuilder.notLike(getPath(getRoot()), "%" + queryCase.getValue() + "%"));
            }
        }

        if (OrivisFilter.EQUALS_OPERATOR.equalsIgnoreCase(queryCase.getOperator())) {

            if (selectInsensitive) {
                predicates.add(criteriaBuilder.equal(criteriaBuilder.lower(getPath(getRoot())), valueEx.toLowerCase()));
            } else {
                predicates.add(criteriaBuilder.equal(getPath(getRoot()), queryCase.getValue()));
            }
        }
        if (OrivisFilter.NOT_EQUALS_OPERATOR.equalsIgnoreCase(queryCase.getOperator())) {
            addIsNullOrNotEqualPredicate(criteriaBuilder, predicates, queryCase.getValue());
        }
        if (OrivisFilter.STARTS_WITH_OPERATOR.equalsIgnoreCase(queryCase.getOperator())) {

            if (selectInsensitive) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(getPath(getRoot())),  queryCase.getValue().toString().toLowerCase() + "%"));
            } else {
                predicates.add(criteriaBuilder.like(getPath(getRoot()),  queryCase.getValue() + "%"));
            }

        }
        if (OrivisFilter.NOT_STARTS_WITH_OPERATOR.equalsIgnoreCase(queryCase.getOperator())) {


            if (selectInsensitive) {
                predicates.add(criteriaBuilder.notLike(criteriaBuilder.lower(getPath(getRoot())),  queryCase.getValue().toString().toLowerCase() + "%"));
            } else {
                predicates.add(criteriaBuilder.notLike(getPath(getRoot()),  queryCase.getValue() + "%"));
            }

        }

        if (OrivisFilter.ENDS_WITH_OPERATOR.equalsIgnoreCase(queryCase.getOperator())) {

            if (selectInsensitive) {
                predicates.add(criteriaBuilder.like(getPath(getRoot()), "%" + queryCase.getValue()));
            } else {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(getPath(getRoot())), "%" + queryCase.getValue().toString().toLowerCase()));
            }
        }
        if (OrivisFilter.NOT_ENDS_WITH_OPERATOR.equalsIgnoreCase(queryCase.getOperator())) {

            if (selectInsensitive) {
                predicates.add(criteriaBuilder.notLike(getPath(getRoot()), "%" + queryCase.getValue()));
            } else {
                predicates.add(criteriaBuilder.notLike(criteriaBuilder.lower(getPath(getRoot())), "%" + queryCase.getValue().toString().toLowerCase()));
            }

        }
    }

    @Override
    public Object getValue(Object value) {
        return value;
    }
}
