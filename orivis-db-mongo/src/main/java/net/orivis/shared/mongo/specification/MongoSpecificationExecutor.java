package net.orivis.shared.mongo.specification;

import net.orivis.shared.mongo.repository.MongoSpecification;
import net.orivis.shared.mongo.model.OrivisEntity;
import net.orivis.shared.repository.OrivisFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MongoSpecificationExecutor<T extends OrivisEntity> {

    /**
     * Returns a single entity matching the given {@link MongoSpecification} or {@link Optional#empty()} if none found.
     *
     * @param spec can be {@literal null}.
     * @return never {@literal null}.
     * @throws org.springframework.dao.IncorrectResultSizeDataAccessException if more than one entity found.
     */
    Optional<T> filteredOne(@Nullable OrivisFilter<?> spec);
    Optional<T> filteredFirst(@Nullable OrivisFilter<?> spec);

    /**
     * Returns all entities matching the given {@link MongoSpecification}.
     *
     * @param spec can be {@literal null}.
     * @return never {@literal null}.
     */
    Page<T> filteredData(@Nullable OrivisFilter<?> spec);

    /**
     * Returns a {@link Page} of entities matching the given {@link MongoSpecification}.
     *
     * @param spec     can be {@literal null}.
     * @param pageable must not be {@literal null}.
     * @return never {@literal null}.
     */
    Page<T> filteredData(@Nullable OrivisFilter<?> spec, Pageable pageable);

    /**
     * Returns all entities matching the given {@link MongoSpecification} and {@link Sort}.
     *
     * @param spec can be {@literal null}.
     * @param sort must not be {@literal null}.
     * @return never {@literal null}.
     */
    Page<T> filteredData(@Nullable OrivisFilter<?> spec, Sort sort);

    /**
     * Returns the number of instances that the given {@link MongoSpecification} will return.
     *
     * @param spec the {@link MongoSpecification} to count instances for. Can be {@literal null}.
     * @return the number of instances.
     */
    long filteredCount(@Nullable OrivisFilter<?> spec);
}
