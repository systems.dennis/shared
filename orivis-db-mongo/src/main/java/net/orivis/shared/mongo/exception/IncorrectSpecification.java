package net.orivis.shared.mongo.exception;

import org.springframework.http.HttpStatus;
import net.orivis.shared.exceptions.StatusException;

public class IncorrectSpecification extends StatusException {
    public IncorrectSpecification (String s){
        super(s, HttpStatus.BAD_REQUEST);
    }
}
