package net.orivis.shared.mongo.model;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import lombok.Data;
import net.orivis.shared.model.OrivisIDPresenter;

@MappedSuperclass
@Data
public abstract class OrivisEntity extends OrivisIDPresenter<String> {
    @Id
    @GeneratedValue
    private String id;

    @Override
    public boolean isIdSet() {
        return id != null && !id.isBlank();
    }
}
