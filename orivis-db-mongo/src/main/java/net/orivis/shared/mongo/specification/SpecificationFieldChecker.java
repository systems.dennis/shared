package net.orivis.shared.mongo.specification;


import net.orivis.shared.mongo.model.OrivisEntity;
import net.orivis.shared.mongo.exception.IncorrectSpecification;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class SpecificationFieldChecker {
    private static final String DEFAULT_FIELDS = "DEFAULT_FIELDS";
    private static List<String> defaultFields = new ArrayList<>();

    private SpecificationFieldChecker() {
    }

    public static void checkFieldExistsInClass(final String field, final Class<? extends OrivisEntity> o) {
        defaultFields.clear();
        if (o == null) {
            throw new IncorrectSpecification("Class should not be null on request");
        }
        defaultFields = collectAllDefaultFields(o).stream().distinct().collect(Collectors.toList());
        if (field == null) {
            throw new IncorrectSpecification("field should not be null");
        } else if (!defaultFields.contains(field)) {

            if (field.contains(".")) {
                checkComplexField(o, field);
            } else {
                checkSimpleType(o, field);
            }
        }
    }

    private static List<String> collectAllDefaultFields(Class<? extends OrivisEntity> clazz) {
        if (clazz != OrivisEntity.class) {
            collectAllDefaultFields((Class<? extends OrivisEntity>) clazz.getSuperclass());
        }
        Arrays.stream(clazz.getDeclaredFields())
                .filter(field -> field.getName().equals(DEFAULT_FIELDS))
                .forEach(field -> {
                    try {
                        defaultFields.addAll((Collection<String>) field.get(null));
                    } catch (IllegalAccessException e) {
                        throw new IncorrectSpecification( "Class " + clazz + " does not has property " +  field.getName());
                    }
                });
        return defaultFields;
    }

    private static void checkComplexField(final Class<? extends OrivisEntity> o, final String field) {
        checkSimpleType(o, field.split("\\.")[0]);
        //todo check complex search fields
    }

    private static void checkSimpleType(final Class<? extends OrivisEntity> o, final String field) {
        try {
            o.getDeclaredField(field);
        } catch (Exception e) {
            throw new IncorrectSpecification( "Class " + o.getSimpleName() + " does not has property " +  field);
        }
    }
}

