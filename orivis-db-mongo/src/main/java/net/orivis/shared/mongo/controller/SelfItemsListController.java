package net.orivis.shared.mongo.controller;

import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.controller.generated.AbstractSelfItemsListController;
import net.orivis.shared.mongo.form.OrivisPojo;
import net.orivis.shared.mongo.model.OrivisEntity;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Secured
public interface SelfItemsListController<T extends OrivisEntity,E extends OrivisPojo> extends AbstractSelfItemsListController<T,E, String> {

    @GetMapping("/list")
    @ResponseBody
    @WithRole
    @Override
    default ResponseEntity<Page<E>> getData(
                                            @RequestParam (value = "limit", required = false) Integer limit,
                                            @RequestParam(value = "page", required = false) Integer page) {
        return AbstractSelfItemsListController.super.getData( limit, page);
    }
}
