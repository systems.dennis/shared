package net.orivis.shared.mongo.controller;

import jakarta.servlet.http.HttpServletRequest;
import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.controller.generated.AbstractListItemController;
import net.orivis.shared.controller.orivis.OrivisRequest;
import net.orivis.shared.mongo.form.OrivisPojo;
import net.orivis.shared.mongo.model.OrivisEntity;
import net.orivis.shared.pojo_view.GeneratedPojoList;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Secured
public interface ListItemController<T extends OrivisEntity,E extends OrivisPojo> extends AbstractListItemController<T,E, String> {

    @GetMapping("/list")
    @ResponseBody
    @WithRole
    @Override
    default ResponseEntity<Page<Map<String, Object>>> get(
                                                          @RequestParam(value = "limit", required = false) Integer limit,
                                                          @RequestParam(value = "page", required = false) Integer page) {
        return AbstractListItemController.super.get( limit, page);
    }

    @PostMapping(value = {"/root/fetch/data", "/root/fetch/data/"}, produces = "application/json", consumes = "application/json")
    @ResponseBody
    @WithRole
    @Override
    default Page<Map<String, Object>> fetchData(@RequestBody OrivisRequest request) {
        return AbstractListItemController.super.fetchData(request);
    }


    @GetMapping(value = {"/root/fetch/list", "/root/fetch/list/"}, produces = "application/json")
    @ResponseBody
    @WithRole
    @Override
    default GeneratedPojoList fetchForm(HttpServletRequest request) {
        return AbstractListItemController.super.fetchForm(request);
    }
}
