package net.orivis.shared.mongo.service;

import net.orivis.shared.mongo.model.OrivisEntity;
import net.orivis.shared.repository.OrivisFilter;
import net.orivis.shared.service.AbstractParentalService;
import net.orivis.shared.service.AbstractService;
import org.springframework.data.domain.Page;

public interface ParentalService <DB_TYPE extends OrivisEntity> extends AbstractService<DB_TYPE, String>,
        AbstractParentalService<DB_TYPE, String> {

    @Override
    default long countByParent(String id) {
        OrivisFilter<DB_TYPE> specification = getFilterImpl().eq(getParentField(), id);
        return getRepository().filteredCount(specification.and(getAdditionalSpecification()));
    }

    @Override
    default Page<DB_TYPE> findByParent(String parentId, int limit, int page) {
        OrivisFilter<DB_TYPE> specification = getFilterImpl().eq(getParentField(), parentId);
        return find(specification.and(getAdditionalSpecification()),  limit, page);
    }
}
