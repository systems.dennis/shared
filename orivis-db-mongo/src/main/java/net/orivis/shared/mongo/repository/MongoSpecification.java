package net.orivis.shared.mongo.repository;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import net.orivis.shared.controller.orivis.OrivisOrder;
import net.orivis.shared.controller.orivis.OrivisRequest;
import net.orivis.shared.entity.OrivisID;
import net.orivis.shared.exceptions.StandardException;
import net.orivis.shared.model.OrivisIDPresenter;
import net.orivis.shared.mongo.exception.IncorrectSpecification;
import net.orivis.shared.mongo.repository.query_processors.AbstractClassProcessor;
import net.orivis.shared.repository.OrivisFilter;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.query.Criteria;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Data
@Slf4j
public class MongoSpecification<T extends OrivisIDPresenter<?>> implements OrivisFilter<T> {
    private static final String ID_FIELD = "id";
    public static final String OR_OPERATOR = "or";
    public static final String AND_OPERATOR = "and";

    private boolean empty;
    private boolean closed;
    private boolean insensitive;
    private boolean complex;

    private Class<?> type;


    private String operationType;
    private Object value;
    private String field;
    private String on;

    boolean calculated = false;

    private List<MongoSpecification<T>> or = new ArrayList<>();

    private List<MongoSpecification<T>> and = new ArrayList<>();

    private Criteria criteria;

    @Override
    public MongoSpecification<T> operator(String field, Object value, String type) {
        if (this.isClosed()) {
            throw new StandardException("query_was_already_closed", "only add/or functions are now available. " +
                    "Please check that 'operation' is performed after additional parameter' ");
        }

        if (field != null && type != null) {
            setEmpty(false);
        } else {
            setEmpty(true);
            return this;
        }

        if (isCalculated()) {
            throw new StandardException("query_was_already_closed", "due to limitations of Query you cannot use Query after you had already called method 'getCriteriaRoot()'");
        }

        this.field = field;
        this.value = value;
        this.operationType = type;


        criteria = Criteria.where(getField());
        var qq = AbstractClassProcessor.processor(this);
        if (!qq.isNotNullCase()) {
            qq.addToNullOrNotNullPredicate();
        } else {
            qq.processDefault();
        }
        this.closed = true;
        return this;

    }

    @Override
    public <E extends OrivisFilter<?>> E and(E filter) {

        checkCriteria();
        if (filter.isEmpty()) {

            return (E) this.copy((MongoSpecification )filter);
        }

        if (isCalculated()) {
            throw new StandardException("query_was_already_closed", "due to limitations of Query you cannot use Query after you had already called method 'getCriteriaRoot()'");
        }
        and.add((MongoSpecification<T>) filter);
        this.empty = false;
        return (E) this;

    }

    private void checkCriteria() {
        if (criteria == null){
            criteria = Criteria.where(field);
        }
    }

    private MongoSpecification copy(MongoSpecification filter) {
        this.criteria = filter.criteria;
        this.type = filter.type;
        this.and = filter.and;
        this.or = filter.or;
        this.on = filter.on;
        this.complex = filter.complex;
        this.insensitive = filter.insensitive;
        this.field = field;
        this.setEmpty(false);
        return this;
    }

    @Override
    public <E extends OrivisFilter<?>> E or(E filter) {
        if (isCalculated()) {
            throw new StandardException("query_was_already_closed", "due to limitations of Query you cannot use Query after you had already called method 'getCriteriaRoot()'");
        }

        if (filter.isEmpty()) {

            return (E) this.copy((MongoSpecification) filter);
        }

        or.add((MongoSpecification<T>) filter);

        this.empty = false;
        return (E) this;

    }

    @Override
    public <E extends OrivisFilter<T>> E comparasionType(Class<?> type) {
        this.type = type;
        return (E) this;
    }

    @Override
    public boolean isClosed() {
        return closed;
    }

    @Override
    public OrivisFilter<T> setInsensitive(boolean insensitive) {
        this.insensitive = insensitive;
        return this;
    }

    @Override
    public OrivisFilter<T> setComplex(boolean complex) {
        this.complex = complex;
        return this;
    }

    @Override
    public OrivisFilter<T> setJoinOn(String on) {
        this.on = on;
        return this;
    }

    public List<AggregationOperation> toAggregationOperations(Class<?> entityClass, MongoTemplate mongoTemplate) {
        List<AggregationOperation> operations = new ArrayList<>();

        Criteria finalCriteria = buildCriteriaRecursively(operations, entityClass, mongoTemplate);

        if (finalCriteria != null && !finalCriteria.getCriteriaObject().isEmpty()) {
            operations.add(Aggregation.match(finalCriteria));
        }

        return operations;
    }

    private Criteria buildCriteriaRecursively(List<AggregationOperation> operations, Class<?> entityClass, MongoTemplate mongoTemplate) {
        Criteria currentCriteria = new Criteria();

        Criteria localCriteria;
        if (this.on != null) {
            localCriteria = handleJoin(operations, entityClass, mongoTemplate);
        } else {
            localCriteria = handleNoJoin(operations, entityClass, mongoTemplate);
        }

        if (localCriteria != null) {
            currentCriteria = localCriteria;
        }

        if (!this.or.isEmpty()) {
            List<Criteria> orCriterias = new ArrayList<>();
            for (MongoSpecification<T> spec : this.or) {

                Criteria orCriteria = spec.buildCriteriaRecursively(operations, entityClass, mongoTemplate);
                if (orCriteria != null && !orCriteria.getCriteriaObject().isEmpty()) {
                    orCriterias.add(orCriteria);
                }
            }
            if (!orCriterias.isEmpty()) {
                if (!currentCriteria.getCriteriaObject().isEmpty()) {
                    orCriterias.add(currentCriteria);
                }
                currentCriteria = new Criteria().orOperator(orCriterias.toArray(new Criteria[0]));
            }
        }

        if (!this.and.isEmpty()) {
            List<Criteria> andCriterias = new ArrayList<>();
            for (MongoSpecification<T> spec : this.and) {
                Criteria andCriteria = spec.buildCriteriaRecursively(operations, entityClass, mongoTemplate);
                if (andCriteria != null && !andCriteria.getCriteriaObject().isEmpty()) {
                    andCriterias.add(andCriteria);
                }
            }
            if (!andCriterias.isEmpty()) {
                currentCriteria = currentCriteria.andOperator(andCriterias.toArray(new Criteria[0]));
            }
        }

        if (currentCriteria.getCriteriaObject().containsKey("id")) {
            Object value = currentCriteria.getCriteriaObject().get("id");
            if (value.getClass() == String.class) {
                currentCriteria = new Criteria("_id").is(new ObjectId((String) value));
            }
        }

        return currentCriteria;
    }

    public Boolean shouldAggregate(Class<?> entityClass) {
        if (this.on != null) {
            return true;
        }

        for (MongoSpecification<T> spec : this.or) {
            if (spec.shouldAggregate(entityClass)) {
                return true;
            }
        }

        for (MongoSpecification<T> spec : this.and) {
            if (spec.shouldAggregate(entityClass)) {
                return true;
            }
        }

        return false;
    }

    private Criteria handleJoin(List<AggregationOperation> operations, Class<?> entityClass, MongoTemplate mongoTemplate) {
        try {
            String[] joinPathParts = this.on.split("\\.");
            String previousAlias = mongoTemplate.getCollectionName(entityClass);
            Class<?> currentClass = entityClass;

            for (int i = 0; i < joinPathParts.length; i++) {
                String currentFieldPart = joinPathParts[i];
                Field joinField;

                try {
                    joinField = currentClass.getDeclaredField(currentFieldPart);
                } catch (NoSuchFieldException e) {
                    throw new IncorrectSpecification("Field " + currentFieldPart + " not found in " + currentClass.getSimpleName());
                }

                Class<?> joinClass = joinField.getType();
                String joinCollectionName = mongoTemplate.getCollectionName(joinClass);
                String asAlias = previousAlias + "_" + currentFieldPart;

                if (i == 0) {
                    operations.add(Aggregation.lookup(joinCollectionName, currentFieldPart + ".$id", "_id", asAlias));
                } else {
                    operations.add(Aggregation.lookup(joinCollectionName, previousAlias + "." + currentFieldPart + ".$id", "_id", asAlias));
                }
                operations.add(Aggregation.unwind(asAlias, true));

                if (i == joinPathParts.length - 1) {
                    Object value = this.criteria.getCriteriaObject().get(this.getField());
                    Field searchField = joinClass.getDeclaredField(this.getField());

                    if (searchField.isAnnotationPresent(DBRef.class)) {
                        Class<?> referencedClass = searchField.getType();
                        String referencedCollectionName = mongoTemplate.getCollectionName(referencedClass);
                        String referencedAlias = asAlias + "_" + currentFieldPart;
                        operations.add(Aggregation.lookup(referencedCollectionName, asAlias + "." + this.getField() + ".$id", "_id", referencedAlias));
                        operations.add(Aggregation.unwind(referencedAlias, true));

                        value = new ObjectId((String) value);
                        return Criteria.where(referencedAlias + "._id").is(value);
                    } else {
                        return Criteria.where(asAlias + "." + this.getField()).is(value);
                    }
                }

                previousAlias = asAlias;
                currentClass = joinClass;
            }
        } catch (Exception e) {
            throw new IncorrectSpecification("Error processing join fields");
        }

        return null;
    }

    private Criteria handleNoJoin(List<AggregationOperation> operations, Class<?> entityClass, MongoTemplate mongoTemplate) {
        Field field;
        try {
            field = entityClass.getDeclaredField(this.field);
        } catch (Exception e) {
            field = null;
        }

        if (Objects.nonNull(field) && field.isAnnotationPresent(DBRef.class)) {
            return handleNoJoinDbRef(operations, field.getType(), mongoTemplate);
        } else {
            Object criteriaValue = this.criteria.getCriteriaObject().get(this.field);

            if (criteriaValue != null) {
                return Criteria.where(this.field).is(criteriaValue);
            } else {
                return null;
            }
        }
    }

    private Criteria handleNoJoinDbRef(List<AggregationOperation> operations, Class<?> entityClass, MongoTemplate mongoTemplate) {

        String rootCollectionName = mongoTemplate.getCollectionName(entityClass);
        String as = rootCollectionName + "_" + this.field;
        String value = this.criteria.getCriteriaObject().get(this.getField()).toString();
        Criteria matchCriteria = Criteria.where(as + "._id").is(new ObjectId(value));

        operations.add(Aggregation.lookup(rootCollectionName, this.getField() + ".$id", "_id", as));

        return matchCriteria;
    }

    @Override
    public Serializable getIdValue(Object id) {
        return String.valueOf(id);
    }

    @Override
    public String getOperator() {
        return operationType;
    }

    @Override
    public Class<?> getFieldClass() {
        return type == null ? (value == null ? String.class : value.getClass()) : type ;
    }


    @Override
    public <E> E getQueryRoot() {

        if (calculated) return (E) criteria;

        for (MongoSpecification<T> mongoSpecification : or) {
            mongoSpecification.getQueryRoot();

        }

        if (!or.isEmpty()) {
            List<Criteria> existingCriteria = new ArrayList<>();
            existingCriteria.add(criteria);
            existingCriteria.addAll(or.stream().map(MongoSpecification::getCriteria).collect(Collectors.toList()));

            criteria = new Criteria().orOperator(existingCriteria.toArray(new Criteria[0]));
        }


        for (MongoSpecification<T> tMongoSpecification : and) {
            tMongoSpecification.getQueryRoot();

        }
        if (!and.isEmpty())
            criteria.andOperator(and.stream().map(MongoSpecification::getCriteria).collect(Collectors.toList()));
        calculated = true;

        return (E) criteria;
    }

    public Criteria getRoot() {
        return criteria;
    }


    public boolean getInsensitive() {
        return insensitive;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();

        sb.append("field=").append(this.field != null ? this.field : "null");
        sb.append(";operationType=").append(this.operationType != null ? this.operationType : "null");
        sb.append(";value=").append(this.value != null ? this.value.toString() : "null");
        sb.append(";insensitive=").append(this.insensitive);
        sb.append(";complex=").append(this.complex);

        if (!this.or.isEmpty()) {
            sb.append(";or=[");
            for (MongoSpecification<T> spec : this.or) {
                sb.append(spec.toString()).append(",");
            }
            sb.deleteCharAt(sb.length() - 1);
            sb.append("]");
        }

        if (!this.and.isEmpty()) {
            sb.append(";and=[");
            for (MongoSpecification<T> spec : this.and) {
                sb.append(spec.toString()).append(",");
            }
            sb.deleteCharAt(sb.length() - 1);
            sb.append("]");
        }

        return sb.toString();
    }
}
