package net.orivis.shared.mongo.repository;

import net.orivis.shared.beans.OrivisFilterProvider;
import net.orivis.shared.model.OrivisIDPresenter;
import net.orivis.shared.repository.OrivisFilter;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Service @Primary
public class MongoFilterProvider implements OrivisFilterProvider {
    @Override
    public <T extends OrivisIDPresenter<?>> OrivisFilter<T> get() {
        return new MongoSpecification<T>();
    }
}
