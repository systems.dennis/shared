package net.orivis.shared.mongo.controller;

import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.controller.generated.AbstractAddListItemController;
import net.orivis.shared.exceptions.ItemForAddContainsIdException;
import net.orivis.shared.mongo.form.OrivisPojo;
import net.orivis.shared.mongo.model.OrivisEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Secured
public interface AddListItemController<T extends OrivisEntity,E extends OrivisPojo> extends AbstractAddListItemController<T,E, String> {

    @PostMapping(value = "/add/list",  consumes = {"application/json", "application/json;charset=UTF-8"}, produces = {"application/json", "application/json;charset=UTF-8"})
    @ResponseBody
    @WithRole
    @Override
    default ResponseEntity<List<E>> addList(@RequestBody List<E> forms) throws ItemForAddContainsIdException {
        return AbstractAddListItemController.super.addList(forms);
    }
}
