package net.orivis.shared.mongo.repository.query_processors;

import net.orivis.shared.mongo.repository.MongoSpecification;
import net.orivis.shared.repository.OrivisFilter;
import org.springframework.data.mongodb.core.query.Criteria;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class CollectionPredicateProcessor extends AbstractClassProcessor {

    public CollectionPredicateProcessor(OrivisFilter<?> queryCase, Criteria root) {
        super(queryCase, root);
    }


    @Override
    public void processDefault() {

        var queryCase = getFilter();

        Criteria root = ((MongoSpecification<?>)queryCase).getRoot();
        if (OrivisFilter.CONTAINS_OPERATOR.equalsIgnoreCase(queryCase.getOperator())) {
            root.in(Collections.singleton(queryCase.getValue()));
        }
        if (OrivisFilter.NOT_CONTAINS_OPERATOR.equalsIgnoreCase(queryCase.getOperator())) {
            getRoot().nin(Collections.singleton(queryCase.getValue()));
        }
        if (OrivisFilter.NOT_EMPTY.equalsIgnoreCase(queryCase.getOperator())) {
            getRoot().ne(new ArrayList<>());
        }
        if (OrivisFilter.EMPTY.equalsIgnoreCase(queryCase.getOperator())) {
            getRoot().is(new ArrayList<>());
        }
        if (OrivisFilter.IN.equalsIgnoreCase(queryCase.getOperator())) {
            getRoot().in((Collection) queryCase.getValue());

        }
        if (OrivisFilter.NOT_IN.equalsIgnoreCase(queryCase.getOperator())) {
           getRoot().nin((Collection) queryCase.getValue());
        }
        if (OrivisFilter.NOT_EQUALS_OPERATOR.equalsIgnoreCase(queryCase.getOperator())) {
            getRoot().ne(queryCase.getValue());
        }

    }

    @Override
    public Object getValue(Object value) {
        return value;
    }
}
