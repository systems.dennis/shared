package net.orivis.shared.mongo.repository.query_processors;

import net.orivis.shared.repository.OrivisFilter;
import org.springframework.data.mongodb.core.query.Criteria;

import java.math.BigDecimal;

public class BigDecimalProcessor extends AbstractClassProcessor {
    public BigDecimalProcessor(OrivisFilter<?> queryCase, Criteria root) {
        super(queryCase, root);
    }


    @Override
    public Object getValue(Object value) {
        if (value == null){
            return null;
        }
        if (value instanceof BigDecimal) {
            return value;
        }
        if (value instanceof String) {
            return new BigDecimal((String) value);
        }
        if (value instanceof Double){
            return BigDecimal.valueOf((Double) value);
        }
        if (value instanceof Float){
            return BigDecimal.valueOf((Float) value);
        }

        if (value instanceof Long){
            return new BigDecimal((Long) value);
        }

        if (value instanceof Integer){
            return new BigDecimal((Integer) value);
        }

        throw new UnsupportedOperationException();
    }
}
