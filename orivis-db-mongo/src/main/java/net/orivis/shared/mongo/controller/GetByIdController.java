package net.orivis.shared.mongo.controller;

import lombok.SneakyThrows;
import net.orivis.shared.controller.generated.AbstractGetByIdController;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.mongo.form.OrivisPojo;
import net.orivis.shared.mongo.model.OrivisEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

public interface GetByIdController<DB_TYPE extends OrivisEntity, FORM extends OrivisPojo> extends AbstractGetByIdController<DB_TYPE, FORM,  String > {
    @SneakyThrows
    @GetMapping("/id/{id}")
    @ResponseBody
    default ResponseEntity<FORM> get(@PathVariable("id") String id) {
        DB_TYPE type = (DB_TYPE) getService().findById(id).orElseThrow(() -> ItemNotFoundException.fromId(id));
        if (type == null){
            throw ItemNotFoundException.fromId(id);
        }
        return ResponseEntity.ok(toForm(type));
    }
}
