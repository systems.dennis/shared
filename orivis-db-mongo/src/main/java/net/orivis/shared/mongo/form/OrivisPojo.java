package net.orivis.shared.mongo.form;

import net.orivis.shared.entity.KeyValue;
import net.orivis.shared.form.AbstractOrivisForm;
import org.springframework.data.domain.Sort;

public interface OrivisPojo extends AbstractOrivisForm<String> {
    String ID_FIELD= "id";
    String getId();


    default KeyValue defaultSearchOrderField(){
        return  new KeyValue(ID_FIELD,  Sort.Direction.DESC);
    }
    default String asValue(){
        return toString();
    }
}
