package net.orivis.shared.mongo.repository;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;
import net.orivis.shared.mongo.model.OrivisEntity;
import net.orivis.shared.mongo.specification.OrivisRepostory;


@Repository @NoRepositoryBean
public class PaginationRepository<T extends OrivisEntity> extends OrivisRepostory<T> {


    public PaginationRepository(MongoTemplate mongoTemplate) {
        super(mongoTemplate);
    }


}
