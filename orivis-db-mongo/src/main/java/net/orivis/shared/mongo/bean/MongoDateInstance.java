package net.orivis.shared.mongo.bean;

import net.orivis.shared.annotations.entity.PrimaryDateSetter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


import java.time.LocalDate;

@Component
@Scope("singleton")
public class MongoDateInstance implements PrimaryDateSetter {
    @Override
    public <T> T getNewDateValue() {
        return (T) LocalDate.now();
    }
}
