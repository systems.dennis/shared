package net.orivis.shared.mongo.controller;

import net.orivis.shared.annotations.security.Id;
import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.controller.generated.AbstractDeleteItemController;
import net.orivis.shared.exceptions.ItemForAddContainsIdException;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.exceptions.ItemNotUserException;
import net.orivis.shared.mongo.model.OrivisEntity;
import net.orivis.shared.utils.security.DefaultIdChecker;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Secured
public interface DeleteItemController<T extends OrivisEntity> extends AbstractDeleteItemController<T,String> {


    @DeleteMapping(value = "/delete/{id}",  consumes = {"application/json", "application/json;charset=UTF-8"}, produces = {"application/json", "application/json;charset=UTF-8"})
    @ResponseBody
    @WithRole
    @Override
    default void delete(@Id(checker = DefaultIdChecker.class) @PathVariable String id) throws ItemForAddContainsIdException {
        AbstractDeleteItemController.super.delete(id);
    }

    @Override
    @WithRole
    @RequestMapping(value="/delete/deleteItems", method= RequestMethod.DELETE)
    default void deleteItems(@RequestParam List<String> ids) throws ItemNotUserException, ItemNotFoundException {
        AbstractDeleteItemController.super.deleteItems(ids);
    }
}
