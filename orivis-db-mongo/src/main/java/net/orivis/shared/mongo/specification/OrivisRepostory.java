package net.orivis.shared.mongo.specification;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.repository.NoRepositoryBean;
import net.orivis.shared.mongo.model.OrivisEntity;

@NoRepositoryBean
public class OrivisRepostory<T extends OrivisEntity> extends MongoSpecificationExecutorImpl<T> {


    public OrivisRepostory(final MongoTemplate mongoTemplate) {
        super(mongoTemplate);
    }

}
