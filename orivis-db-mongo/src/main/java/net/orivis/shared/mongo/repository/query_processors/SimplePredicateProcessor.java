package net.orivis.shared.mongo.repository.query_processors;

import net.orivis.shared.repository.OrivisFilter;
import org.springframework.data.mongodb.core.query.Criteria;

public class SimplePredicateProcessor extends AbstractClassProcessor {
    public SimplePredicateProcessor(OrivisFilter queryCase, Criteria root) {
        super(queryCase, root);
    }

    @Override
    public Object getValue(Object value) {
        return value;
    }
}
