package net.orivis.shared.mongo.bean;

import org.springframework.stereotype.Service;
import net.orivis.shared.beans.IdValidator;

@Service
public class StringIdValidator extends IdValidator<String> {
    @Override
    public boolean isIdSet(String id) {
        return id != null && !id.isBlank();
    }
}
