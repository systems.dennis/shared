package net.orivis.shared.mongo.controller;

import lombok.SneakyThrows;
import net.orivis.shared.controller.generated.AbstractGetOrCreateByIdController;
import net.orivis.shared.mongo.form.OrivisPojo;
import net.orivis.shared.mongo.model.OrivisEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

public interface GetByIdOrCreateController<DB_TYPE extends OrivisEntity, FORM extends OrivisPojo> extends AbstractGetOrCreateByIdController<DB_TYPE, FORM,  String > {
    @SneakyThrows
    @GetMapping("/id/{id}")
    @ResponseBody
    default ResponseEntity<FORM> get(@PathVariable("id") String id, @RequestParam(required = false) String param) {
        DB_TYPE type = fetchById(id);
        if (type == null) {
            var res = getService().save(fromForm(createNew(id, param)));
            return ResponseEntity.ok(toForm(res));
        }
        return ResponseEntity.ok(toForm(type));
    }
}
