package net.orivis.shared.mongo.service;

import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.mongo.model.OrivisEntity;
import net.orivis.shared.mongo.repository.MongoSpecification;
import net.orivis.shared.service.AbstractPaginationService;

public class PaginationService<DB_TYPE extends OrivisEntity> extends AbstractPaginationService<DB_TYPE, String> {
    public PaginationService(OrivisContext holder) {
        super(holder);
    }

    @Override
    public MongoSpecification<DB_TYPE> getFilterImpl() {
        return (MongoSpecification<DB_TYPE>) super.getFilterImpl();
    }
}
