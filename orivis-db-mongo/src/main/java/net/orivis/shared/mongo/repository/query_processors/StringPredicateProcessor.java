package net.orivis.shared.mongo.repository.query_processors;

import net.orivis.shared.repository.OrivisFilter;
import org.springframework.data.mongodb.core.query.Criteria;

public class StringPredicateProcessor extends AbstractClassProcessor {

    public StringPredicateProcessor(OrivisFilter<?> queryCase, Criteria root) {
        super(queryCase, root);
    }


    @Override
    public void processDefault( ) {
        boolean selectInsensitive = true;
        var queryCase = getFilter();
        var value = getValue(queryCase.getValue());
        try {
            selectInsensitive = true; // (boolean)getFilter().getInsensitive();
        } catch (Exception ignored) {

        }
        String valueEx = String.valueOf(queryCase.getValue());

        if (OrivisFilter.CONTAINS_OPERATOR.equalsIgnoreCase(queryCase.getOperator())) {

            if (selectInsensitive) {
               getRoot().regex(".*" + value + ".*", "i");
            } else {
                getRoot().regex(".*" + value + ".*");
            }
        }
        if (OrivisFilter.NOT_CONTAINS_OPERATOR.equalsIgnoreCase(queryCase.getOperator())) {

            if (selectInsensitive) {
                getRoot().not().regex(".*" + value + ".*", "i");
            } else {
                getRoot().not().regex(".*" + value + ".*");
            }
        }

        if (OrivisFilter.EQUALS_OPERATOR.equalsIgnoreCase(queryCase.getOperator())) {

            if (!selectInsensitive) {
                getRoot().regex("" + value + "", "i");
            } else {
                getRoot().is(value);
            }
        }
        if (OrivisFilter.NOT_EQUALS_OPERATOR.equalsIgnoreCase(queryCase.getOperator())) {
            if (!selectInsensitive) {
                getRoot().not().regex("" + value + "", "i");
            } else {
                getRoot().ne(value);
            }
        }
        if (OrivisFilter.STARTS_WITH_OPERATOR.equalsIgnoreCase(queryCase.getOperator())) {


            if (!selectInsensitive) {
                getRoot().regex("^" + value, "i");
            } else {
                getRoot().regex("^" + value);
            }

        }
        if (OrivisFilter.NOT_STARTS_WITH_OPERATOR.equalsIgnoreCase(queryCase.getOperator())) {


            if (!selectInsensitive) {
                getRoot().not().regex("^" + value, "i");
            } else {
                getRoot().not().regex("^" + value);
            }


        }

        if (OrivisFilter.ENDS_WITH_OPERATOR.equalsIgnoreCase(queryCase.getOperator())) {


            if (!selectInsensitive) {
                getRoot().regex(value + "$", "i");
            } else {
                getRoot().regex(value + "$");
            }

        }
        if (OrivisFilter.NOT_ENDS_WITH_OPERATOR.equalsIgnoreCase(queryCase.getOperator())) {

            if (!selectInsensitive) {
                getRoot().not().regex("" + value + "$", "i");
            } else {
                getRoot().not().regex("" + value + "$");
            }


        }
    }

    @Override
    public Object getValue(Object value) {
        return value;
    }
}
