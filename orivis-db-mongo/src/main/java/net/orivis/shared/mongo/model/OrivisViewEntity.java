package net.orivis.shared.mongo.model;

import jakarta.persistence.Column;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;

import java.util.UUID;

@MappedSuperclass
public class OrivisViewEntity {
    private  static int nextVal = 0;
    @Id
    @Column(updatable = false, insertable = false)
    private String id = generateRandomId();



    public void setId(String id){

    }

    public static String generateRandomId(){
        return UUID.randomUUID().toString();
    }
}
