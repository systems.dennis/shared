package net.orivis.shared.mongo.repository.query_processors;

import lombok.Data;
import net.orivis.shared.mongo.repository.MongoSpecification;
import net.orivis.shared.repository.OrivisFilter;
import org.springframework.data.mongodb.core.query.Criteria;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;


@Data
public abstract class AbstractClassProcessor {
    private OrivisFilter filter;
    private Criteria root;

    private Class<?> type = String.class;

    private Object parameter = null;

    public AbstractClassProcessor(OrivisFilter AbstractDataFilter, Criteria root) {

        this.filter = AbstractDataFilter;
        this.root = root;

        if (AbstractDataFilter.getFieldClass() == null && AbstractDataFilter.getValue() != null) {
            type = AbstractDataFilter.getValue().getClass();
        }

    }

    public boolean isNotNullCase() {
        return !filter.NOT_NULL_OPERATOR.equalsIgnoreCase(filter.getOperator())
                && !filter.NULL_OPERATOR.equalsIgnoreCase(filter.getOperator());
    }

    public void addToNullOrNotNullPredicate( ) {
        if (Objects.equals(getFilter().getOperator(), filter.NULL_OPERATOR)) {
            getRoot().isNull();
        } else {
            getRoot().ne(null);
        }
    }


    public void processDefault() {

        Object value = getValue(filter.getValue());

        if (value == null) {
            throw new ArithmeticException("Item is wrong type");
        }
        if (filter.LESS_THEN.equalsIgnoreCase(filter.getOperator())) {
           getRoot().lt(value);
        }
        if (filter.MORE_THEN.equalsIgnoreCase(filter.getOperator())) {
           getRoot().gt(value);
        }
        if (filter.LESS_EQUALS.equalsIgnoreCase(filter.getOperator())) {
            getRoot().lte(value);
        }
        if (filter.MORE_EQUALS.equalsIgnoreCase(filter.getOperator())) {
            getRoot().gte(value);
        }


        if (filter.NOT_NULL_OPERATOR.equalsIgnoreCase(filter.getOperator())) {
            getRoot().ne(null);
        }
        if (filter.NULL_OPERATOR.equalsIgnoreCase(filter.getOperator())) {
            getRoot().isNull();
        }
        if (filter.EQUALS_OPERATOR.equalsIgnoreCase(filter.getOperator()) && !String.class.isAssignableFrom(value.getClass())) {
            getRoot().is(value);
        }
        if (filter.NOT_EQUALS_OPERATOR.equalsIgnoreCase(filter.getOperator()) && !String.class.isAssignableFrom(value.getClass())) {
            getRoot().ne(value);
        }

        if (filter.IN.equalsIgnoreCase(filter.getOperator())) {

            Collection cValue = (Collection) value;
            cValue.forEach((x) -> filter.getValue());
            getRoot().in(value);

        }


    }


    public abstract Object getValue(Object value);

    public static AbstractClassProcessor processor(OrivisFilter<?> filter) {
        Criteria root = ((MongoSpecification<?>)filter).getRoot();

        if (filter.getFieldClass() != null && Collection.class.isAssignableFrom(filter.getFieldClass())) {
            return new CollectionPredicateProcessor(filter, root);
        }

        if (Date.class.equals(filter.getFieldClass()) && !Date.class.isAssignableFrom(filter.getValue().getClass())) {
            return new DatePredicateProcessor(filter, root);
        }

        if (BigDecimal.class.equals(filter.getFieldClass())){
            return new BigDecimalProcessor(filter, root);
        }

        if (filter.getFieldClass() != null && (Number.class.isAssignableFrom(filter.getFieldClass()) || isPrimitiveNumber(filter.getFieldClass()))) {
            return new NumberPredicateProcessor(filter, root);
        }

        if (filter.getFieldClass() != null && (String.class.isAssignableFrom(filter.getFieldClass()))) {
            return new StringPredicateProcessor(filter, root);
        } else {
            return new SimplePredicateProcessor(filter, root);
        }


    }

    private static boolean isPrimitiveNumber(Class c) {
        return int.class == c || long.class == c || short.class == c || double.class == c || float.class == c;
    }

}
