package net.orivis.shared.mongo.controller;

import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.controller.generated.AbstractParentalController;
import net.orivis.shared.form.CountResponse;
import net.orivis.shared.mongo.form.OrivisPojo;
import net.orivis.shared.mongo.model.OrivisEntity;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Secured
public interface ParentalController<T extends OrivisEntity,E extends OrivisPojo> extends AbstractParentalController<T,E, String> {

    @GetMapping("/tree/count/parent/{id}")
    @ResponseBody
    @WithRole
    @Override
    default ResponseEntity<CountResponse> countByParent(@PathVariable("id") String id) {
        return AbstractParentalController.super.countByParent(id);
    }

    @GetMapping("/tree/parent/{id}")
    @ResponseBody
    @WithRole
    @Override
    default ResponseEntity<Page<Map<String, Object>>> findByParent(@PathVariable ("id") String id,
                                                                   @RequestParam(value = "withCount", required = false, defaultValue = "false") Boolean withCount,
                                                                   @RequestParam(value = "limit", required = false) Integer limit,
                                                                   @RequestParam(value = "page", required = false) Integer page) {
        return AbstractParentalController.super.findByParent(id, withCount, limit, page);
    }

    @GetMapping("/tree/root")
    @ResponseBody
    @WithRole
    @Override
    default ResponseEntity<Page<Map<String, Object>>> findRootElements(@RequestParam(value = "withCount", required = false, defaultValue = "false") Boolean withCount,
                                                                       @RequestParam(value = "limit", required = false) Integer limit,
                                                                       @RequestParam(value = "page", required = false) Integer page) {
        return AbstractParentalController.super.findRootElements(withCount, limit, page);
    }
}
