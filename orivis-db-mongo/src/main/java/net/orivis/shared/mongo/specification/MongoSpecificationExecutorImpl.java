package net.orivis.shared.mongo.specification;

import jakarta.transaction.Transactional;
import lombok.Data;
import net.orivis.shared.repository.OrivisFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.lang.Nullable;
import net.orivis.shared.mongo.repository.MongoSpecification;
import net.orivis.shared.mongo.model.OrivisEntity;

import net.orivis.shared.repository.AbstractFilterRepo;
import net.orivis.shared.repository.AbstractRepository;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@NoRepositoryBean
public class MongoSpecificationExecutorImpl<T extends OrivisEntity> implements MongoSpecificationExecutor<T>, AbstractRepository<T, String>, AbstractFilterRepo<T, String> {

    private final MongoTemplate mongoTemplate;
    private final Class<T> entityClass;

    public MongoSpecificationExecutorImpl(final MongoTemplate mongoTemplate) {

        this.mongoTemplate = mongoTemplate;
        entityClass = getEntityClass();
    }

    public final Class<T> getEntityClass() {
        try {
            return (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        } catch (Exception e){
            return null;
        }
    }

    /**
     * if you are not sure that object is only one, please use #findFirst() method instead
     * @param spec can be {@literal null}.
     * @return the only existing row
     */
    @Override

    public Optional<T> filteredOne(@Nullable OrivisFilter<?> spec) {

        assert spec != null;

        if (((MongoSpecification<?>) spec).shouldAggregate(entityClass)) {

            List<AggregationOperation> operations = ((MongoSpecification<?>) spec).toAggregationOperations(entityClass, mongoTemplate);

            Aggregation aggregation = Aggregation.newAggregation(operations)
                    .withOptions(AggregationOptions.builder().allowDiskUse(true).build());


            List<T> results = mongoTemplate.aggregate(aggregation, mongoTemplate.getCollectionName(entityClass), entityClass)
                    .getMappedResults();

            return results.isEmpty() ? Optional.empty() : Optional.of(results.get(0));
        }

        return Optional.ofNullable(mongoTemplate.findOne(Query.query(spec.getQueryRoot()), entityClass));
    }


    @Override
    public Optional<T> filteredFirst(OrivisFilter<?> spec) {
        return  filteredFirst(spec, null);
    }

    @Override
    public Optional<T> filteredFirst(OrivisFilter<?> first, Sort sort) {


        var data = filteredData(first, Pageable.ofSize(1), sort);
        if (data.isEmpty()){
            return  Optional.empty();
        } else {
            return  Optional.of(data.getContent().get(0));
        }
    }

    @Override
    public Page<T> filteredData(@Nullable OrivisFilter<?> spec) {
      return this.filteredData(spec, (Sort) null);
    }


    @Override
    public Page<T> filteredData(@Nullable OrivisFilter<?> spec, final Pageable pageable) {

        return  filteredData(spec, pageable, null);
    }

    @Override
    public Page<T> filteredData(OrivisFilter<?> spec, Sort sort) {
        if (spec == null) {
            return filteredData(spec, Pageable.unpaged(), sort);
        }

        return filteredData(spec,Pageable.unpaged(), sort);
    }

    @Override
    public Page<T> filteredData(OrivisFilter<?> spec, Pageable pageable, Sort sort) {
        if (spec == null) {
            return findAll(pageable);
        }

        if (((MongoSpecification<?>)spec).shouldAggregate(entityClass)) {
            List<AggregationOperation> operations = ((MongoSpecification<?>)spec).toAggregationOperations(entityClass, mongoTemplate);

            if (Objects.nonNull(sort) && !sort.isUnsorted()) {
                operations.add(Aggregation.sort(sort));
            } else if (pageable.getSort().isSorted()) {
                operations.add(Aggregation.sort(pageable.getSort()));
            }

            if (!pageable.isUnpaged()) {
                operations.add(Aggregation.skip(pageable.getOffset()));
                operations.add(Aggregation.limit(pageable.getPageSize()));
            }

            Aggregation aggregation = Aggregation.newAggregation(operations)
                    .withOptions(AggregationOptions.builder().allowDiskUse(true).build());

            List<T> items = mongoTemplate.aggregate(aggregation, mongoTemplate.getCollectionName(entityClass), entityClass).getMappedResults();
            return new PageImpl<>(items, pageable, countAggregation(spec));
        }

        var query = Query.query(spec.getQueryRoot());
        if (sort != null) query = query.with(sort);
        if (pageable != null) query = query.with(pageable);
        final List<T> items = mongoTemplate.find( query, entityClass);
        final long count = this.filteredCount(spec);
        return new PageImpl<>(items, pageable == null ? Pageable.unpaged() : pageable, count);
    }



    @Override
    public long filteredCount(@Nullable OrivisFilter<?> spec) {
        if (spec == null || spec.isEmpty()) {
            return count();
        }

        if (((MongoSpecification<?>)spec).shouldAggregate(entityClass)) {
            return countAggregation(spec);
        }

        return mongoTemplate.count(Query.query(spec.getQueryRoot()), entityClass);
    }

    public List<T> filteredData(final Sort sort) {
        return mongoTemplate.find(new Query().with(sort), entityClass);
    }

    @Override
    public Page<T> findAll(final Pageable pageable) {
        final List<T> items = mongoTemplate.find(new Query().with(pageable), entityClass);
        final long count = count();
        return new PageImpl<>(items, pageable, count);
    }

    @Override
    public <S extends T> S save(final S entity) {
        return mongoTemplate.save(entity);
    }

    @Override
    public <S extends T> Iterable<S> saveAll(Iterable<S> entities) {
        List<S> entitiesList = new ArrayList<>();
        entities.forEach(entitiesList::add);
        mongoTemplate.insertAll(entitiesList);
        return entitiesList;
    }

    @Override
    public Optional<T> findById(final String s) {
        return Optional.ofNullable(mongoTemplate.findById(s, entityClass));
    }

    @Override
    public boolean existsById(final String s) {
        return mongoTemplate.exists(idIs(s), entityClass);
    }

    @Override
    public List<T> findAll() {
        return mongoTemplate.findAll(entityClass);
    }

    @Override
    public Iterable<T> findAllById(final Iterable<String> ids) {
        return mongoTemplate.find(new Query(Criteria.where("id").in(ids)), entityClass);
    }

    @Override
    public long count() {
        return mongoTemplate.count(new Query(), entityClass);
    }

    @Override
    public void deleteById(final String s) {
        mongoTemplate.remove(idIs(s), entityClass);
    }

    @Override
    public void delete(final T entity) {
        mongoTemplate.remove(entity);
    }

    @Override
    public void deleteAllById(Iterable<? extends String> strings) {
        strings.forEach(this::deleteById);
    }

    @Override
    public void deleteAll(final Iterable<? extends T> entities) {
        entities.forEach(this::delete);
    }

    @Override
    public void deleteAll() {
        mongoTemplate.findAllAndRemove(new Query(), entityClass);
    }

    @Override
    @Transactional
    public void deleteData(OrivisFilter<?> spec) {
        Query query = Query.query(spec.getQueryRoot());
        mongoTemplate.remove(query, entityClass);
    }

    private Query idIs(final String s) {
        return new Query(Criteria.where("id").is(s));
    }

    private long countAggregation(@Nullable OrivisFilter<?> spec) {
        List<AggregationOperation> countOperations = (((MongoSpecification<?>) spec).toAggregationOperations(entityClass, mongoTemplate));
        countOperations.add(Aggregation.count().as("totalCount"));

        Aggregation countAggregation = Aggregation.newAggregation(countOperations).withOptions(AggregationOptions.builder().allowDiskUse(true).build());
        AggregationResults<CountResult> countResult = mongoTemplate.aggregate(countAggregation, mongoTemplate.getCollectionName(entityClass), CountResult.class);
        return countResult.getUniqueMappedResult() != null ? countResult.getUniqueMappedResult().totalCount : 0;
    }

    @Data
    public static class CountResult {
        private long totalCount;
    }
}
