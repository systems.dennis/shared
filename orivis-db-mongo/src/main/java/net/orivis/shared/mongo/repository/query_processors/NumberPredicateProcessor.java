package net.orivis.shared.mongo.repository.query_processors;

import lombok.extern.slf4j.Slf4j;
import net.orivis.shared.repository.OrivisFilter;
import org.springframework.data.mongodb.core.query.Criteria;

import java.math.BigInteger;

@Slf4j
public class NumberPredicateProcessor extends AbstractClassProcessor {

    public NumberPredicateProcessor(OrivisFilter queryCase, Criteria root) {
        super(queryCase, root);
    }

    @Override
    public Object getValue(Object value) {
        try {
            if (value instanceof Number) {
                return getNumberFromString( String.valueOf(value), getFilter().getFieldClass());
            } else {
                return null;
            }

        } catch (Exception e) {
            log.warn("Search is not able to parse date: " + value, e);
            return null;
        }
    }

    public static Number getNumberFromString(String value, Class<?> fieldClass) {

        if (Integer.class.equals(fieldClass)) {
            return Integer.valueOf(value);
        }
        if (Double.class.equals(fieldClass)) {
            return Double.valueOf(value);
        }
        if (Long.class.equals(fieldClass)) {
            return Long.valueOf(value);
        }
        if (Short.class.equals(fieldClass)) {
            return Short.valueOf(value);
        }

        if (BigInteger.class.equals(fieldClass)) {
            return BigInteger.valueOf(Long.parseLong(value));
        }

        throw new IllegalArgumentException(" cannot transform String to number! ");
    }

}
