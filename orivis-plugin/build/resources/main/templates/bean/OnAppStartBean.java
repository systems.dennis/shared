package ${base_package}.bean;

import net.orivis.shared.dbupdater.service.Updater;
import net.orivis.shared.scopes.service.AppSettingsService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import net.orivis.shared.beans.OnAppStart;
import net.orivis.shared.config.WebContext;
import net.orivis.shared.utils.ApplicationContext;

import java.io.IOException;

@Service
@Primary
public class OnAppStartBean extends ApplicationContext implements OnAppStart {

    public OnAppStartBean(WebContext context) {
        super(context);
    }

    @Override
    public void onAppRun(WebContext context) {
        getBean(AppSettingsService.class).saveApplicationSettingsIntoDB();

        try {
            context.getBean(Updater.class).updateDb();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}

