package ${package_name};

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.client.RestTemplate;
import net.orivis.shared.${database_type}.repository.PaginationRepositoryImpl;

@EntityScan(basePackages = {
        "net.orivis.shared.*", "${package_name}.*"
})
@EnableJpaRepositories(basePackages = {
        "net.orivis.shared.*", "${package_name}.**"}, repositoryBaseClass = PaginationRepositoryImpl.class)
@SpringBootApplication(scanBasePackages = {"org.springframework.web.*", "${package_name}.*", "net.orivis.shared.*"})
@EnableScheduling
@CrossOrigin
public class ${app_name} {

    public static void main(String[] args) {
        SpringApplication.run(${app_name}.class, args);
    }
}
