package ${package_name}.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.config.OrivisContextable;
import net.orivis.shared.controller.SearchEntityApi;
import net.orivis.shared.controller.items.magic.MagicList;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.exceptions.ItemNotUserException;
import net.orivis.shared.${database_type}.controller.AddItemController;
import net.orivis.shared.${database_type}.controller.DeleteItemController;
import net.orivis.shared.${database_type}.controller.ListItemController;
import net.orivis.shared.${database_type}.controller.AddListItemController;
import net.orivis.shared.${database_type}.controller.GetByIdController;
import net.orivis.shared.${database_type}.controller.EditItemController;
import net.orivis.shared.${database_type}.controller.ParentalController;
import net.orivis.shared.repository.AbstractDataFilter;
import net.orivis.shared.utils.OrivisContext;
import net.orivis.shared.utils.bean_copier.BeanCopier;
import net.orivis.shared.annotations.security.Secured;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Secured
@RestController
@RequestMapping("${path}")
@OrivisController(value = ${entity}Service.class)
@CrossOrigin
public class ${entity}Controller extends OrivisContextable
        implements AddItemController<${entity}Model,${entity}Form>,
        implements EditItemController<${entity}Model,${entity}Form>,
        implements GetByIdController<${entity}Model,${entity}Form>,
        implements DeleteController<${entity}Model>


        {

    public ${entity}Controller(OrivisContext context) {
        super(context);
    }


}
