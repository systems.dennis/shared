package ${package_name}.repo;

import org.springframework.stereotype.Repository;
import ${package_name}.model.${entity}Model;
import net.orivis.shared.postgres.repository.PaginationRepository;

@Repository
public interface ${entity}Repo extends PaginationRepository<${entity}Model> {

}
