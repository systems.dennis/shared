package ${package_name}.repo;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;
import ${package_name}.model.${entity}Model;
import net.orivis.shared.mongo.repository.PaginationRepository;

@Repository
public class ${entity}Repo extends PaginationRepository<${entity}Model> {

    public ${entity}Repo(MongoTemplate mongoTemplate) {
        super(mongoTemplate);
    }
}
