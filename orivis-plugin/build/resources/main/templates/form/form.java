package ${package_name}.form;

import lombok.Data;
import net.orivis.shared.annotations.Validation;
import net.orivis.shared.${database_type}.form.OrivisPojo;
import net.orivis.shared.pojo_form.Checkable;
import net.orivis.shared.pojo_form.PojoFormElement;
import net.orivis.shared.pojo_view.UIAction;
import net.orivis.shared.pojo_view.list.PojoListView;
import net.orivis.shared.pojo_view.list.PojoListViewField;
import net.orivis.shared.pojo_view.list.Remote;
import net.orivis.shared.validation.ValueNotEmptyValidator;

import static net.orivis.shared.pojo_view.DEFAULT_TYPES.CHECKBOX;
import static net.orivis.shared.pojo_view.DEFAULT_TYPES.HIDDEN;
import static net.orivis.shared.pojo_view.DEFAULT_TYPES.NUMBER;
import static net.orivis.shared.pojo_view.DEFAULT_TYPES.TEXT_AREA;

@Data
@PojoListView(actions = {${actions}})
public class ${entity}Form implements OrivisPojo {

    private Long id;
    ${fields}

    @Override
    public String asValue() {
        return "";
    }
}
