package net.orivis.plugin;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.vfs.VirtualFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class CreateOrivisStructureAction extends AnAction {

        @Override
        public void actionPerformed(AnActionEvent event) {
            VirtualFile selectedDir = event.getDataContext().getData(com.intellij.openapi.actionSystem.CommonDataKeys.VIRTUAL_FILE);

            if (selectedDir == null || !selectedDir.isDirectory()) {
                Messages.showErrorDialog("Please select a directory to create the structure.", "Error");
                return;
            }

            String userInput = Messages.showInputDialog(event.getProject(), "Enter the name for the structure:", "Structure Name", Messages.getQuestionIcon());
            String type = Messages.showInputDialog(event.getProject(), "Enter the type for the structure:", "Structure Type (Default Postgres. You can also use Mongo instead)", Messages.getQuestionIcon(), "postgres", null);
            String packageName = Messages.showInputDialog(event.getProject(), "Enter the package name:", "Package name", Messages.getQuestionIcon(), getPackageName(selectedDir), null);


            if (type == null) type = "postgres";
            if (userInput == null || userInput.trim().isEmpty()) {
                Messages.showErrorDialog("The name cannot be empty.", "Error");
                return;
            }
            String path = Messages.showInputDialog(event.getProject(), "Api path:", "Enter Requests base path", Messages.getQuestionIcon(), "/api/v2/" + userInput.toLowerCase(), null);
            String baseName = capitalize(userInput.trim());


            String finalType = type;
            WriteCommandAction.runWriteCommandAction(event.getProject(), () -> {
                try {
                    for (String folderName : new String[]{"model", "form", "service", "repo", "controller"}) {
                        VirtualFile targetFolder = selectedDir.findChild(folderName);
                        if (targetFolder == null) {
                            targetFolder = selectedDir.createChildDirectory(this, folderName);
                        }


                        String templatePath = "/templates/" + folderName + "/" + folderName + ".java";
                        String templateContent = loadTemplate(templatePath);
                        String className = baseName + capitalize(folderName);
                        String fileName = className + ".java";

                        String fullPackageName = packageName.isEmpty() ? folderName : packageName ;


                        templateContent = templateContent
                                .replace("${entity}", baseName)
                                .replace("${path}", path)
                                .replace("${database_type}", finalType)
                                .replace("${package_name}", fullPackageName);

                        createFileWithContent(targetFolder, fileName, templateContent);
                    }

                    Messages.showInfoMessage("Orivis structure created successfully.", "Success");
                } catch (IOException e) {
                    Messages.showErrorDialog("Error while creating structure: " + e.getMessage(), "Error");
                }
            });
        }

        private String loadTemplate(String templatePath) throws IOException {
            try (InputStream inputStream = getClass().getResourceAsStream(templatePath);
                 Scanner scanner = new Scanner(inputStream, StandardCharsets.UTF_8.name())) {
                return scanner.useDelimiter("\\A").next();
            }
        }

        private void createFileWithContent(VirtualFile folder, String fileName, String content) throws IOException {
            VirtualFile file = folder.findChild(fileName);
            if (file == null) {
                file = folder.createChildData(this, fileName);
                file.setBinaryContent(content.getBytes(StandardCharsets.UTF_8));
            }
        }

        private String capitalize(String input) {
            if (input == null || input.isEmpty()) return input;
            return input.substring(0, 1).toUpperCase() + input.substring(1);
        }

        private String getPackageName(VirtualFile directory) {
            String path = directory.getPath().replace("/", ".");
            int srcIndex = path.indexOf("src.main.java");
            if (srcIndex != -1) {
                return path.substring(srcIndex + "src.main.java".length() + 1);
            }
            return "";
        }

        @Override
        public void update(AnActionEvent event) {
            VirtualFile selectedDir = event.getDataContext().getData(com.intellij.openapi.actionSystem.CommonDataKeys.VIRTUAL_FILE);
            event.getPresentation().setEnabledAndVisible(selectedDir != null && selectedDir.isDirectory());
        }
}