package ${package_name}.model;

import jakarta.persistence.Entity;
import lombok.Data;
import net.orivis.shared.${database_type}.model.OrivisEntity;

@Data
@Entity
public class ${entity}Model extends OrivisEntity {

${fields}

    @Override
    public String asValue() {
        return "";
    }
}
