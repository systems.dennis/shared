package ${package_name}.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import net.orivis.shared.annotations.DataRetrieverDescription;
import net.orivis.shared.annotations.security.ISecurityUtils;
import net.orivis.shared.config.WebContext;
import net.orivis.shared.exceptions.ItemAlreadyExistsException;
import net.orivis.shared.exceptions.ItemForAddContainsIdException;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.exceptions.ItemNotUserException;

import ${package_name}.form.${entity}Form;
import ${package_name}.model.${entity}Model;
import ${package_name}.repo.${entity}Repo;
import net.orivis.shared.pojo_view.list.PojoListView;
import net.orivis.shared.${database_type}.service.PaginationService;
import net.orivis.shared.repository.AbstractDataFilter;

@Slf4j
@Service
@OrivisService(model = ${entity}Model.class, form = ${entity}Form.class, repo = ${entity}Repo.class)
public class ${entity}Service extends PaginationService<${entity}Model> {

    public ${entity}Service(OrivisContext context) {
        super(context);
    }
}
