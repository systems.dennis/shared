package net.orivis.shared.servers.repository;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;
import net.orivis.shared.mongo.repository.PaginationRepository;
import net.orivis.shared.servers.model.ServerConfigType;

@Repository
public class ServerConfigTypeRepo extends PaginationRepository<ServerConfigType> {
    public ServerConfigTypeRepo(MongoTemplate mongoTemplate) {
        super(mongoTemplate);
    }
}
