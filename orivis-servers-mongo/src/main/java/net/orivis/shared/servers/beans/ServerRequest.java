package net.orivis.shared.servers.beans;

import lombok.Getter;
import net.orivis.shared.annotations.NeverNullResponse;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.servers.model.ServerConfig;
import net.orivis.shared.servers.service.ServerConfigService;
import net.orivis.shared.utils.Supplier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

public class ServerRequest<T> {
    @Getter
    private RestTemplate restTemplate;
    @Getter
    private OrivisContext context;

    @Getter
    private Map<Integer, Supplier<?>> onError = new HashMap<>();
    @Getter
    private T result;

    @Getter
    private String url;

    public  static String AUTH_TYPE_HEADER = "AUTH_TYPE";
    public  static String AUTH_SCOPE_HEADER = "AUTH_SCOPE";
    public static final String AUTH_TYPE_DEFAULT = "DEFAULT";
    public  static String AUTH_TYPE_VIRTUAL = "VIRTUAL";

    public ServerRequest(RestTemplate restTemplate, OrivisContext context) {
        this.restTemplate = restTemplate;

        this.context = context;
    }


    @Getter
    private final HttpHeaders headers = new HttpHeaders();

    private ServerConfig server;

    @NeverNullResponse
    public ServerRequest<T> virtualAuth() {
        headers.set(AUTH_TYPE_HEADER, AUTH_TYPE_VIRTUAL);
        return this;
    }

    @NeverNullResponse
    public ServerRequest<T> uri(String uri) {

        this.url = uri;
        return this;
    }

    @NeverNullResponse
    public ServerRequest<T> server(Long type) {
        this.server = context.getBean(ServerConfigService.class).findByType(type, true);
        return this;
    }

    @NeverNullResponse
    public ServerRequest<T> server(ServerConfig server) {
        this.server = context.getBean(ServerConfigService.class).findByIdOrThrow(server.getId());
        return this;
    }

    @NeverNullResponse
    public ServerRequest<T> token(String token) {
        headers.setBearerAuth(token);
        return this;
    }

    @NeverNullResponse
    public ServerRequest<T> scope() {
        headers.set(AUTH_SCOPE_HEADER, getCurrentScope());
        return this;
    }

    @NeverNullResponse
    public ServerRequest<T> scope(String scope) {
        headers.set(AUTH_SCOPE_HEADER, scope);
        return this;
    }

    @NeverNullResponse
    public ServerRequest<T> onErrorStatusError(int status, Supplier<?> e) {
        onError.put(status, e);
        return this;
    }

    @NeverNullResponse
    public ServerRequest<T> onAnyErrorStatusError(Supplier<?> e) {
        onError.put(-1, e);
        return this;
    }


    /**
     * @deprecated as normally default type doesn't require this header
     */
    @Deprecated
    @NeverNullResponse
    public ServerRequest<T> defaultAuthType() {
        headers.set(AUTH_TYPE_HEADER, AUTH_TYPE_DEFAULT);
        return this;
    }


    public <E> T executePost(E object, Class<T> tClass) {
        var el = new HttpEntity<>(object, this.headers);
        var resp = restTemplate.postForEntity(server.getRoot() + url, el, tClass);
        validateExceptions(resp);

        this.result = resp.getBody();
        return this.result;
    }

    private void validateExceptions(ResponseEntity<T> resp) {
        var anyError = onError.get(-1);
        if (200 != resp.getStatusCode().value()) {
            this.onError.keySet().forEach((code) -> {
                if (code != -1) {
                    if (resp.getStatusCode().value() == code) {
                        onError.get(code).onNull(resp.getStatusCode().value());
                    }
                }
            });

            if (anyError != null) {
                anyError.onNull(resp.getStatusCode().value());
            }
        }


    }

    public <E> T executeGet(Class<T> tClass) {
        var el = new HttpEntity<>(this.headers);
        var resp = restTemplate.exchange(server.getRoot() + url, HttpMethod.GET, el, tClass);
        validateExceptions(resp);

        this.result = resp.getBody();
        return this.result;
    }

    public void executeDelete(Class<T> tClass) {
        var requestEntity = new HttpEntity<>(this.headers);
        var resp = restTemplate.exchange(server.getRoot() + url, HttpMethod.DELETE, requestEntity, tClass);
        validateExceptions(resp);
    }

    private String getCurrentScope() {
        return context.getEnv("dennis.systems.security.scope.id", "Default");
    }


}
