package net.orivis.shared.servers.service;

import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.mongo.service.PaginationService;
import net.orivis.shared.servers.form.ServerConfigTypeForm;
import net.orivis.shared.servers.model.ServerConfigType;
import net.orivis.shared.servers.repository.ServerConfigTypeRepo;
import org.springframework.stereotype.Service;

@Service
@OrivisService(repo = ServerConfigTypeRepo.class, form = ServerConfigTypeForm.class, model = ServerConfigType.class)
public class ServerConfigTypeService extends PaginationService<ServerConfigType> {
    public ServerConfigTypeService(OrivisContext holder) {
        super(holder);
    }

}
