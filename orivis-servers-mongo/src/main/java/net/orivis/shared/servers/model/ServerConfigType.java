package net.orivis.shared.servers.model;

import lombok.Data;
import net.orivis.shared.mongo.model.OrivisEntity;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class ServerConfigType extends OrivisEntity {
    private String name;

    @Override
    public String asValue() {
        return name;
    }
}
