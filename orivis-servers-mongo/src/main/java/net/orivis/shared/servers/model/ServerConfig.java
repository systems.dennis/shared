package net.orivis.shared.servers.model;

import jakarta.persistence.Transient;
import lombok.Data;
import net.orivis.shared.annotations.ObjectByIdPresentation;
import net.orivis.shared.mongo.model.OrivisEntity;
import net.orivis.shared.servers.service.ServerConfigTypeService;
import net.orivis.shared.utils.bean_copier.OrivisIdToObjectTransformer;
import net.orivis.shared.utils.bean_copier.OrivisTranformer;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
public class ServerConfig  extends OrivisEntity {
    public static final String SERVER_CONFIG_FIELD = "serverConfigType";

    private String host;
    private String name;
    private String userName;
    private String password;
    private Integer port;
    private Long type;
    private Boolean active;
    private String serverParam;
    private String timeZone;

    @DBRef
    @ObjectByIdPresentation
    @OrivisTranformer(transformWith = OrivisIdToObjectTransformer.class, additionalClass = ServerConfigTypeService.class)
    private ServerConfigType serverConfigType;

    @Transient
    public String getRoot(){
        return host + ":" + port + "/";
    }

    @Override
    public String asValue() {
        return getRoot();
    }
}
