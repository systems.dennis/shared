package net.orivis.shared.servers.form;

import lombok.Data;
import net.orivis.shared.annotations.Validation;
import net.orivis.shared.mongo.form.OrivisPojo;
import net.orivis.shared.pojo_form.PojoFormElement;
import net.orivis.shared.pojo_view.UIAction;
import net.orivis.shared.pojo_view.list.PojoListView;
import net.orivis.shared.pojo_view.list.PojoListViewField;
import net.orivis.shared.validation.FieldIsUniqueValidator;
import net.orivis.shared.validation.ValueNotEmptyValidator;

import static net.orivis.shared.pojo_view.DEFAULT_TYPES.HIDDEN;
import static net.orivis.shared.pojo_view.DEFAULT_TYPES.TEXT;

@Data
@PojoListView(actions = {"download",  "new", "settings"})
public class ServerConfigTypeForm implements OrivisPojo {

    @PojoFormElement (type = HIDDEN)
    private String id;

    @PojoFormElement (type = TEXT)
    @Validation (value = {ValueNotEmptyValidator.class, FieldIsUniqueValidator.class})
    private String name;


    @PojoListViewField(showContent = false, actions = {
            @UIAction(component = "edit"), @UIAction(component = "delete", allowOnMultipleRows = true)
    })

    @PojoFormElement(type = HIDDEN)
    private Long actions;
}
