package net.orivis.shared.servers.repository;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;
import net.orivis.shared.mongo.repository.PaginationRepository;
import net.orivis.shared.servers.model.ServerConfig;

@Repository
public class ServerConfigRepo extends PaginationRepository<ServerConfig> {

    public ServerConfigRepo(MongoTemplate mongoTemplate) {
        super(mongoTemplate);
    }
}
