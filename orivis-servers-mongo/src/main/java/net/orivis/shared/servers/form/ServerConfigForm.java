package net.orivis.shared.servers.form;

import lombok.Data;
import net.orivis.shared.annotations.ObjectByIdPresentation;
import net.orivis.shared.annotations.Validation;
import net.orivis.shared.mongo.form.OrivisPojo;
import net.orivis.shared.pojo_form.Checkable;
import net.orivis.shared.pojo_form.PojoFormElement;
import net.orivis.shared.pojo_view.UIAction;
import net.orivis.shared.pojo_view.list.PojoListView;
import net.orivis.shared.pojo_view.list.PojoListViewField;
import net.orivis.shared.pojo_view.list.Remote;
import net.orivis.shared.servers.controller.ServerTypeController;
import net.orivis.shared.servers.providers.ServerTypeProvider;
import net.orivis.shared.servers.service.ServerConfigTypeService;
import net.orivis.shared.servers.validation.ServerTypeValidator;
import net.orivis.shared.utils.bean_copier.OrivisIdToObjectTransformer;
import net.orivis.shared.utils.bean_copier.OrivisTranformer;
import net.orivis.shared.validation.ValueIsIntAndMoreThenZero;

import static net.orivis.shared.pojo_view.DEFAULT_TYPES.*;

@Data
@PojoListView (actions = {"download",  "new", "settings"})
public class ServerConfigForm implements OrivisPojo {

    @PojoFormElement(type = HIDDEN)
    private String id;

    @PojoFormElement(required = true)
    @PojoListViewField (searchable = true)
    private String name;

    @PojoFormElement(required = true)
    @PojoListViewField (searchable = true)
    private String host;

    @PojoListViewField (searchable = true)
    @PojoFormElement (type =  "number")
    @Validation (ValueIsIntAndMoreThenZero.class)
    private Integer port;

    @PojoListViewField (searchable = true)

    @PojoFormElement
    private String userName;

    @PojoFormElement(type = PASSWORD)
    @PojoListViewField(available = false)
    private String password;

    @PojoListViewField (searchable = true)
    private String serverParam;

    @PojoListViewField (searchable = true)
    @PojoFormElement
    private String timeZone;

    @PojoFormElement(type = DROP_DOWN, dataProvider = ServerTypeProvider.class)
    @Validation({ServerTypeValidator.class})
    private Long type;

    @PojoFormElement (remote = @Remote(controller = ServerTypeController.class), type = OBJECT_SEARCH)
    @PojoListViewField(remote = @Remote(controller = ServerTypeController.class), type = OBJECT_SEARCH)

    @ObjectByIdPresentation
    @OrivisTranformer(transformWith = OrivisIdToObjectTransformer.class, additionalClass = ServerConfigTypeService.class)
    private String serverConfigType;

    @PojoFormElement(type = CHECKBOX, checked = @Checkable(checked = true), remote = @Remote(searchType = "CHECKBOX"))
    private Boolean active;

    @PojoListViewField(showContent = false, actions = {
            @UIAction(component = "edit"), @UIAction(component = "delete", allowOnMultipleRows = true)
    })

    @PojoFormElement(type = HIDDEN)
    private Long actions;

    @Override
    public String asValue() {
        return name  + " " + type;
    }
}
