package net.orivis.shared.servers.controller;

import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.mongo.controller.AddItemController;
import net.orivis.shared.mongo.controller.DeleteItemController;
import net.orivis.shared.mongo.controller.EditItemController;
import net.orivis.shared.mongo.controller.ListItemController;
import net.orivis.shared.servers.form.ServerConfigTypeForm;
import net.orivis.shared.servers.model.ServerConfigType;
import net.orivis.shared.servers.service.ServerConfigTypeService;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Provides Role adding, editing, deleting and assigning
 */
@RestController
@RequestMapping("/api/v2/shared/servers_type")
@Secured (roles = "ROLE_ADMIN")
@CrossOrigin
@OrivisController(ServerConfigTypeService.class)
public class ServerTypeController extends OrivisContextable implements
        DeleteItemController<ServerConfigType>,
        AddItemController<ServerConfigType, ServerConfigTypeForm>,
        EditItemController<ServerConfigType, ServerConfigTypeForm>,
        ListItemController<ServerConfigType, ServerConfigTypeForm> {

    public ServerTypeController(OrivisContext context) {
        super(context );
    }

}
