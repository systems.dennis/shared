package net.orivis.shared.dictionary.repository;

import net.orivis.shared.postgres.repository.OrivisRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import net.orivis.shared.dictionary.model.DictionaryModel;

import java.io.Serializable;
import java.util.List;

@Repository
public interface DictionaryRepo<ID_TYPE extends Serializable> extends OrivisRepository<DictionaryModel> {

    @Query (nativeQuery = true, value = "select distinct type from dictionary_model")
    List<String> getDictionaryTypes();
}
