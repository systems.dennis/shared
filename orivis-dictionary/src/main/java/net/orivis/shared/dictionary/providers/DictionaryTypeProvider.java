package net.orivis.shared.dictionary.providers;

import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.dictionary.service.DictionaryService;
import net.orivis.shared.pojo_form.DataProvider;
import net.orivis.shared.pojo_form.ItemValue;

import java.util.ArrayList;

public class DictionaryTypeProvider implements DataProvider<String> {
    @Override
    public ArrayList<ItemValue<String>> getItems(OrivisContext context) {
        var types = context.getBean(DictionaryService.class).getDictionaryTypes();

        var items = new ArrayList<ItemValue<String>>();

        for (var type : types){
            items.add(new ItemValue<>(type, type));
        }


        return items;
    }
}
