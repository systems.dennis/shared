package net.orivis.shared.dictionary.form;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ImportRegionsHolder {
    private List<ImportRegionsModel> values  = new ArrayList<>();
}
