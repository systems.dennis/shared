package net.orivis.shared.dictionary.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToOne;
import lombok.Data;
import net.orivis.shared.annotations.ObjectByIdPresentation;
import net.orivis.shared.dictionary.service.DictionaryService;
import net.orivis.shared.postgres.model.OrivisEntity;
import net.orivis.shared.utils.bean_copier.OrivisIdToObjectTransformer;
import net.orivis.shared.utils.bean_copier.OrivisTranformer;

@Data
@Entity
public class  DictionaryModel extends OrivisEntity {

    public static final int TYPE_INT = 0;
    public static final int TYPE_STRING = 1;
    public static final String DICTIONARY_FIELD = "value";
    @ObjectByIdPresentation
    @OneToOne @OrivisTranformer(transformWith = OrivisIdToObjectTransformer.class, additionalClass = DictionaryService.class)
    private DictionaryModel parent;

    private String type;

    private Long dictionaryTypeId;

    private String icon;

    @Column(name = "order_number")
    private Long order;

    private Integer dataType = TYPE_STRING;

    private String value;

    @Override
    public String asValue() {

        if (parent == null){
            return  value;
        }
        return  value + " (" + parent.value + ")" ;
    }
}
