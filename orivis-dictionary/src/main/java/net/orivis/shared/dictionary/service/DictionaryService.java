package net.orivis.shared.dictionary.service;

import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.repository.OrivisFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import net.orivis.shared.postgres.service.PaginationService;
import net.orivis.shared.dictionary.exception.DictionaryException;
import net.orivis.shared.dictionary.form.DictionaryForm;
import net.orivis.shared.dictionary.model.DictionaryModel;
import net.orivis.shared.dictionary.repository.DictionaryRepo;
import net.orivis.shared.dictionary.response.DictionaryTypesResponse;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Service
@OrivisService(model = DictionaryModel.class, form = DictionaryForm.class, repo = DictionaryRepo.class)
public class DictionaryService extends PaginationService<DictionaryModel> {
    public DictionaryService(OrivisContext holder) {
        super(holder);
    }

    @Override
    public Page<DictionaryModel> search(String field, String subtype, String value, int page, Integer size, Serializable[] additionalIds) {


        try {
            OrivisFilter<DictionaryModel> spec = getSearchRequestSpecification(DictionaryModel.DICTIONARY_FIELD, value, updateIds(additionalIds));
            spec = spec.and(getFilterImpl().eq("type", subtype));
            return getRepository().filteredData(spec, PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, DictionaryModel.DICTIONARY_FIELD)));
        } catch (Exception e) {
            throw new DictionaryException("global.search.dictionary.failed", HttpStatus.BAD_REQUEST);
        }

    }

    public OrivisFilter<DictionaryModel> getSearchRequestSpecification(String field, String value, Long[] additionalIds) {


        var filter = getFilterImpl().setInsensitive(true).contains(field, value).and(getAdditionalSpecification());

        if (additionalIds != null) {
            filter.and(getFilterImpl().eq("group", additionalIds[0]));
        }

        return filter;
    }

    public DictionaryTypesResponse getTypes() {
        DictionaryTypesResponse dictionaryTypesResponse = new DictionaryTypesResponse();
        dictionaryTypesResponse.setTypes(new ArrayList<>(getRepository().getDictionaryTypes()));
        return dictionaryTypesResponse;
    }


    @Override
    public boolean isPublicSearchEnabled() {
        return true;
    }

    @Override
    public DictionaryRepo getRepository() {
        return super.getRepository();
    }

    public List<String> getDictionaryTypes() {
        return getRepository().getDictionaryTypes();
    }
}
