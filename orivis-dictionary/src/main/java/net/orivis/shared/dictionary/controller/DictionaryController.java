package net.orivis.shared.dictionary.controller;

import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.exceptions.ItemForAddContainsIdException;
import net.orivis.shared.postgres.controller.AddItemController;
import net.orivis.shared.postgres.controller.DeleteItemController;
import net.orivis.shared.postgres.controller.EditItemController;
import net.orivis.shared.postgres.controller.GetByIdController;
import net.orivis.shared.postgres.controller.ListItemController;
import net.orivis.shared.dictionary.form.DictionaryForm;
import net.orivis.shared.dictionary.model.DictionaryModel;
import net.orivis.shared.dictionary.response.DictionaryTypesResponse;
import net.orivis.shared.dictionary.service.DictionaryService;

@RestController
@RequestMapping("/api/v2/shared/dictionary")
@OrivisController(value = DictionaryService.class)
@CrossOrigin
public class DictionaryController
        extends OrivisContextable
        implements AddItemController<DictionaryModel, DictionaryForm>,
        EditItemController<DictionaryModel, DictionaryForm >,
        ListItemController<DictionaryModel, DictionaryForm >,
        DeleteItemController<DictionaryModel>,
        GetByIdController<DictionaryModel, DictionaryForm > {


    public DictionaryController(OrivisContext context) {
        super(context);
    }

    @PostMapping(value = "/add",  consumes = {"application/json", "application/json;charset=UTF-8"}, produces = {"application/json", "application/json;charset=UTF-8"})
    @ResponseBody
    @WithRole("ROLE_ADMIN")
    @Override
    public ResponseEntity<DictionaryForm> add(DictionaryForm form) throws ItemForAddContainsIdException {
        return AddItemController.super.add(form);
    }

    @PutMapping(value = "/edit",  consumes = {"application/json", "application/json;charset=UTF-8"}, produces = {"application/json", "application/json;charset=UTF-8"})
    @ResponseBody
    @WithRole("ROLE_ADMIN")
    @Override
    public ResponseEntity<DictionaryForm> edit(DictionaryForm form) throws ItemForAddContainsIdException {
        return EditItemController.super.edit(form);
    }

    @RequestMapping(value="/delete/{id}", method= RequestMethod.DELETE)
    @WithRole("ROLE_ADMIN")
    @Override
    public void delete(Long id) throws ItemForAddContainsIdException {
        DeleteItemController.super.delete(id);
    }

    @GetMapping("/types")
    @WithRole
    public DictionaryTypesResponse getTypes() {
        return getService().getTypes();
    }

    @Override
    public DictionaryService getService() {
        return  getBean(DictionaryService.class);
    }

    @Override
    public String getDefaultField() {
        return "type";
    }
}
