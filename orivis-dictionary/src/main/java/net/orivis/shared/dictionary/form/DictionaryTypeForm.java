package net.orivis.shared.dictionary.form;

import lombok.Data;
import net.orivis.shared.annotations.Validation;
import net.orivis.shared.pojo_form.PojoFormElement;
import net.orivis.shared.pojo_view.UIAction;
import net.orivis.shared.pojo_view.list.PojoListView;
import net.orivis.shared.pojo_view.list.PojoListViewField;
import net.orivis.shared.postgres.form.OrivisPojo;
import net.orivis.shared.validation.ValueNotEmptyValidator;

import static net.orivis.shared.pojo_view.DEFAULT_TYPES.*;

@Data

@PojoListView (actions = {UIAction.ACTION_NEW, UIAction.ACTION_DOWNLOAD, UIAction.ACTION_SETTINGS })
public class DictionaryTypeForm implements OrivisPojo {

    @PojoFormElement(type = HIDDEN)
    private Long id;

    @PojoFormElement(type = TEXT)
    @Validation(ValueNotEmptyValidator.class)
    private String name;

    @PojoFormElement (type = TEXT_AREA)
    private String description;

    @PojoFormElement (type = NUMBER)
    private Long order;

    @PojoFormElement (type = FILE)
    private String icon;

    @PojoListViewField (actions = {@UIAction(UIAction.ACTION_DELETE), @UIAction(UIAction.ACTION_EDIT)})
    @PojoFormElement(available = false)
    private int actions = 0;

    @Override
    public String asValue() {
        return "";
    }
}
