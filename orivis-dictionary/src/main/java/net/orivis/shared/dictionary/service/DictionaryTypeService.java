package net.orivis.shared.dictionary.service;

import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.config.OrivisContext;
import org.springframework.stereotype.Service;
import net.orivis.shared.postgres.service.PaginationService;
import net.orivis.shared.dictionary.form.DictionaryTypeForm;
import net.orivis.shared.dictionary.model.DictionaryTypeModel;
import net.orivis.shared.dictionary.repository.DictionaryTypeRepository;

@Service
@OrivisService(model = DictionaryTypeModel.class, form = DictionaryTypeForm.class, repo = DictionaryTypeRepository.class)
public class DictionaryTypeService extends PaginationService<DictionaryTypeModel> {
    public DictionaryTypeService(OrivisContext holder) {
        super(holder);
    }

}
