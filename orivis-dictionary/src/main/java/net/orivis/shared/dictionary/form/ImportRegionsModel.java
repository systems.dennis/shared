package net.orivis.shared.dictionary.form;

import lombok.Data;

@Data
public class ImportRegionsModel {
    private String region;
    private String city;
}
