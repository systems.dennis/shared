package net.orivis.shared.dictionary.repository;

import net.orivis.shared.postgres.repository.OrivisRepository;
import org.springframework.stereotype.Repository;
import net.orivis.shared.dictionary.model.DictionaryTypeModel;

@Repository
public interface DictionaryTypeRepository extends OrivisRepository<DictionaryTypeModel> {
}
