package net.orivis.shared.dictionary.exception;

import org.springframework.http.HttpStatus;
import net.orivis.shared.exceptions.StatusException;

public class DictionaryException extends StatusException {
    public DictionaryException(String s, HttpStatus code) {
        super(s, code);
    }
}
