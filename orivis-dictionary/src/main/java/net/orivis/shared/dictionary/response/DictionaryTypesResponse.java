package net.orivis.shared.dictionary.response;

import lombok.Data;

import java.util.List;

@Data
public class DictionaryTypesResponse {

    private List<String> types;
}
