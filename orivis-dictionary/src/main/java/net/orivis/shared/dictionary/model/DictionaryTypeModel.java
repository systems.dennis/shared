package net.orivis.shared.dictionary.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import lombok.Data;
import net.orivis.shared.postgres.model.LongAssignableEntity;

@Data
@Entity
public class DictionaryTypeModel extends LongAssignableEntity {
    private String name;

    @Column(columnDefinition = "text")
    private String description;

    @Column(name = "order_number")
    private Long order;

    private String icon;

    @Override
    public String asValue() {
        return "";
    }
}
