package net.orivis.shared.dictionary.controller;

import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import net.orivis.shared.annotations.security.WithRole;
import net.orivis.shared.exceptions.ItemForAddContainsIdException;
import net.orivis.shared.postgres.controller.*;
import net.orivis.shared.dictionary.form.DictionaryTypeForm;
import net.orivis.shared.dictionary.model.DictionaryTypeModel;
import net.orivis.shared.dictionary.service.DictionaryTypeService;

@RestController
@RequestMapping("/api/v2/shared/dictionary_type")
@OrivisController(value = DictionaryTypeService.class)
@CrossOrigin
public class DictionaryType
        extends OrivisContextable
        implements AddItemController<DictionaryTypeModel, DictionaryTypeForm>,
        EditItemController<DictionaryTypeModel, DictionaryTypeForm >,
        ListItemController<DictionaryTypeModel, DictionaryTypeForm >,
        DeleteItemController<DictionaryTypeModel>,
        GetByIdController<DictionaryTypeModel, DictionaryTypeForm > {


    public DictionaryType(OrivisContext context) {
        super(context);
    }

    @PostMapping(value = "/add",  consumes = {"application/json", "application/json;charset=UTF-8"}, produces = {"application/json", "application/json;charset=UTF-8"})
    @ResponseBody
    @WithRole("ROLE_ADMIN")
    @Override
    public ResponseEntity<DictionaryTypeForm> add(DictionaryTypeForm form) throws ItemForAddContainsIdException {
        return AddItemController.super.add(form);
    }

    @PutMapping(value = "/edit",  consumes = {"application/json", "application/json;charset=UTF-8"}, produces = {"application/json", "application/json;charset=UTF-8"})
    @ResponseBody
    @WithRole("ROLE_ADMIN")
    @Override
    public ResponseEntity<DictionaryTypeForm> edit(DictionaryTypeForm form) throws ItemForAddContainsIdException {
        return EditItemController.super.edit(form);
    }

    @RequestMapping(value="/delete/{id}", method= RequestMethod.DELETE)
    @WithRole("ROLE_ADMIN")
    @Override
    public void delete(@PathVariable Long id) throws ItemForAddContainsIdException {
        DeleteItemController.super.delete(id);
    }

    @Override
    public DictionaryTypeService getService() {
        return  getBean(DictionaryTypeService.class);
    }

    @Override
    public String getDefaultField() {
        return "type";
    }
}
