package net.orivis.shared.dictionary.form;

import lombok.Data;
import net.orivis.shared.annotations.ObjectByIdPresentation;
import net.orivis.shared.pojo_form.PojoFormElement;
import net.orivis.shared.pojo_view.DEFAULT_TYPES;
import net.orivis.shared.pojo_view.UIAction;
import net.orivis.shared.pojo_view.list.PojoListView;
import net.orivis.shared.pojo_view.list.PojoListViewField;
import net.orivis.shared.pojo_view.list.Remote;
import net.orivis.shared.dictionary.model.DictionaryModel;
import net.orivis.shared.dictionary.service.DictionaryService;
import net.orivis.shared.postgres.form.OrivisPojo;
import net.orivis.shared.utils.bean_copier.OrivisIdToObjectTransformer;
import net.orivis.shared.utils.bean_copier.OrivisTranformer;

import static net.orivis.shared.pojo_view.DEFAULT_TYPES.*;
import static net.orivis.shared.pojo_view.UIAction.ACTION_DELETE;
import static net.orivis.shared.pojo_view.UIAction.ACTION_EDIT;

@Data
@PojoListView (actions = {UIAction.ACTION_NEW})
public class DictionaryForm  implements OrivisPojo {
    @PojoFormElement(type = HIDDEN)
    private Long id;

    @ObjectByIdPresentation
    @OrivisTranformer(transformWith = OrivisIdToObjectTransformer.class, additionalClass = DictionaryService.class)
    @PojoFormElement (type = DEFAULT_TYPES.OBJECT_SEARCH, remote = @Remote(searchField = "value", searchType = DEFAULT_TYPES.OBJECT_SEARCH, searchName = "dictionary"))
    @PojoListViewField
    private Long parent;

    @PojoFormElement
    @PojoListViewField
    private String type;

    @PojoFormElement (type = REFERENCED_ID, remote = @Remote(searchType = REFERENCED_ID))
    @PojoListViewField(type = REFERENCED_ID, remote = @Remote(searchType = REFERENCED_ID, searchField = "name"))
    private Long dictionaryTypeId;

    @PojoFormElement (type = FILE)
    @PojoListViewField(type =  FILE)
    private String icon;

    @PojoFormElement (type = NUMBER)
    @PojoListViewField(type =  NUMBER)
    private Long order;

    @PojoFormElement (type = NUMBER)
    @PojoListViewField
    private Integer dataType = DictionaryModel.TYPE_STRING;

    @PojoFormElement
    private String value;

    @PojoListViewField(actions = {@UIAction(component = ACTION_EDIT), @UIAction(ACTION_DELETE)})
    private Long actions;


    @Override
    public String asValue() {
        return type + " "+ value;
    }
}
