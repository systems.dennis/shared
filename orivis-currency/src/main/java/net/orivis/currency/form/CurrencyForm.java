package net.orivis.currency.form;

import lombok.Data;
import net.orivis.shared.annotations.Validation;
import net.orivis.shared.pojo_form.Checkable;
import net.orivis.shared.pojo_form.PojoFormElement;
import net.orivis.shared.pojo_view.UIAction;
import net.orivis.shared.pojo_view.list.PojoListView;
import net.orivis.shared.pojo_view.list.PojoListViewField;
import net.orivis.shared.pojo_view.list.Remote;
import net.orivis.shared.postgres.form.OrivisPojo;
import net.orivis.shared.validation.ValueNotEmptyValidator;

import static net.orivis.shared.pojo_view.DEFAULT_TYPES.CHECKBOX;
import static net.orivis.shared.pojo_view.DEFAULT_TYPES.HIDDEN;
import static net.orivis.shared.pojo_view.DEFAULT_TYPES.NUMBER;
import static net.orivis.shared.pojo_view.DEFAULT_TYPES.TEXT_AREA;

@Data
@PojoListView(actions = {"download", "new", "settings"})
public class CurrencyForm implements OrivisPojo {

    @PojoFormElement(type = HIDDEN)
    private Long id;

    @PojoFormElement(required = true)
    @Validation(ValueNotEmptyValidator.class)
    @PojoListViewField(searchable = true)
    private String code;

    @PojoFormElement(required = true)
    @Validation(ValueNotEmptyValidator.class)
    @PojoListViewField(searchable = true)
    private String name;

    @PojoFormElement(type = TEXT_AREA)
    @PojoListViewField(searchable = true)
    private String description;

    @PojoFormElement (checked = @Checkable, type = NUMBER, remote = @Remote(searchType = NUMBER))
    @PojoListViewField (type = NUMBER, remote = @Remote(searchType = NUMBER))
    private Double rateToBase;

    @PojoFormElement (checked = @Checkable, type = CHECKBOX, remote = @Remote(searchType = CHECKBOX))
    @PojoListViewField (type = CHECKBOX, remote = @Remote(searchType = CHECKBOX))
    private Boolean def = false;

    @PojoListViewField(actions = {@UIAction(component = "edit"), @UIAction(component = "delete", allowOnMultipleRows = true)}, visible = false)
    @PojoFormElement(available = false)
    private Integer action;

    @Override
    public String asValue() {
        return name;
    }
}
