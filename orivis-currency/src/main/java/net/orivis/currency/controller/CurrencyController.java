package net.orivis.currency.controller;

import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import net.orivis.currency.form.CurrencyForm;
import net.orivis.currency.model.CurrencyModel;
import net.orivis.currency.service.CurrencyService;
import net.orivis.shared.controller.SearchEntityApi;
import net.orivis.shared.controller.SearcherInfo;
import net.orivis.shared.postgres.controller.AddItemController;
import net.orivis.shared.postgres.controller.DeleteItemController;
import net.orivis.shared.postgres.controller.EditItemController;
import net.orivis.shared.postgres.controller.ListItemController;

@RestController
@RequestMapping("/api/v2/shared/currency")
@OrivisController(value = CurrencyService.class)
@CrossOrigin
public class CurrencyController extends OrivisContextable
        implements AddItemController<CurrencyModel, CurrencyForm>,
        EditItemController<CurrencyModel, CurrencyForm>,
        ListItemController<CurrencyModel, CurrencyForm>,
        DeleteItemController<CurrencyModel> {

    public CurrencyController(OrivisContext context) {
        super(context);
    }

    static {
        SearchEntityApi.registerSearch("currency", new SearcherInfo("name", CurrencyService.class));
    }

    @Override
    public CurrencyService getService() {
        return getContext().getBean(CurrencyService.class);
    }
}
