package net.orivis.currency.service;

import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.config.OrivisContext;
import org.springframework.stereotype.Service;
import net.orivis.currency.form.CurrencyForm;
import net.orivis.currency.model.CurrencyModel;
import net.orivis.currency.repository.CurrencyRepo;
import net.orivis.shared.annotations.DeleteStrategy;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.postgres.service.PaginationService;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static net.orivis.shared.annotations.DeleteStrategy.DELETE_STRATEGY_PROPERTY;

@Service
@OrivisService(model = CurrencyModel.class, form = CurrencyForm.class, repo = CurrencyRepo.class)
@DeleteStrategy(value =  DELETE_STRATEGY_PROPERTY)
public class CurrencyService extends PaginationService<CurrencyModel> {

    public CurrencyService(OrivisContext holder) {
        super(holder);
    }

    public CurrencyModel findBase() {
        CurrencyRepo repo = getRepository();
        return repo.findFirstByDefIsTrue().orElseThrow(()-> new ItemNotFoundException(" No def currency in system"));
    }

    public BigDecimal convertToBaseCurrency(BigDecimal amount, String name) {
        CurrencyModel currency = findByName(name);

        BigDecimal rateToBase = BigDecimal.valueOf(currency.getRateToBase());
        return amount.divide(rateToBase, RoundingMode.HALF_UP);
    }

    public CurrencyModel findByName(String name) {
        return getRepository().filteredFirst(getFilterImpl().eq("name", name))
                .orElseThrow(() -> new  ItemNotFoundException(name));

    }
}
