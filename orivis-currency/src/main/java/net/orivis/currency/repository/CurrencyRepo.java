package net.orivis.currency.repository;


import net.orivis.currency.model.CurrencyModel;
import net.orivis.shared.postgres.repository.OrivisRepository;

import java.util.Optional;

public interface CurrencyRepo extends OrivisRepository<CurrencyModel> {

    Optional<CurrencyModel> findFirstByDefIsTrue();
}
