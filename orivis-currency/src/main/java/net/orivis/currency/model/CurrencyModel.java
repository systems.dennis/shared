package net.orivis.currency.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import lombok.Data;
import net.orivis.shared.annotations.FormTransient;
import net.orivis.shared.postgres.model.LongAssignableEntity;

@Data
@Entity
public class CurrencyModel extends LongAssignableEntity {

    private String code;

    private String name;

    private String description;

    private int divider;

    @FormTransient
    private Double rateToBase;

    @Column(nullable = true)
    private Boolean def;

    @Override
    public String asValue() {
        return name;
    }
}
