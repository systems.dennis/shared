package net.orivis.shared.reactions.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReactionResponse {
    private Boolean enabled;
    private String result;

}
