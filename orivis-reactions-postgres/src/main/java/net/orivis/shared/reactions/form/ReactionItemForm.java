package net.orivis.shared.reactions.form;

import lombok.Data;
import net.orivis.shared.annotations.Validation;
import net.orivis.shared.pojo_form.PojoFormElement;
import net.orivis.shared.pojo_view.UIAction;
import net.orivis.shared.pojo_view.list.PojoListView;
import net.orivis.shared.pojo_view.list.PojoListViewField;
import net.orivis.shared.postgres.form.OrivisPojo;
import net.orivis.shared.reactions.validation.ReactionStoreLimitValidator;
import net.orivis.shared.validation.ValueNotEmptyValidator;

import static net.orivis.shared.pojo_view.DEFAULT_TYPES.HIDDEN;

@Data
@PojoListView(actions = {})
public class ReactionItemForm implements OrivisPojo {

    @PojoFormElement(type = HIDDEN)
    private Long id;

    private String modelId;

    @PojoFormElement(required = true)
    @PojoListViewField(searchable = true)
    private String type;

    @PojoFormElement (available = false)
    @PojoListViewField( available = false)
    @Validation(ReactionStoreLimitValidator.class)
    private Long userDataId;

    @PojoFormElement(required = true)
    @PojoListViewField(searchable = true)
    @Validation(ValueNotEmptyValidator.class)
    private String reactionType;

    @PojoListViewField(actions = {@UIAction(component = "delete")})
    @PojoFormElement(available = false)
    private Integer action;

    @Override
    public String asValue() {
        return type + " "  + userDataId + " " + modelId;
    }
}
