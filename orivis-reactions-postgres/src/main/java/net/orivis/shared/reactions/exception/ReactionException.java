package net.orivis.shared.reactions.exception;

import net.orivis.shared.exceptions.StatusException;
import org.springframework.http.HttpStatus;

public class ReactionException extends StatusException {
    public ReactionException(String s) {
        super(s, HttpStatus.BAD_REQUEST);
    }
}
