package net.orivis.shared.reactions.validation;

import net.orivis.shared.annotations.ValidationContent;
import net.orivis.shared.pojo_form.ValidationResult;
import net.orivis.shared.reactions.form.ReactionItemForm;
import net.orivis.shared.reactions.service.ReactionItemService;
import net.orivis.shared.validation.ValueValidator;

public class ReactionStoreLimitValidator implements ValueValidator {

    public static final String REACTION_STORE_LIMIT_VALIDATOR_ERROR_MESSAGE = "global.settings.reaction.store.limit.exceeded";

    @Override
    public ValidationResult validate(Object element, Object value, ValidationContent content) {
        ReactionItemService service = (ReactionItemService)content.getContext().getBean(content.getServiceClass());
        return service.isReactionLimitNotExceeded((ReactionItemForm) element)
                ? ValidationResult.PASSED
                : ValidationResult.fail(REACTION_STORE_LIMIT_VALIDATOR_ERROR_MESSAGE);
    }
}
