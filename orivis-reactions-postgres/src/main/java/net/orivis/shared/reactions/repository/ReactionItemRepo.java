package net.orivis.shared.reactions.repository;

import net.orivis.shared.postgres.repository.OrivisRepository;
import net.orivis.shared.reactions.model.ReactionItemModel;
import org.springframework.stereotype.Repository;

@Repository
public interface ReactionItemRepo extends OrivisRepository<ReactionItemModel> {

}
