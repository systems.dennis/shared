package net.orivis.shared.reactions.service;

import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.annotations.security.ISecurityUtils;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.controller.SearchEntityApi;
import net.orivis.shared.exceptions.ItemAlreadyExistsException;
import net.orivis.shared.exceptions.ItemForAddContainsIdException;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.exceptions.ItemNotUserException;
import net.orivis.shared.postgres.service.PaginationService;
import net.orivis.shared.reactions.exception.ReactionException;
import net.orivis.shared.reactions.form.ReactionItemForm;
import net.orivis.shared.reactions.model.ReactionItemModel;
import net.orivis.shared.reactions.repository.ReactionItemRepo;
import net.orivis.shared.repository.OrivisFilter;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@OrivisService(model = ReactionItemModel.class, form = ReactionItemForm.class, repo = ReactionItemRepo.class)
public class ReactionItemService extends PaginationService<ReactionItemModel > {

    public static final String GLOBAL_SETTINGS_REACTION_ENABLED = "global.settings.reaction.enabled";
    public static final String GLOBAL_SETTINGS_REACTION_LIMIT = "global.settings.reaction.limit";

    public ReactionItemService(OrivisContext holder) {
        super(holder);
    }

    @Override
    public ReactionItemModel preAdd(ReactionItemModel object) throws ItemForAddContainsIdException {

        if (!getIsReactionEnabled()){
            throw new ReactionException("global.exceptions.reaction.without_type.disabled");
        }

        Long currentUser = getCurrentUser();
        if (object.getType() == null){
            throw new ReactionException ("global.exceptions.reaction.without_type");
        }

        if (existByModelAndUser(object.getModelId(), object.getType(), currentUser)) {
            throw new ItemAlreadyExistsException(object.getModelId().toString());
        }
        object.setUserDataId(currentUser);
        return object;
    }

    public String findBySearchName(String name){
        return SearchEntityApi.findServiceByType(name).getAnnotation(OrivisService.class).form().getSimpleName();
    }



    public void delete(ReactionItemForm id) throws ItemNotUserException, ItemNotFoundException {
        id.setUserDataId(getCurrentUser());

        var item =  findByModelAndUser(id.getModelId(), id.getType(), id.getUserDataId());
        item.ifPresent(reactionItemModel -> delete(reactionItemModel.getId()));

    }

    public Boolean getIsReactionEnabled(){
        return getContext().getEnv(GLOBAL_SETTINGS_REACTION_ENABLED, true);
    }

    public boolean isReactionForObject(ReactionItemForm form){
        form.setUserDataId(getCurrentUser());
        if (form.getModelId() == null){
            throw new ReactionException("global.exceptions.reaction.reaction.no_model_id");
        }

        return existByModelAndUser(form.getModelId(), form.getType(), form.getUserDataId());

    }

    private Boolean existByModelAndUser(String modelId, String entityDescriptor, Long currentUser) {
        if (!getIsReactionEnabled()){
            return false;
        }

        long count = getRepository().filteredCount(getFilterImpl().eq("userDataId", currentUser)
                .and(getFilterImpl().eq("type", entityDescriptor))
                .and(getFilterImpl().eq("modelId", modelId)));
        return count > 0;
    }

    private Optional<ReactionItemModel> findByModelAndUser(String modelId, String entityDescriptor, Long currentUser) {
        if (!getIsReactionEnabled()){
            return Optional.empty();
        }

        return getRepository().filteredOne(getFilterImpl().eq("userDataId", currentUser)
                .and(getFilterImpl().eq("type", entityDescriptor))
                .and(getFilterImpl().eq("modelId", modelId)));
    }

    public boolean isReactionLimitNotExceeded(ReactionItemForm form) {
        if (!getIsReactionEnabled()){
            return false;
        }

        if (getContext().getBean(ISecurityUtils.class).isAdmin()){
            return true;
        }


        Integer reactionLimit = getLimit(form);

        if ( reactionLimit == null) {
            return true;
        }

        long count = getRepository().filteredCount(getSelfCreatedItemsQuery(getCurrentUser())
                .and(getFilterImpl().eq("type", form.getType()))
                .and(getFilterImpl().eq("reactionType", form.getReactionType())));

        return count < reactionLimit;
    }

    @Override
    public OrivisFilter<ReactionItemModel> getAdditionalSpecification() {
        var res =  super.getAdditionalSpecification();
        res = res.and(getFilterImpl().ofUser(ReactionItemModel.class, getCurrentUser()));
        return res;
    }

    private Integer getLimit(ReactionItemForm form) {
        String reaction = form.getReactionType();
        return getContext().getEnv("global.settings.favorite.limit." + reaction, null);
    }
}
