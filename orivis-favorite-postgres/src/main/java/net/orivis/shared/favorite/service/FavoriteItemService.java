package net.orivis.shared.favorite.service;

import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.postgres.model.OrivisEntity;
import net.orivis.shared.repository.OrivisFilter;
import org.springframework.stereotype.Service;
import net.orivis.shared.annotations.security.ISecurityUtils;
import net.orivis.shared.controller.SearchEntityApi;
import net.orivis.shared.exceptions.ItemAlreadyExistsException;
import net.orivis.shared.exceptions.ItemForAddContainsIdException;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.exceptions.ItemNotUserException;
import net.orivis.shared.favorite.exception.FavoriteException;
import net.orivis.shared.favorite.form.FavoriteItemForm;
import net.orivis.shared.favorite.model.FavoriteItemModel;
import net.orivis.shared.favorite.repository.FavoriteItemRepo;
import net.orivis.shared.pojo_view.list.PojoListView;
import net.orivis.shared.postgres.service.PaginationService;

import java.util.Optional;

@Service
@OrivisService(model = FavoriteItemModel.class, form = FavoriteItemForm.class, repo = FavoriteItemRepo.class)
public class FavoriteItemService extends PaginationService<FavoriteItemModel > {

    public static final String GLOBAL_SETTINGS_FAVORITE_ENABLED = "global.settings.favorite.enabled";
    public static final String GLOBAL_SETTINGS_FAVORITE_LIMIT = "global.settings.favorite.limit";

    public FavoriteItemService(OrivisContext holder) {
        super(holder);
    }

    @Override
    public FavoriteItemModel preAdd(FavoriteItemModel object) throws ItemForAddContainsIdException {

        if (!getIsFavoriteEnabled()){
            throw new FavoriteException("global.exceptions.favorite.without_type.disabled");
        }

        Long currentUser = getCurrentUser();
        if (object.getType() == null){
            throw new FavoriteException ("global.exceptions.favorite.without_type");
        }

        if (existByModelAndUser(object.getModelId(), object.getType(), currentUser)) {
            throw new ItemAlreadyExistsException(object.getModelId().toString());
        }
        object.setUserDataId(currentUser);
        return object;
    }

    public String findBySearchName(String name){
        return SearchEntityApi.findServiceByType(name).getAnnotation(OrivisService.class).form().getSimpleName();
    }



    public void delete(FavoriteItemForm id) throws ItemNotUserException, ItemNotFoundException {
        id.setUserDataId(getCurrentUser());

        var item =  findByModelAndUser(id.getModelId(), id.getType(), id.getUserDataId());
        item.ifPresent(favoriteItemModel -> delete(favoriteItemModel.getId()));

    }

    public Boolean getIsFavoriteEnabled(){
        return getContext().getEnv(GLOBAL_SETTINGS_FAVORITE_ENABLED, true);
    }

    public boolean isFavoriteForObject(FavoriteItemForm form){
        form.setUserDataId(getCurrentUser());
        if (form.getModelId() == null){
            throw new FavoriteException("global.exceptions.favorite.favorite.no_model_id");
        }

        return existByModelAndUser(form.getModelId(), form.getType(), form.getUserDataId());

    }

    public boolean isFavoriteForObject(OrivisEntity entity) {
        if (!getIsFavoriteEnabled()){
            return false;
        }
        if (entity == null) {
            return false;
        }


        String entityDescriptor = entity.getClass().getSimpleName();

        if (entity.getClass().getAnnotation(PojoListView.class) != null){
            entityDescriptor =  entityDescriptor.getClass().getAnnotation(PojoListView.class).favoriteType();
        }

        return this.existByModelAndUser(entity.getId(), entityDescriptor, getCurrentUser());
    }

    private Boolean existByModelAndUser(Long modelId, String entityDescriptor, Long currentUser) {
        if (!getIsFavoriteEnabled()){
            return false;
        }

        long count = getRepository().filteredCount(getFilterImpl().eq("userDataId", currentUser)
                .and(getFilterImpl().eq("type", entityDescriptor))
                .and(getFilterImpl().eq("modelId", modelId)));
        return count > 0;
    }

    private Optional<FavoriteItemModel> findByModelAndUser(Long modelId, String entityDescriptor, Long currentUser) {
        if (!getIsFavoriteEnabled()){
            return Optional.empty();
        }

        return getRepository().filteredOne(getFilterImpl().eq("userDataId", currentUser)
                .and(getFilterImpl().eq("type", entityDescriptor))
                .and(getFilterImpl().eq("modelId", modelId)));
    }

    public boolean isFavoriteLimitNotExceeded(FavoriteItemForm form) {
        if (!getIsFavoriteEnabled()){
            return false;
        }

        if (getContext().getBean(ISecurityUtils.class).isAdmin()){
            return true;
        }
        Long favoriteStoreLimit = getContext().getEnv(GLOBAL_SETTINGS_FAVORITE_LIMIT, 100L);
        long count = getRepository().filteredCount(getSelfCreatedItemsQuery(getCurrentUser())
                .and(getFilterImpl().eq("type", form.getType())));
        return count < favoriteStoreLimit;
    }

    @Override
    public OrivisFilter<FavoriteItemModel> getAdditionalSpecification() {
        var res =  super.getAdditionalSpecification();
        res = res.and(getFilterImpl().ofUser(FavoriteItemModel.class, getCurrentUser()));
        return res;
    }
}
