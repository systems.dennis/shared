package net.orivis.shared.favorite.model;

import jakarta.persistence.Entity;
import lombok.Data;
import net.orivis.shared.annotations.FormTransient;
import net.orivis.shared.annotations.entity.CreatedBy;
import net.orivis.shared.postgres.model.OrivisEntity;

@Data
@Entity
public class FavoriteItemModel extends OrivisEntity {

    private Long modelId;

    private String type;

    private String reactionType;

    @CreatedBy
    @FormTransient
    private Long userDataId;

    @Override
    public String asValue() {
        return type + "model_" +  getId()  + userDataId ;
    }
}
