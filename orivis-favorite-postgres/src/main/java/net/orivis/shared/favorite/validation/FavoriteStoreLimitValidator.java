package net.orivis.shared.favorite.validation;

import net.orivis.shared.annotations.ValidationContent;
import net.orivis.shared.favorite.form.FavoriteItemForm;
import net.orivis.shared.pojo_form.ValidationResult;
import net.orivis.shared.favorite.service.FavoriteItemService;
import net.orivis.shared.validation.ValueValidator;

public class FavoriteStoreLimitValidator implements ValueValidator {

    public static final String FAVORITE_STORE_LIMIT_VALIDATOR_ERROR_MESSAGE = "global.settings.favorite.store.limit.exceeded";

    @Override
    public ValidationResult validate(Object element, Object value, ValidationContent content) {
        FavoriteItemService service = (FavoriteItemService)content.getContext().getBean(content.getServiceClass());
        return service.isFavoriteLimitNotExceeded((FavoriteItemForm) element)
                ? ValidationResult.PASSED
                : ValidationResult.fail(FAVORITE_STORE_LIMIT_VALIDATOR_ERROR_MESSAGE);
    }
}
