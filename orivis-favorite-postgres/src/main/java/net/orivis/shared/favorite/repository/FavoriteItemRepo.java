package net.orivis.shared.favorite.repository;

import net.orivis.shared.favorite.model.FavoriteItemModel;
import net.orivis.shared.postgres.repository.OrivisRepository;

public interface FavoriteItemRepo extends OrivisRepository<FavoriteItemModel> {

}
