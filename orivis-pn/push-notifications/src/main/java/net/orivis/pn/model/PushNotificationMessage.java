package net.orivis.pn.model;
//

import lombok.Data;

@Data
public class PushNotificationMessage {
    private String title;
    private String body;
    private String fromService;
    private String userId;
    private boolean prio = false;

}
