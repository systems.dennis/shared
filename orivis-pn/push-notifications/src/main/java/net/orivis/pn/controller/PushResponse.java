//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package net.orivis.pn.controller;

import lombok.Data;

@Data
public class PushResponse {
    private String status;
    private String channel;
    private Boolean success;

    public static PushResponse ofError(Exception e) {
        PushResponse response = new PushResponse();
        response.setSuccess(false);
        response.setStatus(e.getMessage());
        return response;
    }
}
