//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package net.orivis.pn.controller;

import java.security.KeyPair;
import java.security.interfaces.ECPublicKey;
import java.util.Base64;
import net.orivis.pn.providers.WebPushNotificationProvider;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/api/pn"})
@CrossOrigin
public class PublicServerKeyController extends OrivisContextable {
    public PublicServerKeyController(OrivisContext context) {
        super(context);
    }

    @GetMapping(
        value = {"/public/key"},
        produces = {"application/json"}
    )
    private PushResponse getPublicKey() {
        KeyPair keyPair = WebPushNotificationProvider.getKeys(this.getContext());
        ECPublicKey publicKey = (ECPublicKey)keyPair.getPublic();
        PushResponse response = new PushResponse();
        byte[] x = publicKey.getW().getAffineX().toByteArray();
        byte[] y = publicKey.getW().getAffineY().toByteArray();
        x = ensureLength(x, 32);
        y = ensureLength(y, 32);
        byte[] rawPublicKey = new byte[65];
        rawPublicKey[0] = 4;
        System.arraycopy(x, 0, rawPublicKey, 1, 32);
        System.arraycopy(y, 0, rawPublicKey, 33, 32);
        String urlSafeBase64PublicKey = Base64.getUrlEncoder().withoutPadding().encodeToString(rawPublicKey);
        System.out.println("✅ VAPID Public Key (Base64-URL): " + urlSafeBase64PublicKey);
        response.setSuccess(true);
        response.setStatus(urlSafeBase64PublicKey);
        return response;
    }

    private static byte[] ensureLength(byte[] keyPart, int length) {
        if (keyPart.length == length) {
            return keyPart;
        } else {
            byte[] padded = new byte[length];
            System.arraycopy(keyPart, Math.max(0, keyPart.length - length), padded, Math.max(0, length - keyPart.length), Math.min(keyPart.length, length));
            return padded;
        }
    }
}
