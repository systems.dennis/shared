//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package net.orivis.pn.providers;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import net.orivis.apptodb.model.AppSettingsModel;
import net.orivis.apptodb.service.AppSettingsService;
import net.orivis.pn.controller.PushResponse;
import net.orivis.pn.model.PushNotificationMessage;
import net.orivis.pn.model.PushSubscriberModel;
import net.orivis.shared.config.OrivisContext;
import nl.martijndwars.webpush.Notification;
import nl.martijndwars.webpush.PushService;
import nl.martijndwars.webpush.Subscription;
import org.jose4j.json.internal.json_simple.JSONObject;

public class WebPushNotificationProvider implements AbstractNotificationProvider {
    public static final String WEB_PUSH_NOTIFICATION = "web";
    private final PushService pushService;

    public WebPushNotificationProvider(OrivisContext context) {
        try {
            KeyPair keyPair = getKeys(context);
            System.out.println(Base64.getUrlEncoder().withoutPadding().encodeToString(keyPair.getPublic().getEncoded()));
            System.out.println(Base64.getUrlEncoder().withoutPadding().encodeToString(keyPair.getPrivate().getEncoded()));
            this.pushService = new PushService(Base64.getUrlEncoder().withoutPadding().encodeToString(keyPair.getPublic().getEncoded()), Base64.getUrlEncoder().withoutPadding().encodeToString(keyPair.getPrivate().getEncoded()));
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    private static KeyPair generateVapidKeys() throws Exception {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("EC");
        keyPairGenerator.initialize(256);
        return keyPairGenerator.generateKeyPair();
    }

    public static KeyPair getKeys(OrivisContext context) {
        try {
            String key = (String)context.getEnv("app.web_push_notification_key_public", "");
            String value = (String)context.getEnv("app.web_push_notification_key_private", "");
            if (!key.isEmpty() && !value.isEmpty()) {
                return getKeyPair(key, value);
            } else {
                KeyPair keys = generateVapidKeys();
                AppSettingsModel model = new AppSettingsModel();
                model.setKey("app.web_push_notification_key_public");
                model.setValue(Base64.getEncoder().encodeToString(keys.getPublic().getEncoded()));
                ((AppSettingsService)context.getBean(AppSettingsService.class)).save(model);
                AppSettingsModel modelPrivate = new AppSettingsModel();
                modelPrivate.setKey("app.web_push_notification_key_private");
                modelPrivate.setValue(Base64.getEncoder().encodeToString(keys.getPrivate().getEncoded()));
                ((AppSettingsService)context.getBean(AppSettingsService.class)).save(modelPrivate);
                return keys;
            }
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public static KeyPair getKeyPair(String publicKeyBase64, String privateKeyBase64) throws Exception {
        byte[] publicKeyBytes = Base64.getDecoder().decode(publicKeyBase64);
        byte[] privateKeyBytes = Base64.getDecoder().decode(privateKeyBase64);
        KeyFactory keyFactory = KeyFactory.getInstance("EC");
        PublicKey publicKey = keyFactory.generatePublic(new X509EncodedKeySpec(publicKeyBytes));
        PrivateKey privateKey = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(privateKeyBytes));
        return new KeyPair(publicKey, privateKey);
    }

    public PushResponse send(PushNotificationMessage message, PushSubscriberModel model) {
        try {
            JSONObject payload = new JSONObject();
            payload.put("title", message.getTitle());
            payload.put("body", message.getBody());
            Subscription subscription = new Subscription();
            new Subscription(model.getPlatform(), new Subscription.Keys(model.getP256dh(), model.getAuth()));
            Notification notification = new Notification(subscription, payload.toJSONString());
            this.pushService.send(notification);
            return new PushResponse();
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }
}
