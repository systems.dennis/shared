//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package net.orivis.pn.providers;

import net.orivis.pn.controller.PushResponse;
import net.orivis.pn.model.PushNotificationMessage;
import net.orivis.pn.model.PushSubscriberModel;

public interface AbstractNotificationProvider {
    PushResponse send(PushNotificationMessage message, PushSubscriberModel model);
}
