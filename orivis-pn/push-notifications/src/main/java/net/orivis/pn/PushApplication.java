//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package net.orivis.pn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication(
        scanBasePackages = {"net.orivis.*"}
)
@EntityScan(
        basePackages = {"net.orivis.*.*"}
)
@EnableScheduling
@CrossOrigin
public class PushApplication {

    public PushApplication() {
    }

    public static void main(String[] args) {
        SpringApplication.run(PushApplication.class, args);
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}
