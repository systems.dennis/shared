//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package net.orivis.pn.model;

import lombok.Data;
import lombok.Generated;
import net.orivis.shared.annotations.entity.Created;
import net.orivis.shared.mongo.model.OrivisEntity;
import net.orivis.shared.utils.bean_copier.DateToUTCConverter;
import net.orivis.shared.utils.bean_copier.OrivisTranformer;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
@Data
public class PushSubscriberModel extends OrivisEntity {
    private String userId;
    private String deviceToken;
    private String platform;
    private String channel;
    private String endpoint;
    private String p256dh;
    private String auth;
    @Created
    @OrivisTranformer(
        transformWith = DateToUTCConverter.class
    )
    private Date subscribedAt;

    public String asValue() {
        return this.userId + " - " + this.deviceToken;
    }

    @Generated
    public PushSubscriberModel() {
    }

}
