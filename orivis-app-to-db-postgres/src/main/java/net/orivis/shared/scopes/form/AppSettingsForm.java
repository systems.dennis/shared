package net.orivis.shared.scopes.form;

import lombok.Data;
import net.orivis.shared.pojo_form.PojoFormElement;
import net.orivis.shared.pojo_view.UIAction;
import net.orivis.shared.pojo_view.list.PojoListView;
import net.orivis.shared.pojo_view.list.PojoListViewField;
import net.orivis.shared.postgres.form.OrivisPojo;

import static net.orivis.shared.pojo_view.DEFAULT_TYPES.HIDDEN;

@Data
@PojoListView(actions = {"new", "download"})
public class AppSettingsForm implements OrivisPojo {
    @PojoFormElement(type = HIDDEN)
    private Long id;

    @PojoFormElement(required = true)
    @PojoListViewField(searchable = true)
    private String key;

    @PojoFormElement(required = true)
    @PojoListViewField(searchable = true)
    private String value;

    @PojoFormElement()
    @PojoListViewField(searchable = true)
    private String valueClassType;

    @PojoListViewField(actions = {@UIAction(component = "edit"), @UIAction(component = "delete")}, sortable = false)
    @PojoFormElement(available = false)
    private Integer action;
}
