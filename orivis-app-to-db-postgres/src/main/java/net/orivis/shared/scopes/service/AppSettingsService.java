package net.orivis.shared.scopes.service;

import net.orivis.shared.annotations.OrivisService;
import net.orivis.shared.beans.OrivisSettingsResolver;
import net.orivis.shared.config.OrivisContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import net.orivis.shared.exceptions.ItemForAddContainsIdException;
import net.orivis.shared.exceptions.ItemNotFoundException;
import net.orivis.shared.exceptions.SettingKeyNotFoundException;
import net.orivis.shared.exceptions.SettingValueTypeNotFoundException;
import net.orivis.shared.exceptions.UnmodifiedItemSaveAttemptException;
import net.orivis.shared.postgres.service.PaginationService;
import net.orivis.shared.scopes.converters.AppSettingsValueTypeIdentifier;
import net.orivis.shared.scopes.form.AppSettingsForm;
import net.orivis.shared.scopes.model.AppSettingsModel;
import net.orivis.shared.scopes.repository.AppSettingsRepo;

import java.util.*;
import java.util.stream.Collectors;

import static net.orivis.shared.converters.SettingsValueType.*;

@Service @Primary
@OrivisService(model = AppSettingsModel.class, form = AppSettingsForm.class, repo = AppSettingsRepo.class)
public class AppSettingsService extends PaginationService<AppSettingsModel> implements OrivisSettingsResolver {

    public static final String SPRING_PREFIX = "spring.";

    private Map<String, AppSettingsModel> settingsCache = new HashMap<>();
    @Autowired
    private AppSettingsValueTypeIdentifier identifier;
    @Autowired
    Environment environment;

    public static final Map<String, String> SHARED_SETTINGS_WITH_DEFAULT_VALUE = Map.ofEntries(
            Map.entry("pages.auth.login", "/client_login"),
            Map.entry("global.use_cache_translation", "true"),
            Map.entry("global.messages.path", "./i18n"),
            Map.entry("global.use_db_updater", "false"),
            Map.entry("global.project_update_file", "null"),
            Map.entry("global.basic_update_file", "db/root-sql-init/db.yml"),
            Map.entry("global.settings.favorite.enabled", "true")
    );

    private final Logger log = LoggerFactory.getLogger(AppSettingsService.class);

    @Value("${global.app.settings.enabled: true}")
    private Boolean isEnabled;

    public AppSettingsService(OrivisContext holder) {
        super(holder);
    }

    public <T> T getSetting(String settingKey) throws SettingKeyNotFoundException,
            SettingValueTypeNotFoundException {
        String valueType = "";
        if (!settingsCache.containsKey(settingKey)) {
            throw new SettingKeyNotFoundException(settingKey, "shared.settings.value.not_defined");
        } else {
            AppSettingsModel setting = settingsCache.get(settingKey);
            return (T) setting.getRealValue();
        }


    }

    public void saveApplicationSettingsIntoDB() {

        if (isEnabled) {


            List<AppSettingsModel> settings = readApplicationSettings();
            List<AppSettingsModel> newComingSettings = settings.stream().filter(
                    x -> isEmpty(getFilterImpl().eq("key", x.getKey()))).toList();
            newComingSettings.forEach(this::save);
            settings.forEach(this::addToCache);
            List<AppSettingsModel> models =  getFromDb();
            models.forEach(this::addToCache);
        }
    }

    private List<AppSettingsModel> getFromDb() {
        var service = getContext().getBean(AppSettingsService.class);
        return service.getRepository().filteredData(service.getNotDeletedQuery(), Pageable.unpaged()).toList();
    }

    public List<String> getEnvProperties() {
        var it = ((AbstractEnvironment) environment).getPropertySources().iterator();
        List<String> res = new ArrayList();
        while (it.hasNext()) {
            var sources = it.next().getSource();

            if (sources instanceof Map){
                Map item = (Map<String, String>) sources;
                if (item.containsKey("server.port")) {
                    res.addAll(item.keySet());

                }
            }
        }

        return res;

    }


    private List<AppSettingsModel> readApplicationSettings() {
        var keys = getEnvProperties();

        List<AppSettingsModel> models = new ArrayList<>();
        for (String key : keys) {
            if (!key.startsWith(SPRING_PREFIX)) {
                models.add(createSettingObject(key, environment.getProperty(key)));
            }
        }
        models.addAll(getSettingsNotContainingInAppProperties(models));
        return models;
    }

    private AppSettingsModel createSettingObject(String key, String value) {
        AppSettingsModel setting = new AppSettingsModel();
        setting.setKey(key);
        setting.setValue(value);
        setting.setValueClassType(identifier.identify(key, value));
        return setting;
    }

    private List<AppSettingsModel> getSettingsNotContainingInAppProperties(List<AppSettingsModel> models) {
        List<AppSettingsModel> result = new ArrayList<>();
        List<String> appSettingKeys = models.stream().map(AppSettingsModel::getKey).collect(Collectors.toList());

        Set<String> differenceOfKeys = SHARED_SETTINGS_WITH_DEFAULT_VALUE.keySet().stream()
                .filter(key -> !appSettingKeys.contains(key))
                .collect(Collectors.toSet());
        differenceOfKeys.forEach(key -> {
            AppSettingsModel setting = createSettingObject(key, SHARED_SETTINGS_WITH_DEFAULT_VALUE.get(key));
            result.add(setting);
        });
        return result;
    }


    public AppSettingsModel addToCache(AppSettingsModel setting) {

        setting.setRealValue(calcRealValue(setting));
        settingsCache.put(setting.getKey(), setting);

        return setting;
    }

    private Object calcRealValue(AppSettingsModel setting) {
        String settingValue = setting.getValue();
        var valueType = setting.getValueClassType();

        if (valueType.equalsIgnoreCase(STRING.name())) {
            return settingValue;
        }
        if(valueType.equalsIgnoreCase(PASSWORD.name())) {
            return settingValue;
        }
        if (valueType.equalsIgnoreCase(INTEGER.name())) {
            return Integer.valueOf(settingValue);
        }
        if (valueType.equalsIgnoreCase(BOOLEAN.name())) {
            return Boolean.valueOf(settingValue);
        }
        throw new SettingValueTypeNotFoundException(valueType, "shared.settings.value_type.not.found");
    }

    @Override
    public AppSettingsModel preAdd(AppSettingsModel object) throws ItemForAddContainsIdException {
        if (Objects.isNull(object.getValueClassType())) {
            object.setValueClassType(AppSettingsModel.DEFAULT_TYPE);
        }
        return object;
    }

    @Override
    public AppSettingsModel preEdit(AppSettingsModel object, AppSettingsModel original) throws UnmodifiedItemSaveAttemptException, ItemNotFoundException {
        if (Objects.isNull(object.getValueClassType())) {
            object.setValueClassType(AppSettingsModel.DEFAULT_TYPE);
        }
        return object;
    }

    @Override
    public void afterEdit(AppSettingsModel object, AppSettingsModel original) {
        addToCache(object);
    }

    @Override
    public AppSettingsModel afterAdd(AppSettingsModel object) {
        return addToCache(object);
    }

    @Override
    public AppSettingsModel afterDelete(AppSettingsModel object) {
        settingsCache.remove(object.getKey());
        return object;
    }

    @Override
    public boolean hasSetting(String setting) {
        return settingsCache.containsKey(setting) ;
    }

    @Override
    public <T> T getEnv(String key) {
        return getSetting(key);
    }

    @Override
    public <T> T getEnv(String key, Object def) {
        try {
            return  getEnv(key);
        }catch (SettingKeyNotFoundException e){
            return (T) def;
        }
    }
}
