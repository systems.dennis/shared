package net.orivis.shared.scopes.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Transient;
import lombok.Data;
import net.orivis.shared.postgres.model.OrivisEntity;

@Data
@Entity
public class AppSettingsModel extends OrivisEntity {
    public static String DEFAULT_TYPE = "STRING";

    private String key;
    private String value;
    private String valueClassType;

    @Transient
    private Object realValue;

    @Override
    public String asValue() {
        return key + " " + value;
    }
}
