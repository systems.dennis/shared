package net.orivis.shared.scopes.controller;

import net.orivis.shared.annotations.OrivisController;
import net.orivis.shared.config.OrivisContext;
import net.orivis.shared.utils.OrivisContextable;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import net.orivis.shared.annotations.security.Secured;
import net.orivis.shared.exceptions.*;
import net.orivis.shared.postgres.controller.AddItemController;
import net.orivis.shared.postgres.controller.DeleteItemController;
import net.orivis.shared.postgres.controller.EditItemController;
import net.orivis.shared.postgres.controller.ListItemController;
import net.orivis.shared.scopes.form.AppSettingsForm;
import net.orivis.shared.scopes.model.AppSettingsModel;
import net.orivis.shared.scopes.service.AppSettingsService;

import java.util.Map;

@RestController
@RequestMapping("/api/v2/shared/app_settings")
@OrivisController(value = AppSettingsService.class)
@CrossOrigin
@Secured (roles = "ROLE_ADMIN")
public class AppSettingsController
        extends OrivisContextable

        implements
        DeleteItemController<AppSettingsModel>,
        ListItemController<AppSettingsModel, AppSettingsForm>,
        EditItemController<AppSettingsModel, AppSettingsForm>,
        AddItemController<AppSettingsModel, AppSettingsForm>
       {

    public AppSettingsController(OrivisContext context) {
        super(context);
    }

    @Override
    @GetMapping("/list")
    public ResponseEntity<Page<Map<String, Object>>> get( Integer limit, Integer page) {
        return ListItemController.super.get( limit, page);
    }

    @Override
    @PutMapping( "/edit/{id}")

    public ResponseEntity<AppSettingsForm> edit(AppSettingsForm form) throws ItemForAddContainsIdException, ItemDoesNotContainsIdValueException, UnmodifiedItemSaveAttemptException, ItemNotFoundException {
        return EditItemController.super.edit(form);
    }
    @Override
    @PutMapping("/add")

    public ResponseEntity<AppSettingsForm> add(AppSettingsForm form) throws ItemForAddContainsIdException, ItemDoesNotContainsIdValueException, UnmodifiedItemSaveAttemptException, ItemNotFoundException {
        return EditItemController.super.edit(form);
    }


    @Override
    @DeleteMapping ("/delete/{id}")
    public void delete(@PathVariable Long id) throws ItemNotUserException, ItemNotFoundException {
        DeleteItemController.super.delete(id);
    }


}
